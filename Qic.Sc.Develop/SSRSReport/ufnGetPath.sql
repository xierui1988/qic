USE [SCAuditTrail]
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'ufnGetPath')
                    AND type IN ( N'FN' ) ) 
	DROP FUNCTION [dbo].[ufnGetPath]
GO

/****** Object:  UserDefinedFunction [dbo].[ufnGetPath]    Script Date: 4/1/2015 2:39:59 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



Create function [dbo].[ufnGetPath]( @text VARCHAR(MAX)) 
returns varchar(MAX) as

Begin


declare @lastpart int
select @lastpart=charindex('/',REVERSE(@text))

declare @newpath varchar(MAX)
select @newpath= SUBSTRING(@text,len(@text)-@lastpart+2,@lastpart)
--select @newpath

return   @newpath

end
GO

GRANT EXECUTE ON [dbo].[ufnGetPath] TO [QIC_report_role];
GO

