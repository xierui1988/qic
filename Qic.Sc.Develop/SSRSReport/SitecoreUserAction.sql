USE [SCAuditTrail]
GO

/****** Object:  Table [dbo].[SitecoreUserAction]    Script Date: 21/04/2015 1:40:21 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'SitecoreUserAction')
                    AND type = 'U' ) 
	DROP TABLE [dbo].[SitecoreUserAction]
GO

/****** Object:  Table [dbo].[SitecoreUserAction]    Script Date: 21/04/2015 1:40:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SitecoreUserAction](
	[ID] [int] NOT NULL,
	[SCAction] [varchar](100) NOT NULL
) ON [PRIMARY]


INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (1,'Change Password Email')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (2,'Document Deleted')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (3,'Document Published With Email')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (4,'Document Published Without Email')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (5,'Document Unpublished')
	   
INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (6,'Document Uploaded to Sitecore')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (7,'Document Viewed')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (8,'Execute workflow command')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (9,'Failed Email Notification')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (10,'Form Deleted')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (11,'Form Published')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (12,'Form Unpublished')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (13,'Login')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (14,'Logout')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (15,'Publish item')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (16,'Rename item')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (17,'Save item')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (18,'Sent Email Notification')

INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (19,'User Changed Email Settings')
		   
INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (20,'Welcome Email')
		   
INSERT INTO [dbo].[SitecoreUserAction]
           ([ID]
           ,[SCAction])
     VALUES
           (21,'Publishing summary')
GO

SET ANSI_PADDING OFF
GO


