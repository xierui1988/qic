USE [SCAuditTrail]
GO

/****** Object:  StoredProcedure [dbo].[AuditReportData]    Script Date: 15/04/2015 3:38:52 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'AuditReportData')
                    AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE [dbo].[AuditReportData]
GO

/****** Object:  StoredProcedure [dbo].[AuditReportData]    Script Date: 15/04/2015 3:38:52 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[AuditReportData]
	@User varchar(256) = null,
	@Action  varchar(256) = null,
	@StartDate datetime = null,
	@EndDate datetime = null
AS

DECLARE @SelectAllConst varchar(256) = 'Select All'

BEGIN

    SET NOCOUNT ON;

	SELECT Date,  SCUser, SCAction, dbo.ufnGetPath(SCItemPath) as SCItemPath,  dbo.ufnGetState(scmisc) as SCMisc
			FROM Logs
			WHERE ((@User is null or @User = @SelectAllConst) or (SCUser = @User))
				AND ((@Action is null or @Action = @SelectAllConst) or (SCAction = @Action))
				AND ((@StartDate is null) or ([Date] >= @StartDate))
				AND ((@EndDate is null) or ([Date] < DATEADD(day, 1, @EndDate)))
				AND SCAction in (SELECT SCAction FROM SitecoreUserAction)
			ORDER BY [Date] DESC
END
GO


