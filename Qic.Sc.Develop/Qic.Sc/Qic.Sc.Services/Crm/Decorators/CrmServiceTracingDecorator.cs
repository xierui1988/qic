﻿namespace Qic.Sc.Services.Crm.Decorators
{
    using System;

    using Qic.Sc.Interfaces.Crm;

    internal partial class CrmServiceTracingDecorator : TracingBaseDecorator, ICrmService
    {
        private readonly ICrmService crmService;

        public CrmServiceTracingDecorator(ICrmService crmService)
        {
            if (crmService == null)
            {
                throw new ArgumentNullException("crmService");
            }
            this.crmService = crmService;
        }
    }
}