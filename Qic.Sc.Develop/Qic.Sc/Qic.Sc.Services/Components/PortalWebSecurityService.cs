﻿namespace Qic.Sc.Services.Components
{
    using Qic.Sc.Entities;
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Crm;
    using Sitecore.Security.Accounts;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// Web Security
    /// </summary>
    public class PortalWebSecurityService : IPortalWebSecurityService
    {
        private readonly ICrmService crmService;

        private readonly IAuditService auditService;

        private readonly IWebSecurityService webSecurity;

        /// <summary>
        /// Initializes a new instance of the MembershipProvider
        /// </summary>
        public PortalWebSecurityService(ICrmService crmService, IAuditService auditService, IWebSecurityService webSecurity)
        {
            if (webSecurity == null)
            {
                throw new ArgumentNullException("webSecurity");
            }
            if (crmService == null)
            {
                throw new ArgumentNullException("crmService");
            }
            if (auditService == null)
            {
                throw new ArgumentNullException("auditService");
            }

            this.webSecurity = webSecurity;
            this.crmService = crmService;
            this.auditService = auditService;
        }

        /// <summary>
        /// The is administrator level user.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <returns>
        /// True if the user is an administrator.
        /// </returns>
        public bool IsAdmin(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            var contact = User.FromName(username, true);
            if (contact == null)
            {
                throw new ApplicationException("Unknown user name.");
            }

            bool isAdmin;
            return bool.TryParse(contact.Profile["IsAdminLogin"], out isAdmin) && isAdmin;
        }

        /// <summary>
        /// Gets all the accessible reports for a user. 
        /// </summary>
        /// <param name="username">User name</param>
        public IEnumerable<ReportAccessMetadata> GetReportAccessFor(string username)
        {
            return // Get user access from CRM and cache them for the session.
                this.IsAdmin(username)
                    ? this.crmService.GetSuperUserReportAccess()
                    : this.crmService.GetUserReportAccess(username.Substring(Constants.CrmDomain.Length + 1));
        }

        /// <summary>
        /// The user's Profile acces
        /// </summary>
        /// <param name="username">User name</param>
        public IEnumerable<ReportAccessMetadata> GetProfileReportAccessFor(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentException("No username was provided.");
            }

            return this.crmService.GetUserReportAccess(username.Substring(Constants.CrmDomain.Length + 1));
        }

        /// <summary>
        /// Set the new Update Report Access
        /// </summary>
        /// <param name="username">The user name or email</param>
        /// <param name="updatedReportAccess">The report Access entities</param>
        public IEnumerable<ReportAccessMetadata> UpdateUserReportAccessNotifications(string username, IEnumerable<ReportAccessMetadata> updatedReportAccess)
        {
            // Get the current Report Access
            var currentReportAccess = this.GetProfileReportAccessFor(username).ToArray();

            //check for differences
            var changeList = new List<ReportAccessMetadata>();
            var changedRecorded = new List<string>();

            foreach (var updated in updatedReportAccess)
            {
                ReportAccessMetadata current = currentReportAccess.FirstOrDefault(x => x.ID == updated.ID);

                if (current != null && current.EmailNotification != updated.EmailNotification)
                {
                    changeList.Add(updated);
                    current.EmailNotification = updated.EmailNotification;

                    // Cannot use "to" as it will clip from last to word
                    changedRecorded.Add(string.Format(
                        "{0} of report {1} notification has been {2}",
                        current.GetClientName(),
                        current.ReportName,
                        current.EmailNotification ? "enabled" : "disabled"));
                }
            }

            // send differences in here
            this.crmService.UpdateUserReportAccessNotifications(username, changeList);

            // Audit if user have edited their notifications
            foreach (var change in changedRecorded)
            {
                this.auditService.Audit(
                    new AuditMetadata
                        {
                            UserName = this.webSecurity.GetCurrentAuthenticatedUsername(),
                            Action = EnumUtils<SitecoreUserAction>.GetDescription(SitecoreUserAction.UpdateProfileAction),
                            Db = Constants.WebDb,
                            Path = Constants.Content.ClientPortal.Home.Profile.Path,
                            Id = Constants.Content.ClientPortal.Home.Profile.ItemID.ToString(),
                            Language = Constants.DefaultLanguage,
                            Version = 1,
                            Miscellaneous = change
                        });
            }

            // return the changed list
            return currentReportAccess;
        }

        /// <summary>
        /// The get all report types.
        /// </summary>
        /// <returns>
        /// The all known report types.
        /// </returns>
        public IEnumerable<ReportTypeMetadata> GetAllReportTypes()
        {
            return this.crmService.GetReportTypes();
        }

        /// <summary>
        /// The get report type meta data for specified user.
        /// </summary>
        /// <param name="accessibleReports">
        /// Accessible reports filter.
        /// </param>
        /// <returns>
        /// Return get report type information accessible to the current user.
        /// </returns>
        public IEnumerable<ReportTypeMetadata> GetReportTypeMetadataFor(ICollection<string> accessibleReports)
        {
            if (accessibleReports == null || !accessibleReports.Any())
            {
                throw new ArgumentException("accessibleReports");
            }

            return this.crmService.GetReportTypes().GetAccessibleReportTypes(accessibleReports);
        }
    }
}