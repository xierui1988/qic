﻿namespace Qic.Sc.Services.Components
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Text;

    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// The HMAC token service.
    /// </summary>
    public class HmacTokenService : ITokenService
    {
        private readonly TimeSpan timeout;
        private readonly HMACSHA512 hasher;
        private readonly ASCIIEncoding encoder = new ASCIIEncoding();

        /// <summary>
        /// Initializes a new instance of the <see cref="HmacTokenService"/> class.
        /// </summary>
        public HmacTokenService(IConfigurationService configurationService)
        {
            if (configurationService == null)
            {
                throw new ArgumentNullException("configurationService");
            }

            var tokenSetting = ConfigurationManager.ConnectionStrings["SecureTokenKey"];
            if (tokenSetting == null || string.IsNullOrEmpty(tokenSetting.ConnectionString))
            {
                throw new ApplicationException("Missing required connection string 'SecureTokenKey' used to generate password reset tokens");
            }

            // Configured token expiry timeout (Default: 1hr)
            this.timeout = new TimeSpan(0, Convert.ToInt32(configurationService.GetGlobalSetting("ResetTokenTimeouInMinutes", "30")), 0);

            // Configured secret key
            var keyBytes = this.encoder.GetBytes(tokenSetting.ConnectionString);
            this.hasher = new HMACSHA512(keyBytes);
        }

        /// <summary>
        /// The generate token.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="utcTimestamp">
        /// The times tamp in UTC.
        /// </param>
        /// <returns>
        /// Generated token for the provided email and time stamp.
        /// </returns>
        public string GenerateToken(string email, DateTime utcTimestamp)
        {
            // convert the time stamp to a byte array
            var timeStampBytes = BitConverter.GetBytes(utcTimestamp.ToBinary());

            // Convert the email address to a byte array
            var emailBytes = this.encoder.GetBytes(email);

            // join them together and compute a url encoded string
            var concatBytes = timeStampBytes.Concat(emailBytes).ToArray();
            return Convert.ToBase64String(this.hasher.ComputeHash(concatBytes));
        }

        /// <summary>
        /// The validate token.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="utcTimestamp">
        /// The UTC timestamp.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool ValidateToken(string token, string email, DateTime utcTimestamp)
        {
            // recompute the token
            var recomputedToken = this.GenerateToken(email, utcTimestamp);

            // check for valid token
            if (token != recomputedToken)
            {
                return false;
            }

            // check if time stamp is still valid
            var now = DateTime.UtcNow;
            if (((now - utcTimestamp) > this.timeout) || utcTimestamp > now)
            {
                return false;
            }

            // valid token
            return true;
        }
    }
}
