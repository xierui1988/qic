﻿namespace Qic.Sc.Services.Components
{
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;

    using System;

    /// <summary>
    /// The email service.
    /// </summary>
    public class PortalEmailService : IPortalEmailService
    {
        private readonly IBackgroundService backgroundService;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class.
        /// </summary>
        public PortalEmailService(IBackgroundService backgroundService)
        {
            if (backgroundService == null)
            {
                throw new ArgumentNullException("backgroundService");
            }
            this.backgroundService = backgroundService;
        }

        /// <summary>
        /// The send report notification. Will be sent to all current users that
        /// are authorized to receive the report and have opted in for notification.
        /// </summary>
        /// <param name="reportId">
        ///     The Id of the report document to generate notifications for.
        /// </param>
        public void SendReportNotification(Guid reportId)
        {
            // Dispatch document in the background
            this.backgroundService.Run<DocumentBaseEmailDispatchService>(dispatch => dispatch.Dispatch(reportId.ToString()));
        }
    }
}
