﻿namespace Qic.Sc.Services.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;
    using Qic.Sc.Interfaces.Components;

    internal partial class FormServiceUserMetadataDecorator : IFormService
    {
        private readonly IWebSecurityService securityService;
        private readonly IFormService formService;

        public FormServiceUserMetadataDecorator(IFormService formService, IWebSecurityService securityService)
        {
            if (formService == null)
            {
                throw new ArgumentNullException("formService");
            }
            if (securityService == null)
            {
                throw new ArgumentNullException("securityService");
            }
            this.formService = formService;
            this.securityService = securityService;
        }

        public IEnumerable<IForm> GetFormsBy(string username, int skip, int take, out int total)
        {
            var metadata = this.securityService.GetUserMetadataFor(this.securityService.GetCurrentAuthenticatedUsername());
            var documents = this.formService.GetFormsBy(username, skip, take, out total).ToArray();
            return !documents.Any()
                       ? documents
                       : documents.Select(
                           d =>
                               {
                                   // augment documents with favourite indicator
                                   d.IsFavorite = metadata.Favourites.Items.Contains(d.EntityId);
                                   return d;
                               });
        }
    }
}
