﻿namespace Qic.Sc.Services.Components
{
    using System;

    using Sitecore.Data.Items;
    using Sitecore.Modules.EmailCampaign;
    using Sitecore.Modules.EmailCampaign.Core;
    using Sitecore.Modules.EmailCampaign.Core.Dispatch;
    using Sitecore.Modules.EmailCampaign.Core.MessageTransfer;
    using Sitecore.Modules.EmailCampaign.Core.Pipelines;
    using Sitecore.Modules.EmailCampaign.Core.Pipelines.DispatchNewsletter;
    using Sitecore.Modules.EmailCampaign.Messages;
    using Sitecore.Pipelines;

    /// <summary>
    /// The base email dispatch service.
    /// </summary>
    public abstract class BaseEmailDispatchService
    {
        private static object sync = new object();
        private static SMTPSettings settings;

        /// <summary>
        /// The create message.
        /// </summary>
        protected static HtmlMail CreateMessage(string itemId)
        {
            // Create a new message by cloning an existing authored template.
            var item = new ItemUtilExt().GetItem(itemId);
            if (item == null || !HtmlMail.IsCorrectMessageItem(item))
            {
                throw new InvalidOperationException("Specified item is not a valid HTML message.");
            }

            return new CustomHtmlEmailItem(item);
        }

        #region Send Email
        /// <summary>
        /// The send email.
        /// </summary>
        protected static bool SendEmail(MessageItem message)
        {
            try
            {
                var task = new CustomMessageTask(message, GetSmtpSettings());
                var args = new SendMessageArgs(message, task);
                CorePipeline.Run("SendEmail", args);

                return !args.Aborted && !args.Task.HasReceivedStopRequest;
            }
            catch (Exception)
            {
                return false;
            }
        }

        internal class CustomMessageTask : TestMessageTask
        {
            public CustomMessageTask(MessageItem message, SMTPSettings getSmtpSettings)
                : base(new DispatchNewsletterArgs(message, getSmtpSettings, new SendingProcessData()))
            {
            }
        }
        #endregion

        #region Settings
        /// <summary>
        /// The get SMTP settings.
        /// </summary>
        protected static SMTPSettings GetSmtpSettings()
        {
            if (settings == null)
            {
                lock (sync)
                {
                    string error;
                    var configuration = SendingManager.GetSmtpSettings(out error);
                    if (configuration == null)
                    {
                        throw new ApplicationException("Failed to read SMTP settings: " + error);
                    }

                    settings = configuration;
                }
            }

            return settings;
        }
        #endregion

        #region Custom HtmlEmailItem

        /// <summary>
        /// This implementation does not interfere with the links inside
        /// email. Since we're not using analytics, default redirects would not work.
        /// </summary>
        private class CustomHtmlEmailItem : HtmlMail
        {
            public CustomHtmlEmailItem(Item item)
                : base(item)
            {
            }
            public override object Clone()
            {
                var htmlMail = new CustomHtmlEmailItem(this.InnerItem);
                this.CloneFields(htmlMail);
                return htmlMail;
            }

            /// <summary>
            /// This implementation does not touch links (href).
            /// </summary>
            protected override string CorrectHtml(string html)
            {
                var htmlHelper = new HtmlHelper(html);
                htmlHelper.CleanHtml();

                var now = DateTime.Now;
                htmlHelper.InsertStyleSheets();
                Util.TraceTimeDiff("Insert style sheets", now);

                return html;
            }
        }

        #endregion
    }
}
