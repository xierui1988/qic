﻿namespace Qic.Sc.Services.Components
{
    using System.Net.Mail;

    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;

    using Sitecore.Modules.EmailCampaign;

    using System;

    using ClientPortal = Entities.Constants.Content.ClientPortal;

    /// <summary>
    /// The email service.
    /// </summary>
    public class EmailService : IEmailService
    {
        private readonly NotificationEmailDispatchService emailDispatch;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailService"/> class.
        /// </summary>
        public EmailService(IBackgroundService backgroundService, NotificationEmailDispatchService emailDispatch)
        {
            this.emailDispatch = emailDispatch;
            if (backgroundService == null)
            {
                throw new ArgumentNullException("backgroundService");
            }
        }

        /// <summary>
        /// The send welcome notification. Email will have a password reset token.
        /// </summary>
        /// <param name="userName">
        ///     The user name.
        /// </param>
        /// <param name="resetToken">
        ///     Password reset token.
        /// </param>
        public void SendWelcomeNotification(string userName, string resetToken)
        {
            var contact = Contact.FromName(userName);
            if (contact == null)
            {
                throw new ApplicationException("Contact information can't be resolved for user : " + userName);
            }

            this.SendPasswordResetMessage(
                contact.Profile.Email,
                resetToken,
                ClientPortal.Configuration.Notifications.Templates.UserNotifications.WelcomeNotification.ID);
        }

        /// <summary>
        /// The send password reset notification. Email will have a password reset token.
        /// </summary>
        /// <param name="userName">
        ///     The user name including domain name.
        /// </param>
        /// <param name="resetToken">
        ///     Password reset token.
        /// </param>
        public void SendPasswordResetNotification(string userName, string resetToken)
        {
            var contact = Contact.FromName(userName);
            if (contact == null)
            {
                throw new ApplicationException("Contact information can't be resolved for user : " + userName);
            }

            this.SendPasswordResetMessage(
                contact.Profile.Email,
                resetToken,
                ClientPortal.Configuration.Notifications.Templates.UserNotifications.PasswordResetNotification.ID);
        }

        /// <summary>
        /// Send Message directly
        /// </summary>
        /// <param name="message">Message to be sent</param>
        public void SendMessageDirectly(MailMessage message)
        {
            if (message == null)
            {
                throw new ArgumentNullException("message");
            }

            this.emailDispatch.Dispatch(message);
        }

        private void SendPasswordResetMessage(string email, string resetToken, string templateItemId)
        {
            this.emailDispatch.Dispatch(email, resetToken, templateItemId);
        }
    }
}
