﻿namespace Qic.Sc.Services.Components.Decorators
{
    using System;

    using Qic.Sc.Interfaces.Components;

    internal partial class EmailServiceTracingDecorator : TracingBaseDecorator, IEmailService
    {
        private readonly IEmailService emailService;

        public EmailServiceTracingDecorator(IEmailService emailService)
        {
            if (emailService == null)
            {
                throw new ArgumentNullException("emailService");
            }
            this.emailService = emailService;
        }
    }
}
