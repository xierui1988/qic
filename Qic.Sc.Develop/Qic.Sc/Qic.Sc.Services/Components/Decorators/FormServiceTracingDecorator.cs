﻿namespace Qic.Sc.Services.Components.Decorators
{
    using System;

    using Qic.Sc.Interfaces.Components;

    internal partial class FormServiceTracingDecorator : TracingBaseDecorator, IFormService
    {
        private readonly IFormService formService;

        public FormServiceTracingDecorator(IFormService formService)
        {
            if (formService == null)
            {
                throw new ArgumentNullException("formService");
            }
            this.formService = formService;
        }
    }
}
