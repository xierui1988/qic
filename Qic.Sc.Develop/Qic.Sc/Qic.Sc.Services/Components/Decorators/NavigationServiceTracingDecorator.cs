﻿namespace Qic.Sc.Services.Components.Decorators
{
    using Qic.Sc.Interfaces.Components;

    internal partial class NavigationServiceTracingDecorator : TracingBaseDecorator, INavigationService
    {
        private readonly INavigationService navigationService;

        public NavigationServiceTracingDecorator(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }
    }
}
