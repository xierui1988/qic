﻿namespace Qic.Sc.Services.Core
{
    using System;
    using System.Web;

    using Glass.Mapper.Sc;

    using Sitecore.Sites;

    /// <summary>
    /// Various contexts factory.
    /// </summary>
    internal class SitecoreContextFactory
    {
        /// <summary>
        /// Glass mapper site specific context mapper.
        /// </summary>
        internal static ISitecoreContext GetSitecoreContext()
        {
            var siteContext = Sitecore.Context.Site ?? GetSiteContext();
            if (siteContext == null)
            {
                throw new ApplicationException("Can't resolve site context.");
            }

            return new SitecoreContext(siteContext.Database);
        }

        /// <summary>
        /// Get sitecore context for a specific database.
        /// </summary>
        /// <param name="databaseName">The name of the database used for context.</param>
        internal static ISitecoreContext GetSitecoreContext(string databaseName)
        {
            if (databaseName == null)
            {
                throw new ArgumentNullException("databaseName");
            }

            return new SitecoreContext(Sitecore.Data.Database.GetDatabase(databaseName));
        }

        /// <summary>
        /// Runtime sitecore site context.
        /// </summary>
        private static SiteContext GetSiteContext()
        {
            var httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                return null;
            }

            var requestUri = httpContext.Request.Url;
            return SiteContextFactory.GetSiteContext(requestUri.Host, httpContext.Request.FilePath.ToLower(), requestUri.Port);
        }
    }
}
