﻿namespace Qic.Sc.Services.Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Qic.Sc.Interfaces.Core;

    using Sitecore.ContentSearch;
    using Sitecore.Search;

    /// <summary>
    /// Sitecore search service implementation.
    /// </summary>
    public class SitecoreSearchService : IContentSearchService
    {
        private readonly IProviderSearchContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreSearchService"/> class.
        /// </summary>
        /// <param name="context">Sitecore search context.</param>
        public SitecoreSearchService(IProviderSearchContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            this.context = context;
        }

        /// <summary>
        /// Dispose of service context.
        /// </summary>
        public void Dispose()
        {
            this.context.Dispose();
        }

        /// <summary>
        /// Initializes a query instance. Note: only concrete types can be used as a template
        /// parameters.
        /// </summary>
        /// <typeparam name="TEntity">A strongly typed entity to return as a result of the search.</typeparam>
        /// <returns>
        /// A query definition, must be executed for data to be returned.
        /// </returns>
        public IQueryable<TEntity> GetQueryable<TEntity>()
        {
            return this.context.GetQueryable<TEntity>();
        }

        /// <summary>
        /// The search.
        /// </summary>
        /// <param name="query">
        /// The query.
        /// </param>
        /// <param name="skip">
        /// The skip.
        /// </param>
        /// <param name="take">
        /// The take.
        /// </param>
        /// <returns>
        /// The <see cref="IEnumerable"/>.
        /// </returns>
        public IEnumerable<TEntity> Search<TEntity>(string query, int skip, int take) where TEntity : class
        {
            var index = SearchManager.GetIndex("system");
            using (var ctx = index.CreateSearchContext())
            {
                var fullText = new FullTextQuery(query);
                var hist = ctx.Search(fullText, 5000); // TODO: Make configurable max result

                return hist.FetchResults(skip, take).Select(h => h.GetObject<TEntity>()).ToArray();
            }
        }
    }
}
