namespace Qic.Sc.Services.Core
{
    using System;
    using System.Collections.Concurrent;
    using System.Threading;

    using Autofac;
    using Autofac.Core.Lifetime;

    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// The background service.
    /// </summary>
    public class BackgroundService : IBackgroundService
    {
        private static readonly BlockingCollection<Action> Tasks = new BlockingCollection<Action>();
        private static Thread worker;

        private readonly ILifetimeScope scope;

        /// <summary>
        /// Initializes a new instance of the <see cref="BackgroundService"/> class.
        /// </summary>
        public BackgroundService(ILifetimeScope scope)
        {
            if (scope == null)
            {
                throw new ArgumentNullException("scope");
            }
            this.scope = scope;
        }

        /// <summary>
        /// Execute in the background.
        /// </summary>
        public void Run<T>(Action<T> task)
        {
            this.EnsureBackgroundWorker();
            using (var taskscope = this.BackgroundScope())
            {
                var service = taskscope.Resolve<T>();
                this.ProcessInBackground(() => task(service));
            }
        }

        /// <summary>
        /// Execute in the background.
        /// </summary>
        public void Run<T1, T2, T3, T4>(Action<T1, T2, T3, T4> task)
        {
            this.EnsureBackgroundWorker();
            using (var taskscope = this.BackgroundScope())
            {
                var service1 = taskscope.Resolve<T1>();
                var service2 = taskscope.Resolve<T2>();
                var service3 = taskscope.Resolve<T3>();
                var service4 = taskscope.Resolve<T4>();
                this.ProcessInBackground(() => task(service1, service2, service3, service4));
            }
        }

        #region Background Processor
        /// <summary>
        /// Tasks that run in the background can't rely on the scope of the initiating
        /// request. Services may be disposed before the background task is complete or
        /// even started. Background tasks should use this scope which will be based on
        /// the scope of the initiating request.
        /// </summary>
        protected ILifetimeScope BackgroundScope()
        {
            return this.scope.BeginLifetimeScope(MatchingScopeLifetimeTags.RequestLifetimeScopeTag);
        }

        /// <summary>
        /// The process in background.
        /// </summary>
        protected void ProcessInBackground(Action task)
        {
            this.EnsureBackgroundWorker();
            Tasks.Add(task);
        }

        private void EnsureBackgroundWorker()
        {
            // Checks a background thread is alive and start one if needed.
            if (worker == null || !worker.IsAlive)
            {
                lock (Tasks)
                {
                    worker = new Thread(this.Worker) { IsBackground = true, Priority = ThreadPriority.BelowNormal };
                    worker.Start();
                }
            }
        }

        private void Worker()
        {
            // Use context of the initiating web request as a container scope for the background thread
            using (var threadscope = this.BackgroundScope())
            {
                var log = threadscope.Resolve<ILoggerService>();
                while (true)
                {
                    try
                    {
                        Action task;
                        if (Tasks.TryTake(out task, 100))
                        {
                            task();
                        }

                        // A crude log based progress indicator implementation
                        // will periodically log the size of the queue as it grows and shrinks.
                        if (Tasks.Count > 0 && Tasks.Count % 10 == 0)
                        {
                            log.Info("Email background jobs in the queue: " + Tasks.Count);
                        }
                    }
                    catch (Exception ex)
                    {
                        log.Error("Email background processor error.", ex);
                    }
                }
            }
        }
        #endregion
    }
}