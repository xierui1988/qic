﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Qic.Sc.Entities;
using Qic.Sc.Entities.UserDefined.Components.Site.Configuration;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Qic.Sc.Interfaces.Components;
using Qic.Sc.Services.Components;

namespace Qic.Sc.Services.Tests
{
    using Qic.Sc.Entities.Metadata;
    using System.Collections.Generic;

    using Qic.Sc.Interfaces.Core;

    [TestClass]
    public class ReportServiceTests : TestBase
    {
        private const string entityName = "ENTITYNAME";
        private static readonly string accountEntityCode = EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.AccountCode);
        private readonly string holdingAccountEntityCode = EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.HoldingAccountCode);
        private readonly string productEntityCode = EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.ProductCode);
        private static readonly DateTime date = DateTime.Now;
        private const string reportCode = "REPORTCODE";
        private const string extention = "pdf";
        private static readonly string filename = String.Format("{0}+{1}+{2}+{3}.{4}",entityName, accountEntityCode, date.ToString("yyyyMMdd"), reportCode, extention); 
        private static readonly Stream fileBody = GenerateStreamFromString(filename);

        private readonly IEnumerable<ReportMetadata> crmData = new ReportMetadata[] { new ReportMetadata
        {
            ReportName = "Report Name",
            ReportCode = "Report Code",
            AccountCode = "entityCode",
            AccountName = "Client Name"
        }};

        private readonly ReportNameMetadata reportobj = new ReportNameMetadata
        {
            EntityName = entityName,
            EntityCode = accountEntityCode,
            ReportDate = date,
            ReportCode = reportCode,
            FileExtension = extention,
            FileName = filename,
            FileStream = fileBody,
            MimeType = "pdf"
        };
   
        private IEnumerable<ReportMetadata> crmEmptyData = new ReportMetadata[] { };

        public static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;
        }

        private IReportService reportService;

        [TestInitialize]
        public override void Setup()
        {
            base.Setup();
            this.reportService = new ReportService(
                this.MockCrmService.Object,
                this.MockContentRepository.Object,
                this.MockMasterContentRepository.As<IMasterContentRepository>().Object,
                this.MockWebSecurityService.Object,
                this.MockPortalWebSecurityService.Object,
                this.MockAuditService.Object);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailParseReportNullEntityName()
        {
            // Arrange
            // Act
            ReportNameMetadata.ParseFileName("+PR999+20150202+IVNT_20121212_true.pdf");

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailParseAndValidateReportEmptyEntityName()
        {
            // Arrange
            // Act
            ReportNameMetadata.ParseFileName("+PR999+20150202+IVNT_20121212_true.pdf");

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailParseAndValidateReportNullEntityCode()
        {
            // Arrange
            // Act
            ReportNameMetadata.ParseFileName("Brisbane Council++20150202+IVNT_20121212_true.pdf");

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailParseAndValidateReportEmptyEntityCode()
        {
            // Arrange
            // Act
            ReportNameMetadata.ParseFileName("Brisbane Council++20150202+IVNT_20121212_true.pdf");
            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailParseAndValidateReportNullReportCode()
        {
            // Arrange
            // Act
            ReportNameMetadata.ParseFileName("Brisbane Council++20150202+_20121212_true.pdf");
            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailParseAndValidateReportEmptyReportCode()
        {
            // Arrange
            // Act
            ReportNameMetadata.ParseFileName("Brisbane Council++20150202+_20121212_true.pdf");

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailParseAndValidateReportNullExtention()
        {
            // Arrange
            // Act
            ReportNameMetadata.ParseFileName("Brisbane Council++20150202+_20121212_true.");

            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailParseAndValidateReportEmptyExtenstion()
        {
            // Arrange
            // Act
            ReportNameMetadata.ParseFileName("Brisbane Council++20150202+_20121212_true");
            // Assert
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void FailCreateReportCrmServiceError()
        {
            // Arrange
            this.MockCrmService.Setup(s => s.GetReportMetadata(It.Is<EntityCodeType>(c => c.Equals(EntityCodeType.AccountCode)), accountEntityCode, reportCode)).Throws<ApplicationException>();

            // Act
            this.reportService.CreateReport(reportobj);

            // Assert
            this.MockCrmService.VerifyAll();
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void FailCreateReportCrmNoClientCodesForClientEntityCode()
        {
            // Arrange
            this.MockCrmService.Setup(s => s.GetReportMetadata(It.Is<EntityCodeType>(c => c.Equals(EntityCodeType.AccountCode)), accountEntityCode, reportCode)).Throws<ApplicationException>();

            // Act
            reportobj.EntityCode = accountEntityCode;
            this.reportService.CreateReport(reportobj);

            // Assert
            this.MockCrmService.VerifyAll();
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void FailCreateReportCrmNoClientCodesForHoldingAccountEntityCode()
        {
            // Arrange
            this.MockCrmService.Setup(s => s.GetReportMetadata(It.Is<EntityCodeType>(c => c.Equals(EntityCodeType.HoldingAccountCode)), holdingAccountEntityCode, reportCode)).Throws<ApplicationException>();

            // Act
            reportobj.EntityCode = holdingAccountEntityCode;
            this.reportService.CreateReport(reportobj);

            // Assert
            this.MockCrmService.VerifyAll();
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void FailCreateReportCrmNoClientCodesForProductEntityCode()
        {
            // Arrange
            this.MockCrmService.Setup(s => s.GetReportMetadata(It.Is<EntityCodeType>(c => c.Equals(EntityCodeType.ProductCode)), productEntityCode, reportCode)).Throws<ApplicationException>();

            // Act
            reportobj.EntityCode = productEntityCode;
            this.reportService.CreateReport(reportobj);

            // Assert
            this.MockCrmService.VerifyAll();
        }
       
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void FailCreateReportFailToSaveToSitecoreDb()
        {
            // Arrange
            this.MockCrmService.Setup(s => s.GetReportMetadata(It.Is<EntityCodeType>(c => c.Equals(EntityCodeType.AccountCode)), accountEntityCode, reportCode)).Returns(crmData);
            
            this.MockMasterContentRepository.Setup(
                c =>
                    c.CreateEntity(It.IsAny<IConfiguration>(), It.IsAny<DocumentBlobEntity>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>())).Throws<Exception>();

            // Act
            this.reportService.CreateReport(reportobj);

            // Assert
            this.MockCrmService.VerifyAll();
            this.MockMasterContentRepository.VerifyAll();
        }

        [TestMethod]
        [Ignore]
        public void SuccessCreateReportToSaveToSitecoreDb()
        {
            // Arrange
            this.MockCrmService.Setup(s => s.GetReportMetadata(It.Is<EntityCodeType>(c => c.Equals(EntityCodeType.AccountCode)), accountEntityCode, reportCode)).Returns(crmData);
            this.MockWebSecurityService.Setup(s => s.GetCurrentAuthenticatedUsername()).Returns(It.IsAny<string>());
            this.MockAuditService.Setup(s => s.Audit(It.IsAny<AuditMetadata>()));

            var searchService = new Mock<IContentSearchService>();
            this.MockMasterContentRepository.Setup(s => s.GetSearchService()).Returns(searchService.Object);

            this.MockMasterContentRepository.Setup(c =>
                    c.CreateEntity(
                    It.IsAny<IConfiguration>(),
                    It.Is<DocumentBlobEntity>(x => x.WorkflowState == Constants.System.Workflows.QICPortalPublishingWorkflow.New.ItemID.ToString()),
                    It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()));

            // Act
            this.reportService.CreateReport(reportobj);

            // Assert
            this.MockCrmService.VerifyAll();
            this.MockWebSecurityService.VerifyAll();
            this.MockAuditService.VerifyAll();
            this.MockMasterContentRepository.VerifyAll();
            this.MockMasterContentRepository.VerifyAll();
        }

        [TestMethod]
        [Ignore]
        public void SuccessCreateMigratedReportToSaveToSitecoreDb()
        {
            // Arrange
            this.MockCrmService.Setup(s => s.GetReportMetadata(It.Is<EntityCodeType>(c => c.Equals(EntityCodeType.AccountCode)), accountEntityCode, reportCode)).Returns(crmData);
            this.MockWebSecurityService.Setup(s => s.GetCurrentAuthenticatedUsername()).Returns(It.IsAny<string>());
            this.MockAuditService.Setup(s => s.Audit(It.IsAny<AuditMetadata>())).Throws<ApplicationException>();

            var searchService = new Mock<IContentSearchService>();
            this.MockMasterContentRepository.Setup(s => s.GetSearchService()).Returns(searchService.Object);

            this.MockMasterContentRepository.Setup(c =>
                    c.CreateEntity(
                    It.IsAny<IConfiguration>(),
                    It.Is<DocumentBlobEntity>(x => x.WorkflowState == Constants.System.Workflows.QICPortalPublishingWorkflow.Published.ItemID.ToString()),
                    It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<bool>()));

            string filename = string.Format(
                "{0}+{1}+{2}+{3}_{4}.{5}",
                entityName,
                accountEntityCode,
                DateTime.Now.ToString(Constants.ReportDateFormat),
                reportCode,
                DateTime.Now.ToString(Constants.PublishedDateFormat),
                extention);

            // Act
            this.reportService.CreateReport(ReportNameMetadata.ParseFileName(filename));

            // Assert
            this.MockCrmService.VerifyAll();
            this.MockWebSecurityService.VerifyAll();
            this.MockAuditService.VerifyAll();
            this.MockMasterContentRepository.VerifyAll();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void FailCreateReportWithOptionalParameterPlus()
        {
            string filename = string.Format(
                "{0}+{1}+{2}+{3}+{4}.{5}",
                entityName,
                accountEntityCode,
                DateTime.Now.ToString(Constants.ReportDateFormat),
                reportCode,
                DocumentNotification.Email.ToString(),
                extention);
            ReportNameMetadata.ParseFileName(filename);

            // Assert
            this.MockCrmService.VerifyAll();
        }

        [TestMethod]
        public void SucceessCreateReportWithOptionalEmail()
        {
            string filename = string.Format(
                "{0}+{1}+{2}+{3}_{4}.{5}",
                entityName,
                accountEntityCode,
                DateTime.Now.ToString(Constants.ReportDateFormat),
                reportCode,
                DocumentNotification.Email.ToString(),
                extention);
            ReportNameMetadata.ParseFileName(filename);

            // Assert
            this.MockCrmService.VerifyAll();
        }

        [TestMethod]
        public void SucceessCreateReportWithOptionalPublishDate()
        {
            string filename = string.Format(
                "{0}+{1}+{2}+{3}_{4}.{5}",
                entityName,
                accountEntityCode,
                DateTime.Now.ToString(Constants.ReportDateFormat),
                reportCode,
                DateTime.Now.ToString(Constants.PublishedDateFormat),
                extention);
            ReportNameMetadata.ParseFileName(filename);

            // Assert
            this.MockCrmService.VerifyAll();
        }
    }
}