﻿using Qic.Sc.Entities;
using System.Collections.Generic;

namespace Qic.Sc.Interfaces.Core
{
    /// <summary>
    /// The content repository. Abstracts CRUD operations over content.
    /// Enables search capability over content.
    /// </summary>
    public interface IContentRepository
    {
        /// <summary>
        /// Retrieve entity from content repository based on the 
        /// </summary>
        /// <param name="identity">
        /// The identity of an entity. Intention is to have an key or any other identifier that
        /// uniquely identifies an entity. Implementation specific refer to implementation
        /// documentation for further details.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of entity to be retrieved.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/> that matches specified identity.
        /// </returns>
        TModel GetEntity<TModel>(string identity) where TModel : class;

        /// <summary>
        /// Retrieve entity from content repository.
        /// </summary>
        /// <param name="identity">
        /// The identity of an entity. Intention is to have an key or any other identifier that
        /// uniquely identifies an entity. Implementation specific refer to implementation
        /// documentation for further details.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of entity to be retrieved.
        /// </typeparam>
        /// <typeparam name="TModel1">
        /// Additional entity type to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/> that matches specified identity.
        /// </returns>
        TModel GetEntity<TModel, TModel1>(string identity)
            where TModel : class
            where TModel1 : class;

        /// <summary>
        /// Retrieve entity from content repository.
        /// </summary>
        /// <param name="identity">
        /// The identity of an entity. Intention is to have an key or any other identifier that
        /// uniquely identifies an entity. Implementation specific refer to implementation
        /// documentation for further details.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of entity to be retrieved.
        /// </typeparam>
        /// <typeparam name="TModel1">
        /// Additional entity type X to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <typeparam name="TModel2">
        /// Additional entity type Y to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/> that matches specified identity.
        /// </returns>
        TModel GetEntity<TModel, TModel1, TModel2>(string identity)
            where TModel : class
            where TModel1 : class
            where TModel2 : class;

        /// <summary>
        /// Retrieve entity from content repository.
        /// </summary>
        /// <param name="identity">
        /// The identity of an entity. Intention is to have an key or any other identifier that
        /// uniquely identifies an entity. Implementation specific refer to implementation
        /// documentation for further details.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of entity to be retrieved.
        /// </typeparam>
        /// <typeparam name="TModel1">
        /// Additional entity type X to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <typeparam name="TModel2">
        /// Additional entity type Y to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <typeparam name="TModel3">
        /// Additional entity type Z to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/> that matches specified identity.
        /// </returns>
        TModel GetEntity<TModel, TModel1, TModel2, TModel3>(string identity)
            where TModel : class
            where TModel1 : class
            where TModel2 : class
            where TModel3 : class;

        /// <summary>
        /// Load missing information into the model.
        /// </summary>
        /// <param name="model">
        /// The model to be back-loaded.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of the model.
        /// </typeparam>
        /// <returns>
        /// Same object as passed in after the its been back-loaded with missing
        /// information. Useful for chaining the calls.
        /// </returns>
        TModel MapEntity<TModel>(TModel model);

        /// <summary>
        /// Get the parent
        /// </summary>
        /// <param name="childObject">
        /// The Id.
        /// </param>
        /// <typeparam name="TModel">
        /// Entity type to return for the parent.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/>.
        /// </returns>
        TModel GetParent<TModel>(object childObject) where TModel : class;

        /// <summary>
        /// Get a specific ancestor identified by <paramref name="contextItem"/> the of a content
        /// entity identified by <paramref name="identity"/>. Refer to implementation details
        /// for more specific information.
        /// </summary>
        /// <param name="contextItem">
        /// The target.
        /// </param>
        /// <param name="identity">
        /// The identity.
        /// </param>
        /// <typeparam name="TModel">
        /// Entity type to return for the parent.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/>.
        /// </returns>
        TModel FindAncestorItemBy<TModel>(object contextItem, string identity) where TModel : class;

        /// <summary>
        /// Get a default search context based on provided identity.
        /// </summary>
        /// <returns>
        /// The <see cref="IContentSearchService"/>.
        /// </returns>
        IContentSearchService GetSearchService();

        /// <summary>
        /// Queries a single entity. If you know the identity of the entity use one
        /// of the GetEntity methods instead they are much more efficient.
        /// </summary>
        /// <typeparam name="TModel">The type of the entity to return.</typeparam>
        /// <param name="query">Query string</param>
        /// <param name="isLazy">Lazy load the result set.</param>
        /// <param name="inferType">Infer types of th result set items.</param>
        /// <returns>A single item that matches the query or null.</returns>
        TModel QuerySingle<TModel>(string query, bool isLazy = false, bool inferType = false) where TModel : class;

        /// <summary>
        /// Queries repository for the entities.
        /// </summary>
        /// <typeparam name="TModel">The type of entity to return.</typeparam>
        /// <param name="query">Query string</param>
        /// <param name="isLazy">Lazy load the result set.</param>
        /// <param name="inferType">Infer types of the items in the result set.</param>
        /// <returns>A sequence of entities that match the query or an empty set.</returns>
        IEnumerable<TModel> Query<TModel>(string query, bool isLazy = false, bool inferType = false) where TModel : class;

        /// <summary>
        /// The create entity.
        /// </summary>
        /// <param name="parent">
        ///     The parent entity.
        /// </param>
        /// <param name="newEntity">
        ///     The new entity.
        /// </param>
        /// <param name="updateStatistics">
        ///     Whether the operation should affect statistic (updated time-stamp and version).
        ///     (Default: true)
        /// </param>
        /// <param name="silent">
        ///     Whether the operation would NOT raise events.
        ///     (Default: false)
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        TEntity CreateEntity<TEntity, TParentEntity>(TParentEntity parent, TEntity newEntity, bool disableSecurity = false, bool updateStatistics = true, bool silent = false)
            where TEntity : class
            where TParentEntity : class;

        /// <summary>
        /// The save entity.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="updateStatistics">
        ///     Whether the operation should affect statistic (updated time-stamp and version).
        ///     (Default: true)
        /// </param>
        /// <param name="silent">
        ///     Whether the operation would NOT raise events.
        ///     (Default: false)
        /// </param>
        void SaveEntity<TModel>(TModel entity, bool disableSecurity = false, bool updateStatistics = true, bool silent = false) where TModel : class;

        /// <summary>
        /// Creates a new version for that Entity
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="disableSecurity">
        ///     It will allow the option to disable security (intended for Tibco webservice)
        ///     (Default: false)
        /// </param>
        TModel AddVersion<TModel>(TModel entity, bool disableSecurity = false) where TModel : class;
    }
}
