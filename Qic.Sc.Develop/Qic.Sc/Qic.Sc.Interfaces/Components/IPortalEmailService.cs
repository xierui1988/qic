﻿namespace Qic.Sc.Interfaces.Components
{
    using System;

    /// <summary>
    /// The email service interface.
    /// </summary>
    public interface IPortalEmailService
    {
        /// <summary>
        /// The send report notification. Will be sent to all current users that
        /// are authorized to receive the report and have opted in for notification.
        /// </summary>
        void SendReportNotification(Guid reportId);
    }
}