﻿namespace Qic.Sc.Interfaces.Components
{
    using System;

    /// <summary>
    /// The token provider.
    /// </summary>
    public interface ITokenService
    {
        /// <summary>
        /// The generate token.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="utcTimestamp">
        /// The times tamp in UTC.
        /// </param>
        /// <returns>
        /// Generated token for the provided email and time stamp.
        /// </returns>
        string GenerateToken(string email, DateTime utcTimestamp);

        /// <summary>
        /// The validate token.
        /// </summary>
        /// <param name="token">
        /// The token.
        /// </param>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="utcTimestamp">
        /// The utc timestamp.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool ValidateToken(string token, string email, DateTime utcTimestamp);
    }
}
