﻿namespace Qic.Sc.Interfaces.Components
{
    using System.Collections.Generic;
    using System.Security.Principal;

    using Qic.Sc.Entities.Metadata;

    /// <summary>
    /// Provides the security and authentication features, including the ability to create user accounts, log users in and out, reset or change passwords, lock and unlock accounts, and perform related tasks.
    /// </summary>
    public interface IPortalWebSecurityService
    {
        /// <summary>
        /// Gets all the accessible reports for a user.
        /// </summary>
        /// <param name="username">User name</param>
        IEnumerable<ReportAccessMetadata> GetReportAccessFor(string username);

        /// <summary>
        /// Gets all the accessible reports for a user.
        /// </summary>
        /// <param name="username">User name</param>
        IEnumerable<ReportAccessMetadata> GetProfileReportAccessFor(string username);

        /// <summary>
        /// Set the new Update Report Access
        /// </summary>
        /// <param name="username">The username - email address</param>
        /// <param name="reportAccess">The report Access entities</param>
        /// <returns>The updated filled list</returns>
        IEnumerable<ReportAccessMetadata> UpdateUserReportAccessNotifications(string username, IEnumerable<ReportAccessMetadata> reportAccess);

        /// <summary>
        /// The get all report types.
        /// </summary>
        /// <returns>
        /// The all known report types.
        /// </returns>
        IEnumerable<ReportTypeMetadata> GetAllReportTypes();

        /// <summary>
        /// The get report type meta data for specified user.
        /// </summary>
        /// <param name="accessibleReports">
        /// Accessible reports filter.
        /// </param>
        /// <returns>
        /// Return get report type information accessible to the current user.
        /// </returns>
        IEnumerable<ReportTypeMetadata> GetReportTypeMetadataFor(ICollection<string> accessibleReports);
    }
}