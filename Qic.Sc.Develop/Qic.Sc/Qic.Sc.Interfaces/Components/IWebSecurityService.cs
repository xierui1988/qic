﻿namespace Qic.Sc.Interfaces.Components
{
    using System.Collections.Generic;
    using System.Security.Principal;

    using Qic.Sc.Entities.Metadata;

    /// <summary>
    /// Provides the security and authentication features, including the ability to create user accounts, log users in and out, reset or change passwords, lock and unlock accounts, and perform related tasks.
    /// </summary>
    public interface IWebSecurityService
    {
        /// <summary>
        /// Verifies if whether provided password is valid.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// true if the user the password is valid.
        /// </returns>
        bool IsValidPassword(string username, string password);

        /// <summary>
        /// The is administrator level user.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool IsAdmin(string username);

        /// <summary>
        /// Resets a password by using a password reset token.
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="newPassword">New Password</param>
        void ResetPassword(string username, string newPassword);

        /// <summary>
        /// The get user meta data for specified user.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="UserMetadata"/>.
        /// </returns>
        UserMetadata GetUserMetadataFor(string username);

        /// <summary>
        /// The updated user meta data for.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <param name="metadata">
        /// The meta data.
        /// </param>
        void UpdatedUserMetadataFor(string username, UserMetadata metadata);

        /// <summary>
        /// The get current user name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetCurrentAuthenticatedUsername();

        /// <summary>
        /// Authenticates the XRM credentials.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        bool AuthenticateXrmCredentials(string id);

        /// <summary>
        /// The get active user.
        /// </summary>
        /// <returns>
        /// The <see cref="IPrincipal"/>.
        /// </returns>
        IPrincipal GetActiveUser();

        /// <summary>
        /// The get active portal user name.
        /// </summary>
        /// <returns>
        /// First name for portal user
        /// </returns>
        string GetActiveUserDisplayName();
    }
}