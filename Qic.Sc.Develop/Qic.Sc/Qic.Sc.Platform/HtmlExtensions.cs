﻿namespace Qic.Sc.Web
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.WebPages;

    using Autofac;

    using Glass.Mapper.Sc.Fields;

    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Platform;
    using Qic.Sc.Platform.Mvc;

    using Sitecore.Data.Items;
    using Sitecore.Mvc.Common;

    /// <summary>
    /// Html extension to provide script support for views.
    /// </summary>
    public static class HtmlExtensions
    {
        #region Rendering script infrastructure

        private static IList<string> GetScriptStore(HtmlHelper self)
        {
            if (self == null || self.ViewContext == null || self.ViewContext.HttpContext == null)
            {
                return null;
            }

            return GetScriptStore(self.ViewContext.HttpContext.ApplicationInstance.Context);
        }

        private static IList<string> GetScriptStore(HttpContext self)
        {
            const string PageScriptStorageKey = "Sc.Views.RequestScriptStore";

            if (self == null)
            {
                return null;
            }

            var items = self.Items;
            if (!items.Contains(PageScriptStorageKey))
            {
                items[PageScriptStorageKey] = new List<string>();
            }

            return items[PageScriptStorageKey] as IList<string>;
        }

        private static IList<string> GetRenderingScriptStore()
        {
            var context = ContextService.Get().GetCurrentOrDefault<RenderingScriptContext>();
            return context == null ? null : context.ScriptStore;
        }

        /// <summary>
        /// The register page script with current request.
        /// </summary>
        /// <param name="self">
        /// The self.
        /// </param>
        /// <param name="scripts">
        /// The scripts.
        /// </param>
        public static void RegisterPageScript(this HttpContext self, IList<string> scripts)
        {
            var store = GetScriptStore(self);
            if (store == null)
            {
                return;
            }

            foreach (var script in scripts)
            {
                store.Add(script);
            }
        }

        /// <summary>
        /// Register a page script.
        /// </summary>
        /// <param name="self">The class to extend.</param>
        /// <param name="render">The script to render.</param>
        /// <returns>Empty rendering as the actual script will be rendered later.</returns>
        public static HelperResult RegisterPageScript(this HtmlHelper self, Func<object, HelperResult> render)
        {
            // Use rendering pipeline script store if we are in a rendering context
            // otherwise revert to request level script store directly. Rendering
            // script store honors Sitecore output cache and as such should
            // be considered first.
            var store = GetRenderingScriptStore() ?? GetScriptStore(self);
            if (store != null)
            {
                // Render script now, while view context still exists and
                // store rendered output. This ensures that correct behavior
                // for output cached scripts.
                var writer = new StringWriter();
                render(null).WriteTo(writer);

                // Cache script as a rendered string.
                store.Add(writer.ToString());
            }

            return new HelperResult(writer => { }); // Empty rendering
        }

        /// <summary>
        /// Render the previously registered script fragments.
        /// </summary>
        /// <param name="self">The class to extend.</param>
        /// <returns>The result of the rendering.</returns>
        public static HelperResult RenderPageScript(this HtmlHelper self)
        {
            return new HelperResult(
                writer =>
                {
                    foreach (var script in GetScriptStore(self) ?? Enumerable.Empty<string>())
                    {
                        writer.Write(script);
                    }
                });
        }

        #endregion

        #region Configuration infrastructure

        /// <summary>
        /// Get configuration service.
        /// </summary>
        /// <returns>An instance of current configuration service.</returns>
        public static IConfigurationService Configuration(this HtmlHelper self)
        {
            return DependencyResolver.Current.GetService<IConfigurationService>();
        }

        #endregion

        #region Entity based relative URL generation

        /// <summary>
        /// Generate a path to the item.
        /// </summary>
        /// <returns>LinkUrl as string based on type</returns>
        public static string ContentLink(this UrlHelper self, Link linkField, string sitename = null)
        {
            if (linkField == null)
            {
                return string.Empty;
            }

            switch (linkField.Type)
            {
                case LinkType.Internal:
                    // Generate a relative path to the item.
                    return self.ForItem(linkField.TargetId, sitename);
                case LinkType.External:
                    // Just return external links.
                    return linkField.Url;
                default:
                    throw new NotSupportedException("Link type " + linkField.Type + " is not supported.");
            }
        }

        /// <summary>
        /// Generates a relative URL/path to the item identified by the passed
        /// in identifier.
        /// </summary>
        /// <returns>
        ///     Path to the item.
        /// </returns>
        public static string ForItem(this UrlHelper self, Guid itemId, string sitename = null)
        {
            using (var scope = ServiceLocator.BeginScopeForSite(sitename))
            {
                // Get the item matching the id.
                var item = scope.Resolve<IContentRepository>().GetEntity<Item>(itemId.ToString());
                if (item == null)
                {
                    return string.Empty;
                }

                // Check for custom link provider specific for template.
                var templateId = item.TemplateID.Guid.ToString();
                if (scope.IsRegisteredWithName<ILinkService>(templateId))
                {
                    var relative = scope.ResolveNamed<ILinkService>(templateId).GenerateUrl(itemId.ToString());
                    if (!string.IsNullOrEmpty(sitename))
                    {
                        // Canonical URL
                        var canonicalUrl = scope.Resolve<ISiteService>().GetCanonicalUri();
                        return new Uri(canonicalUrl, relative).ToString();
                    }

                    return relative;
                }

                // User generic link provider.
                return scope.Resolve<ILinkService>().GenerateUrl(itemId.ToString());
            }
        }

        /// <summary>
        /// Generates a relative URL/path to the item identified by the passed
        /// in identifier.
        /// </summary>
        /// <returns>
        ///     Path to the item.
        /// </returns>
        private static string GenerateRelativeUrl(Guid itemId)
        {
            using (var scope = ServiceLocator.BeginScopeForCurrentSite())
            {
                var item = scope.Resolve<IContentRepository>().GetEntity<Item>(itemId.ToString());
                if (item == null)
                {
                    return string.Empty;
                }

                var templateId = item.TemplateID.ToString();
                if (scope.IsRegisteredWithName<ILinkService>(templateId))
                {
                    return scope.ResolveNamed<ILinkService>(templateId).GenerateUrl(itemId.ToString());
                }

                // Use LinkMananger for internal links, if link is not empty
                return scope.Resolve<ILinkService>().GenerateUrl(itemId.ToString());
            }
        }

        /// <summary>
        /// Generates a relative URL/path to the item identified by the passed
        /// in identifier.
        /// </summary>
        /// <returns>
        ///     Path to the item.
        /// </returns>
        public static string GenerateRelativeUrl(this Controller self, Guid itemId)
        {
            return GenerateRelativeUrl(itemId);
        }
        #endregion

        #region Rendering image URLs with sizes

        /// <summary>
        /// The get image source URL with max width.
        /// </summary>
        public static string GetMaxWidthSrc(this Image img, int width)
        {
            if (img == null || string.IsNullOrEmpty(img.Src))
            {
                return null;
            }

            return img.Src + "?mw=" + width;
        }

        #endregion
    }
}
