﻿namespace Qic.Sc.Platform
{
    using System;

    using Autofac;

    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;

    using Sitecore.Data.Items;

    internal static class AuditExtensions
    {
        public static AuditMetadata FromItem(this AuditMetadata self, Item item)
        {
            if (item == null)
            {
                return null;
            }

            self.Db = item.Database.Name;
            self.Path = item.Paths.FullPath;
            self.Language = item.Language.Name;
            self.Version = item.Version.Number;
            self.Id = item.ID.ToString();

            return self;
        }

        public static void Audit(this AuditMetadata self)
        {
            if (self == null)
            {
                return;
            }

            try
            {
                using (var scropt = ServiceLocator.BeginScopeForMasterDatabase())
                {
                    var service = scropt.Resolve<IAuditService>();
                    service.Audit(self);
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Failed to perform audit operation.", ex);
            }
        }
    }
}
