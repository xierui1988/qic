﻿namespace Qic.Sc.Web
{
    using System.Linq;
    using System.Web;

    using Qic.Sc.Platform.Mvc;

    using Sitecore.Mvc.Extensions;
    using Sitecore.Mvc.Helpers;
    using Sitecore.Mvc.Pipelines;
    using Sitecore.Mvc.Pipelines.Response.RenderRendering;
    using Sitecore.Mvc.Presentation;

    /// <summary>
    /// Sitecore helper extensions.
    /// </summary>
    public static class SitecoreHelperExtensions
    {
        #region Rendering with cache support
        /// <summary>
        /// Call sitecore controller.
        /// </summary>
        /// <param name="self">
        /// Extended object.
        /// </param>
        /// <param name="idOrPath">
        /// Id or a path to the rendering item.
        /// </param>
        /// <param name="proerties">
        /// Rendering properties to set or overwrite.
        /// </param>
        /// <returns>Action result of invoking controller.</returns>
        public static HtmlString RenderingEx(this SitecoreHelper self, string idOrPath, object proerties = null)
        {
            var rendering = AddRenderingItemProperties(GetRendering(idOrPath, proerties));

            var stringWriter = new System.IO.StringWriter();
            PipelineService.Get().RunPipeline("mvc.renderRendering", new RenderRenderingArgs(rendering, stringWriter));
            return new HtmlString(stringWriter.ToString());
        }

        private static Rendering GetRendering(string idOrPath, object parameters)
        {
            var rendering = new Rendering { RenderingType = string.Empty, RenderingItemPath = idOrPath };

            if (parameters != null)
            {
                var properties = TypeHelper.GetProperties(parameters);
                properties.Each(delegate(System.Collections.Generic.KeyValuePair<string, object> pair)
                {
                    rendering.Properties[pair.Key] = pair.Value.ValueOrDefault(o => o.ToString());
                    rendering.Parameters[pair.Key] = pair.Value.ValueOrDefault(o => o.ToString());
                });
            }

            return rendering;
        }

        private static Rendering AddRenderingItemProperties(Rendering rendering)
        {
            var renderingItem = rendering.RenderingItem;
            if (renderingItem == null)
            {
                return rendering;
            }

            if (rendering.DataSource.IsEmptyOrNull())
            {
                rendering.DataSource = renderingItem.DataSource;
            }

            if (rendering.Placeholder.IsEmptyOrNull())
            {
                rendering.Placeholder = renderingItem.Placeholder;
            }

            if (!rendering.Parameters.Any())
            {
                rendering.Parameters = new RenderingParameters(renderingItem.Parameters);
            }

            // Use cache settings from the underlying rendering item is cache is enabled.
            if (!rendering.Caching.Cacheable && renderingItem.Caching.Cacheable)
            {
                rendering.Caching.Cacheable = renderingItem.Caching.Cacheable;
                rendering.Caching.VaryByData = renderingItem.Caching.VaryByData;
                rendering.Caching.VaryByDevice = renderingItem.Caching.VaryByDevice;
                rendering.Caching.VaryByLogin = renderingItem.Caching.VaryByLogin;
                rendering.Caching.VaryByParameters = renderingItem.Caching.VaryByParm;
                rendering.Caching.VaryByQueryString = renderingItem.Caching.VaryByQueryString;
                rendering.Caching.VaryByUser = renderingItem.Caching.VaryByUser;

                // BUG: Sitecore does not seem to provide a way to set the
                // BUG: Clear on index update flag of the MVC rendering.
                rendering[XmlBasedRenderingParserWithFullCacheSupport.ClearOnIndexUpdatePropertyName] =
                    renderingItem.Caching.ClearOnIndexUpdate.ToBoolString();

                //Custom caching properties.
                rendering[XmlBasedRenderingParserWithFullCacheSupport.VaryByContextItemPropertyName] =
                    rendering.RenderingItem.InnerItem[XmlBasedRenderingParserWithFullCacheSupport.VaryByContextItemPropertyName].ToStringOrEmpty();
            }

            return rendering;
        }
        #endregion
    }
}
