﻿using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using Qic.Sc.Entities.Metadata;

namespace Qic.Sc.Platform.ContentSearch.ComputedFields
{
    /// <summary>
    /// The QIC Security Field to check if users have access to specific reports
    /// </summary>
    public class SecurityComputedField : IComputedIndexField
    {
        /// <summary>
        /// Sitecore Field Name of Index
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Sitecore Return Type
        /// </summary>
        public string ReturnType { get; set; }

        /// <summary>
        /// Gets the Security Value to check if user is supposed to have access to this
        /// </summary>
        /// <param name="indexable">The Index information</param>
        /// <returns>The entity code should see everything</returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;
            if (item == null)
            {
                return null;
            }

            return item.TemplateID == IDocumentConstants.TemplateId ? string.Join(" ", item.GetSecurityCode()) : null;
        }
    }
}
