﻿namespace Qic.Sc.Platform.Pipelines.HttpRequest
{
    using System.Net;
    using System.Web;

    using Autofac;

    using Qic.Sc.Interfaces.Core;

    using Sitecore;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Links;
    using Sitecore.Pipelines.HttpRequest;
    using Sitecore.StringExtensions;
    using Sitecore.Web;

    /// <summary>
    /// Custom execute request processor to enable site level 404 pages.
    /// </summary>
    public class ExecuteRequest : Sitecore.Pipelines.HttpRequest.ExecuteRequest
    {
        private readonly ILoggerService loggerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ExecuteRequest"/> class.
        /// </summary>
        public ExecuteRequest()
        {
            this.loggerService = ServiceLocator.ResolvePlatformLoggerService();
        }

        /// <summary>
        /// Perform a re-direct on item not found. This implementation
        /// will try to redirect to a site specific item if one is configured.
        /// Otherwise redirect will be to the provided URL.
        /// </summary>
        /// <param name="url">
        /// Configured, platform wide redirect location.
        /// </param>
        protected override void RedirectOnItemNotFound(string url)
        {
            if (!this.SiteSpecificNotFoundBehaviour())
            {
                // Revert to default behavior
                base.RedirectOnItemNotFound(url);
            }
        }

        /// <summary>
        /// Perform a re-direct on layout not found. This implementation
        /// will try to redirect to a site specific item if one is configured.
        /// Otherwise redirect will be to the provided URL.
        /// </summary>
        /// <param name="url">
        /// Configured, platform wide redirect location.
        /// </param>
        protected override void RedirectOnLayoutNotFound(string url)
        {
            if (!this.SiteSpecificNotFoundBehaviour())
            {
                // Revert to default behavior
                base.RedirectOnLayoutNotFound(url);
            }
        }

        private bool SiteSpecificNotFoundBehaviour()
        {
            // If site has a registered permanent redirect service try a redirect
            // first. Otherwise processes as a 404
            if (this.RedirectToSiteSpecific301())
            {
                return true; // 301 Redirect
            }

            HttpContext.Current.SetSitecoreResponseStatusCode(HttpStatusCode.NotFound);
            if (this.RedirectToSiteSpecific404())
            {
                return true; // 404 rewrite
            }

            return false;
        }

        #region 404

        /// <summary>
        /// Resolve Sitecore item that is configured as a custom not found (404) item.
        /// </summary>
        /// <returns>
        /// An item if its configured and actually exist, null otherwise.
        /// </returns>
        private Item GetSiteSpecificNotFoundItem()
        {
            var site = Context.Site;
            if (site == null)
            {
                return null;
            }

            var siteService = ServiceLocator.BeginScopeForSite(site.Name).Resolve<ISiteService>();
            var notFoundItemSetting = siteService.Get404Item();
            if (string.IsNullOrEmpty(notFoundItemSetting))
            {
                return null;
            }

            if (ID.IsID(notFoundItemSetting) || notFoundItemSetting.StartsWith("/sitecore/content"))
            {
                return site.Database.GetItem(notFoundItemSetting);
            }
            return site.Database.GetItem("{0}/{1}".FormatWith(site.ContentStartPath, notFoundItemSetting));
        }

        /// <summary>
        /// Perform a 404 redirect
        /// </summary>
        /// <returns>
        /// True if redirect was enacted, false otherwise.
        /// </returns>
        private bool RedirectToSiteSpecific404()
        {
            var item = this.GetSiteSpecificNotFoundItem();
            if (item == null)
            {
                return false;
            }

            this.loggerService.Warn(
                string.Format(
                    "404: Site '{2}', RawUrl '{0}', RewrtieTo '{1}'.",
                    HttpContext.Current.Request.RawUrl,
                    LinkManager.GetItemUrl(item),
                    Context.Site == null ? "unknown" : Context.Site.Name));

            // Set up new context
            Context.Item = item;
            WebUtil.RewriteUrl(LinkManager.GetItemUrl(item));

            // Run pipeline again
            var args = new HttpRequestArgs(HttpContext.Current, HttpRequestType.Begin);
            Sitecore.Pipelines.CorePipeline.Run("httpRequestBegin", args);
            return true;
        }

        #endregion

        #region 301 Redirect Implementation

        private bool RedirectToSiteSpecific301()
        {
            var site = Context.Site;
            if (site == null)
            {
                this.loggerService.Warn(
                    "301 processor can't detect current site, "
                    + "ensure processor is configured correctly. Processor should "
                    + "run (be registered) after sitecore site information is resolved.");

                // Not enough context, possible misconfiguration.
                // Fail gracefully.
                return false;
            }

            var siteRedirectServiceName = site.Properties["redirectServiceName"];
            if (string.IsNullOrEmpty(siteRedirectServiceName))
            {
                // Current site does not define a redirect service.
                // No redirect rules will be checked. Skipping.
                return false;
            }

            using (var scope = ServiceLocator.BeginScopeForCurrentSite())
            {
                if (!scope.IsRegisteredWithName<IRedirecService>(siteRedirectServiceName))
                {
                    this.loggerService.Warn(
                        string.Format(
                            "301 processor is unable to resolve redirect service '{1}' for site '{0}'."
                            + " Check configuration and ensure the service with the specified name is registered "
                            + " with the dependency injection infrastructure.",
                            site.Name,
                            siteRedirectServiceName));

                    // A redirect service is registered, but could not be located.
                    // Possible configuration or implementation error. Fail gracefully.
                    return false;
                }

                var redirectService = scope.ResolveNamed<IRedirecService>(siteRedirectServiceName);
                var redirectTo = redirectService.PermanentRedirectFor(HttpContext.Current.Request.Url);
                if (string.IsNullOrEmpty(redirectTo))
                {
                    // Current URL does not mat any rule that require a redirect.
                    // No redirect is required.
                    return false;
                }

                // Perform a 301 redirect. To a URL returned by the redirect service.
                var response = HttpContext.Current.Response;
                response.StatusCode = 301;
                response.Status = "301 Moved Permanently";
                response.RedirectLocation = redirectTo;
                response.End();

                return true;
            }
        }

        #endregion
    }
}
