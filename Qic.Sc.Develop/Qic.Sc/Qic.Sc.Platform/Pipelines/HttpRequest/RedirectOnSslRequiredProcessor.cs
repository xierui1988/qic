﻿namespace Qic.Sc.Platform.Pipelines.HttpRequest
{
    using System;

    using Autofac;

    using Qic.Sc.Interfaces.Core;

    using Sitecore;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.Mvc.Extensions;
    using Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// Processor to implement the SSL
    /// Reference :  http://laubplusco.net/https-in-sitecore/
    /// </summary>
    public class RedirectOnSslRequiredProcessor : HttpRequestProcessor
    {
        private const string RequiresSecureTransportFieldName = "Requires Secure Transport";
        private readonly ISiteService siteService;
        private readonly IConfigurationService configurationService;

        /// <summary>
        /// check on a custom Sitecore setting if https should be used or not
        /// </summary>
        /// <returns>return true or false value</returns>
        public bool IsSecuredConnectionCheckingEnabled()
        {
            return this.configurationService.GetGlobalSetting("TransportLayerSecurity.Enable", "true").ToBool();
        }

        /// <summary>
        /// RequiresTransportSecurityService used to check whether or not this request requires https.
        /// </summary>
        /// <param name="item">Sitecore Item</param>
        /// <returns>return true or false</returns>
        public bool IsTransportSecurityRequiredByItem(Item item)
        {
            if (item != null && item.Fields[RequiresSecureTransportFieldName] != null)
            {
                return new CheckboxField(item.Fields[RequiresSecureTransportFieldName]).Checked;
            }
            return false;
        }

        /// <summary>
        /// Is revert from secured to canonical URL enabled for the site.
        /// Default is false, must be explicitly configured at a site level.
        /// </summary>
        /// <returns>True is revert to canonical is enabled, false other wise (default).</returns>
        private bool IsRevertSecuredToCanonicalUrlEnabled()
        {
            return
                this.configurationService.GetSiteSetting("TransportLayerSecurity.RevertToCanonical", @default: "false")
                    .ToBool();
        }

        /// <summary>
        /// Change To Secure Scheme
        /// </summary>
        /// <param name="uri">Uri as string to change</param>
        /// <returns>return updated Uri</returns>
        public string ChangeToSecuredUrl(string uri)
        {
            // Check whether we have a secure hostname configured (e.g. secure.somedomain.com.au)
            var secureHostName = this.configurationService.GetSiteSetting(
                "TransportLayerSecurity.SecureHostName",
                @default: null);

            if (string.IsNullOrEmpty(secureHostName))
            {
                // Simply flip http to https, the rest of the url stays the same.
                var secureUri = uri.Substring(4);
                return string.Concat(Uri.UriSchemeHttps, secureUri);
            }

            // Change both the scheme and the host name.
            return
                new UriBuilder(uri) { Host = secureHostName, Scheme = Uri.UriSchemeHttps, Port = -1 /* force default port */ }
                    .Uri.AbsoluteUri;
        }

        /// <summary>
        /// Reverts current request URL to unsecured scheme (http) and site configured canonical URL
        /// if one is configured.
        /// </summary>
        /// <param name="url">URL of the request.</param>
        /// <param name="canonicalBaseUrl">Canonical base URL.</param>
        /// <returns>A canonical URL that for the passed in URL.</returns>
        private string ChangeToCannonicalUrl(string url, string canonicalBaseUrl)
        {
            if (string.IsNullOrEmpty(canonicalBaseUrl))
            {
                // Simply flip https to http, the rest of the url stays the same.
                var secureUri = url.Substring(4);
                return string.Concat(Uri.UriSchemeHttps, secureUri);
            }

            // Change back to canonical site URL
            var canonicalUri = new Uri(canonicalBaseUrl);
            return
                new UriBuilder(url)
                {
                    Host = canonicalUri.Host,
                    Scheme = canonicalUri.Scheme,
                    Port = canonicalUri.IsDefaultPort ? -1 /* force default port */ : canonicalUri.Port
                }.Uri.AbsoluteUri;
        }

        /// <summary>
        /// RedirectOnSslRequicedProcessor Constructor
        /// </summary>
        public RedirectOnSslRequiredProcessor()
            : this(
                ServiceLocator.BeginScopeForCurrentSite().Resolve<IConfigurationService>(),
                ServiceLocator.BeginScopeForCurrentSite().Resolve<ISiteService>())
        {
        }

        /// <summary>
        /// RedirectOnSslRequicedProcessor Constructor 
        /// </summary>
        /// <param name="configurationService">configurationService interface</param>
        /// <param name="siteService">site service interface</param>
        private RedirectOnSslRequiredProcessor(IConfigurationService configurationService, ISiteService siteService)
        {
            this.siteService = siteService;
            if (configurationService == null)
            {
                throw new ArgumentNullException("configurationService");
            }
            if (siteService == null)
            {
                throw new ArgumentNullException("siteService");
            }
            this.configurationService = configurationService;
        }

        /// <summary>
        /// httpRequestBegin pipeline to check if  the current request 
        /// should use https and if it is not then change the scheme of 
        /// the current url and redirect.
        /// </summary>
        /// <param name="args">Argument parameter to check the request</param>
        public override void Process(HttpRequestArgs args)
        {
            // If we have no item nothing to secure
            // OR
            // Secured connection checking is disabled.
            // OR
            // Ignore any URL that contains "/sitecore", the intent is to ignore sitecore OOTB pages,
            // but in fact will ignore any URL that contains that string.
            if (Context.Item == null || !this.IsSecuredConnectionCheckingEnabled() || args.Context.Request.Url.OriginalString.Contains("/sitecore"))
            {
                return; // Ignored
            }

            var isSecureConnection = args.Context.Request.IsSecureConnection;
            var isSecureConnectionRequired = this.IsTransportSecurityRequiredByItem(Context.Item);

            if (isSecureConnection && isSecureConnectionRequired)
            {
                return; // Secured and security is required by the item, ignored.
            }

            if (isSecureConnection && this.IsRevertSecuredToCanonicalUrlEnabled())
            {
                args.Context.Response.Redirect(
                    this.ChangeToCannonicalUrl(args.Context.Request.Url.AbsoluteUri, this.siteService.GetTargetHostName()),
                    true);

                return; // Secured, but security is not required by the item and revert to canonical Url is enabled.
            }

            if (!isSecureConnection && isSecureConnectionRequired)
            {
                args.Context.Response.Redirect(this.ChangeToSecuredUrl(args.Context.Request.Url.AbsoluteUri), true);
                return; // Unsecured, but security is requested by the item, redirect to secure Url.
            }

            // Ignored
        }
    }
}
