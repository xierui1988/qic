﻿using Sitecore.Data.Items;
using Sitecore.Pipelines.HttpRequest;
using Sitecore.Sites;
using Sitecore.Web;
using System.Web;

namespace Qic.Sc.Platform.Pipelines.HttpRequest
{
    /// <summary>
    /// This class is used to Handle Login requests
    /// </summary>
    public class PortalSecurityResolver : HttpRequestProcessor
    {
        /// <summary>
        /// The Http Request Processor is called after Layout Resolver and work out if User has access to the site Client Portal
        /// </summary>
        /// <param name="args">The request processor args</param>
        public override void Process(HttpRequestArgs args)
        {
            // Get the site context 
            if (!Entities.Constants.Entities.Content.ClientPortal.Home.Path.StartsWith(args.StartPath))
            {
                return;
            }

            if (Sitecore.Context.Item == null)
            {
                return;
            }

            // Checks if the user can enter the site and is not anonymous, if they are redirect to login page
            if (!SiteManager.CanEnter(Sitecore.Context.Site.Name, Sitecore.Context.User) || Sitecore.Context.User.Name == string.Format("{0}\\{1}", Sitecore.Context.Site.Domain, Entities.UserDefined.Constants.Anonymous))
            {
                Item contextItem = Sitecore.Context.Item;

                // Ensuring Login page, Password Change, and Password reset pages are not redirected
                if (contextItem.ID == Entities.Constants.Entities.Content.ClientPortal.Home.Login.ItemID ||
                    contextItem.ID == Entities.Constants.Entities.Content.ClientPortal.Home.Login.Reset.ItemID ||
                    contextItem.ID == Entities.Constants.Entities.Content.ClientPortal.Home.Login.RequestReset.ItemID)
                {
                    return;
                }

                // Adding a return url, in case someone is asked to login
                WebUtil.Redirect(string.Format("{0}?returnUrl={1}", Sitecore.Context.Site.LoginPage, HttpContext.Current.Server.HtmlEncode(HttpContext.Current.Request.RawUrl)));
            }
        }
    }
}
