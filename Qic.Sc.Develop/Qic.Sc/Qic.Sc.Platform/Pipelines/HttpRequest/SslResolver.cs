﻿namespace Qic.Sc.Platform.Pipelines.HttpRequest
{
    using System;
    using Qic.Sc.Services.Core;
    using Sitecore.Pipelines.HttpRequest;

    /// <summary>
    /// The HTTP pipeline processor that detect the request if this should be secure and redirect to secure page if required.
    /// </summary>
    public class SslResolver : HttpRequestProcessor
    {
        /// <summary>
        /// Check if the current request should be secure https and then redirect to https if required.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public override void Process(HttpRequestArgs args)
        {
            if (Sitecore.Context.Item == null || Sitecore.Context.Site == null)
            {
                return;
            }

            if (!IsSecurePage())
            {
                return;
            }

            if (!args.Context.Request.IsSecureConnection)
            {
                args.Context.Response.Redirect(ChangeToSecureUri(args.Context.Request.Url.AbsoluteUri));
            }
        }

        private static string ChangeToSecureUri(string absoluteUri)
        {
            var secureUri = absoluteUri.Substring(4);
            return string.Concat(Uri.UriSchemeHttps, secureUri);
        }

        private static bool IsSecurePage()
        {
            return new SiteService().IsSslRequired();
        }
    }
}
