﻿namespace Qic.Sc.Platform.Pipelines.Publish
{
    using System;

    using Qic.Sc.Entities;

    /// <summary>
    /// The document publish options.
    /// </summary>
    public class DocumentPublishResult
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Gets or sets the action.
        /// </summary>
        public SitecoreUserAction Action { get; set; }

        /// <summary>
        /// Gets or sets the document name.
        /// </summary>
        public string DocumentName { get; set; }

        /// <summary>
        /// Gets or sets the document clients.
        /// </summary>
        public string DocumentClients { get; set; }

        /// <summary>
        /// Gets or sets the document path.
        /// </summary>
        public string DocumentPath { get; set; }
    }
}
