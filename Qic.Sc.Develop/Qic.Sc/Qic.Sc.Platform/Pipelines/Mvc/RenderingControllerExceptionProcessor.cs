﻿namespace Qic.Sc.Platform.Pipelines.Mvc
{
    using System;

    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Services.Core;

    using Sitecore.Mvc.Pipelines.MvcEvents.Exception;

    /// <summary>
    /// Custom error handling for rendering controllers. Re-enables MVC error handling for controllers
    /// </summary>
    public class RenderingControllerExceptionProcessor : ExceptionProcessor
    {
        /// <summary>
        /// Gets the logging service.
        /// </summary>
        public ILoggerService LoggerService { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingControllerExceptionProcessor"/> class.
        /// </summary>
        public RenderingControllerExceptionProcessor()
            : this(new LoggerService(typeof(RenderingControllerExceptionProcessor)))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingControllerExceptionProcessor"/> class.
        /// </summary>
        /// <param name="loggerService">
        /// The logger service.
        /// </param>
        private RenderingControllerExceptionProcessor(ILoggerService loggerService)
        {
            this.LoggerService = loggerService;
        }

        /// <summary>
        /// Sitecore pipeline entry point. Class must be registered by adding pipeline
        /// configuration. Intended to be used as a part of mvc.exception pipeline.
        /// NOTE: Register as early as possible, definitely before the default handler.
        /// </summary>
        /// <param name="args">
        /// Pipeline arguments.
        /// </param>
        public override void Process(ExceptionArgs args)
        {
            if (args == null || args.ExceptionContext == null)
            {
                this.LoggerService.Error(
                    "An unhanded exception trapped by the pipeline, but there is not enough context to process it.",
                    null);

                return;
            }

            var renderingController = args.ExceptionContext.Controller as IRenderingController;
            if (renderingController == null)
            {
                return; // Not a rendering controller
            }

            try
            {
                renderingController.OnRenderingException(args.ExceptionContext);
            }
            catch (Exception ex)
            {
                this.LoggerService.Error("An exception during error processing.", ex);
            }

            if (args.ExceptionContext.ExceptionHandled)
            {
                args.AbortPipeline(); // Exception handled successfully
            }

            // Give up, let the rest of the pipeline handle the error.
        }
    }
}
