﻿namespace Qic.Sc.Platform.Pipelines.Mvc
{
    using Qic.Sc.Platform.Mvc;

    using Sitecore.Diagnostics;
    using Sitecore.Mvc.Extensions;
    using Sitecore.Mvc.Pipelines.Response.RenderRendering;
    using Sitecore.Mvc.Presentation;

    /// <summary>
    /// The generate custom cache key.
    /// </summary>
    public class GenerateCustomCacheKey : RenderRenderingProcessor
    {
        /// <summary>
        /// Called by the pipeline.
        /// </summary>
        /// <param name="args">
        /// Context arguments.
        /// </param>
        public override void Process(RenderRenderingArgs args)
        {
            Assert.ArgumentNotNull(args, "args");
            if (args.Rendered)
            {
                return;
            }

            if (!args.Cacheable || args.CacheKey.IsEmptyOrNull())
            {
                return;
            }

            args.CacheKey += this.GenerateKey(args.Rendering, args);
        }

        private string GenerateKey(Rendering rendering, RenderRenderingArgs args)
        {
            var key = string.Empty;

            // Context item is the item being rendered (the page). Sitecore default VaryByData cache flag
            // is not alway sufficient. For example a menu rendering that has a custom data source but needs
            // to be cached once per page to ensure currently selected item works as expected. Setting this
            // flag allows caching by both DataSource and CurrentItem.
            if (rendering[XmlBasedRenderingParserWithFullCacheSupport.VaryByContextItemPropertyName].ToBool())
            {
                key += "_#:contextPath:" + args.PageContext.Item.ID;
            }

            // Cache item by URL path. Useful for caching '*' items.
            if (rendering[XmlBasedRenderingParserWithFullCacheSupport.VaryByUrlPathPropertyName].ToBool())
            {
                key += "_#:urlPath:" + args.PageContext.RequestContext.HttpContext.Request.Path;
            }

            return key;
        }
    }
}
