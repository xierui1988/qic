﻿using Qic.Sc.Interfaces.Components;
using Qic.Sc.Interfaces.Core;
using Sitecore;
using Sitecore.Data;
using Sitecore.Web.UI.HtmlControls;
using System.Web.UI;
using Autofac;

using Qic.Sc.Entities;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Qic.Sc.Services.Core;
using System.Linq;
using System.Collections.Generic;

namespace Qic.Sc.Platform.Workflow.Controls
{
    /// <summary>
    /// The Report Filter renders the Report Entities in a drop down, that should be fetched from Client Portal site.
    /// </summary>
    public class ReportNameFilter : BaseDropDown
    {
        /// <summary>
        /// This method is used to set the Filter label and the Report ID label
        /// </summary>
        public ReportNameFilter()
        {
            this.Label = Entities.Constants.ReportNameDisplay;
            this.PropertyIDName = Entities.Constants.ReportNameRegistry;
        }

        /// <summary>
        /// Its responsible for providing the data to Render the Report Filter
        /// The Core Item that triggers this is currently located at: /sitecore/content/Applications/Workbox/Ribbon/Home/QIC Filters/Report Name Filter
        /// </summary>
        /// <param name="output">The provided HTML Text Write Output</param>
        protected override void GenerateInsertOptions(HtmlTextWriter output)
        {
            // Get Selected Value
            string selectedReport = this.GetSelectionValue(Entities.Constants.ReportNameRegistry);

            // All
            output.AddInsertOptionHelper(Entities.Constants.AllDefaultValue, Entities.Constants.AllDefaultValue, selectedReport == Entities.Constants.AllDefaultValue);

            // Populate all other Report Name Values
            using (var scope = ServiceLocator.BeginScopeFor(Entities.Constants.MasterDb))
            {
                var contentRepository = scope.Resolve<IContentRepository>();

                using (var search = contentRepository.GetSearchService())
                {
                    // Get the list of Account Names and codes = Key pair
                    var options = (from doc in search.QueryForWorkflowDocuments().ToArray()
                                   where !string.IsNullOrEmpty(doc.ReportCode) && !string.IsNullOrEmpty(doc.ReportName)
                                   group doc by doc.ReportCode into grp
                                   select new { Key = grp.Key, Value = grp.Max(d => d.ReportName) }).ToArray();

                    // Now listing them
                    foreach (var report in options)
                    {
                        output.AddInsertOptionHelper(report.Value, report.Key, selectedReport == report.Key);
                    }
                }
            }
        }
    }
}
