﻿namespace Qic.Sc.Platform.Workflow.Action
{
    using Qic.Sc.Entities;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;

    using Sitecore.Data.Items;
    using Sitecore.SecurityModel;
    using Sitecore.Web;
    using Sitecore.Web.UI.Sheer;
    using Sitecore.Workflows.Simple;

    /// <summary>
    /// The set notification flag of a document.
    /// </summary>
    public class SetNotificationFlag
    {
        /// <summary>
        /// Invoked by sitecore as a part of a workflow.
        /// </summary>
        public void Process(WorkflowPipelineArgs args)
        {
            var item = args.DataItem;
            if (item == null)
            {
                SheerResponse.Alert("No Item was provided in the workflow command. Aborting operation.");
                return;
            }

            using (new SecurityDisabler())
            {
                var notificationFiled = item.Fields[IDocumentConstants.IsNotificationRequiredFieldId];
                if (notificationFiled == null)
                {
                    // The item is not a document.
                    return;
                }

                var processorItem = args.ProcessorItem.InnerItem;
                var parameters = WebUtil.ParseUrlParameters(processorItem[Constants.WorkflowActionParameters]);
                var value = parameters[Constants.ParameterNameAction] ?? "0"; // Default to clear

                if (notificationFiled.Value == value)
                {
                    // No change
                    return;
                }

                using (new EditContext(item))
                {
                    notificationFiled.Value = value;
                }
            }
        }
    }
}
