﻿using Qic.Sc.Entities;
using Sitecore.Data.Items;
using Sitecore.Workflows.Simple;
using System;

using Autofac;
using Qic.Sc.Interfaces.Components;
using Qic.Sc.Entities.Metadata;
using System.Collections.Specialized;
using Sitecore.Web;
using Sitecore.Data;
using Sitecore.Web.UI.Sheer;
using Sitecore.Diagnostics;

namespace Qic.Sc.Platform.Workflow.Action
{
    /// <summary>
    /// This class is a generic class to record extra audit information
    /// </summary>
    public class AuditInformation
    {
        /// <summary>
        /// Sitecore Action Item triggers this method.
        /// </summary>
        /// <param name="args">The Workflow Arguments of the item in workflow</param>
        public void Process(WorkflowPipelineArgs args)
        {
            Item item = args.DataItem;
            if (item == null)
            {
                SheerResponse.Alert("No Item was provided in the workflow command. Aborting operation.");
                return;
            }

            try
            {
                var processorItem = args.ProcessorItem.InnerItem;
                NameValueCollection parameters = WebUtil.ParseUrlParameters(processorItem[Constants.WorkflowActionParameters]);

                SitecoreUserAction userAction;
                string value = parameters[Constants.ParameterNameAction];
                bool successfullyParsed = Enum.TryParse(value, true, out userAction);

                Database db = item.Database;

                if (!successfullyParsed)
                {
                    SheerResponse.Alert(string.Format("There was no recognised AuditAction in {0} paramater.", Constants.ParameterNameAction));
                    return;
                }

                using (var scope = ServiceLocator.BeginScopeFor(Constants.MasterDb))
                {
                    var audit = scope.Resolve<IAuditService>();

                    audit.Audit(new AuditMetadata()
                        {
                            UserName = Sitecore.Context.User.Profile.UserName,
                            Action = EnumUtils<SitecoreUserAction>.GetDescription(userAction),
                            Db = item.Database.Name,
                            Path = item.Paths.FullPath,
                            Language = item.Language.Name,
                            Version = item.Version.Number,
                            Id = item.ID.ToString(),
                            Miscellaneous = item.GetAccountNamesFromDocumentItem()
                        });
                }
            }
            catch (Exception e)
            {
                Log.Error("Audit Information Action threw an error whilst trying to create record an audit.", e, this);
                SheerResponse.Alert("Could not capture extra Auditing information.");
            }
        }
    }
}
