﻿using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.SecurityModel;
using Sitecore.Web.UI.Sheer;
using Sitecore.Workflows.Simple;
using System;

namespace Qic.Sc.Platform.Workflow.Action
{
    /// <summary>
    /// This class is meant to be triggered when an Item has entered the Publish state (Final state) of the workflow
    /// </summary>
    public class UpdatePublishDate
    {
        /// <summary>
        /// Sitecore Action Item triggers this method.
        /// </summary>
        /// <param name="args">The Workflow Arguments of the item in workflow</param>
        public void Process(WorkflowPipelineArgs args)
        {
            Item item = args.DataItem;
            if (item == null)
            {
                SheerResponse.Alert("No Item was provided in the workflow command Update Publish Date. Aborting operation.");
                return;
            }

            try
            {
                using (new SecurityDisabler())
                {
                    using (new EditContext(item))
                    {
                        item[IDocumentConstants.PublishedDateFieldId] = Sitecore.DateUtil.ToIsoDate(DateTime.Now);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(string.Format("There was a problem in trying to update the {0} field on item name {1} with ID {2}", IDocumentConstants.PublishedDateFieldName, item.Name, item.ID.ToString()), e, this);
                SheerResponse.Alert(string.Format("Could not update the {0}", IDocumentConstants.PublishedDateFieldName));
            }
        }
    }
}
