﻿using Sitecore.Workflows.Simple;

namespace Qic.Sc.Platform.Workflow.Action
{
    using Qic.Sc.Platform.Commands;

    /// <summary>
    /// This class is meant to be triggered when an Item has entered the Publish state (Final state) of the workflow
    /// </summary>
    public class PublishDocument
    {
        /// <summary>
        /// Sitecore Action Item triggers this method.
        /// </summary>
        /// <param name="args">The Workflow Arguments of the item in workflow</param>
        public void Process(WorkflowPipelineArgs args)
        {
            if (args == null)
            {
                return;
            }

            if (args.Parameters.Length > 0 && args.Parameters[0] as ExtendedWorkboxForm != null)
            {
                // Check if called in the context of our custom work box,
                // for performance reasons work box publishes items in bulk.
                // Do not trigger a single item publish in that case.
                return;
            }

            DocumentPublisher.PublishDocuments(new[] { args.DataItem });
        }
    }
}
