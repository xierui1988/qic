﻿namespace Qic.Sc.Platform.Mvc
{
    using System.Web.Mvc;

    using Sitecore.Mvc.Controllers;
    using Sitecore.Mvc.Presentation;

    /// <summary>
    /// Extends Sitecore to be able to correctly render Views for Areas without specifying the full context.
    /// </summary>
    public class AreaControllerRunner : ControllerRunner
    {
        private const string ControllerKey = "controller";
        private const string ActionKey = "action";
        private const string AreaKey = "area";
        private const string NamespacesKey = "Namespaces";
        private const string UseNamespaceFallbackKey = "UseNamespaceFallback";

        /// <summary>
        /// Initializes a new instance of the <see cref="AreaControllerRunner"/> class.
        /// </summary>
        /// <param name="controllerName">The name of the controller.</param>
        /// <param name="actionName">The name of the action.</param>
        /// <param name="areaName">The name of the area.</param>
        /// <param name="nameSpace">The name space of the are controllers.</param>
        public AreaControllerRunner(string controllerName, string actionName, string areaName, string nameSpace)
            : base(controllerName, actionName)
        {
            this.AreaName = areaName;
            this.ControllerName = controllerName;
            this.ActionName = actionName;
            this.Namespace = nameSpace;
        }

        /// <summary>
        /// Gets or sets the name of the area.
        /// </summary>
        public string AreaName { get; set; }

        /// <summary>
        /// Gets or sets the namespace for the area.
        /// </summary>
        public string Namespace { get; set; }

        /// <summary>
        /// Gets the controller appropriate for a given MVC area. Restores normal MVC behavior
        /// inside Sitecore.
        /// </summary>
        /// <returns>The controller.</returns>
        protected override Controller GetController()
        {
            var requestContext = PageContext.Current.RequestContext;

            // Save global state
            var area = requestContext.RouteData.DataTokens[AreaKey];
            var namespaces = requestContext.RouteData.DataTokens[NamespacesKey];
            var fallback = requestContext.RouteData.DataTokens[UseNamespaceFallbackKey];

            try
            {
                // Set transient state
                requestContext.RouteData.DataTokens[AreaKey] = this.AreaName;
                requestContext.RouteData.DataTokens[NamespacesKey] = new[] { this.Namespace };
                requestContext.RouteData.DataTokens[UseNamespaceFallbackKey] = false;

                return base.GetController();
            }
            finally
            {
                // Restore global state
                requestContext.RouteData.DataTokens[AreaKey] = area;
                requestContext.RouteData.DataTokens[NamespacesKey] = namespaces;
                requestContext.RouteData.DataTokens[UseNamespaceFallbackKey] = fallback;
            }
        }

        /// <summary>
        /// Executes a given controller with the Sitecore context.
        /// </summary>
        /// <param name="controllerInstance">The controller to execute.</param>
        protected override void ExecuteController(Controller controllerInstance)
        {
            // Save global state
            var requestContext = PageContext.Current.RequestContext;
            var controller = requestContext.RouteData.Values[ControllerKey];
            var action = requestContext.RouteData.Values[ActionKey];
            var area = requestContext.RouteData.DataTokens[AreaKey];

            try
            {
                // Set transient state
                requestContext.RouteData.Values[ControllerKey] = this.ActualControllerName;
                requestContext.RouteData.Values[ActionKey] = this.ActionName;
                requestContext.RouteData.DataTokens[AreaKey] = this.AreaName;

                ((IController)controllerInstance).Execute(PageContext.Current.RequestContext);
            }
            finally
            {
                // Restore global state
                requestContext.RouteData.Values[ControllerKey] = controller;
                requestContext.RouteData.Values[ActionKey] = action;
                requestContext.RouteData.DataTokens[AreaKey] = area;
            }
        }
    }
}
