﻿namespace Qic.Sc.Platform.Mvc
{
    using System;
    using System.Web.WebPages;
    using System.Xml.Linq;

    using Sitecore.Mvc.Extensions;
    using Sitecore.Mvc.Presentation;

    /// <summary>
    /// The xml based rendering parser with full cache support.
    /// </summary>
    public class XmlBasedRenderingParserWithFullCacheSupport : XmlBasedRenderingParser
    {
        public const string ClearOnIndexUpdatePropertyName = "Cache_ClearOnIndexUpdate";
        public const string VaryByContextItemPropertyName = "CustomCache_VaryByContextItem";
        public const string VaryByUrlPathPropertyName = "CustomCache_VaryByUrlPath";
        public const string TimeoutPropertyName = "CustomCache_Timeout";

        /// <summary>
        /// Extension to the OOTB implementation of the rendering parser.
        /// Adds support for missing cache modifiers.
        /// </summary>
        /// <param name="node">
        /// Layout xml.
        /// </param>
        /// <param name="parseChildNodes">
        /// The parse child nodes.
        /// </param>
        /// <returns>
        /// The <see cref="Rendering"/>.
        /// </returns>
        public override Rendering Parse(XElement node, bool parseChildNodes)
        {
            var rendering = base.Parse(node, parseChildNodes);
            if (rendering[ClearOnIndexUpdatePropertyName].IsEmptyOrNull())
            {
                // The correct attribute was found by decompiling Sitecore.Layouts.RenderingDefinition
                rendering[ClearOnIndexUpdatePropertyName] = node.GetAttributeValueOrNull("ciu");
            }

            return rendering;
        }

        /// <summary>
        /// The add rendering item properties. Extends base with support for ClearOnIndexUpdate.
        /// </summary>
        /// <param name="rendering">
        /// The rendering.
        /// </param>
        protected override void AddRenderingItemProperties(Rendering rendering)
        {
            var origianlCacheFlag = rendering.Caching.Cacheable;
            base.AddRenderingItemProperties(rendering);

            if (origianlCacheFlag != rendering.Caching.Cacheable)
            {
                //// BUG FIX: A work around a partial implementation of the cache support
                //// BUG FIX: by Sitecore 7.2 to add support for ClearOnIndexUpdate cache
                //// BUG FIX: flag. Should be removed once fixed by Sitecore.

                // If the flag changed item level caching is been applied
                // Sitecore 7.2 was missing a check for ClearOnIndexUpdate
                // so add it in.
                if (rendering.RenderingItem.Caching.ClearOnIndexUpdate)
                {
                    rendering[ClearOnIndexUpdatePropertyName] = true.ToBoolString();
                }
            }

            // NOTE: Add custom cache properties to the MVC rendering. Custom propertied
            // NOTE: can only be set at a rendering not layout level. There is no way to
            // NOTE: overwrite them at layout level either. If set they always take affect.
            if (rendering.Caching.Cacheable)
            {
                // Propagate context time cache flag from underlying rendering item to MVC rendering
                // object. Used to set custom cache keys from a pipeline.
                rendering[VaryByContextItemPropertyName] =
                    rendering.RenderingItem.InnerItem[VaryByContextItemPropertyName].ToStringOrEmpty();

                // Propagate context time cache flag from underlying rendering item to MVC rendering
                // object. Used to set custom cache keys from a pipeline.
                rendering[VaryByUrlPathPropertyName] =
                    rendering.RenderingItem.InnerItem[VaryByUrlPathPropertyName].ToStringOrEmpty();

                // Check if timeout value is set and update caching settings accordingly.
                var timeout = rendering.RenderingItem.InnerItem[TimeoutPropertyName];
                if (!timeout.IsEmptyOrNull())
                {
                    var seconds = timeout.AsInt(0);
                    if (seconds != 0)
                    {
                        rendering.Caching.Timeout = new TimeSpan(0, 0,  timeout.AsInt());
                    }
                }
            }
        }
    }
}
