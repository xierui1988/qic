﻿namespace Qic.Sc.Platform.Mvc
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using Sitecore.Mvc.Extensions;
    using Sitecore.Mvc.Presentation;

    /// <summary>
    /// Value provider factory for enabling Sitecore renderings to access values conventionally available in ASP.NET MVC.
    /// </summary>
    public class RenderingParametersValueProviderFactory : RenderingValueProviderFactoryBase
    {
        /// <summary>
        /// Gets an MVC value provider driven by a context for a given rendering.
        /// </summary>
        /// <param name="httpContext">The http context.</param>
        /// <param name="renderingContext">The rendering context.</param>
        /// <returns>The MVC value provider.</returns>
        protected override IValueProvider GetValueProvider(HttpContextBase httpContext, RenderingContext renderingContext)
        {
            // If parameters are missing we can't build a provider.
            if (renderingContext.Rendering.Parameters == null)
            {
                return null;
            }

            // Convert rendering parameters into something MVC understands.
            var parameters = renderingContext.Rendering.Parameters.ToDictionary(parameter => parameter.Key, parameter => Uri.UnescapeDataString(parameter.Value));

            // Add data source and context item
            parameters.Add("renderingDataSource", renderingContext.Rendering.DataSource.IsEmptyOrNull() ? null : renderingContext.Rendering.DataSource);
            parameters.Add("renderingContextItem", renderingContext.ContextItem.ID.Guid.ToString("D"));

            return new DictionaryValueProvider<string>(parameters, CultureInfo.CurrentCulture);
        }

        /// <summary>
        /// Registers data provider with MVC runtime.
        /// <remarks>
        /// Caller must ensure this code is called exactly once.
        /// </remarks>
        /// </summary>
        internal static void Register()
        {
            ValueProviderFactories.Factories.Add(new RenderingParametersValueProviderFactory());
        }
    }
}
