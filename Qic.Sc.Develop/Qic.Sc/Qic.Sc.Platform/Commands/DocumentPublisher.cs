﻿namespace Qic.Sc.Platform.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Platform.Pipelines.Publish;

    using Sitecore;
    using Sitecore.Data;
    using Sitecore.Data.Items;
    using Sitecore.Data.Managers;
    using Sitecore.Publishing;

    using Constants = Qic.Sc.Entities.Constants;

    internal static class DocumentPublisher
    {
        public static void PublishDocuments(IEnumerable<Item> items)
        {
            new Publisher(
                new DocumentPublishOptions(
                    Context.ContentDatabase,
                    Database.GetDatabase(Constants.WebDb),
                    PublishMode.SingleItem,
                    LanguageManager.DefaultLanguage,
                    DateTime.Now)
                {
                    RootItem = Context.ContentDatabase.GetItem(Constants.Content.ClientPortal.Documents.ItemID),
                    PublishRelatedItems = false,
                    Deep = false,
                    Documents =
                        items.ToDictionary(
                            i => i.ID.Guid,
                            i =>
                            new DocumentPublishResult
                            {
                                Id = i.ID.Guid,
                                Action = SitecoreUserAction.DocumentNotPublishedAction,
                                DocumentName = string.IsNullOrEmpty(i.DisplayName) ? i.Name : i.DisplayName,
                                DocumentPath = i.Paths.FullPath,
                                DocumentClients = i.GetAccountNamesFromDocumentItem()
                            })
                }).PublishAsync();
        }
    }
}
