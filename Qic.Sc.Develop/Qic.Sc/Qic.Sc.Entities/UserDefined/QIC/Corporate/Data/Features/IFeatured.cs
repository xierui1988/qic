﻿namespace Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Features
{
    using Glass.Mapper.Sc.Configuration;
    using Glass.Mapper.Sc.Configuration.Attributes;

    using global::System.Collections.Generic;

    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.Editorial;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;

    public partial interface IFeatures
    {
        /// <summary>
        /// Feature Entities
        /// </summary>
        [SitecoreField(IFeaturesConstants.FeaturesFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        IEnumerable<IFeature> FeatureEntities { get; set; }

        /// <summary>
        /// Feature Entities
        /// </summary>
        [SitecoreField(IFeaturesConstants.FeaturesFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        IEnumerable<IEditorial> EditorialEntities { get; set; }
    }
}
