﻿namespace Qic.Sc.Entities.UserDefined.QIC.Corporate.Pages.PropertyContentpage
{
    using Glass.Mapper.Sc.Configuration;
    using Glass.Mapper.Sc.Configuration.Attributes;

    using global::System.Collections.Generic;

    using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Features;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.Editorial;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;
    public partial class PropertyContentPageEntity
    {
        /// <summary>
        /// Gets or sets the feature entities.
        /// </summary>
        [SitecoreField(IFeaturesConstants.FeaturesFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        public IEnumerable<IFeature> FeatureEntities { get; set; }

        /// <summary>
        /// Feature Entities
        /// </summary>
        [SitecoreField(IFeaturesConstants.FeaturesFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        public virtual IEnumerable<IEditorial> EditorialEntities { get; set; }
    }
}
