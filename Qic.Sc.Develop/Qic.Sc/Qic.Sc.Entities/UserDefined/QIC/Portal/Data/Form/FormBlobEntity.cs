﻿using System.IO;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form
{
    public partial class FormBlobEntity : FormEntity, IFormBlob
    {
        /// <summary>
        /// Gets or sets the blob.
        /// </summary>
        [SitecoreField("Blob")]
        public virtual Stream Blob { get; set; }
    }
}
