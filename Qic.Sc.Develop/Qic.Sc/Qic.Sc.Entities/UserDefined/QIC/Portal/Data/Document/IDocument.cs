﻿using Glass.Mapper.Sc.Configuration.Attributes;
using System;
namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document
{
    public partial interface IDocument
    {
        /// <summary>
        /// Gets or sets a value indicating whether is favorite.
        /// </summary>
        bool IsFavorite { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is an unread recent document.
        /// </summary>
        bool IsUnreadRecent { get; set; }

        /// <summary>
        /// Gets or sets the published date
        /// </summary>
        [SitecoreField("Published Date")]
        DateTime? PublishedDate { get; set; }

        /// <summary>
        /// Get or sets the Report Type name
        /// </summary>
        string ReportTypeName { get; set; }
    }
}
