namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document
{
    using Glass.Mapper.Sc.Configuration.Attributes;

    using global::System;
    using global::System.IO;

    using Sitecore.Data.Managers;

    /// <summary>
    /// The document with file entity.
    /// </summary>
    public class DocumentBlobEntity : DocumentEntity, IDocumentBlob
    {
        /// <summary>
        /// Gets or sets the blob.
        /// </summary>
        [SitecoreField("Blob")]
        public Stream Blob { get; set; }
    }
}