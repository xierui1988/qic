﻿namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data
{
    using Glass.Mapper.Sc.Configuration.Attributes;

    using global::System.Collections.Generic;

    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.ReportGroup;

    /// <summary>
    /// The report grouping.
    /// </summary>
    public class ReportGroupingEntity
    {
        /// <summary>
        /// Gets the sub items for a menu item.
        /// </summary>
        [SitecoreChildren(IsLazy = true)]
        public IEnumerable<IReportGroup> ReportGroups { get; set; }
    }
}
