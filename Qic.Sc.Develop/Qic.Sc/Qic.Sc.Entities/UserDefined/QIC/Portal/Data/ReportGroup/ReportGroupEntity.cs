﻿using System.Collections.Generic;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.ReportGroup
{
    using Glass.Mapper.Sc.Configuration.Attributes;

    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Report;

    public partial class ReportGroupEntity
    {
        /// <summary>
        /// Gets the sub items for a menu item.
        /// </summary>
        [SitecoreChildren(IsLazy = true)]
        public virtual IEnumerable<IReport> ReportEntities { get; set; }
    }
}
