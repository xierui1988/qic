﻿using Sitecore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qic.Sc.Entities.UserDefined.QIC.Shared.Published
{
    public static partial class IPublishedConstants
    {
        /// <summary>
        /// The Published Date Field Name
        /// </summary>
        public const string PublishedDateFieldName = "Published Date";

        /// <summary>
        /// The Published Date ID
        /// </summary>
        public static readonly ID PublishedDateFieldId = new ID("25f3070b-d702-4b92-b4d7-a849bdb9165d");
    }
}
