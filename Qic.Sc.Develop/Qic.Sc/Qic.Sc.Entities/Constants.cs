﻿namespace Qic.Sc.Entities
{
    /// <summary>
    /// Constants needed for project e.g. "master", "publish"
    /// </summary>
    public partial class Constants
    {
        // Sitecore Database / Connection
        public const string MasterDb = "master";
        public const string WebDb = "web";

        // Sitecore - Other
        public const string PublishJobName = "publish";
        public const string StandardValues = "__Standard Values";
        public const string ContentPath = "/sitecore/content";
        public const string ItemIDFormat = "{{{0}}}";
        public const string WorkflowActionParameters = "parameters";
        public const string DefaultLanguage = "en";
        public const string ParameterNameAction = "Action";

        // Sitecore - User
        public const string Anonymous = "Anonymous";
        public const string CrmDomain = "crm";
        public const string CrmSyncFirstName = "FirstName";

        // Corporate - Formats
        public const string DateFormat = "dd MMM yyyy";

        // Business Entities
        public const string DocumentNameSeparator = " - ";
        public const string DocumentDateFormat = "dd MMM yyyy";
        public const string UploadedDateFormat = "hh:mm dd MMM yyyy";

        // Business
        public const string UserReportAccessCookieName = "UserReportAccess";
        public const string ClientPortalSiteName = "clientportal.qic.com.au";
        public const string CorporateSiteName = "www.qic.com.au";

        // EmailTokens
        public const string ResetToken = "resettoken";

        public const string FirstNameToken = "firstname";
        public const string ClientNameToken = "clientname";
        public const string ReportNameToken = "reportname";
        public const string ReportDateToken = "reportdate";
        public const string DocumentLinkToken = "documentlink";
        public const string ProductNameToken = "productname";
        public const string DocumentNameToken = "documentname";
        public const string ContactOwnerToken = "contactowner";

        public const string ClientListSeparator = ", ";
        public const string WelcomeMessage = "Welcome";

        // Cookies
        public const string QicUserNameCookie = "qicuser";
        public const string QicDisclaimerCookie = "qicuserdisclaimer";

        //MimeTypes
        public const string WordMimeType = "application\\msword";
        public const string ExcelMimeType = "application\\vnd.ms-excel";
        public const string PDFMimeType = "application\\pdf"; 
        public const string DefaultMimeType = "application\\octet-stream";

        //RequireEmail
        public const string SendEmail = "email";
        public const string NoEmail = "noemail";
        
        //PublishedDate DateTime Format
        public const string PublishedDateFormat = "yyyy-MM-dd HHmmss";

        //ReportDate Date Format
        public const string ReportDateFormat = "yyyyMMdd";

        public const string AllReportGroups = "All";

        //FormDate Date Format
        public const string FormDateFormat = "yyyyMMdd";

        // Workbox - custom
        public const string AllDefaultValue = "All";
        public const string WorkboxRegistryPath = "/Current_User/Workbox";

        public const string AccountDisplay = "Account"; //Client
        public const string AccountRegistry = "Account";

        public const string ReportNameRegistry = "ReportName";
        public const string ReportNameDisplay = "Report Name";

        public const string HoldingAccountDisplay = "Holding Account"; // Account
        public const string HoldingAccountRegistry = "HoldingAccount";

        public const string ProductDisplay = "Product";
        public const string ProductRegistry = "Product";

        public const double PublishDayListFromNow = -1.0;

        // User profile field
        public const string ProfileMetadataField = "UserMetadata";
        public const string ReoprtMetadataField = "ReoprtMetadata";

        // Xrm credentials configuration name.
        public const string XrmUiCredentials = "XrmUiCredentials";

        // Commands
        public const int UnpublishDocumentsAlertThreshold = 100;
        public const string WorkflowState = "__workflow state";

        //Favourites
        public const int NumberOfFavourites = 20;
        public const string FavouriteLimitTitle = "Maximum number of favourites reached";
        public static readonly string FavouriteLimitMessage = string.Format("You can only have up to {0} favourites saved.", NumberOfFavourites);
    }
}
