﻿namespace Qic.Sc.Entities
{
    using Glass.Mapper;

    /// <summary>
    /// The entity extensions.
    /// </summary>
    public static class EntityExtensions
    {
        /// <summary>
        /// Ensures menu item has a valid title derived from a field, display name or name of the
        /// item.
        /// </summary>
        public static string GetDisplayNameOrName(this IGlassBase self)
        {
            if (self == null)
            {
                return null;
            }

            if (self.EntityDisplayName.IsNotNullOrEmpty())
            {
                return self.EntityDisplayName;
            }

            return self.EntityName;
        }
    }
}
