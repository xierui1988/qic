﻿using System.Collections.Generic;
using System.Linq;

namespace Qic.Sc.Entities.UserDefined.Components.Site.Configuration.MenuItem
{
    using Castle.Core.Internal;

    using Glass.Mapper;

    /// <summary>
    /// Extension methods for menu items.
    /// </summary>
    public static class MenuItemExtensions
    {
        /// <summary>
        /// Ensures menu item has a valid title derived from a field, display name or name of the
        /// item.
        /// </summary>
        public static string GetTitleOrName(this IMenuItem self)
        {
            if (self == null)
            {
                return null;
            }

            if (self.Title.IsNotNullOrEmpty())
            {
                return self.Title;
            }

            if (self.EntityDisplayName.IsNotNullOrEmpty())
            {
                return self.EntityDisplayName;
            }

            return self.EntityName;
        }

        /// <summary>
        /// Converts item name into an id.
        /// </summary>
        public static string GetItemNameAsId(this IMenuItem self)
        {
            return self == null ? null : self.EntityName.Replace(' ', '-').ToLower();
        }

        /// <summary>
        /// Gets the first set of active items Active Items. Optimised for 2 layers.
        /// </summary>
        /// <param name="self">The menu item Instance.</param>
        /// <returns>First set of depth first search results.</returns>
        public static IEnumerable<IMenuItem> GetBreadCrumbs(this IEnumerable<IMenuItem> self)
        {
            var current = self;
            while (current != null)
            {
                var active = current.FirstOrDefault(c => c.IsActive);
                if (active == null)
                {
                    break;
                }

                yield return active;

                if (active.SubItems == null)
                {
                    break;
                }
                current = active.SubItems;
            }
        }

        /// <summary>
        /// Get the leaf active item.
        /// </summary>
        /// <param name="self">The menu item Instance.</param>
        /// <returns>Gets the leaf active menu item, or null if no menu items are active.</returns>
        public static IMenuItem GetActive(this IEnumerable<IMenuItem> self)
        {
            return self.GetBreadCrumbs().LastOrDefault();
        }
    }
}
