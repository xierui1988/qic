﻿using System;
namespace Qic.Sc.Entities.Metadata
{
    /// <summary>
    /// Defines user meta data
    /// </summary>
    public class ReportAccessMetadata 
    {
        /// <summary>
        /// The ReportAccess record id
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// The Account name - Client Name
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// The Account Code
        /// </summary>
        public string AccountCode { get; set; }

        /// <summary>
        /// The Holding Account Name
        /// </summary>
        public string HoldingAccountName { get; set; }

        /// <summary>
        /// The Holding Account Code
        /// </summary>
        public string HoldingAccountCode { get; set; }

        /// <summary>
        /// The Product Name
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// The Product Code
        /// </summary>
        public string ProductCode { get; set; }

        /// <summary>
        /// Report Code
        /// </summary>
        public string ReportCode { get; set; }

        /// <summary>
        /// Report Name
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// Report Name
        /// </summary>
        public string ReportTypeName { get; set; }

        /// <summary>
        /// The Contact Owner's Manager's name
        /// </summary>
        public string ContactOwner { get; set; }

        /// <summary>
        /// The user's Email Address As well as their username
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// The user's Report Access Email notification setting
        /// </summary>
        public bool EmailNotification { get; set; }

        /// <summary>
        /// Gets or sets the first name.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the full name.
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Gets or sets the parent account.
        /// </summary>
        public string ParentAccount { get; set; }

        /// <summary>
        /// Gets its own entity name
        /// </summary>
        /// <returns>Returns the entity name of the document</returns>
        public string GetEntityName()
        {
            if (!string.IsNullOrEmpty(this.HoldingAccountCode))
            {
                return this.HoldingAccountName;
            }
            if (!string.IsNullOrEmpty(this.ProductCode))
            {
                return this.ProductName;
            }
            
            return this.AccountName;
        }

        /// <summary>
        /// This is the entity that would be displayed in the User Manager's Profile
        /// </summary>
        /// <returns>The Client Name or Client Name and other Entity Name</returns>
        public string GetClientName()
        {
            if (!string.IsNullOrEmpty(this.HoldingAccountCode))
            {
                return string.Format("{0}{1}{2}", this.AccountName, Constants.DocumentNameSeparator, this.HoldingAccountName);
            }
            if (!string.IsNullOrEmpty(this.ProductCode))
            {
                return string.Format("{0}{1}{2}", this.AccountName, Constants.DocumentNameSeparator, this.ProductName);
            }

            return this.AccountName;
        }
    }
}