﻿namespace Qic.Sc.Entities.Metadata
{
    /// <summary>
    /// The report type meta data.
    /// </summary>
    public class ReportTypeMetadata
    {
        /// <summary>
        /// Gets or sets the report code.
        /// </summary>
        public string ReportCode { get; set; }

        /// <summary>
        /// Gets or sets the report type.
        /// </summary>
        public string ReportTypeName { get; set; }

        /// <summary>
        /// Gets or sets the report type id.
        /// </summary>
        public int ReportTypeId { get; set; }
        
        /// <summary>
        /// Get or sets the order which the reoport is ordered in CRM
        /// </summary>
        public int CrmIndexOrder { get; set; }
    }
}
