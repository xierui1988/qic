﻿using System;
using System.Globalization;
using System.IO;
using System.Linq;

namespace Qic.Sc.Entities.Metadata
{
    using global::System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Report name meta data
    /// </summary>
    public class ReportNameMetadata
    {
        /// <summary>
        /// Entity's Name
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Entity's Code
        /// </summary>
        public string EntityCode { get; set; }

        /// <summary>
        /// File Stream
        /// </summary>
        public Stream FileStream { get; set; }

        /// <summary>
        /// File Extension
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// Report Code
        /// </summary>
        public string ReportCode { get; set; }

        /// <summary>
        /// File Name
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Report Date
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime ReportDate { get; set; }

        /// <summary>
        /// Migration Published Date
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:hh:mm tt MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? MigrationPublishedDate { get; set; }

        /// <summary>
        /// IsNotification Required
        /// </summary>
        public bool? IsNotificationRequired { get; set; }

        /// <summary>
        /// If the Document is recognised as Migrated document
        /// </summary>
        public bool IsMigration { get; set; }

        /// <summary>
        /// Mime Type
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// Report Name
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// The parse file name.
        /// </summary>
        /// <param name="fileName">
        /// The file name.
        /// </param>
        /// <param name="publishDateString">
        /// The published date as string.
        /// </param>
        /// <param name="notificationString">
        /// The notification option (email/noemail) as string.
        /// </param>
        /// <returns>
        /// The <see cref="ReportNameMetadata"/>.
        /// </returns>
        public static ReportNameMetadata ParseFileName(string fileName, string publishDateString = null, string notificationString = null)
        {
            var publishedDate = ParsePublishedDate(publishDateString);
            var notification = ParseDocumentNotification(notificationString);

            return ParseFileName(fileName, publishedDate, notification);
        }

        private static DocumentNotification? ParseDocumentNotification(string notificationString)
        {
            DocumentNotification? notificationPromise = null;

            // Parse provided notification configuration
            if (!string.IsNullOrEmpty(notificationString))
            {
                DocumentNotification notification;
                if (!Enum.TryParse(notificationString, true, out notification))
                {
                    throw new ArgumentException(string.Format("Invalid migration notification option: {0}", notificationString));
                }
                notificationPromise = notification;
            }

            return notificationPromise;
        }

        private static DateTime? ParsePublishedDate(string publishDateString)
        {
            DateTime? publishedDatePromise = null;
            if (!string.IsNullOrEmpty(publishDateString))
            {
                DateTime publishedDate;

                if (!DateTime.TryParseExact(
                    publishDateString,
                    Constants.PublishedDateFormat,
                    CultureInfo.InvariantCulture,
                    DateTimeStyles.None,
                    out publishedDate))
                {
                    throw new ArgumentException(
                        string.Format(
                            "Published Date({0}) is Invalid.The Correct Format is ({1})",
                            string.IsNullOrEmpty(publishDateString) ? string.Empty : publishDateString,
                            Constants.PublishedDateFormat));
                }
                publishedDatePromise = publishedDate;
            }

            return publishedDatePromise;
        }

        /// <summary>
        /// Parses File/Report Name.
        /// </summary>
        /// <param name="fileName">
        /// The File name.
        /// </param>
        /// <param name="publishedDate">
        /// Published Date
        /// </param>
        /// <param name="requireEmail">
        /// Notification Required
        /// </param>
        public static ReportNameMetadata ParseFileName(string fileName, DateTime? publishedDate, DocumentNotification? requireEmail)
        {
            // Get the meta data part of the file name, preserve extension.
            var metadata = Path.GetFileNameWithoutExtension(fileName) ?? string.Empty;
            var extension = (Path.GetExtension(fileName) ?? string.Empty).TrimStart('.');

            // Tokenize file name into required meta data fields.
            // Required fields are separated by +.
            var documentNameList = metadata.Split('+').Select(m => m.Trim()).ToArray();

            // Check we have minimum required fields.
            if (documentNameList.Length != 4)
            {
                throw new ArgumentException(
                    string.Format(
                        "Invalid File Name: {0}. File name does not meet the naming convention. "
                        + "Correct Format is :<EntityName>+<Entity Code>+<Report Date>+<Report Code>.[Ext]",
                        fileName));
            }

            // Tokenize file name by optional meta data fields.
            // Required fields are separated by _. Optional fields
            // may only appear at the end.
            var migrationValues = documentNameList[documentNameList.Length - 1].Split('_').Select(m => m.Trim()).ToArray();
            documentNameList = documentNameList.Take(documentNameList.Length - 1).ToArray().Concat(migrationValues).ToArray();

            // Check required meta data fields are not empty
            if (documentNameList.Any(string.IsNullOrEmpty))
            {
                throw new ArgumentException(
                    string.Format(
                        "Invalid File Name: {0}. File name does not meet the naming convention."
                        + "Correct Format is :<EntityName>+<Entity Code>+<Report Date>+<Report Code>.[Ext]",
                        fileName));
            }

            // Check file has an extension
            if (string.IsNullOrEmpty(extension))
            {
                throw new ArgumentException(string.Format("Extension : [{0}] is invalid ", extension));
            }

            // Check extension is supported.
            FileExtensionToMimeMap mimeExtension;
            if (!Enum.TryParse(extension, true, out mimeExtension))
            {
                throw new ArgumentException(string.Format("Extension : [{0}] is invalid ", extension));
            }

            var reportName = new ReportNameMetadata();
            reportName.FileExtension = extension;
            reportName.MimeType = EnumUtils<FileExtensionToMimeMap>.GetDescription(mimeExtension);
            reportName.FileName = fileName;
            reportName.EntityName = documentNameList[0] ?? string.Empty;
            reportName.EntityCode = documentNameList[1] ?? string.Empty;
            reportName.ReportCode = documentNameList[3] ?? string.Empty;

            // Parse report date
            DateTime reportDate;
            if (!DateTime.TryParseExact(documentNameList[2].Replace(" ", string.Empty), Constants.ReportDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out reportDate))
            {
                throw new ArgumentException(
                    string.Format(
                        "Report Date({0}) is empty or has invalid format.The Correct format is {1}",
                        documentNameList[2],
                        Constants.ReportDateFormat));
            }

            reportName.ReportDate = reportDate;

            // Process optional fields, publish date and notification.
            if (documentNameList.Length == 5)
            {
                if (!string.IsNullOrEmpty(documentNameList[4]))
                {
                    DateTime publishDate;

                    if (!DateTime.TryParseExact(
                        documentNameList[4],
                        Constants.PublishedDateFormat,
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.None,
                        out publishDate))
                    {
                        DocumentNotification notification;
                        if (!Enum.TryParse(documentNameList[4], true, out notification))
                        {
                            throw new ArgumentException(string.Format("Invalid migration notification or published date option: {0}", documentNameList[4]));
                        }

                        if (!requireEmail.HasValue)
                        {
                            requireEmail = ParseDocumentNotification(documentNameList[4]);
                            reportName.IsMigration = true;
                        }
                    }
                    else
                    {
                        if (!publishedDate.HasValue)
                        {
                            reportName.MigrationPublishedDate = ParsePublishedDate(documentNameList[4]);
                            reportName.IsMigration = true;
                        }
                    }
                }
            }

            if (documentNameList.Length == 6)
            {
                if (!publishedDate.HasValue)
                {
                    reportName.MigrationPublishedDate = ParsePublishedDate(documentNameList[4]);
                }

                if (!requireEmail.HasValue)
                {
                    requireEmail = ParseDocumentNotification(documentNameList[5]);
                }
                reportName.IsMigration = true;
            }

            // Check notification type. (Default: Email)
            reportName.IsNotificationRequired = !requireEmail.HasValue || requireEmail.Value == DocumentNotification.Email;

            return reportName;
        }
    }
}
