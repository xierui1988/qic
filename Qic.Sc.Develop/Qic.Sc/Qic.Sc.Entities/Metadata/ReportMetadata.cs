﻿namespace Qic.Sc.Entities.Metadata
{
    /// <summary>
    /// Report's metadata
    /// </summary>
    public class ReportMetadata
    {
        /// <summary>
        /// Report's Name
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// Report's Code
        /// </summary>
        public string ReportCode { get; set; }

        /// <summary>
        /// Client's Name
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Client's Name
        /// </summary>
        public string AccountCode { get; set; }

        /// <summary>
        /// Account's Name
        /// </summary>
        public string HoldingAccountName { get; set; }

        /// <summary>
        /// Account's Code
        /// </summary>
        public string HoldingAccountCode { get; set; }

        /// <summary>
        /// Product's Name
        /// </summary>
        public string ProductName { get; set; }

        /// <summary>
        /// Product's Code
        /// </summary>
        public string ProductCode { get; set; }
    }
}
