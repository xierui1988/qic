﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qic.Sc.Entities.Metadata
{
    /// <summary>
    /// Form Metadata
    /// </summary>
    public class FormMetadata
    {
        /// <summary>
        /// Account Name
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Account Code
        /// </summary>
        public string AccountCode { get; set; }

        /// <summary>
        /// Report Date
        /// </summary>
        [DisplayFormat(DataFormatString = "{0:dd MMM yyyy}", ApplyFormatInEditMode = true)]
        public DateTime FormPublishedDate { get; set; }
        
        /// <summary>
        /// File Name
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// File Stream
        /// </summary>
        public Stream FileStream { get; set; }

        /// <summary>
        /// File Extension
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// Mime Type
        /// </summary>
        public string MimeType { get; set; }

        /// <summary>
        /// Parses File/Report Name.
        /// </summary>
        /// <param name="fileName">
        /// The File name.
        /// </param>
        public static FormMetadata ParseFileName(string fileName)
        {
            // Get the meta data part of the file name, preserve extension.
            var metadata = Path.GetFileNameWithoutExtension(fileName) ?? string.Empty;
            var extension = (Path.GetExtension(fileName) ?? string.Empty).TrimStart('.');

            // Tokenize file name into required meta data fields.
            // Required fields are separated by +.
            var documentNameList = metadata.Split('+').Select(m => m.Trim()).ToArray();

            // Check we have minimum required fields.
            if (documentNameList.Length != 3)
            {
                throw new ArgumentException(
                    string.Format(
                        "Invalid File Name: {0}. File name does not meet the naming convention. "
                        + "Correct Format is :<Account Name>+<Account Code>+<Published Date>.[Ext]",
                        fileName));
            }

            // Tokenize file name by optional meta data fields.
            // Required fields are separated by _. Optional fields
            // may only appear at the end.
            var migrationValues = documentNameList[documentNameList.Length - 1].Split('_').Select(m => m.Trim()).ToArray();
            documentNameList = documentNameList.Take(documentNameList.Length - 1).ToArray().Concat(migrationValues).ToArray();

            // Check required meta data fields are not empty
            if (documentNameList.Any(string.IsNullOrEmpty))
            {
                throw new ArgumentException(
                    string.Format(
                        "Invalid File Name: {0}. File name does not meet the naming convention."
                        + "Correct Format is :<Account Name>+<Account Code>+<Published Date>.[Ext]",
                        fileName));
            }

            // Check file has an extension
            if (string.IsNullOrEmpty(extension))
            {
                throw new ArgumentException(string.Format("Extension : [{0}] is invalid ", extension));
            }

            // Check extension is supported.
            FileExtensionToMimeMap mimeExtension;
            if (!Enum.TryParse(extension, true, out mimeExtension))
            {
                throw new ArgumentException(string.Format("Extension : [{0}] is invalid ", extension));
            }

            var formName = new FormMetadata();
            formName.FileExtension = extension;
            formName.MimeType = EnumUtils<FileExtensionToMimeMap>.GetDescription(mimeExtension);
            formName.AccountName = documentNameList[0] ?? string.Empty;
            formName.AccountCode = documentNameList[1] ?? string.Empty;

            // Parse report date
            DateTime formPublishedDate;
            if (!DateTime.TryParseExact(documentNameList[2].Replace(" ", string.Empty), Constants.FormDateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out formPublishedDate))
            {
                throw new ArgumentException(
                    string.Format(
                        "Form Published Date({0}) is empty or has invalid format.The Correct format is {1}",
                        documentNameList[2],
                        Constants.FormDateFormat));
            }

            formName.FormPublishedDate = formPublishedDate;

            return formName;
        }
    }
}
