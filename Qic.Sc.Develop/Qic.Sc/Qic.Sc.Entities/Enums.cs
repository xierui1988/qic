﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Qic.Sc.Entities
{
    /// <summary>
    /// File Type of the File.
    /// </summary>
    public enum EntityCodeType
    {
        None = 0,
        [Description("CL")]
        AccountCode = 1,
        [Description("AC")]
        HoldingAccountCode = 2,
        [Description("PR")]
        ProductCode = 3,
        [Description("EC")]
        ProductECCode = 4
    }

    /// <summary>
    /// File Extension of the File.
    /// </summary>
    public enum FileExtensionToMimeMap
    {
       [Description("application/msword")]
       doc = 1,
       [Description("application/vnd.openxmlformats-officedocument.wordprocessingml.document")]
       docx = 2,
       [Description("application/pdf")]
       pdf = 3,
       [Description("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")]
       xlsx = 4,
       [Description("application/vnd.ms-excel")]
       xls = 5,
       [Description("text/csv")]
       csv = 6
    }

    /// <summary>
    /// The document notification.
    /// </summary>
    public enum DocumentNotification
    {
        Email,
        NoEmail,
    }

    /// <summary>
    /// File Extension of the File.
    /// </summary>
    public enum SitecoreUserAction
    {
        [Description("Document Uploaded to Sitecore")]
        DocumentUpload,
        [Description("Document Viewed")]
        DocumentDownload,
        [Description("Document Published With Email")]
        DocumentPublishSendEmailAction,
        [Description("Document Published Without Email")]
        DocumentPublishNoNotificationAction,
        [Description("Document Not Published")]
        DocumentNotPublishedAction,
        [Description("Publishing Summary")]
        DocumentPublishedSummary,
        [Description("Welcome Email")]
        WelcomeNotification,
        [Description("Change Password Email")]
        ChangePassword,
        [Description("Sent Email Notification")]
        UserNotification,
        [Description("Failed Email Notification")]
        FailedUserNotification,
        [Description("User Changed Email Settings")]
        UpdateProfileAction,
        [Description("Document Deleted")]
        DeleteDocument,
        [Description("Document Unpublished")]
        UnpublishedDocument,
        [Description("Form Published")]
        FormPublish,
        [Description("Form Unpublished")]
        UnpublishedForm,
        [Description("Form Deleted")]
        DeleteForm
    }

    /// <summary>
    /// Enum Extensions Methods.
    /// </summary>
    /// <typeparam name="T">
    /// Enum Type.
    /// </typeparam>
    public static class EnumUtils<T>
    {
        /// <summary>
        /// Get Descrition for Enum.
        /// </summary>
        /// <param name="enumValue">
        /// Enum Type.
        /// </param>
        /// <param name="defDesc">
        /// Defined string Descrption.
        /// </param>
        /// <returns>
        /// String Description.
        /// </returns>
        public static string GetDescription(T enumValue, string defDesc)
        {
            FieldInfo fi = enumValue.GetType().GetField(enumValue.ToString());

            if (null != fi)
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return defDesc;
        }

        /// <summary>
        /// Get Descrition for Enum.
        /// </summary>
        /// <param name="enumValue">
        /// Enum Type.
        /// </param>
        /// <returns>
        /// String Descirption.
        /// </returns>
        public static string GetDescription(T enumValue)
        {
            return GetDescription(enumValue, string.Empty);
        }

        /// <summary>
        /// Get Enum from string description.
        /// </summary>
        /// <param name="description">
        /// Descrption of enum.
        /// </param>
        /// <returns>
        /// Enum Type.
        /// </returns>
        public static T FromDescription(string description)
        {
            Type t = typeof(T);
            foreach (FieldInfo fi in t.GetFields())
            {
                object[] attrs = fi.GetCustomAttributes(typeof(DescriptionAttribute), true);
                if (attrs.Length > 0)
                {
                    if (attrs.Cast<DescriptionAttribute>().Any(attr => attr.Description.Equals(description)))
                    {
                        return (T)fi.GetValue(null);
                    }
                }
            }
            return default(T);
        }
    }
}
