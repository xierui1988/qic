// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
#pragma warning disable 1591, 3008, 3009, 0108
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Qic.Sc.Web.Generated;
namespace Qic.Sc.Web.Areas.Corporate.Controllers
{
    public partial class EditorialController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected EditorialController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Index()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Metadata()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Metadata);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Banner()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Banner);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Attachment()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Attachment);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public EditorialController Actions { get { return MVC.Corporate.Editorial; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "Corporate";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Editorial";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "Editorial";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string Index = "Index";
            public readonly string Metadata = "Metadata";
            public readonly string Banner = "Banner";
            public readonly string Attachment = "Attachment";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string Index = "Index";
            public const string Metadata = "Metadata";
            public const string Banner = "Banner";
            public const string Attachment = "Attachment";
        }


        static readonly ActionParamsClass_Index s_params_Index = new ActionParamsClass_Index();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Index IndexParams { get { return s_params_Index; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Index
        {
            public readonly string name = "name";
            public readonly string date = "date";
        }
        static readonly ActionParamsClass_Metadata s_params_Metadata = new ActionParamsClass_Metadata();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Metadata MetadataParams { get { return s_params_Metadata; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Metadata
        {
            public readonly string name = "name";
            public readonly string date = "date";
        }
        static readonly ActionParamsClass_Banner s_params_Banner = new ActionParamsClass_Banner();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Banner BannerParams { get { return s_params_Banner; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Banner
        {
            public readonly string name = "name";
            public readonly string date = "date";
        }
        static readonly ActionParamsClass_Attachment s_params_Attachment = new ActionParamsClass_Attachment();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Attachment AttachmentParams { get { return s_params_Attachment; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Attachment
        {
            public readonly string name = "name";
            public readonly string date = "date";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
            }
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_EditorialController : Qic.Sc.Web.Areas.Corporate.Controllers.EditorialController
    {
        public T4MVC_EditorialController() : base(Dummy.Instance) { }

        [NonAction]
        partial void IndexOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string name, string date);

        [NonAction]
        public override System.Web.Mvc.ActionResult Index(string name, string date)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Index);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "name", name);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "date", date);
            IndexOverride(callInfo, name, date);
            return callInfo;
        }

        [NonAction]
        partial void MetadataOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string name, string date);

        [NonAction]
        public override System.Web.Mvc.ActionResult Metadata(string name, string date)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Metadata);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "name", name);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "date", date);
            MetadataOverride(callInfo, name, date);
            return callInfo;
        }

        [NonAction]
        partial void BannerOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string name, string date);

        [NonAction]
        public override System.Web.Mvc.ActionResult Banner(string name, string date)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Banner);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "name", name);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "date", date);
            BannerOverride(callInfo, name, date);
            return callInfo;
        }

        [NonAction]
        partial void AttachmentOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string name, string date);

        [NonAction]
        public override System.Web.Mvc.ActionResult Attachment(string name, string date)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Attachment);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "name", name);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "date", date);
            AttachmentOverride(callInfo, name, date);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108
