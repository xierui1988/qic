﻿namespace Qic.Sc.Web.Areas.Portal
{
    using System.Web.Mvc;

    /// <summary>
    /// The client area registration.
    /// </summary>
    public class PortalAreaRegistration : AreaRegistration
    {
        /// <summary>
        /// Gets the area name.
        /// </summary>
        public override string AreaName
        {
            get
            {
                return "Portal";
            }
        }

        /// <summary>
        /// The register area with the framework.
        /// </summary>
        /// <param name="context">
        /// The context, passed in by the framework at registration time.
        /// </param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Portal_ReportUpload",
                "Portal/Report/Upload/",
                new { controller = "Report", action = "Upload" }
            );
            context.MapRoute(
                "Portal_ReportDownload",
                "Portal/Report/Download/{documentID}/{fileName}",
                new { controller = "Download", action = "Download" }
            );
            context.MapRoute(
                "Portal_FormDownload",
                "Portal/Report/DownloadForm/{documentID}/{fileName}",
                new { controller = "Download", action = "DownloadForm" }
            );
            context.MapRoute(
                "Portal_AccountSendNotification",
                "Portal/Account/SendPasswordNotification/{id}/{email}",
                new { controller = "Account", action = "SendPasswordNotification" }
            );
            context.MapRoute(
                "Portal_default",
                "Portal/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}