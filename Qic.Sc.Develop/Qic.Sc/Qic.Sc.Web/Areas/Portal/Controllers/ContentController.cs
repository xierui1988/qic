﻿using Qic.Sc.Entities.UserDefined.Components.Pages.Pagemetadata;
using Qic.Sc.Web.Areas.Portal.Models;

namespace Qic.Sc.Web.Areas.Portal.Controllers
{
    using System;
    using System.Web.Mvc;

    using Qic.Sc.Entities.UserDefined.Components.Pages.Pagecontent;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Web.Controllers;

    /// <summary>
    /// The content controller.
    /// </summary>
    public partial class ContentController : RenderingController
    {
        private readonly IContentRepository contentRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentController"/> class.
        /// </summary>
        public ContentController(IContentRepository contentRepository)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            this.contentRepository = contentRepository;
        }

        /// <summary>
        /// Renders page content
        /// </summary>
        /// <param name="renderingDataSource">
        ///     The page to render.
        /// </param>
        /// <param name="renderingContextItem">
        ///     If page to render if null will user current context item.
        /// </param>
        /// <param name="isRaw">
        ///     Do not wrap content, return as is. Otherwise use default wrapping markup.
        /// </param>
        public virtual ActionResult Index(string renderingDataSource, string renderingContextItem, bool isRaw = false)
        {
            var content = this.contentRepository.GetEntity<IPageContent>(renderingDataSource ?? renderingContextItem);
            var pageMetadata = this.contentRepository.GetEntity<IPageMetadata>(renderingDataSource ?? renderingContextItem);
            if (content == null || pageMetadata == null)
            {
                return this.HttpNotFound("Page not found");
            }

            if (isRaw)
            {
                return this.View(MVC.Portal.Content.Views.ViewNames.Raw);
            }

            return this.View(new ContentModels
            {
                Title = pageMetadata.Title,
                PageMetadata = content
            });
        }
    }
}