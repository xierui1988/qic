﻿namespace Qic.Sc.Web.Areas.Portal.Controllers
{
    using System.Web;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;

    using System;
    using System.Web.Mvc;

    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Web.Areas.Portal.Models;
    using System.IO;

    /// <summary>
    /// The report controller.
    /// </summary>
    public partial class ReportController : Controller
    {
        private readonly IReportService reportService;
        private readonly IFormService formService;
        private readonly IWebSecurityService webSecurityService;
        private readonly ILoggerService loggerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportController"/> class.
        /// </summary>
        public ReportController(IReportService reportService, IFormService formService, IWebSecurityService webService, ILoggerService loggerService)
        {
            if (reportService == null)
            {
                throw new ArgumentNullException("reportService");
            }
            if (formService == null)
            {
                throw new ArgumentNullException("formService");
            }
            if (webService == null)
            {
                throw new ArgumentNullException("webService");
            }
            if (loggerService == null)
            {
                throw new ArgumentNullException("loggerService");
            }

            this.formService = formService;
            this.reportService = reportService;
            this.webSecurityService = webService;
            this.loggerService = loggerService;
        }

        /// <summary>
        /// The upload form.
        /// </summary>
        public virtual ActionResult Upload(string id, DocumentUploadType type, string entityName, string entityCode, string reportCode)
        {
            if (!this.webSecurityService.AuthenticateXrmCredentials(id))
            {
                return new HttpUnauthorizedResult("Invalid credentials");
            }

            return
                this.View(
                        new DocumentUploadModel
                        {
                            UploadType = type,
                            EntityName = entityName, //Account Name for Form
                            EntityCode = entityCode, //Account Code for Form
                            ReportCode = reportCode,
                            Id = id,
                        });
        }

        /// <summary>
        /// Document upload processor.
        /// </summary>
        [HttpPost]
        public virtual ActionResult Upload(DocumentUploadModel model)
        {
            if (!this.webSecurityService.AuthenticateXrmCredentials(model.Id))
            {
                return new HttpUnauthorizedResult("Invalid credentials");
            }

            var isValidDate = false;
            model.IsFailed = false;

            try
            {
                isValidDate = this.ValidateUploadDate(model);
            }
            catch (ApplicationException ex)
            {
                model.Messages.Add(ex.Message);
                model.IsFailed = true;
            }

            var uploadedFile = this.Request.Files["file"];
            if (uploadedFile == null || string.IsNullOrEmpty(uploadedFile.FileName))
            {
                model.Messages.Add("No file was send for upload.");
                model.IsFailed = true;
            }
            else
            {
                try
                {
                    switch (model.UploadType)
                    {
                        default:
                            model.Messages.Add("Unsupported document type: " + model.UploadType);
                            model.IsFailed = true;
                            break;

                        case DocumentUploadType.Report:
                            if (isValidDate)
                            {
                                this.UploadReport(model, uploadedFile);
                                model.Messages.Add("The report file has been uploaded.");
                                model.IsFailed = false;
                            }
                            break;

                        case DocumentUploadType.Form:
                            if (isValidDate)
                            {
                                this.UploadForm(model, uploadedFile);
                                model.Messages.Add("The form has been uploaded.");
                                model.IsFailed = false;
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    this.loggerService.Error("Manual upload failed.", ex);

                    model.Messages.Add(ex.Message);
                    model.IsFailed = true;
                }
            }

            return this.View(model);
        }

        private void UploadReport(DocumentUploadModel model, HttpPostedFileBase file)
        {
            var reportMetadata =
                ReportNameMetadata.ParseFileName(
                    string.Format(
                        "{0}+{1}+{2}+{3}.{4}",
                        model.EntityName,
                        model.EntityCode,
                        model.ReportDate.ToString("yyyyMMdd"),
                        model.ReportCode,
                        (Path.GetExtension(file.FileName) ?? string.Empty).TrimStart('.')),
                        publishedDate: null,
                        requireEmail: DocumentNotification.Email);

            reportMetadata.FileName = file.FileName;
            reportMetadata.FileStream = file.InputStream;

            this.reportService.CreateReport(reportMetadata);
        }

        private void UploadForm(DocumentUploadModel model, HttpPostedFileBase file)
        {
            // Some browser (old IE) send fine names including full paths even though they
            // should not. We need to normalize the file names to just he file name. Taking
            // into account different path separators '/' or '\'. If we got a path instead of
            var filename = Path.GetFileName(file.FileName.Replace("/", "\\"));

            if (string.IsNullOrEmpty(filename))
            {
                throw new ApplicationException("File name can't be interpreted: " + file.FileName);
            }

            var formMetadata = FormMetadata.ParseFileName(
                 string.Format(
                        "{0}+{1}+{2}.{3}",
                        model.EntityName,
                        model.EntityCode,
                        model.ReportDate.ToString("yyyyMMdd"),
                        (Path.GetExtension(filename) ?? string.Empty).TrimStart('.')));

            formMetadata.FileName = Path.GetFileNameWithoutExtension(filename);
            formMetadata.FileStream = file.InputStream;
            
            this.formService.CreateForm(formMetadata);
        }

        private bool ValidateUploadDate(DocumentUploadModel model)
        {
            try
            {
                var date = new DateTime(model.ReportDateYear, model.ReportDateMonth, model.ReportDateDay);
                if (date <= DateTime.Now)
                {
                    return true;
                }

                throw new ApplicationException("Date cannot be in the future");
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error("Report Date is invalid", ex);
                throw new ApplicationException("Report Date is invalid");
            }
        }
    }
}
