﻿namespace Qic.Sc.Web.Areas.Portal.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Web.Areas.Portal.Models;
    using Qic.Sc.Web.Controllers;

    /// <summary>
    /// The content controller.
    /// </summary>
    public partial class SearchController : RenderingController
    {
        private readonly IReportService reportService;
        private readonly IPortalWebSecurityService webSecurityService;
        private readonly IWebSecurityService securityService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentController"/> class.
        /// </summary>
        public SearchController(IReportService reportService, IPortalWebSecurityService webSecurityService, IWebSecurityService securityService)
        {
            if (reportService == null)
            {
                throw new ArgumentNullException("reportService");
            }
            if (webSecurityService == null)
            {
                throw new ArgumentNullException("webSecurityService");
            }
            if (securityService == null)
            {
                throw new ArgumentNullException("securityService");
            }
            this.reportService = reportService;
            this.webSecurityService = webSecurityService;
            this.securityService = securityService;
        }

        /// <summary>
        /// Renders the logic for the Search Results page
        /// </summary>
        /// <returns>Returns the Search Result rendering</returns>
        public virtual ActionResult Results(string q, int skip = 0, int take = 20)
        {
            if (q == null)
            {
                return this.View();
            }

            var userName = this.securityService.GetCurrentAuthenticatedUsername();
            var isAdmin = this.securityService.IsAdmin(userName);
            var access = this.webSecurityService.GetReportAccessFor(userName).ToArray();

            // Create filtering options based on user access.
            var filter = new DocumentListFilterModel().SetFilterOptionsFrom(access);

            // Get security trimmed list of documents for the current user.
            int total;
            var documents = this.reportService.SearchDocumentsBy(userName, new ReportFilterAndSortMetadata { Filters = filter }, q, skip, take, out total);

            return this.View(new DocumentListModel
            {
                Filter = filter,
                Documents = documents.Select(d => new DocumentModel().FromDocument(d, this.Url.ForItem(d.EntityId), isAdmin ? null : access.GetAccessibleAccountCodes())),
                Take = take,
                Total = total
            });
        }
    }
}