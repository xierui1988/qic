﻿using System.Collections.Generic;

namespace Qic.Sc.Web.Areas.Portal.Models
{
    using Qic.Sc.Entities.Metadata;

    /// <summary>
    /// Profile Model
    /// </summary>
    public class ProfileModel
    {
        /// <summary>
        /// User Details
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// The User's email (Also their username)
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// The User's Access to Accounts - Client Relationship Manager (Comma separated if more than one)
        /// </summary>
        public string ContactOwner { get; set; }

        /// <summary>
        /// Account Report Access
        /// </summary>
        public List<ReportAccessMetadata> ReportAccess { get; set; }
    }
}