﻿using System.ComponentModel.DataAnnotations;

namespace Qic.Sc.Web.Areas.Portal.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// The report models.
    /// </summary>
    public class DocumentUploadModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentUploadModel"/> class.
        /// </summary>
        public DocumentUploadModel()
        {
            this.Messages = new List<string>();
        }

        /// <summary>
        /// Gets or sets the upload type.
        /// </summary>
        public DocumentUploadType UploadType { get; set; }

        /// <summary>
        /// Gets or sets the entity code.
        /// </summary>
        public string EntityName { get; set; }

        /// <summary>
        /// Gets or sets the entity code.
        /// </summary>
        public string EntityCode { get; set; }

        /// <summary>
        /// Gets or sets the report code.
        /// </summary>
        public string ReportCode { get; set; }

        /// <summary>
        /// Gets or sets the report date.
        /// </summary>
        [DataType(DataType.Date)]
        public DateTime ReportDate 
        {
            get
            {
                try
                {
                    return new DateTime(this.ReportDateYear, this.ReportDateMonth, this.ReportDateDay);
                }
                catch
                {
                    return DateTime.Now;
                } 
            }
        }

        /// <summary>
        /// Gets or sets the report date day.
        /// </summary>
        public int ReportDateDay { get; set; }

        /// <summary>
        /// Gets or sets the report date month.
        /// </summary>
        public int ReportDateMonth { get; set; }

        /// <summary>
        /// Gets or sets the report date year.
        /// </summary>
        public int ReportDateYear { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether upload failed.
        /// </summary>
        public bool IsFailed { get; set; }

        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        public List<string> Messages { get; set; }

        /// <summary>
        /// Id used for authentication
        /// </summary>
        public string Id { get; set; }
    }
}