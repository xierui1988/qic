namespace Qic.Sc.Web.Areas.Portal.Models
{
    /// <summary>
    /// The report upload type.
    /// </summary>
    public enum DocumentUploadType
    {
        Report,
        Form,
    }
}