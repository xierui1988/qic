﻿namespace Qic.Sc.Web.Areas.Corporate.Model
{
    /// <summary>
    /// The content model.
    /// </summary>
    public class ContentModel
    {
        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets the headline.
        /// </summary>
        public string Headline { get; set; }
    }
}