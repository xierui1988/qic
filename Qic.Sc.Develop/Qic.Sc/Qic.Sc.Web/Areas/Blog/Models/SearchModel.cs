﻿using System.Collections.Generic;
using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;
using Qic.Sc.Entities.UserDefined.QIC.Blog.Data.BlogArticle;

namespace Qic.Sc.Web.Areas.Blog.Model
{
    /// <summary>
    /// Search Page Model
    /// </summary>
    public class SearchModel
    {
        /// <summary>
        /// Search String
        /// </summary>
        public string SearchQuery { get; set; }

        /// <summary>
        /// Search Results
        /// </summary>
        public IEnumerable<IBlogArticle> SearchResults { get; set; }

        /// <summary>
        /// Page number
        /// </summary>
        //public int Page { get; set; }

        /// <summary>
        /// Page number for results
        /// </summary>
        //public int PageCount { get; set; }

        /// <summary>
        /// Search Results per page
        /// </summary>
        public int ResultsCount { get; set; }

        /// <summary>
        /// Gets or sets the previous.
        /// </summary>
        //public string Previous { get; set; }

        /// <summary>
        /// Gets or sets the next.
        /// </summary>
        //public string Next { get; set; }
    }
}