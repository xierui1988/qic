﻿namespace Qic.Sc.Web.Controllers
{
    using System;
    using System.Net;
    using System.Web.Mvc;

    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Platform;

    /// <summary>
    /// The HTTP content not found result. Add support for Sitecore site specific
    /// custom 404 pages.
    /// </summary>
    public class HttpContentNotFoundResult : HttpNotFoundResult
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="HttpContentNotFoundResult"/> class.
        /// </summary>
        /// <param name="statusDescription">
        /// The status description for 404 error.
        /// </param>
        public HttpContentNotFoundResult(string statusDescription)
            : base(statusDescription)
        {
        }

        /// <summary>
        /// The execute result. Invoked by the MVC as a part of the page
        /// life cycle. The implementation will try to resolve to a custom
        /// site specific 404 page, if non could be resolved base <see cref="HttpNotFoundResult"/>
        /// implementation will be used.
        /// </summary>
        /// <param name="context">
        /// The current MVC context.
        /// </param>
        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            var siteService = DependencyResolver.Current.GetService<ISiteService>();
            var linkService = DependencyResolver.Current.GetService<ILinkService>();
            if (siteService != null && linkService != null)
            {
                var redirectTo = linkService.GenerateUrl(siteService.Get404Item());

                if (!string.IsNullOrEmpty(redirectTo))
                {
                    // Transfer to the 404 handler with the status code
                    context.HttpContext.TransferWithSitecoreResponseStatusCode(
                        redirectTo,
                        (HttpStatusCode)this.StatusCode);

                    return;
                }
            }

            // Revert to default behavior
            base.ExecuteResult(context);
        }
    }
}
