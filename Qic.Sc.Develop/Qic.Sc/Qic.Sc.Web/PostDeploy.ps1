Write-Host "Removing *.config transform files."
get-childitem .\ -include *.Prod.config, *.Test.config, *.Dev.config, *.Release.config, *.Debug.config, *.Lab.config -recurse | foreach ($_) {remove-item $_.fullname -force}

Write-Host "Encrypting configuration."
Write-Host "  Encrypting section - 'sitecore'."
& "c:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe" -pef "sitecore" "$PWD"
Write-Host "  Encrypting section - 'system.web/membership'."
& "c:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe" -pef "system.web/membership" "$PWD"
Write-Host "  Encrypting section - 'system.web/roleManager'."
& "c:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe" -pef "system.web/roleManager" "$PWD"
Write-Host "  Encrypting section - 'system.web/profile'."
& "c:\Windows\Microsoft.NET\Framework64\v4.0.30319\aspnet_regiis.exe" -pef "system.web/profile" "$PWD"

Write-Host "Stating IIS."
iisreset.exe /start

Write-Host "Deploying sitecore items ($PWD\App_Data\Qic.Sc.scitems.update)."
& ".\App_Data\PackageInstaller.exe" -v --packagePath "$PWD\App_Data\Qic.Sc.scitems.update" --sitecoreUrl http://localhost/ --sitecoreDeployFolder "$PWD"
