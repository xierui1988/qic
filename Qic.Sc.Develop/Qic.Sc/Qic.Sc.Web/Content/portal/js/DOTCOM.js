$(document).ready(function () {
    "use strict";
    var hasDetails = $(".has-details"),
    windowWidth = 0,
    defaultFooterBtn = $("footer").find(".btn-default"),
    footerLogo = $("#logo-li"),
    disclaimerList = footerLogo.parent(),
    mobileCollapsable = $(".collapsable-trigger");

    onResizeEvent();

    $(window).resize(function () {
        onResizeEvent();
    });

    $(".input-date").datepicker();

    $(".input-radio-custom").on("change", function () {
        var checked = $(".input-radio-custom:checked"),
        parentDiv = checked.parents(".radio-container");
        parentDiv.find(".radio-toggle").removeClass("active");
        checked.parent().addClass("active");
    });

    $(".remove-tag").on("click", function () {
        var _self = $(this);
        _self.parent().remove();
    });

    $(".input-tags").on("click", function () {
        var _self = $(this),
        input = _self.prev();
        addTag(input);
    });

    $("#custom-tags").keyup(function (event) {
        if (event.keyCode == 13) {
            var _self = $(this);
            addTag(_self);
        }
    });

    $(".fixed-icon").on("mouseenter", function () {
        var details = $(this).prev();
        $(".fixed-description").hide();
        details.effect("slide", { direction: "right" }, 200);
    });

    $(".icon-top").on("click", function () {
        $(".fixed-description").hide();
        $("html, body").animate({ scrollTop: 0 }, 500);
    });
    $("#mobile-menu-icon").on("click", function () {
        $("#mobile-content-menu").fadeToggle('fast');
        $(".secondary-list").hide();
        mobileCollapsable.removeClass("collapsable-custom");
    });

    mobileCollapsable.on("click", function () {
        var _self = $(this),
        submenu = _self.parents(".main-list-item").find(".secondary-list");
        submenu.toggle("blind");
        _self.toggleClass("collapsable-custom");
    });

    $(".search-li.opener").on("click", function () {
        $(this).hide();
        $("#mega-nav-menu .has-details").css("visibility", "hidden");
        hasDetails.removeClass("active");
        hasDetails.find("a").removeAttr("style");
        $(".search-li.closer").show();
        $(".mega-nav-submenu.active").hide("blind").removeClass("active");
        $("#mega-nav-search").show("blind");
        $("#mega-nav-search input").focus();
    });

    $(".search-li .closer-trigger").on("click", function (e) {
        $("#mega-nav-search").hide("blind");
        $(".search-li.closer").hide();
        $("#mega-nav-menu .has-details").css("visibility", "visible");
        $(".search-li.opener").show();
    });

    hasDetails.on("mouseenter", function () {
        var _self = $(this),
        itemId = _self.attr("id");
        toggleMegaNav(_self);
        hasDetails.each(function (index, item) {
            var detailsBoxId = $(item).attr("id"),
            detailsBox = $("#" + detailsBoxId + "-details");
            if (item.id.indexOf(itemId) > -1) {
                detailsBox.show("blind").addClass("active");
            } else {
                detailsBox.hide().removeClass("active");
            }
        });
    }).on("mouseleave", function () {
        var _self = $(this);
        toggleMegaNav(_self);
    });

    $(".mega-nav-submenu").on("mouseenter", function () {
        var itemId = $(this).attr("id"),
        keyId = itemId.slice(0, itemId.indexOf("-details"));
        toggleMegaNav($("#" + keyId));
    }).on("mouseleave", function () {
        hasDetails.removeClass("active");
        $(this).hide("blind");
        hasDetails.each(function (index, item) {
            var _self = $(item);
            if (!_self.hasClass("active")) {
                _self.prev().find("a").removeAttr("style");
            }
        });
    });

    function toggleMegaNav(activeitem) {
        hasDetails.removeClass("active");
        activeitem.addClass("active").prev().find("a").attr("style", "border: none");
        $("#mega-nav-search").hide("blind");
        $(".search-li.closer").hide();
        $(".search-li.opener").show();
        hasDetails.each(function (index, item) {
            var _self = $(item);
            if (!_self.hasClass("active")) {
                _self.prev().find("a").removeAttr("style");
            }
        });
    }

    function onResizeEvent() {
        var imgPaperList = $(".sub-paper-list").find("img"),
        noList = $(".sub-paper-textbox.no-list"),
        imgWidthList = imgPaperList.css("width"),
        imgHeight = noList.css("height");
        windowWidth = window.innerWidth;
        if (windowWidth <= 1024) {
            defaultFooterBtn.text("Contact");
            footerLogo.appendTo(disclaimerList);
        } else {
            defaultFooterBtn.text("Get in touch");
            if (footerLogo.prev().prop("tagName") !== undefined) {
                disclaimerList.prepend(footerLogo);
            }
        }
        imgPaperList.css("height", imgWidthList);
        noList.prev().css("height", imgHeight);
    }

    function addTag(input) {
        var tagValue = input.val();
        if (tagValue !== "") {
            var list = $("#tags-list"),
            newLi = $("<li />"),
            newSpan = list.find("span").first().clone(true);
            newLi.text(tagValue);
            newLi.append(newSpan);
            newLi.appendTo(list);
            input.val("");
        }
    }

    $(".dotdotdot").dotdotdot();
});
