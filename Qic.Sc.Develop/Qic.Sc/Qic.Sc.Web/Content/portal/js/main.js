/* Show And Hidde the mobile menu*/
$("#menu-mobile").on("click", function() {
    $("#portalNav").toggleClass("menu-mobile-active");
    $("#portalNav div:first").attr('id', 'mobile-content-menu');
    $("#mobile-content-menu").fadeToggle('fast');
});

/* Star Favorite Change Color*/
$(".favorite-table").on("click", function() {
    $(this).toggleClass("favorite-table-activ");
});

/* Display Filter content*/
$("#filter-btn").on("click", function() {
    $(this).toggleClass("btn-filter-activ");
    $("#filter-content").fadeToggle('fast');
});

/* show and hidde desktop-menu*/
$("#desktop").on("click", function() {
    $(this).toggleClass("desktop-menu-active");
    $(".desktop-menu").fadeToggle('fast');
});

/*Show and hidde the notification*/
$(".notification").on("click", function() {
    $(this).toggleClass("notification-active");
    $(".content-notification").fadeToggle('fast');
});

/* show and hidde the action´s bar when a checkbox is checked */
var headerCheck = $("#check-header"),
    checkBoxes = $(".check");

headerCheck.on('click', function() {
    if ($(this).is(':checked')) {
        checkBoxes.each(function(index, item) {
            $(item).prop('checked', true);
        });
    } else {
        checkBoxes.each(function(index, item) {
            $(item).prop('checked', false);
        });
    }

});

/* checked and unchecked all checkboxes*/
checkBoxes.on("click", function() {

    var checked = false,
        bar = $(".body-notification"),
        checkAll = false;
    checkBoxes.each(function(index, item) {
        var _self = $(item);
        if (_self.is(':checked')) {
            //show bar
            checked = true;
        }
    });
    checked ? bar.show() : bar.hide();
});

$(document).ready(function () {
    var footerLogo = $("#logo-li"),
        disclaimerList = footerLogo.parent(),
        windowWidth = 0;
    onResizeEvent();

    $(window).resize(function() {
        onResizeEvent();
        onResizeTable();
    });

    $(".input-date").datepicker();

    function onResizeTable() {
        windowWidth = window.innerWidth;
        var colWidthProfile = "20%",
            colWidthNew = "40%";
        if (windowWidth <= 768) {
            colWidthProfile = "50%";
            colWidthNew = "100%";
            // $(".custom-label").hide();

        } else {
            $(".custom-label").show();
            // $(".glyphicon-star").removeAttr("style");
        }
        $("#tableProfile").find("col").first().attr("style", "width:" + colWidthProfile);
        $("#tableProfile").find("col").last().attr("style", "width:" + "50%");
        $("#clientTable").find("col").first().attr("style", "width:" + colWidthNew);
    }

    function showNotification() {
        $(this).closest(".panel").css("position", "relative");
        posY = $(this).position().top;
        $(this).find("input").trigger("click");

        $(".body-notification").css("position", "absolute");
        $(".body-notification").css("top", posY);
        $(".body-notification").css("right", "0");
        $(".body-notification").show('slide', {
            direction: 'right'
        }, 1000);
    }

    function onResizeEvent() {
        windowWidth = window.innerWidth;
        if (windowWidth <= 1024) {
            footerLogo.appendTo(disclaimerList);



        } else if (footerLogo.prev().prop("tagName") !== undefined) {
            disclaimerList.prepend(footerLogo);

        }
        if (windowWidth <= 768) {


            //$( "#clientTable tbody tr" ).click(showNotification);

        } else {
            $(".body-notification").css("top", "0")
            $("#portalNav div:first").removeAttr("style");

        }
    }

    $(".fixed-icon").on("mouseenter", function() {
        var details = $(this).prev();
        $(".fixed-description").show();
        //details.effect("slide", { direction: "left" }, 200);
    });

    $(".fixed-icon").on("mouseleave", function() {
        var details = $(this).prev();
        $(".fixed-description").hide();
        //details.effect("slide", { direction: "left" }, 200);
    });

    $(".icon-top").on("click", function() {
        $(".fixed-description").hide();
        $("html, body").animate({
            scrollTop: 0
        }, 500);
    });
});

function clearFilterDates() {
    $("#RDateFrom").datepicker('clearDate');
    $("#RDateTo").datepicker('clearDate');
    $("#PDateFrom").datepicker('clearDate');
    $("#PDateTo").datepicker('clearDate');
    $("#RDateFrom").datepicker("setEndDate", "Date.now()");
    $("#RDateTo").datepicker("setEndDate", "Date.now()");
    $("#PDateFrom").datepicker("setEndDate", "Date.now()");
    $("#PDateTo").datepicker("setEndDate", "Date.now()");
    $("#RDateFrom").datepicker("setStartDate", null);
    $("#RDateTo").datepicker("setStartDate", null);
    $("#PDateFrom").datepicker("setStartDate", null);
    $("#PDateTo").datepicker("setStartDate", null);
};

$(document).ready(function() {
    $("#RDateFrom, #RDateTo, #PDateFrom, #PDateTo").datepicker({
        format: "dd/mm/yyyy",
        endDate: "Date.now()",
        clearBtn: true
    });

    $("#RDateFrom").on("change", function () {
        $('#RDateTo').datepicker("setStartDate", $(this).val());
    });

    $("#RDateTo").on("change", function () {
        var $this = $(this).datepicker('getDate') == null ? Date.now() : $(this).val();
        $('#RDateFrom').datepicker("setEndDate", $this);
    });

    $("#PDateFrom").on("change", function () {
        $('#PDateTo').datepicker("setStartDate", $(this).val());
    });

    $("#PDateTo").on("change", function () {
        var $this = $(this).datepicker('getDate') == null ? Date.now() : $(this).val();
        $('#PDateFrom').datepicker("setEndDate", $this);
    });

    $("#RDateFrom, #RDateTo, #PDateFrom, #PDateTo").siblings(".icon-calendar").css("cursor", "pointer").click(function () {
        $(this).siblings().first().datepicker('show');
    });
});

$(document).ready(function() {
    "use strict";
    var hasDetails = $(".has-details"),
        windowWidth = 0,
        defaultFooterBtn = $("footer").find(".btn-default"),
        footerLogo = $("#logo-li"),
        disclaimerList = footerLogo.parent(),
        mobileCollapsable = $(".collapsable-trigger");

    onResizeEvent();

    $(window).resize(function() {
        onResizeEvent();
    });

    $(".input-date").datepicker();

    $(".input-radio-custom").on("change", function() {
        var checked = $(".input-radio-custom:checked"),
            parentDiv = checked.parents(".radio-container");
        parentDiv.find(".radio-toggle").removeClass("active");
        checked.parent().addClass("active");
    });

    $(".remove-tag").on("click", function() {
        var _self = $(this);
        _self.parent().remove();
    });

    $(".input-tags").on("click", function() {
        var _self = $(this),
            input = _self.prev();
        addTag(input);
    });

    $("#custom-tags").keyup(function(event) {
        if (event.keyCode == 13) {
            var _self = $(this);
            addTag(_self);
        }
    });

    $(".fixed-icon").on("mouseenter", function() {
        var details = $(this).prev();
        $(".fixed-description").hide();
        details.effect("slide", {
            direction: "right"
        }, 200);
    });

    $(".icon-top").on("click", function() {
        $(".fixed-description").hide();
        $("html, body").animate({
            scrollTop: 0
        }, 500);
    });
    //$("#mobile-menu-icon").on("click", function () {
    //    $("#mobile-content-menu").fadeToggle('fast');
    //    $(".secondary-list").hide();
    //    mobileCollapsable.removeClass("collapsable-custom");
    //});

    $('#mobile-menu-icon').click(
        function() {
            if ($("#portalNav").hasClass('hidden-sm hidden-xs')) {
                //menu not showing when mobile
                $("#portalNav").removeClass('hidden-sm hidden-xs');
                $("#mobileNavigation").removeAttr("style");
                $("#searchMobile").removeAttr("style");
                $("#portalNav span").hide();
                $("aside .row ul li .badge").show();
                $("#portalNav li").addClass('mobileli');
                $("#portalNav").addClass('mobileMenueOverride');
            } else {
                //menu showing when mobile
                $("#portalNav").addClass('hidden-sm hidden-xs');
                $("#mobileNavigation").hide();
                $("#mobileNavigation").attr("style", "display: none");
                $("#searchMobile").attr("style", "display: none");
                $("#portalNav span").show();
                $("#portalNav div:first").removeAttr('id', 'mobile-content-menu');
                $("#portalNav li").removeClass('mobileli');
                $("#portalNav").removeClass('mobileMenueOverride');
            }
            $(this).toggleClass('active');
        });


    mobileCollapsable.on("click", function() {
        var _self = $(this),
            submenu = _self.parents(".main-list-item").find(".secondary-list");
        submenu.toggle("blind");
        _self.toggleClass("collapsable-custom");
    });

    $(".search-li.opener").on("click", function() {
        $(this).hide();
        $("#mega-nav-menu .has-details").css("visibility", "hidden");
        hasDetails.removeClass("active");
        hasDetails.find("a").removeAttr("style");
        $(".search-li.closer").show();
        $(".mega-nav-submenu.active").hide("blind").removeClass("active");
        $("#mega-nav-search").show("blind");
        $("#mega-nav-search input").focus();
    });

    $(".search-li .closer-trigger").on("click", function(e) {
        $("#mega-nav-search").hide("blind");
        $(".search-li.closer").hide();
        $("#mega-nav-menu .has-details").css("visibility", "visible");
        $(".search-li.opener").show();
    });

    hasDetails.on("mouseenter", function() {
        var _self = $(this),
            itemId = _self.attr("id");
        toggleMegaNav(_self);
        hasDetails.each(function(index, item) {
            var detailsBoxId = $(item).attr("id"),
                detailsBox = $("#" + detailsBoxId + "-details");
            if (item.id.indexOf(itemId) > -1) {
                detailsBox.show("blind").addClass("active");
            } else {
                detailsBox.hide().removeClass("active");
            }
        });
    }).on("mouseleave", function() {
        var _self = $(this);
        toggleMegaNav(_self);
    });

    $(".mega-nav-submenu").on("mouseenter", function() {
        var itemId = $(this).attr("id"),
            keyId = itemId.slice(0, itemId.indexOf("-details"));
        toggleMegaNav($("#" + keyId));
    }).on("mouseleave", function() {
        hasDetails.removeClass("active");
        $(this).hide("blind");
        hasDetails.each(function(index, item) {
            var _self = $(item);
            if (!_self.hasClass("active")) {
                _self.prev().find("a").removeAttr("style");
            }
        });
    });

    function toggleMegaNav(activeitem) {
        hasDetails.removeClass("active");
        activeitem.addClass("active").prev().find("a").attr("style", "border: none");
        $("#mega-nav-search").hide("blind");
        $(".search-li.closer").hide();
        $(".search-li.opener").show();
        hasDetails.each(function(index, item) {
            var _self = $(item);
            if (!_self.hasClass("active")) {
                _self.prev().find("a").removeAttr("style");
            }
        });
    }

    function onResizeEvent() {
        var imgPaperList = $(".sub-paper-list").find("img"),
            noList = $(".sub-paper-textbox.no-list"),
            imgWidthList = imgPaperList.css("width"),
            imgHeight = noList.css("height");
        windowWidth = window.innerWidth;
        if (windowWidth <= 1024) {
            defaultFooterBtn.text("Contact");
            footerLogo.appendTo(disclaimerList);
        } else {
            defaultFooterBtn.text("Get in touch");
            if (footerLogo.prev().prop("tagName") !== undefined) {
                disclaimerList.prepend(footerLogo);
            }
        }
        imgPaperList.css("height", imgWidthList);
        noList.prev().css("height", imgHeight);
    }

    function addTag(input) {
        var tagValue = input.val();
        if (tagValue !== "") {
            var list = $("#tags-list"),
                newLi = $("<li />"),
                newSpan = list.find("span").first().clone(true);
            newLi.text(tagValue);
            newLi.append(newSpan);
            newLi.appendTo(list);
            input.val("");
        }
    }

});
