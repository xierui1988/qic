// <auto-generated />
// This file was generated by a T4 template.
// Don't change it directly as your change would get overwritten.  Instead, make changes
// to the .tt file (i.e. the T4 template) and save it to regenerate this file.

// Make sure the compiler doesn't complain about missing Xml comments and CLS compliance
// 0108: suppress "Foo hides inherited member Foo. Use the new keyword if hiding was intended." when a controller and its abstract parent are both processed
#pragma warning disable 1591, 3008, 3009, 0108
#region T4MVC

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using System.Web.Mvc.Html;
using System.Web.Routing;
using Qic.Sc.Web.Generated;
namespace Qic.Sc.Web.Areas.Corporate.Controllers
{
    public partial class NavigationController
    {
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected NavigationController(Dummy d) { }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoute(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToAction(Task<ActionResult> taskResult)
        {
            return RedirectToAction(taskResult.Result);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return RedirectToRoutePermanent(callInfo.RouteValueDictionary);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        protected RedirectToRouteResult RedirectToActionPermanent(Task<ActionResult> taskResult)
        {
            return RedirectToActionPermanent(taskResult.Result);
        }

        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult MegaMenu()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.MegaMenu);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult FooterMenu()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.FooterMenu);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Legal()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Legal);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult Locations()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Locations);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult TopMenu()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.TopMenu);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult MegaTopMenu()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.MegaTopMenu);
        }
        [NonAction]
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public virtual System.Web.Mvc.ActionResult SideMenu()
        {
            return new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SideMenu);
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public NavigationController Actions { get { return MVC.Corporate.Navigation; } }
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Area = "Corporate";
        [GeneratedCode("T4MVC", "2.0")]
        public readonly string Name = "Navigation";
        [GeneratedCode("T4MVC", "2.0")]
        public const string NameConst = "Navigation";

        static readonly ActionNamesClass s_actions = new ActionNamesClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionNamesClass ActionNames { get { return s_actions; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNamesClass
        {
            public readonly string MegaMenu = "MegaMenu";
            public readonly string FooterMenu = "FooterMenu";
            public readonly string Legal = "Legal";
            public readonly string Locations = "Locations";
            public readonly string TopMenu = "TopMenu";
            public readonly string MegaTopMenu = "MegaTopMenu";
            public readonly string SideMenu = "SideMenu";
        }

        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionNameConstants
        {
            public const string MegaMenu = "MegaMenu";
            public const string FooterMenu = "FooterMenu";
            public const string Legal = "Legal";
            public const string Locations = "Locations";
            public const string TopMenu = "TopMenu";
            public const string MegaTopMenu = "MegaTopMenu";
            public const string SideMenu = "SideMenu";
        }


        static readonly ActionParamsClass_MegaMenu s_params_MegaMenu = new ActionParamsClass_MegaMenu();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_MegaMenu MegaMenuParams { get { return s_params_MegaMenu; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_MegaMenu
        {
            public readonly string renderingDataSource = "renderingDataSource";
            public readonly string renderingContextItem = "renderingContextItem";
            public readonly string contextSite = "contextSite";
        }
        static readonly ActionParamsClass_FooterMenu s_params_FooterMenu = new ActionParamsClass_FooterMenu();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_FooterMenu FooterMenuParams { get { return s_params_FooterMenu; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_FooterMenu
        {
            public readonly string renderingDataSource = "renderingDataSource";
            public readonly string renderingContextItem = "renderingContextItem";
            public readonly string contextSite = "contextSite";
        }
        static readonly ActionParamsClass_Legal s_params_Legal = new ActionParamsClass_Legal();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Legal LegalParams { get { return s_params_Legal; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Legal
        {
            public readonly string renderingDataSource = "renderingDataSource";
            public readonly string contextSite = "contextSite";
        }
        static readonly ActionParamsClass_Locations s_params_Locations = new ActionParamsClass_Locations();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_Locations LocationsParams { get { return s_params_Locations; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_Locations
        {
            public readonly string renderingDataSource = "renderingDataSource";
            public readonly string contextSite = "contextSite";
        }
        static readonly ActionParamsClass_TopMenu s_params_TopMenu = new ActionParamsClass_TopMenu();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_TopMenu TopMenuParams { get { return s_params_TopMenu; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_TopMenu
        {
            public readonly string renderingDataSource = "renderingDataSource";
            public readonly string renderingContextItem = "renderingContextItem";
            public readonly string displayPortalLink = "displayPortalLink";
            public readonly string contextSite = "contextSite";
        }
        static readonly ActionParamsClass_MegaTopMenu s_params_MegaTopMenu = new ActionParamsClass_MegaTopMenu();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_MegaTopMenu MegaTopMenuParams { get { return s_params_MegaTopMenu; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_MegaTopMenu
        {
            public readonly string renderingDataSource = "renderingDataSource";
            public readonly string renderingContextItem = "renderingContextItem";
            public readonly string displayPortalLink = "displayPortalLink";
            public readonly string contextSite = "contextSite";
        }
        static readonly ActionParamsClass_SideMenu s_params_SideMenu = new ActionParamsClass_SideMenu();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ActionParamsClass_SideMenu SideMenuParams { get { return s_params_SideMenu; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ActionParamsClass_SideMenu
        {
            public readonly string renderingDataSource = "renderingDataSource";
            public readonly string renderingContextItem = "renderingContextItem";
        }
        static readonly ViewsClass s_views = new ViewsClass();
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public ViewsClass Views { get { return s_views; } }
        [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
        public class ViewsClass
        {
            static readonly _ViewNamesClass s_ViewNames = new _ViewNamesClass();
            public _ViewNamesClass ViewNames { get { return s_ViewNames; } }
            public class _ViewNamesClass
            {
                public readonly string FooterMenu = "FooterMenu";
                public readonly string Legal = "Legal";
                public readonly string Locations = "Locations";
                public readonly string MegaMenu = "MegaMenu";
                public readonly string MegaTopMenu = "MegaTopMenu";
                public readonly string SideMenu = "SideMenu";
                public readonly string TopMenu = "TopMenu";
            }
            public readonly string FooterMenu = "~/Areas/Corporate/Views/Navigation/FooterMenu.cshtml";
            public readonly string Legal = "~/Areas/Corporate/Views/Navigation/Legal.cshtml";
            public readonly string Locations = "~/Areas/Corporate/Views/Navigation/Locations.cshtml";
            public readonly string MegaMenu = "~/Areas/Corporate/Views/Navigation/MegaMenu.cshtml";
            public readonly string MegaTopMenu = "~/Areas/Corporate/Views/Navigation/MegaTopMenu.cshtml";
            public readonly string SideMenu = "~/Areas/Corporate/Views/Navigation/SideMenu.cshtml";
            public readonly string TopMenu = "~/Areas/Corporate/Views/Navigation/TopMenu.cshtml";
        }
    }

    [GeneratedCode("T4MVC", "2.0"), DebuggerNonUserCode]
    public partial class T4MVC_NavigationController : Qic.Sc.Web.Areas.Corporate.Controllers.NavigationController
    {
        public T4MVC_NavigationController() : base(Dummy.Instance) { }

        [NonAction]
        partial void MegaMenuOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string renderingDataSource, System.Guid? renderingContextItem, string contextSite);

        [NonAction]
        public override System.Web.Mvc.ActionResult MegaMenu(string renderingDataSource, System.Guid? renderingContextItem, string contextSite)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.MegaMenu);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingDataSource", renderingDataSource);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingContextItem", renderingContextItem);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contextSite", contextSite);
            MegaMenuOverride(callInfo, renderingDataSource, renderingContextItem, contextSite);
            return callInfo;
        }

        [NonAction]
        partial void FooterMenuOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string renderingDataSource, System.Guid? renderingContextItem, string contextSite);

        [NonAction]
        public override System.Web.Mvc.ActionResult FooterMenu(string renderingDataSource, System.Guid? renderingContextItem, string contextSite)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.FooterMenu);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingDataSource", renderingDataSource);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingContextItem", renderingContextItem);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contextSite", contextSite);
            FooterMenuOverride(callInfo, renderingDataSource, renderingContextItem, contextSite);
            return callInfo;
        }

        [NonAction]
        partial void LegalOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string renderingDataSource, string contextSite);

        [NonAction]
        public override System.Web.Mvc.ActionResult Legal(string renderingDataSource, string contextSite)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Legal);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingDataSource", renderingDataSource);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contextSite", contextSite);
            LegalOverride(callInfo, renderingDataSource, contextSite);
            return callInfo;
        }

        [NonAction]
        partial void LocationsOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string renderingDataSource, string contextSite);

        [NonAction]
        public override System.Web.Mvc.ActionResult Locations(string renderingDataSource, string contextSite)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.Locations);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingDataSource", renderingDataSource);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contextSite", contextSite);
            LocationsOverride(callInfo, renderingDataSource, contextSite);
            return callInfo;
        }

        [NonAction]
        partial void TopMenuOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string renderingDataSource, System.Guid? renderingContextItem, bool displayPortalLink, string contextSite);

        [NonAction]
        public override System.Web.Mvc.ActionResult TopMenu(string renderingDataSource, System.Guid? renderingContextItem, bool displayPortalLink, string contextSite)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.TopMenu);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingDataSource", renderingDataSource);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingContextItem", renderingContextItem);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "displayPortalLink", displayPortalLink);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contextSite", contextSite);
            TopMenuOverride(callInfo, renderingDataSource, renderingContextItem, displayPortalLink, contextSite);
            return callInfo;
        }

        [NonAction]
        partial void MegaTopMenuOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string renderingDataSource, System.Guid? renderingContextItem, bool displayPortalLink, string contextSite);

        [NonAction]
        public override System.Web.Mvc.ActionResult MegaTopMenu(string renderingDataSource, System.Guid? renderingContextItem, bool displayPortalLink, string contextSite)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.MegaTopMenu);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingDataSource", renderingDataSource);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingContextItem", renderingContextItem);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "displayPortalLink", displayPortalLink);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "contextSite", contextSite);
            MegaTopMenuOverride(callInfo, renderingDataSource, renderingContextItem, displayPortalLink, contextSite);
            return callInfo;
        }

        [NonAction]
        partial void SideMenuOverride(T4MVC_System_Web_Mvc_ActionResult callInfo, string renderingDataSource, System.Guid? renderingContextItem);

        [NonAction]
        public override System.Web.Mvc.ActionResult SideMenu(string renderingDataSource, System.Guid? renderingContextItem)
        {
            var callInfo = new T4MVC_System_Web_Mvc_ActionResult(Area, Name, ActionNames.SideMenu);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingDataSource", renderingDataSource);
            ModelUnbinderHelpers.AddRouteValues(callInfo.RouteValueDictionary, "renderingContextItem", renderingContextItem);
            SideMenuOverride(callInfo, renderingDataSource, renderingContextItem);
            return callInfo;
        }

    }
}

#endregion T4MVC
#pragma warning restore 1591, 3008, 3009, 0108
