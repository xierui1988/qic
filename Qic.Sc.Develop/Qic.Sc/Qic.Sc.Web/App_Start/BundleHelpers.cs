﻿namespace Qic.Sc.Web
{
    using System.Linq;
    using System.Web;
    using System.Web.Optimization;

    /// <summary>
    /// Extension methods for resource bundling.
    /// </summary>
    public static class BundleHelpers
    {
        /// <summary>
        /// Convert T4MVC paths to application relative paths suitable for System.Web.Optimization bundling.
        /// </summary>
        /// <param name="bundle">The bundle.</param>
        /// <param name="virtualPaths">The virtual paths to add.</param>
        /// <returns>The populated bundle.</returns>
        public static Bundle IncludeT4MVC(this Bundle bundle, params string[] virtualPaths)
        {
            bundle.Include(virtualPaths.Select(VirtualPathUtility.ToAppRelative).ToArray());
            return bundle;
        }
    }
}