USE [SCAuditTrail]

GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'UserValues')
                    AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE [dbo].[UserValues]
GO

SET ANSI_NULLS ON

GO

SET QUOTED_IDENTIFIER ON

GO

Create PROCEDURE [dbo].[UserValues]

AS

BEGIN

 

Select 'Select All' AS SCUser

union all

SELECT DISTINCT SCUser
FROM            Logs

END
GO

GRANT EXECUTE ON [dbo].[UserValues] TO [QIC_report_role];
GO