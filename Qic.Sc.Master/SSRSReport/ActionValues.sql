USE [SCAuditTrail]
GO

/****** Object:  StoredProcedure [dbo].[ActionValues]    Script Date: 15/04/2015 3:38:05 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'ActionValues')
                    AND type IN ( N'P', N'PC' ) ) 
	DROP PROCEDURE [dbo].[ActionValues]
GO

/****** Object:  StoredProcedure [dbo].[ActionValues]    Script Date: 15/04/2015 3:38:05 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[ActionValues]

AS

DECLARE @Actions TABLE (
	SCAction  varchar(256)
)

BEGIN
	
	INSERT INTO @Actions (SCAction)
		SELECT SCAction
		FROM SitecoreUserAction
		ORDER BY SCAction

	Select 'Select All' AS SCAction
	union all
	SELECT SCAction
    FROM @Actions
END
GO


