﻿namespace Qic.Sc.Platform.Events
{
    /// <summary>
    /// Document Event saved handlers relating to operations on Sitecore items.
    /// </summary>
    public class DocumentMoveToYearMonthDayFolder : MoveToBase
    {
    }

    /// <summary>
    /// Document Event saved handlers relating to operations on Sitecore items.
    /// </summary>
    public class FormMoveToYearMonthDayFolder : MoveToBase
    {
    }

    /// <summary>
    /// Article Event saved handlers relating to operations on Sitecore items.
    /// </summary>
    public class EditorialCategorisation : MoveToBase
    {
    }

    /// <summary>
    /// Email Campaign Event handlers relating to operations on Sitecore items.
    /// </summary>
    public class MessagesNotificationCategorisation : MoveToBase
    {
    }
}
