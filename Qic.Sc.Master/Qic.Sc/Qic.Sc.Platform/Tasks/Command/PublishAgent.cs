﻿using Qic.Sc.Entities.UserDefined.Components.Site.BetweenTimeSchedule;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Publishing;
using Sitecore.SecurityModel;
using Sitecore.Tasks;
using System;

namespace Qic.Sc.Platform.Tasks.Command
{
    /// <summary>
    /// This class is used to trigger Publishing
    /// </summary>
    public class PublishAgent
    {
        /// <summary>
        /// Checks whether to trigger publish
        /// </summary>
        /// <param name="items">Items that need to run Deep Publish</param>
        /// <param name="command">The command that triggered (Must be Between Time Schedule)</param>
        /// <param name="schedule">The Schedule that triggered the command</param>
        public void TriggerPublish(Item[] items, CommandItem command, ScheduleItem schedule)
        {
            var publishCommand = command.InnerItem;

            // Check if its not Between Time Schedule - then return
            if (publishCommand.TemplateID != IBetweenTimeScheduleConstants.TemplateId)
            {
                return;
            }

            TimeSpan start = Sitecore.DateUtil.IsoDateToDateTime(publishCommand[IBetweenTimeScheduleConstants.StartFieldId]).TimeOfDay;
            TimeSpan end = Sitecore.DateUtil.IsoDateToDateTime(publishCommand[IBetweenTimeScheduleConstants.EndFieldId]).TimeOfDay;
            DateTime lastRun = Sitecore.DateUtil.IsoDateToDateTime(publishCommand[IBetweenTimeScheduleConstants.LastCompletedRunFieldId]);

            // Check if its supposed to run
            if (this.SupposedToRun(start, end, lastRun))
            {
                return;
            }

            // Should be ok to run
            Log.Info(string.Format("Publish started at {0}", DateTime.Now.ToString()), this);

            foreach (var item in items)
            {
                this.RunDeepPublishOnItem(item);
            }

            Log.Info(string.Format("Publish ended at {0}", DateTime.Now.ToString()), this);

            // Update the Last Run Date
            using (new SecurityDisabler())
            {
                using (new EditContext(publishCommand))
                {
                    publishCommand[IBetweenTimeScheduleConstants.LastCompletedRunFieldId] = Sitecore.DateUtil.ToIsoDate(DateTime.Now);
                    Log.Info(string.Format("Updated the {0} field to: {1}", publishCommand.Name, DateTime.Now.ToString()), this);
                }
            }
        }

        private bool SupposedToRun(TimeSpan start, TimeSpan end, DateTime lastRun)
        {
            // If it has aleardy run today, (or in the futture) then don't run
            if (lastRun.Date >= DateTime.Today)
            {
                return false;
            }

            // Check if now is within Start Time and End Time
            TimeSpan now = DateTime.Now.TimeOfDay;

            // If its in range then its ok to run
            if (start < now && now < end)
            {
                return true;
            }
            
            // If not in range then return false
            return false;
        }

        private void RunDeepPublishOnItem(Item publishRoot)
        {
            try
            {
                var master = Database.GetDatabase(Entities.Constants.MasterDb);

                var publishOptions = new PublishOptions(master, Database.GetDatabase(Entities.Constants.WebDb), PublishMode.SingleItem, publishRoot.Language, DateTime.Now);
                var publisher = new Publisher(publishOptions);
                publisher.Options.PublishRelatedItems = false;
                publisher.Options.RootItem = publishRoot;

                // Publish children as well?
                publisher.Options.Deep = true;

                publisher.Publish();

                Log.Info(string.Format("Publish has been succesffuly run for Item Name: {0}, Item ID: {1}", publishRoot.Name, publishRoot.ID), this);
            }
            catch (Exception e)
            {
                Log.Error("Could not run Deep publish", e, this);
            }
        }
    }
}
