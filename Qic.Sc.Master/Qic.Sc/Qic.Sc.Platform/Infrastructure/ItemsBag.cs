﻿namespace Qic.Sc.Platform.Infrastructure
{
    using System;
    using System.Collections.Generic;
    using System.Dynamic;

    /// <summary>
    /// Provides a keyed collection of items, implemented as a dynamic object.
    /// </summary>
    internal class ItemsBag : DynamicObject
    {
        private readonly Dictionary<string, dynamic> properties =
            new Dictionary<string, dynamic>(StringComparer.InvariantCultureIgnoreCase);

        public override bool TryGetMember(GetMemberBinder binder, out dynamic result)
        {
            result = this.properties.ContainsKey(binder.Name) ? this.properties[binder.Name] : null;
            return true;
        }

        public override bool TrySetMember(SetMemberBinder binder, dynamic value)
        {
            if (value == null)
            {
                if (this.properties.ContainsKey(binder.Name))
                {
                    this.properties.Remove(binder.Name);
                }
            }
            else
            {
                this.properties[binder.Name] = value;
            }

            return true;
        }
    }
}