﻿namespace Qic.Sc.Platform
{
    using System.Collections;
    using System.Net;
    using System.Web;

    using Qic.Sc.Platform.Infrastructure;

    using Sitecore.StringExtensions;

    /// <summary>
    /// Extension methods applicable to services.
    /// </summary>
    public static class HttpContextExtensions
    {
        #region Status Code

        private const string QueryStringStatusCodeKey = "___rewrite_statuscode";

        /// <summary>
        /// Perform a server side transfer with a custom response status code.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="path">
        /// The URL of the page or handler to execute.
        /// </param>
        /// <param name="statusCode">
        /// The status code of the resulting response. Used to execute 404 pages etc.
        /// </param>
        public static void TransferWithSitecoreResponseStatusCode(
            this HttpContextBase context, string path,
            HttpStatusCode statusCode)
        {
            context.ApplicationInstance.Context.TransferWithSitecoreResponseStatusCode(path, statusCode);
        }

        /// <summary>
        /// Perform a server side transfer with a custom response status code.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="path">
        /// The URL of the page or handler to execute.
        /// </param>
        /// <param name="statusCode">
        /// The status code of the resulting response. Used to execute 404 pages etc.
        /// </param>
        public static void TransferWithSitecoreResponseStatusCode(
            this HttpContext context, string path,
            HttpStatusCode statusCode)
        {
            if (path == null)
            {
                throw new System.ArgumentNullException("path");
            }

            // If path already has a query string append to it, otherwise create query string.
            var transferPath = path;
            if (path.Contains("?")) 
            { 
                path = "{0}&{1}={2}".FormatWith(path, QueryStringStatusCodeKey, (int)statusCode);
            }
            else
            {
                path = "{0}?{1}={2}".FormatWith(path, QueryStringStatusCodeKey, (int)statusCode);
            }

            context.Server.TransferRequest(transferPath);
        }

        /// <summary>
        /// The set sitecore response status code.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="statusCode">
        /// The status code of the resulting response.
        /// </param>
        public static void SetSitecoreResponseStatusCode(this HttpContextBase context, HttpStatusCode statusCode)
        {
            SetSitecoreResponseStatusCode(context.ApplicationInstance.Context.ItemsBag(), statusCode);
        }

        /// <summary>
        /// The set sitecore response status code.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="statusCode">
        /// The status code of the resulting response.
        /// </param>
        public static void SetSitecoreResponseStatusCode(this HttpContext context, HttpStatusCode statusCode)
        {
            SetSitecoreResponseStatusCode(context.ItemsBag(), statusCode);
        }

        /// <summary>
        /// Get sitecore response status code. Provided one was actually set.
        /// Works with server side request transfers initiated with <see cref="TransferWithSitecoreResponseStatusCode"/>
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The response status code if one was set during request processing, null otherwise.
        /// </returns>
        public static int? GetSitecoreResponseStatusCode(this HttpContext context)
        {
            var statusCode = context.ItemsBag().StatusCode;
            if (statusCode == null && context.Request.QueryString.Count > 0 
                && !context.Request.RawUrl.Equals(context.Request.Url.PathAndQuery)) 
            {
                // Check if request have been transfered/rewritten and use
                // status code provided in the new URL
                int newStatusCode;
                if (int.TryParse(context.Request.QueryString[QueryStringStatusCodeKey], out newStatusCode))
                {
                    statusCode = newStatusCode;
                }
            }

            return statusCode;
        }

        /// <summary>
        /// Get sitecore response status code. Provided one was actually set.
        /// Works with server side request transfers initiated with <see cref="TransferWithSitecoreResponseStatusCode"/>
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <returns>
        /// The response status code if one was set during request processing, null otherwise.
        /// </returns>
        public static bool? GetSitecoreResponseStatusCodeChanged(this HttpContext context)
        {
            return context.ItemsBag().IsStatusChanged;
        }

        /// <summary>
        /// Checks whether current context status code have been manipulated.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public static void SetSitecoreResponseStatusCodeChanged(this HttpContext context)
        {
            context.ItemsBag().IsStatusChanged = true;
        }

        private static void SetSitecoreResponseStatusCode(dynamic bag, HttpStatusCode statusCode)
        {
            bag.StatusCode = (int)statusCode;
        }

        #endregion

        #region Items Bag

        /// <summary>
        /// Get item bag from <see cref="HttpContext"/>
        /// </summary>
        /// <param name="self">
        /// The self reference to the <see cref="HttpContext"/>, this is an extension method.
        /// </param>
        /// <returns>
        /// A dynamic object used for storing information that should last for the
        /// duration of the request.
        /// </returns>
        public static dynamic ItemsBag(this HttpContext self)
        {
            if (self == null)
            {
                return null;
            }

            var items = self.Items;
            return ItemsBag(items);
        }

        /// <summary>
        /// Get item bag from <see cref="HttpContextBase"/>
        /// </summary>
        /// <param name="self">
        /// The self reference to the <see cref="HttpContextBase"/>, this is an extension method.
        /// </param>
        /// <returns>
        /// A dynamic object used for storing information that should last for the
        /// duration of the request.
        /// </returns>
        public static dynamic ItemsBag(this HttpContextBase self)
        {
            if (self == null)
            {
                return null;
            }

            var items = self.Items;
            return ItemsBag(items);
        }

        private static dynamic ItemsBag(IDictionary items)
        {
            const string StorageKey = "Sc.Services.Web.Extensions";

            if (!items.Contains(StorageKey))
            {
                items[StorageKey] = new ItemsBag();
            }

            return items[StorageKey];
        }
        #endregion
    }
}
