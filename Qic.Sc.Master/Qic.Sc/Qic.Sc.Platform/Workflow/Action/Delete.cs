﻿using Qic.Sc.Interfaces.Components;
using Sitecore.Data.Items;
using Sitecore.Workflows.Simple;
using System;
using Autofac;
using Qic.Sc.Entities.Metadata;
using Qic.Sc.Entities;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Sitecore.Web.UI.Sheer;
using Sitecore.Diagnostics;

namespace Qic.Sc.Platform.Workflow.Action
{
    /// <summary>
    /// Delete class is used as part of an action in Sitecore Workflow commands to delete the selected item
    /// </summary>
    public class Delete
    {
        /// <summary>
        /// Sitecore Action Item triggers this method.
        /// </summary>
        /// <param name="args">The Workflow Arguments of the item in workflow</param>
        public void Process(WorkflowPipelineArgs args)
        {
            Item item = args.DataItem;
            if (item == null)
            {
                SheerResponse.Alert("No Item was provided in the workflow command for Deleting. Aborting operation.");
                return;
            }

            var auditAction = item.TemplateID == IDocumentConstants.TemplateId ? SitecoreUserAction.DeleteDocument : SitecoreUserAction.DeleteForm;

            try
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    using (var scope = ServiceLocator.BeginScopeFor(Constants.MasterDb))
                    {
                        var audit = scope.Resolve<IAuditService>();

                        audit.Audit(new AuditMetadata()
                        {
                            UserName = Sitecore.Context.User.Profile.UserName,
                            Action = EnumUtils<SitecoreUserAction>.GetDescription(auditAction),
                            Db = item.Database.Name,
                            Path = item.Paths.FullPath,
                            Language = item.Language.Name,
                            Version = item.Version.Number,
                            Id = item.ID.ToString(),
                            Miscellaneous = item.GetAccountNamesFromDocumentItem()
                        });
                    }

                    item.Delete();
                }
            }
            catch (Exception e)
            {
                args.AbortPipeline();
                Log.Error("There was a problem in trying to delete an item.", e, this);
                SheerResponse.Alert("The item could not be deleted. Aborting and exiting");
            }
        }
    }
}
