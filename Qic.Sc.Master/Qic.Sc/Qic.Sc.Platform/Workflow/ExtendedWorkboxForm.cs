﻿using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Web;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Workflows;
using System;
using System.Linq;
using Autofac;
using Qic.Sc.Interfaces.Core;
using Qic.Sc.Services.Core;
using System.Collections.Generic;
using Qic.Sc.Entities.UserDefined.QIC.Shared.Published;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;

namespace Qic.Sc.Platform.Workflow
{
    using Qic.Sc.Entities;
    using Qic.Sc.Platform.Commands;

    using Sitecore.Web.UI.Sheer;

    /// <summary>
    /// This has the Extended Workbox functionality for QIC Portal
    /// </summary>
    public class ExtendedWorkboxForm : WorkboxForm
    {
        private string accountCode;
        private string reportNameCode;
        private string holdingAccountCode;
        private string productCode;

        /// <summary>
        /// Custom QIC Account Name
        /// </summary>
        public string AccountCode
        {
            get
            {
                if (string.IsNullOrEmpty(this.accountCode))
                {
                    this.accountCode = this.GetRegistryStringValue(Entities.Constants.AccountRegistry);
                }

                return this.accountCode;
            }

            set
            {
                this.SetRegistryStringValue(Entities.Constants.AccountRegistry, value);
                this.accountCode = value;
            }
        }

        /// <summary>
        /// Custom QIC Holding Account Name
        /// </summary>
        public string HoldingAccountCode
        {
            get
            {
                if (string.IsNullOrEmpty(this.holdingAccountCode))
                {
                    this.holdingAccountCode = this.GetRegistryStringValue(Entities.Constants.HoldingAccountRegistry);
                }

                return this.holdingAccountCode;
            }

            set
            {
                this.SetRegistryStringValue(Entities.Constants.HoldingAccountRegistry, value);
                this.holdingAccountCode = value;
            }
        }

        /// <summary>
        /// Custom QIC Product Name
        /// </summary>
        public string ProductCode
        {
            get
            {
                if (string.IsNullOrEmpty(this.productCode))
                {
                    this.productCode = this.GetRegistryStringValue(Entities.Constants.ProductRegistry);
                }

                return this.productCode;
            }

            set
            {
                this.SetRegistryStringValue(Entities.Constants.ProductRegistry, value);
                this.productCode = value;
            }
        }

        /// <summary>
        /// Custom QIC Report Name
        /// </summary>
        public string ReportNameCode
        {
            get
            {
                if (string.IsNullOrEmpty(this.reportNameCode))
                {
                    this.reportNameCode = this.GetRegistryStringValue(Entities.Constants.ReportNameRegistry);
                }

                return this.reportNameCode;
            }

            set
            {
                this.SetRegistryStringValue(Entities.Constants.ReportNameRegistry, value);
                this.reportNameCode = value;
            }
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        /// <param name="state">
        /// The state.
        /// </param>
        /// <param name="workflow">
        /// The workflow.
        /// </param>
        /// <returns>
        /// Array of item DataUri.
        /// </returns>
        protected override DataUri[] GetItems(WorkflowState state, IWorkflow workflow)
        {
            Assert.ArgumentNotNull(state, "state");
            Assert.ArgumentNotNull(workflow, "workflow");

            List<DataUri> dataUriList = new List<DataUri>();

            Item[] items = this.GetItemsExtended(state, workflow);

            foreach (var item in items)
            {
                if (item != null && item.Access.CanRead() && item.Access.CanReadLanguage() && item.Access.CanWriteLanguage() &&
                    (Context.IsAdministrator || item.Locking.CanLock() || item.Locking.HasLock()))
                {
                    if (this.FilterWorkboxItems(item, state, workflow))
                    {
                        dataUriList.Add(new DataUri(item.Uri));
                    }
                }
            }

            return dataUriList.ToArray();
        }

        private Item[] GetItemsExtended(WorkflowState state, IWorkflow workflow)
        {
            if (state.StateID == Entities.Constants.System.Workflows.QICPortalPublishingWorkflow.Published.ItemID.ToString())
            {
                using (var scope = ServiceLocator.BeginScopeFor(Entities.Constants.MasterDb))
                {
                    var contentRepository = scope.Resolve<IMasterContentRepository>() as IContentRepository;

                    using (var search = contentRepository.GetSearchService())
                    {
                        var query = search.GetDescendantsQueryable<PublishedEntity>(Entities.Constants.Content.ClientPortal.Documents.ItemID.Guid);

                        DateTime validAfter = DateTime.Now.AddDays(Entities.Constants.PublishDayListFromNow);

                        // WHERE: Document library is in Published (in 1 day)
                        var documents = query
                            .Where(d => d.WorkflowState == Entities.Constants.System.Workflows.QICPortalPublishingWorkflow.Published.ID && d.IndexPublishedDate > validAfter)
                            .ToArray();

                        return documents.Select(x => Context.ContentDatabase.GetItem(x.EntityId.ToString("B"))).ToArray();
                    }
                }
            }

            DataUri[] items = workflow.GetItems(state.StateID);
            return items.Select(x => Context.ContentDatabase.Items[x]).ToArray();
        }

        /// <summary>
        /// This will allow the items to be filtered based on new Filters such as Report
        /// </summary>
        /// <param name="item">The Item to be determined to be filtered.</param>
        /// <returns>Returns true if passed the report Filter</returns>
        protected bool FilterWorkboxItems(Item item, WorkflowState state, IWorkflow workflow)
        {
            // If Report Name Code is "All" then ok, no need to filter
            if (this.ReportNameCode != Entities.Constants.AllDefaultValue)
            {
                // If document report name does not match the selected Account code then remove from list
                if (this.ReportNameCode != item[IDocumentConstants.ReportCodeFieldId])
                {
                    return false;
                }
            }

            // If Account Code is "All" then ok, no need to filter
            if (this.AccountCode != Entities.Constants.AllDefaultValue)
            {
                if (item.TemplateID == IDocumentConstants.TemplateId)
                {
                    // If document Account Code does not match the selected Report code then remove from list
                    if (string.IsNullOrEmpty(WebUtil.ParseUrlParameters(item[IDocumentConstants.AccountListFieldId]).Get(this.AccountCode)))
                    {
                        return false;
                    }
                }

                if (item.TemplateID == IFormConstants.TemplateId)
                {
                    if (this.AccountCode != item[IFormConstants.AccountCodeFieldId])
                    {
                        return false;
                    }
                }
            }

            // If Holding Account Code is "All" then ok, no need to filter
            if (this.HoldingAccountCode != Entities.Constants.AllDefaultValue)
            {
                // If document Account Code does not match the selected Report code then remove from list
                if (this.HoldingAccountCode != item[IDocumentConstants.HoldingAccountCodeFieldId])
                {
                    return false;
                }
            }

            // If Report Name Code is "All" then ok, no need to filter
            if (this.ProductCode != Entities.Constants.AllDefaultValue)
            {
                // If document report name does not match the selected Account code then remove from list
                if (this.ProductCode != item[IDocumentConstants.ProductCodeFieldId])
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// This is called when has made a selection in the Report Name Drop down list.
        /// This method should be named with the value in "Entities.Constants.ReportNameRegistry"_Change
        /// </summary>
        protected void ReportName_Change()
        {
            this.ReportNameCode = this.GetWorkboxFormValue(Entities.Constants.ReportNameRegistry);
            this.Refresh();
        }

        /// <summary>
        /// This is called when has made a selection in the Account Drop down list.
        /// This method should be named with the value in "Entities.Constants.AccountRegistry"_Change
        /// </summary>
        protected void Account_Change()
        {
            this.AccountCode = this.GetWorkboxFormValue(Entities.Constants.AccountRegistry);
            this.Refresh();
        }

        /// <summary>
        /// This is called when has made a selection in the Holding Account Drop down list.
        /// This method should be named with the value in "Entities.Constants.HoldingAccountRegistry"_Change
        /// </summary>
        protected void HoldingAccount_Change()
        {
            this.HoldingAccountCode = this.GetWorkboxFormValue(Entities.Constants.HoldingAccountRegistry);
            this.Refresh();
        }

        /// <summary>
        /// This is called when has made a selection in the Product Drop down list.
        /// This method should be named with the value in "Entities.Constants.ProductRegistry"_Change
        /// </summary>
        protected void Product_Change()
        {
            this.ProductCode = this.GetWorkboxFormValue(Entities.Constants.ProductRegistry);
            this.Refresh();
        }

        private string GetWorkboxFormValue(string propertyId)
        {
            return Context.ClientPage.ClientRequest.Form[propertyId];
        }

        private string GetRegistryStringValue(string registryName)
        {
            return Registry.GetString(string.Format("{0}/{1}", Entities.Constants.WorkboxRegistryPath, registryName), Entities.Constants.AllDefaultValue);
        }

        private void SetRegistryStringValue(string registryName, string value)
        {
            Registry.SetString(string.Format("{0}/{1}", Entities.Constants.WorkboxRegistryPath, registryName), value);
        }

        /// <summary>
        /// The send.
        /// </summary>
        protected override void Send(Message message)
        {
            this.IfPublishCommandQueueAndPublish(message, this.SendEx);
        }

        /// <summary>
        /// The send all.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        protected override void SendAll(Message message)
        {
            this.IfPublishCommandQueueAndPublish(message, this.SendAllEx);
        }

        /// <summary>
        /// The send selected.
        /// </summary>
        protected override void SendSelected(Message message)
        {
            this.IfPublishCommandQueueAndPublish(message, this.SendSelectedEx);
        }

        private void IfPublishCommandQueueAndPublish(Message message, Func<Message, ICollection<Item>> sendOperation)
        {
            Assert.ArgumentNotNull(message, "message");

            SheerResponse.Alert(
                "Publishing process initiated. This may take some time please allow for the page to refresh.",
                new string[0]);

            // Execute the command
            var items = sendOperation(message);

            if (this.IsPublishCommand(message) && items != null && items.Any())
            {
                this.QueueAndPublishItems(items);
            }
        }

        private bool IsPublishCommand(Message message)
        {
            var command = new Guid(message["command"]);
            return command == Constants.System.Workflows.QICPortalPublishingWorkflow.New.Publish.ItemID.Guid
                   || command
                   == Constants.System.Workflows.QICPortalPublishingWorkflow.New.PublishWithoutNotification
                          .ItemID.Guid;
        }

        private void QueueAndPublishItems(ICollection<Item> items)
        {
            // Publish all items at once
            try
            {
                DocumentPublisher.PublishDocuments(items);
            }
            catch (Exception ex)
            {
                Log.Error("Failed to perform custom bulk operation.", ex, this);
                SheerResponse.Alert("Failed to process one or more items, please try again.", new string[0]);
            }
        }
    }
}
