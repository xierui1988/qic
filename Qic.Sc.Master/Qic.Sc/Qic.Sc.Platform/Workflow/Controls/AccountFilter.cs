﻿using Qic.Sc.Interfaces.Core;

using System.Web.UI;
using Autofac;

using System.Linq;
using System.Collections.Generic;

namespace Qic.Sc.Platform.Workflow.Controls
{
    /// <summary>
    /// The Account Filter renders the Account Entities in a drop down, that should be fetched from Client Portal site.
    /// </summary>
    public class AccountFilter : BaseDropDown
    {
        /// <summary>
        /// This method is used to set the Filter label and the Report ID label
        /// </summary>
        public AccountFilter()
        {
            this.Label = Entities.Constants.AccountDisplay;
            this.PropertyIDName = Entities.Constants.AccountRegistry;
        }

        /// <summary>
        /// Its responsible for providing the data to Render the Account Filter
        /// The Core Item that triggers this is currently located at: /sitecore/content/Applications/Workbox/Ribbon/Home/QIC Filters/Account Filter
        /// </summary>
        /// <param name="output">The provided HTML Text Write Output</param>
        protected override void GenerateInsertOptions(HtmlTextWriter output)
        {
            // Get Account Code Selection
            string selectedAccount = this.GetSelectionValue(Entities.Constants.AccountRegistry);

            // Add the option of All
            output.AddInsertOptionHelper(Entities.Constants.AllDefaultValue, Entities.Constants.AllDefaultValue, selectedAccount == Entities.Constants.AllDefaultValue);

            // Populate the rest of the list
            using (var scope = ServiceLocator.BeginScopeFor(Entities.Constants.MasterDb))
            {
                var contentRepository = scope.Resolve<IMasterContentRepository>() as IContentRepository;

                using (var search = contentRepository.GetSearchService())
                {
                    var documents = search.QueryForWorkflowDocuments().ToArray().Select(d => contentRepository.MapEntity(d));
                    var uniqueAccounts = new Dictionary<string, string>();

                    // Go through List of Documents - in order to get it accounts
                    foreach (var doc in documents)
                    {
                        if (doc == null)
                        {
                            continue;
                        }

                        var accounts = doc.AccountList;
                        
                        if (accounts == null)
                        {
                            continue;
                        }

                        // Go through list of Codes
                        foreach (var code in accounts.AllKeys)
                        {
                            if (!string.IsNullOrEmpty(code) && !uniqueAccounts.ContainsKey(code))
                            {
                                uniqueAccounts.Add(code, accounts[code]);
                            }
                        }
                    }

                    // Now listing them
                    foreach (var account in uniqueAccounts)
                    {
                        output.AddInsertOptionHelper(account.Value, account.Key, selectedAccount == account.Key);
                    }
                }
            }
        }
    }
}
