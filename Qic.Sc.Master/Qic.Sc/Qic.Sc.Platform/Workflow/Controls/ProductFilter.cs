﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;

using Qic.Sc.Entities;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Qic.Sc.Services.Core;
using System.Web.UI;
using Qic.Sc.Interfaces.Core;

namespace Qic.Sc.Platform.Workflow.Controls
{
    /// <summary>
    /// The Product Filter renders the Report Entities in a drop down, that should be fetched from Client Portal site.
    /// </summary>
    public class ProductFilter : BaseDropDown
    {
         /// <summary>
        /// This method is used to set the Filter label and the Report ID label
        /// </summary>
        public ProductFilter()
        {
            this.Label = Entities.Constants.ProductDisplay;
            this.PropertyIDName = Entities.Constants.ProductRegistry;
        }

        /// <summary>
        /// Its responsible for providing the data to Render the Product Filter
        /// The Core Item that triggers this is currently located at: /sitecore/content/Applications/Workbox/Ribbon/Home/QIC Filters/Product Filter
        /// </summary>
        /// <param name="output">The provided HTML Text Write Output</param>
        protected override void GenerateInsertOptions(HtmlTextWriter output)
        {
            // Get Selected Value
            string selectedProduct = this.GetSelectionValue(Entities.Constants.ProductRegistry);

            // All
            output.AddInsertOptionHelper(Entities.Constants.AllDefaultValue, Entities.Constants.AllDefaultValue, selectedProduct == Entities.Constants.AllDefaultValue);

            // Populate all other Report Name Values
            using (var scope = ServiceLocator.BeginScopeFor(Entities.Constants.MasterDb))
            {
                var contentRepository = scope.Resolve<IContentRepository>();

                using (var search = contentRepository.GetSearchService())
                {
                    // Get the list of Account Names and codes = Key pair
                    var options = (from doc in search.QueryForWorkflowDocuments().ToArray()
                                   where !string.IsNullOrEmpty(doc.ProductCode) && !string.IsNullOrEmpty(doc.ProductName)
                                   group doc by doc.ProductCode into grp
                                   select new { Key = grp.Key, Value = grp.Max(d => d.ProductName) }).ToArray();

                    // Now listing them
                    foreach (var product in options)
                    {
                        output.AddInsertOptionHelper(product.Value, product.Key, selectedProduct == product.Key);
                    }
                }
            }
        }
    }
}
