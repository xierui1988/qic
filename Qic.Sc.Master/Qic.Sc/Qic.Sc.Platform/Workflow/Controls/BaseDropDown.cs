﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Shell.Web.UI.WebControls;
using Sitecore.Web.UI.HtmlControls;
using Sitecore.Web.UI.WebControls.Ribbons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace Qic.Sc.Platform.Workflow.Controls
{
    /// <summary>
    /// This Base Drop down class is meant to provide the functinality to create a drop down list in the workflow
    /// </summary>
    public abstract class BaseDropDown : RibbonPanel
    {
        /// <summary>
        /// This Property is used in Rendering the label of the Filter dropdown
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// This Property is used as the Id of the control and the Method Name Change.
        /// The Workbox should therefore implement a Method named "PropertyIDName"_Change <example>ReportName_Change</example>
        /// </summary>
        public string PropertyIDName { get; set; }

        /// <summary>
        /// Renders the panel.
        /// </summary>
        /// <param name="output">The output.</param><param name="ribbon">The ribbon.</param><param name="button">The button.</param><param name="context">The context.</param>
        public override void Render(HtmlTextWriter output, Ribbon ribbon, Item button, CommandContext context)
        {
            output.Write("<div class=\"scRibbonToolbarPanel\">");
            output.Write("<table class=\"scWorkboxPageSize\"><tr><td class=\"scWorkboxPageSizeLabel\">");
            output.Write(string.Format("{0}:", this.Label));
            output.Write("</td><td>");
            output.Write(string.Format("<select class=\"scWorkboxPageSizeCombobox\" id=\"{0}\" style=\"max-width: 175px;text-overflow: ellipsis;\" onchange='javascript:scForm.invoke(\"{0}_Change\")'>", this.PropertyIDName));

            this.GenerateInsertOptions(output);

            output.Write("</select>");
            output.Write("</td></tr></table>");
            output.Write("</div>");
        }

        /// <summary>
        /// This method is intended to be overriden in order to be able to insert all the Drop down options.
        /// Users should use the AddInsertOption Method to render all the options.
        /// </summary>
        /// <param name="output">The HTML output needed to render all the options.</param>
        protected abstract void GenerateInsertOptions(HtmlTextWriter output);

        /// <summary>
        /// Calls the registry and registers the value
        /// </summary>
        /// <param name="registryName">The name of the value registered</param>
        /// <returns>The value of the registry</returns>
        protected string GetSelectionValue(string registryName)
        {
            return Registry.GetString(string.Format("{0}/{1}", Entities.Constants.WorkboxRegistryPath, registryName), Entities.Constants.AllDefaultValue);
        }
    }
}
