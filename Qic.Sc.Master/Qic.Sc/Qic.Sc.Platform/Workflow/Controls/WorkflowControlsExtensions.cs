﻿using Qic.Sc.Interfaces.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using Qic.Sc.Services.Core;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;

namespace Qic.Sc.Platform.Workflow.Controls
{
    /// <summary>
    /// The Extension Helper classes to populate dropdowns in workbox
    /// </summary>
    public static class WorkflowControlsExtensions
    {
        /// <summary>
        /// Creates another option in the drop down.
        /// </summary>
        /// <param name="output">The HtmlWriter</param>
        /// <param name="name">What is visually displayed</param>
        /// <param name="value">The Value of the option</param>
        /// <param name="selected">The selection</param>
        public static void AddInsertOptionHelper(this HtmlTextWriter output, string name, string value, bool selected)
        {
            output.Write(string.Format("<option value=\"{1}\"{2}>{0}</option>", name, value, (selected ? " selected=\"selected\"" : string.Empty)));
        }

        /// <summary>
        /// Gets the Documents to populate the reports
        /// </summary>
        /// <param name="search">The Search Service to quickly retrieve data</param>
        /// <returns>The filtered workbox list of documents</returns>
        public static IQueryable<DocumentEntity> QueryForWorkflowDocuments(this IContentSearchService search)
        {
            var query = search.GetDescendantsQueryable<DocumentEntity>(Entities.Constants.Content.ClientPortal.Documents.ItemID.Guid);

            // WHERE: Document library contains folder items as well as documents, filter by document.
            query = query.Where(d => d.EntityTemplateId == IDocumentConstants.TemplateId.Guid);

            DateTime validAfter = DateTime.Now.AddDays(Entities.Constants.PublishDayListFromNow);
            
            // WHERE: Document library is in New  or Published (in 1 day)
            query = query.Where(d => d.WorkflowState == Entities.Constants.System.Workflows.QICPortalPublishingWorkflow.New.ID ||
                (d.WorkflowState == Entities.Constants.System.Workflows.QICPortalPublishingWorkflow.Published.ID && 
                d.IndexPublishedDate > validAfter));

            return query;
        }
    }
}
