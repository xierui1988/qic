﻿using Qic.Sc.Interfaces.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using Autofac;

namespace Qic.Sc.Platform.Workflow.Controls
{
    /// <summary>
    /// The Holding Account Filter renders the Holding Account Entities in a drop down, that should be fetched from Client Portal site.
    /// </summary>
    public class HoldingAccountFilter : BaseDropDown
    {
        /// <summary>
        /// This method is used to set the Filter label and the Report ID label
        /// </summary>
        public HoldingAccountFilter()
        {
            this.Label = Entities.Constants.HoldingAccountDisplay;
            this.PropertyIDName = Entities.Constants.HoldingAccountRegistry;
        }

        /// <summary>
        /// Its responsible for providing the data to Render the Account Filter
        /// The Core Item that triggers this is currently located at: /sitecore/content/Applications/Workbox/Ribbon/Home/QIC Filters/Holding Account Filter
        /// </summary>
        /// <param name="output">The provided HTML Text Write Output</param>
        protected override void GenerateInsertOptions(HtmlTextWriter output)
        {
            // Get Account Code Selection
            string selectedHoldingAccount = this.GetSelectionValue(Entities.Constants.HoldingAccountRegistry);

            // Add the option of All
            output.AddInsertOptionHelper(Entities.Constants.AllDefaultValue, Entities.Constants.AllDefaultValue, selectedHoldingAccount == Entities.Constants.AllDefaultValue);

            // Populate the rest of the list
            using (var scope = ServiceLocator.BeginScopeFor(Entities.Constants.MasterDb))
            {
                var contentRepository = scope.Resolve<IContentRepository>();

                using (var search = contentRepository.GetSearchService())
                {
                    // Get the list of Account Names and codes = Key pair
                    var options = (from doc in search.QueryForWorkflowDocuments().ToArray()
                                   where !string.IsNullOrEmpty(doc.HoldingAccountCode) && !string.IsNullOrEmpty(doc.HoldingAccountName)
                                   group doc by doc.HoldingAccountCode into grp
                                   select new { Key = grp.Key, Value = grp.Max(d => d.HoldingAccountName) }).ToArray();

                    // Now listing them
                    foreach (var holdingAccount in options)
                    {
                        output.AddInsertOptionHelper(holdingAccount.Value, holdingAccount.Key, selectedHoldingAccount == holdingAccount.Key);
                    }
                }
            }
        }
    }
}
