﻿namespace Qic.Sc.Platform.Mvc
{
    using System;
    using System.Collections.Specialized;
    using System.Globalization;
    using System.Web;
    using System.Web.Mvc;

    using Sitecore.Mvc.Presentation;

    /// <summary>
    /// Value provider factory for enabling Sitecore renderings to access values conventionally available in ASP.NET MVC.
    /// </summary>
    public abstract class RenderingValueProviderFactoryBase : ValueProviderFactory
    {
        protected static readonly IValueProvider EmptyProvider =
            new NameValueCollectionValueProvider(new NameValueCollection(0), CultureInfo.CurrentCulture);

        /// <summary>
        /// Returns a value-provider object for the specified controller context.
        /// </summary>
        /// <returns>
        /// A value-provider object.
        /// </returns>
        /// <param name="controllerContext">An object that encapsulates information about the current HTTP request.</param>
        public override IValueProvider GetValueProvider(ControllerContext controllerContext)
        {
            if (controllerContext == null)
            {
                throw new ArgumentNullException("controllerContext");
            }

            // If not enough context to build a provider, give up and return
            // an empty provider to keep MVC happy.
            var renderingContext = RenderingContext.CurrentOrNull;
            if (renderingContext == null || renderingContext.Rendering == null)
            {
                return EmptyProvider;
            }

            // Build a provider based on current context, if provider for the current
            // context can't be build return an empty provider to keep MVC happy.
            return this.GetValueProvider(controllerContext.HttpContext, renderingContext) ?? EmptyProvider;
        }

        /// <summary>
        /// Get a concrete rendering provider based on provided context.
        /// Parameters are guaranteed to be not null.
        /// <returns>Implementers are advices to return null if provider can't be constructed.</returns>
        /// </summary>
        protected abstract IValueProvider GetValueProvider(HttpContextBase httpContext, RenderingContext rendering);
    }
}
