﻿namespace Qic.Sc.Platform.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    using Sitecore.Data.Items;
    using Sitecore.Mvc.Presentation;
    using Sitecore.StringExtensions;

    /// <summary>
    /// Value provider factory for enabling Sitecore renderings to access values conventionally available in ASP.NET MVC.
    /// </summary>
    public class RenderingRouteDataValueProviderFactory : RenderingValueProviderFactoryBase
    {
        /// <summary>
        /// Registers data provider with MVC runtime.
        /// <remarks>
        /// Caller must ensure this code is called exactly once.
        /// </remarks>
        /// </summary>
        internal static void Register()
        {
            ValueProviderFactories.Factories.Add(new RenderingRouteDataValueProviderFactory());
        }

        /// <summary>
        /// Gets an MVC value provider driven by a context for a given rendering.
        /// </summary>
        /// <param name="httpContext">The http context.</param>
        /// <param name="renderingContext">The rendering.</param>
        /// <returns>The MVC RenderingContext.</returns>
        protected override IValueProvider GetValueProvider(HttpContextBase httpContext, RenderingContext renderingContext)
        {
            // If not enough context to build a provider, give up.
            if (renderingContext.Rendering.RenderingItem == null)
            {
                return null;
            }

            // Build a provider based on current context, if provider for the current
            // context can't be build return an empty provider to keep MVC happy.
            return GetRenderingValueProvider(httpContext, renderingContext.Rendering.RenderingItem.InnerItem);
        }

        #region Rendering value provider implementation

        /// <summary>
        /// Matches configured route information to the current HTTP context. If a match is found
        /// a value provider is returned, null otherwise.
        /// </summary>
        private static IValueProvider GetRenderingValueProvider(HttpContextBase httpContext, Item renderingItem)
        {
            foreach (var route in GetConfiguredRoutes(renderingItem["routes"], renderingItem))
            {
                var values = route.GetRouteData(httpContext);
                if (values != null)
                {
                    // Stop on the first match. Routs should be configured in specific to generic order,
                    // as you would in normal MVC application.
                    return new DictionaryValueProvider<object>(values.Values, CultureInfo.CurrentCulture);
                }
            }

            // No routs matched.
            return null;
        }

        /// <summary>
        /// Convert configured routes into <see cref="Route"/> instances.
        /// Configured routes are assumed to be a delimited string of individual routes.
        /// Most common delimiter is NewLine, other supported route delimiters are - ";,|".
        /// </summary>
        private static IEnumerable<Route> GetConfiguredRoutes(string configuredRoutes, Item renderingItem)
        {
            if (string.IsNullOrEmpty(configuredRoutes))
            {
                // No routes configured.
                return Enumerable.Empty<Route>();
            }

            const string Separators = ";,|\n\r"; // Any of these characters can be used to delimit routes
            var routeUrls = configuredRoutes.Split(Separators.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            var routes = new List<Route>(routeUrls.Length);
            foreach (var routeUrl in routeUrls)
            {
                try
                {
                    routes.Add(new Route(routeUrl, null));
                }
                catch (Exception ex)
                {
                    throw new ApplicationException(
                        "Route '{0}' of rendering '{1}', is invalid.".FormatWith(routeUrl, renderingItem.ID),
                        ex);
                }
            }

            // Configured routes, could be empty if none of the configured
            // routes were valid.
            return routes;
        }

        #endregion
    }
}
