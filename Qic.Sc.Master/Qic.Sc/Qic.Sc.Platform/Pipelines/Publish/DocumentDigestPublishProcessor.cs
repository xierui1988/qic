﻿namespace Qic.Sc.Platform.Pipelines.Publish
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mail;
    using System.Text;

    using Autofac;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;

    using Sitecore.Data.Managers;
    using Sitecore.Publishing.Pipelines.Publish;

    /// <summary>
    /// The document digest publish processor generates a summary audit of the
    /// publishing job.
    /// </summary>
    public class DocumentDigestPublishProcessor : PublishProcessor
    {
        /// <summary>
        /// Invoked by sitecore during publishing.
        /// </summary>
        public override void Process(PublishContext context)
        {
            if (context == null)
            {
                return; // not enough context
            }

            var options = context.PublishOptions as DocumentPublishOptions;
            if (options == null)
            {
                return; // not a document publish
            }

            // Digest stats
            var statistics = this.CreateDigestMessage(options);

            // Create a publication digest
            new AuditMetadata
                {
                    Action = EnumUtils<SitecoreUserAction>.GetDescription(SitecoreUserAction.DocumentPublishedSummary),
                    Db = Constants.MasterDb,
                    Id = Constants.Content.ClientPortal.Documents.ID,
                    Language = LanguageManager.DefaultLanguage.Name,
                    Miscellaneous = statistics,
                    Path = Constants.Content.ClientPortal.Documents.Path,
                    UserName = Sitecore.Context.User.Name
                }.Audit();

            // If there are failures, raise notification
            var failed = options.Documents.Values.Where(d => d.Action == SitecoreUserAction.DocumentNotPublishedAction).ToArray();
            if (failed.Any())
            {
                this.SendFailedDigestNotification(statistics, failed);
            }
        }

        private void SendFailedDigestNotification(string statistics, IEnumerable<DocumentPublishResult> failed)
        {
            var text = new StringBuilder("ATTENTION!").AppendLine()
                .AppendLine("Some of the documents failed to publish. Publishing summary is as follows:").AppendLine()
                .Append("\t").AppendLine(statistics).AppendLine().AppendLine()
                .AppendLine("Failed documents details:");

            using (var scope = ServiceLocator.BeginScopeForMasterDatabase())
            {
                foreach (var doc in failed)
                {
                    text.AppendLine("FAILED")
                        .AppendFormat("\tDocument: \"{0}\"", doc.DocumentName).AppendLine()
                        .AppendFormat("\tClients: ({0})", doc.DocumentClients).AppendLine()
                        .AppendFormat("\tPath: \"{0}\"", doc.DocumentPath)
                        .AppendLine().AppendLine();
                }

                var config = scope.Resolve<IConfigurationService>();
                var message = new MailMessage
                                  {
                                      Subject = "Portal publishing failure summary",
                                      IsBodyHtml = false,
                                      Priority = MailPriority.High,
                                      Body = text.ToString(),
                                      From = new MailAddress(config.GetGlobalSetting("ContactFromEmailAddress")),
                                  };

                message.To.Add(new MailAddress(config.GetGlobalSetting("OperationsEmailAddress")));
                scope.Resolve<IEmailService>().SendMessageDirectly(message);
            }
        }

        private string CreateDigestMessage(DocumentPublishOptions options)
        {
            return string.Format(
                "Total Document Processed({2}), Published Documents Count({0}), Failed Documents Count({1}).",
                options.Documents.Values.Count(o => o.Action != SitecoreUserAction.DocumentNotPublishedAction),
                options.Documents.Values.Count(o => o.Action == SitecoreUserAction.DocumentNotPublishedAction),
                options.Documents.Count);
        }
    }
}
