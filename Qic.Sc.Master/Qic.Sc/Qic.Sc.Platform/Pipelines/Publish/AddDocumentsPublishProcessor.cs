﻿namespace Qic.Sc.Platform.Pipelines.Publish
{
    using Sitecore.Data;
    using Sitecore.Publishing.Pipelines.Publish;

    /// <summary>
    /// The document digest publish processor generates a summary audit of the
    /// publishing job.
    /// </summary>
    public class AddDocumentsPublishProcessor : PublishProcessor
    {
        /// <summary>
        /// Invoked by sitecore during publishing.
        /// </summary>
        public override void Process(PublishContext context)
        {
            if (context == null)
            {
                return; // not enough context
            }

            var options = context.PublishOptions as DocumentPublishOptions;
            if (options == null)
            {
                return; // not a document publish
            }

            foreach (var doc in options.Documents.Values)
            {
                context.Queue.Add(new[] { new PublishingCandidate(new ID(doc.Id), options) });
            }
        }
    }
}
