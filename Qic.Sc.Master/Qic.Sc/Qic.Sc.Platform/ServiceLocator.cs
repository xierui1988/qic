﻿using System;
using Autofac;

namespace Qic.Sc.Platform
{
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Services.Core;

    /// <summary>
    /// Exposes global application IoC container to the platform code as
    /// a service locator.
    /// </summary>
    public static class ServiceLocator
    {
        private static IContainer container;

        /// <summary>
        /// Initialize locator.
        /// </summary>
        /// <param name="container">An instance of the IoC container.</param>
        public static void SetLocator(IContainer container)
        {
            ServiceLocator.container = container;
        }

        /// <summary>
        /// Get current container instance.
        /// </summary>
        /// <returns>Returns scope bound to </returns>
        public static ILifetimeScope BeginScopeForCurrentSite()
        {
            return GetContainer().BeginLifetimeScope();
        }

        /// <summary>
        /// Create a scope to resolve all dependencies withing a specified site context.
        /// </summary>
        /// <param name="siteName">
        /// Configured site name.
        /// </param>
        /// <returns>
        /// The <see cref="ILifetimeScope"/>.
        /// </returns>
        public static ILifetimeScope BeginScopeForSite(string siteName)
        {
            if (string.IsNullOrEmpty(siteName))
            {
                return BeginScopeForCurrentSite();
            }

            return GetContainer().BeginLifetimeScope(s => s.Register(c => new SiteService(siteName)).As<ISiteService>());
        }

        /// <summary>
        /// Resolve dependencies using a 'master' database.
        /// </summary>
        /// <returns>Get the scope bound to master database.</returns>
        public static ILifetimeScope BeginScopeForMasterDatabase()
        {
            // We are telling the IoC container to use a specific instance of IContentRespoitory
            // during dependency resolution. Our repository instance is hardwired to master database.
            // By default repository is resolved to the database of the current (context) site.
            return BeginScopeFor("master");
        }

        /// <summary>
        /// Resolve dependencies using a specific content repository (database).
        /// </summary>
        /// <returns>An instance of the requested type, or null if container is null.</returns>
        public static ILifetimeScope BeginScopeFor(string databaseName)
        {
            // By default content repository resolution is done based on the current site context.
            // To allow access to a specific database we overwrite contextual resolution to a pre
            // configured repository that is bound to the named database.
            return
                GetContainer().BeginLifetimeScope(
                    b => b.Register(c => c.ResolveNamed<IContentRepository>(databaseName)).As<IContentRepository>());
        }

        /// <summary>
        /// Resolve platform logger.
        /// </summary>
        public static ILoggerService ResolvePlatformLoggerService()
        {
            return GetContainer().Resolve<ILoggerService>();
        }

        private static IContainer GetContainer()
        {
            if (ServiceLocator.container == null)
            {
                throw new ApplicationException("Platform service locator have not been initialized.");
            }

            return ServiceLocator.container;
        }
    }
}
