﻿using System.Web.Security;

using Microsoft.Xrm.Sdk;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;
using Qic.Sc.Interfaces.Crm;
using Qic.Sc.Services.Crm;

namespace Qic.Sc.Services
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Reflection;
    using Autofac;
    using Autofac.Builder;
    using Autofac.Core;

    using Glass.Mapper.Sc;

    using Microsoft.Xrm.Client;

    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.EconomicUpdate;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.InvestmentInsight;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.MediaRelease;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Services.Components;
    using Qic.Sc.Services.Components.Decorators;
    using Qic.Sc.Services.Core;
    using Qic.Sc.Entities;
    using Qic.Sc.Services.Crm.Decorators;

    /// <summary>
    /// Dependency Injection registration and configuration.
    /// </summary>
    public class Module : Autofac.Module
    {
        /// <summary>
        /// Dependency injection configuration. Add services exposed by the current assembly
        /// to the container.
        /// </summary>
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<BackgroundService>().As<IBackgroundService>().SingleInstance();

            // Site context aware configuration service.
            builder.RegisterType<SiteService>().As<ISiteService>();
            builder.RegisterType<ConfigurationService>().As<IConfigurationService>();
            builder.Register(c => new LoggerService("Qic.Sc")).As<ILoggerService>();

            // Register site context repository, a repository will be bound to a database
            // of the current site at runtime.
            builder.Register(c => SitecoreContextFactory.GetSitecoreContext()).As<ISitecoreService>();
            builder.RegisterType<SitecoreRepository>().As<IContentRepository>();

            // Register a specific repository instance always bound to master database. This is a named
            // instance as such it require the name passed in at resolution time.
            //  E.g. IConteainer.ResolveNamed<IContentRepository>("master");
            builder.Register(
                c => (IContentRepository) new SitecoreRepository(SitecoreContextFactory.GetSitecoreContext(databaseName: Constants.MasterDb)))
                .Named<IContentRepository>(Constants.MasterDb);

            // Content repository directly mapped to master.
            builder.Register(
                c => new SitecoreRepository(SitecoreContextFactory.GetSitecoreContext(databaseName: Constants.MasterDb)))
                .As<IMasterContentRepository>();

            builder.RegisterType<AuditService>().As<IAuditService>();
            builder.RegisterType<HmacTokenService>().As<ITokenService>();

            builder.Register(
                c =>
                    {
                        try
                        {
                            // Sitecore OOTB CRM connector uses a different connection string format
                            // to the one used by CRM XRM Client. Translate it into format that client understands.
                            var settings =
                                ConfigurationManager.ConnectionStrings["CRMConnString"].ConnectionString.Split(';')
                                    .Select(setting => setting.Split('='))
                                    .ToDictionary(kvp => kvp[0], kvp => kvp[1]);

                            var connectionString = string.Format(
                                "Url={0};Username={1};Password={2}",
                                new Uri(settings["CRM:url"]).GetLeftPart(UriPartial.Authority),
                                settings["user id"],
                                settings["password"]);

                            return new ConnectionStringSettings("CRMConnString-XRM", connectionString);
                        }
                        catch (Exception ex)
                        {
                            throw new ApplicationException(
                                "Failed to initiate CRM connection, bad connection string configuration.",
                                ex);
                        }
                    }).As<ConnectionStringSettings>().SingleInstance();

            // XRM client configuration
            builder.Register(c => new LazyOrganizationServiceDecorator(new CrmConnection(c.Resolve<ConnectionStringSettings>())))
                .As<IOrganizationService>();

            // Register navigation service and decorators
            builder.RegisterServiceDecorator<NavigationService, INavigationService>(
                (c, service) => new NavigationServiceTracingDecorator(service));

            builder.RegisterServiceDecorator<ReportService, IReportService>(
                (c, service) =>
                new ReportServiceTracingDecorator(
                    new ReportServiceWithUserMetadataDecorator(service, c.Resolve<IWebSecurityService>(), c.Resolve<IPortalWebSecurityService>())));

            builder.RegisterServiceDecorator<FormService, IFormService>(
                (c, service) =>
                new FormServiceTracingDecorator(
                    new FormServiceUserMetadataDecorator(service, c.Resolve<IWebSecurityService>())));

            builder.RegisterServiceDecorator<CrmService, ICrmService>(
                (c, service) => new CrmServiceTracingDecorator(new CrmServiceChacheDecorator(service)));

            builder.RegisterServiceDecorator<EmailService, IEmailService>(
                (c, service) => new EmailServiceTracingDecorator(service));

            builder.RegisterServiceDecorator<PortalEmailService, IPortalEmailService>(
                (c, service) => new PortalEmailServiceTracingDecorator(service));

            builder.Register(c => Membership.Provider).As<MembershipProvider>().SingleInstance();

            builder.RegisterServiceDecorator<PortalWebSecurityService, IPortalWebSecurityService>(
                (c, service) =>
                new PortalWebSecurityServiceTracingDecorator(new PortalWebSecurityServiceSessionCacheDecorator(service)))
                .As<IPortalWebSecurityService>();

            builder.RegisterServiceDecorator<WebSecurityService, IWebSecurityService>(
                (c, service) =>
                new WebSecurityServiceTracingDecorator(new WebSecurityServiceSessionCacheDecorator(service)));

            // Dynamic links generation.
            builder.RegisterType<DefaultLinkService>().As<ILinkService>().InstancePerLifetimeScope();

            builder.RegisterType<EditorialLinkService>()
                .Named<ILinkService>(IInvestmentInsightConstants.TemplateIdString);

            builder.RegisterType<EditorialLinkService>()
                .Named<ILinkService>(IMediaReleaseConstants.TemplateIdString);

            builder.RegisterType<EditorialLinkService>()
                .Named<ILinkService>(IEconomicUpdateConstants.TemplateIdString);

            builder.RegisterType<DocumentLinkService>()
                .Named<ILinkService>(IDocumentConstants.TemplateIdString);

            builder.RegisterType<FormLinkService>()
                .Named<ILinkService>(IFormConstants.TemplateIdString);

            builder.RegisterType<DocumentBaseEmailDispatchService>().AsSelf();
            builder.RegisterType<NotificationEmailDispatchService>().AsSelf();

            base.Load(builder);
        }

        #region Container extensions

        /// <summary>
        /// The attach to component registration. Extended to create custom loggers for every
        /// class its injected into.
        /// </summary>
        /// <param name="componentRegistry">
        /// The component registry.
        /// </param>
        /// <param name="registration">
        /// The registration.
        /// </param>
        protected override void AttachToComponentRegistration(IComponentRegistry componentRegistry, IComponentRegistration registration)
        {
            // Handle constructor parameters.
            registration.Preparing += OnComponentPreparing;

            // Handle properties.
            registration.Activated += (sender, e) => InjectLoggerProperties(e.Instance);
        }

        private static void OnComponentPreparing(object sender, PreparingEventArgs e)
        {
            // Register ILogger resolver
            var t = e.Component.Activator.LimitType;
            e.Parameters =
                e.Parameters.Union(
                    new[]
                    {
                        new ResolvedParameter((p, i) => p.ParameterType == typeof(ILoggerService), (p, i) => new LoggerService(t))
                    });
        }

        /// <summary>
        /// To improve performance we can look at caching logger instances.
        /// </summary>
        private static void InjectLoggerProperties(object instance)
        {
            var instanceType = instance.GetType();

            // Get all the injectable properties to set.
            // If you wanted to ensure the properties were only UNSET properties,
            // here's where you'd do it.
            var properties = instanceType
              .GetProperties(BindingFlags.Public | BindingFlags.Instance)
              .Where(p => p.PropertyType == typeof(ILoggerService) && p.CanWrite && p.GetIndexParameters().Length == 0);

            // Set the properties located.
            foreach (var propToSet in properties)
            {
                propToSet.SetValue(instance, new LoggerService(instanceType), null);
            }
        }
        #endregion
    }

    #region ContainerBuilder extension methods
    internal static class ContainerBuilderExtensions
    {
        /// <summary>
        /// Packaging reregistration of a decorator into a single call on the container.
        /// </summary>
        public static IRegistrationBuilder<TServiceInterface, Autofac.Features.LightweightAdapters.LightweightAdapterActivatorData, DynamicRegistrationStyle> RegisterServiceDecorator<TService, TServiceInterface>(this ContainerBuilder builder, Func<IComponentContext, TServiceInterface, TServiceInterface> decorator)
        {
            builder.RegisterType<TService>().Named<TServiceInterface>("service");
            return builder.RegisterDecorator(decorator, "service");
        }
    }
    #endregion
}
