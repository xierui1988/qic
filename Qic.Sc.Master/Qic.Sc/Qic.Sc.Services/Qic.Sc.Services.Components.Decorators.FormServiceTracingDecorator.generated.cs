// <auto-generated />
#pragma warning disable 1591
#region T4Decorator

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Qic.Sc.Services.Components.Decorators
{
    [GeneratedCode("T4Decorator", "1.0"), DebuggerNonUserCode]
    internal partial class FormServiceTracingDecorator : Qic.Sc.Services.Generated.ITracingContextFactory
    {

        public void CreateForm(Qic.Sc.Entities.Metadata.FormMetadata formName)
        {
            using (this.CreateTracingContext("Qic.Sc.Interfaces.Components.IFormService.CreateForm(formName)"))
            {
                this.formService.CreateForm(formName);
            }
        }

        public Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form.IFormBlob GetForm(string idOrPath)
        {
            using (this.CreateTracingContext("Qic.Sc.Interfaces.Components.IFormService.GetForm(idOrPath)"))
            {
                return this.formService.GetForm(idOrPath);
            }
        }

        public System.Collections.Generic.IEnumerable<Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form.IForm> GetFormsBy(string username, int skip, int take, out int total)
        {
            using (this.CreateTracingContext("Qic.Sc.Interfaces.Components.IFormService.GetFormsBy(username, skip, take, out total)"))
            {
                return this.formService.GetFormsBy(username, skip, take, out total);
            }
        }

    }
}
#endregion T4Decorator
#pragma warning restore 1591
