﻿namespace Qic.Sc.Services
{
    using System;
    using System.Configuration;

    using Glimpse.Core.Extensibility;
    using Glimpse.Core.Message;

    using Qic.Sc.Services.Generated;

    internal abstract class TracingBaseDecorator : ITracingContextFactory
    {
        public IDisposable CreateTracingContext(string name)
        {
            return new GlimpseStopwatch(name);
        }

        #region Performance stop watch

        /// <summary>
        /// Stop watch readings can be unreliable if the code being times
        /// is passed between CPU cores. (http://kristofverbiest.blogspot.com.au/2008/10/beware-of-stopwatch.html)
        /// </summary>
        private class GlimpseStopwatch : IDisposable
        {
            private static readonly bool IsEnabled =
                Convert.ToBoolean(ConfigurationManager.AppSettings["Qic.Sc.PerformanceTracingEnabled"] ?? "false");

            private readonly string name;
            private readonly IExecutionTimer timer;
            private readonly IMessageBroker broker;
            private readonly bool isActive;
            private readonly TimeSpan offset;

            public GlimpseStopwatch(string name)
            {
                if (name == null)
                {
                    throw new ArgumentNullException("name");
                }

                this.name = name;

                if (IsEnabled)
                {
                    try
                    {
#pragma warning disable 618
//// This is hack untill v2 of Glimpse is out
//// Read a SO reply by one of the authors of Glimpse
//// http://stackoverflow.com/questions/18721058/how-to-display-timeline-information-for-routines-used-by-controllers-in-glimpse
                        this.broker = Glimpse.Core.Framework.GlimpseConfiguration.GetConfiguredMessageBroker();
                        if (this.broker == null)
                        {
                            return;
                        }
                        var getTimer = Glimpse.Core.Framework.GlimpseConfiguration.GetConfiguredTimerStrategy();
                        if (getTimer == null)
                        {
                            return;
                        }
                        this.timer = getTimer();
                        if (this.timer == null)
                        {
                            return;
                        }
#pragma warning restore 618

                        this.offset = this.timer.Start();
                        this.isActive = true;
                    }
                    catch
                    {
                        this.isActive = false;
                    }
                }
            }

            public void Dispose()
            {
                if (IsEnabled && this.isActive)
                {
                    try
                    {
                        this.broker.Publish(new TimelineMessage(this.name, this.timer.Stop(this.offset)));
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }

        /// <summary>
        /// Glimpse time-line data structure. 
        /// </summary>
        public class TimelineMessage : ITimelineMessage
        {
            private static readonly TimelineCategoryItem DefaultCategory = new TimelineCategoryItem("Qic.Sc.Services", "brown", "blue");

            public TimelineMessage(string eventName, TimerResult result)
            {
                this.Id = Guid.NewGuid();
                this.EventName = eventName;
                this.EventCategory = DefaultCategory;
                this.Offset = result.Offset;
                this.StartTime = result.StartTime;
                this.Duration = result.Duration;
            }

            public Guid Id { get; private set; }
            public TimeSpan Offset { get; set; }
            public TimeSpan Duration { get; set; }
            public DateTime StartTime { get; set; }
            public string EventName { get; set; }
            public TimelineCategoryItem EventCategory { get; set; }
            public string EventSubText { get; set; }
        }

        #endregion
    }
}
