﻿namespace Qic.Sc.Services.Core
{
    using System;
    using System.Linq;

    using Qic.Sc.Entities;
    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// Extensions for <see cref="IQueryable"/> interface to help with searching.
    /// </summary>
    public static class QueriableSerchExtensions
    {
        /// <summary>
        /// Gets a query that will search only withing descendent's of an item identified by.
        /// Please set ToArray before mapping from the return of ByDigitalPropertyQuery when creating a new service.
        /// <paramref name="contextItemId"/>.
        /// </summary>
        /// <typeparam name="TEntity">Type of entities returned/manipulated by the query.</typeparam>
        /// <param name="self">An instance of a search context, must not be NULL.</param>
        /// <param name="contextItemId">
        ///     The id of the context entity, query will be limited to 
        ///     the descendants of this item.
        /// </param>
        /// <returns>An instance of a query that can be executed to fetch data from the index.</returns>
        public static IQueryable<TEntity> GetDescendantsQueryable<TEntity>(this IContentSearchService self, Guid contextItemId) where TEntity : GlassBase
        {
            if (self == null)
            {
                throw new ArgumentNullException("self");
            }

            // Get items that have current container as a parent (children) or as a part of the
            // path (children's children) excluding container itself.
            return from i in self.GetQueryable<TEntity>()
                   where
                       (i.EntityParent == contextItemId || i.EntityPath.Contains(contextItemId))
                       && i.EntityId != contextItemId
                   select i;
        }
    }
}
