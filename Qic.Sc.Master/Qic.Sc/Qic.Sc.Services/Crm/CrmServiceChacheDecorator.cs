﻿namespace Qic.Sc.Services.Crm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Runtime.Caching;

    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Crm;

    internal partial class CrmServiceChacheDecorator : ICrmService
    {
        private readonly ICrmService crmService;

        public CrmServiceChacheDecorator(ICrmService crmService)
        {
            this.crmService = crmService;
        }

        public IEnumerable<ReportTypeMetadata> GetReportTypes()
        {
            const string Key = "Qic.Sc.Services.Crm.CrmServiceChacheDecorator.GetReportTypes()";
            var reportTypes = MemoryCache.Default.Get(Key) as ICollection<ReportTypeMetadata>;
            if (reportTypes == null)
            {
                reportTypes = this.crmService.GetReportTypes().ToArray();

                // Cache for 5 minutes
                // TODO: Make configurable
                MemoryCache.Default.Set(Key, reportTypes, DateTimeOffset.UtcNow.AddMinutes(5));
            }

            return reportTypes;
        }

        public IEnumerable<ReportAccessMetadata> GetReportAccessForEmailBy(string entityCode, string reportCode, bool ignoreNotificationPreference)
        {
            // Get all report access recored in the system from cache or cache it
            const string Key = "Qic.Sc.Services.Crm.CrmServiceChacheDecorator.GetUserReportAccess(string entityCode, string reportCode, bool onlyNotification)";
            var access = MemoryCache.Default.Get(Key) as ICollection<ReportAccessMetadata>;
            if (access == null)
            {
                access = this.GetSuperUserReportAccess().ToArray();
                MemoryCache.Default.Set(Key, access, DateTimeOffset.UtcNow.AddMinutes(10));
            }

            // Apply filtering in memory, filtering takes less time then getting the data from CRM.
            return from acl in access
                   where
                       acl.ReportCode == reportCode
                       && (acl.EmailNotification || acl.EmailNotification != ignoreNotificationPreference)
                       && ((acl.AccountCode == entityCode && acl.HoldingAccountCode == null && acl.ProductCode == null)
                           || (acl.HoldingAccountCode == entityCode) || (acl.ProductCode == entityCode))
                   select acl;
        }
    }
}
