param (
    [parameter(Mandatory = $true, HelpMessage="A url or path to the SDK endpoint to contact for meta-data (E.g. http://localhost/Organization1/XRMServices/2011/Organization.svc).")]
    [string] $Url,

    [parameter(Mandatory = $true, HelpMessage="Username to use when connecting to the server for authentication.")]
    [string] $User,

    [parameter(Mandatory = $true, HelpMessage="Password to use when connecting to the server for authentication.")]
    [string] $Password,

    [parameter(Mandatory = $false, HelpMessage="The filename for the generated proxy code.")]
    [string] $Out = "Generated.cs",

    [parameter(Mandatory = $false, HelpMessage="The namespace for the generated proxy code.  The default namespace is derived from current and parent forlder name.")]
    [string] $Namespace = "$((Get-Item $PWD).Parent.Name).$((Get-Item $PWD).Name)",

	[parameter(Mandatory = $false, HelpMessage="The namespace for the generated proxy code.  The default namespace is derived from current and parent forlder name.")]
    [string] $ServiceContext = "CRMServiceContext",

    [parameter(Mandatory = $false, HelpMessage="Path to the CrmSvcUtil.exe")]
    [string] $CrmSvcUtilExe = '..\bin\coretools\CrmSvcUtil.exe'
)

# Check CRM tools are installed
if (-Not (Test-Path $CrmSvcUtilExe)) {
    error "CrmSvcUtil.exe tool can't be found, NuGet installed 'PM> Install-Package Microsoft.CrmSdk.CoreTools'."
}

# Generate proxy code
Write-Host "$CrmSvcUtilExe" "/url:$Url" "/out:$Out" "/namespace:$Namespace" "/language:CS" "/username:$User" "/password:$Password" "/serviceContextName:$ServiceContext"
&"$CrmSvcUtilExe" "/url:$Url" "/out:$Out" "/namespace:$Namespace" "/language:CS" "/username:$User" "/password:$Password" "/serviceContextName:$ServiceContext"