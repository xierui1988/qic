﻿using Microsoft.Xrm.Sdk;

namespace Qic.Sc.Services.Crm
{
    /// <summary>
    /// Is intended to help with CRM Entities and code readability
    /// </summary>
    public static class CrmServiceExtension
    {
        /// <summary>
        /// Get Entity name or default to null
        /// </summary>
        /// <param name="reference">The Xrm SDK EntityReference</param>
        /// <returns>The Entity Name or null</returns>
        public static string GetEntityNameOrDefault(this EntityReference reference)
        {
            if (reference == null)
            {
                return null;
            }

            return reference.Name;
        }
    }
}
