﻿namespace Qic.Sc.Services.Components
{
    using System;
    using System.Collections.Generic;
    using System.Web;

    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;

    internal partial class PortalWebSecurityServiceSessionCacheDecorator : IPortalWebSecurityService
    {
        private readonly IPortalWebSecurityService webSecurityService;

        public PortalWebSecurityServiceSessionCacheDecorator(IPortalWebSecurityService webSecurityService)
        {
            if (webSecurityService == null)
            {
                throw new ArgumentNullException("webSecurityService");
            }
            this.webSecurityService = webSecurityService;
        }

        public IEnumerable<ReportAccessMetadata> GetReportAccessFor(string username)
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new InvalidOperationException("Can't resolve user identity, HTTP context is empty.");
            }

            var acceesCacheKey = "__qic_respot_access:" + username; // Just to make it more unique, sessions are not shared.
            var access = context.Session[acceesCacheKey] as IEnumerable<ReportAccessMetadata>;
            if (access == null)
            {
                // Get user access from CRM and cache them for the session.
                access = this.webSecurityService.GetReportAccessFor(username);
                context.Session[acceesCacheKey] = access;
            }

            return access;
        }

        public IEnumerable<ReportTypeMetadata> GetAllReportTypes()
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new InvalidOperationException("Can't resolve user identity, HTTP context is empty.");
            }

            const string AcceesCacheKey = "__qic_all_reprottypes_all";
            var cached = context.Session[AcceesCacheKey] as IEnumerable<ReportTypeMetadata>;
            if (cached != null)
            {
                return cached;
            }

            cached = this.webSecurityService.GetAllReportTypes();
            context.Session[AcceesCacheKey] = cached;

            return cached;
        }

        public IEnumerable<ReportTypeMetadata> GetReportTypeMetadataFor(ICollection<string> accessibleReports)
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new InvalidOperationException("Can't resolve user identity, HTTP context is empty.");
            }

            var acceesCacheKey = "__qic_all_reprottypes_accessible" + string.Join("_", accessibleReports);
            var cached = context.Session[acceesCacheKey] as IEnumerable<ReportTypeMetadata>;
            if (cached != null)
            {
                return cached;
            }

            cached = this.webSecurityService.GetReportTypeMetadataFor(accessibleReports);
            context.Session[acceesCacheKey] = cached;

            return cached;
        }
    }
}
