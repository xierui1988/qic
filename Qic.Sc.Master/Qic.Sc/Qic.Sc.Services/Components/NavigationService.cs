﻿namespace Qic.Sc.Services.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Qic.Sc.Entities.UserDefined.Components.Site.Configuration.MenuItem;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Services.Components.Decorators;

    /// <summary>
    /// The navigation service implementation.
    /// </summary>
    public class NavigationService : INavigationService
    {
        private readonly IContentRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationService"/> class.
        /// </summary>
        /// <param name="repository">Content repository.</param>
        public NavigationService(IContentRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// Get menu items under a specified item. IRems will have active flag set
        /// based on the context item provided. Any menu item that points at the
        /// context item or one of its parents will be active.
        /// </summary>
        /// <param name="idOrPath">
        ///     Id or a path to an item which children are returned as menu items.
        /// </param>
        /// <param name="contextItemId">
        ///     Id or a path to the current context item use to resolve active menu items.
        ///     If this parameter is not provided menu items will not provide an implementation
        ///     for the <see cref="IMenuItem.IsActive"/> flag.
        /// </param>
        /// <returns>
        /// A collection of menu items under the specified node, or null if none found.
        /// </returns>
        public IEnumerable<IMenuItem> GetMenu(string idOrPath, Guid? contextItemId = null)
        {
            var menuItem = this.repository.GetEntity<MenuItemEntity>(idOrPath);
            if (menuItem == null || menuItem.SubItems == null || !menuItem.SubItems.Any())
            {
                throw new ApplicationException(string.Format("Navigation information not found : {0}", idOrPath));
            }

            if (contextItemId != null)
            {
                var parentArray = this.ParseContextPath(contextItemId.Value);
                return menuItem.SubItems.Select(i => new ActiveMenuEntityDecorator(this.repository, i, parentArray));
            }

            return menuItem.SubItems;
        }

        /// <summary>
        /// Gets the Context path (List of parent items and current item) from the context id
        /// </summary>
        /// <param name="contextId">Current item that we want the lineage of.</param>
        /// <returns> Lineage List</returns>
        private Guid[] ParseContextPath(Guid contextId)
        {
            using (var context = this.repository.GetSearchService())
            {
                // The reason we're hitting the index rather then the DB is because index
                // actually holds the list of GUIDs of all parent items of an item. It's simple
                // than walking the tree and building it ourself.
                var item = context.GetQueryable<MenuItemEntity>().FirstOrDefault(i => i.EntityId == contextId);
                return item != null ? item.EntityPath.ToArray() : new Guid[0];
            }
        }
    }
}
