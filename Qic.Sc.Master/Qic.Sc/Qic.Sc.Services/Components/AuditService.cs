﻿using log4net;
using Qic.Sc.Entities.Metadata;
using Qic.Sc.Interfaces.Components;

namespace Qic.Sc.Services.Components
{
    /// <summary>
    /// The Audit service implementation.
    /// </summary>
    public class AuditService : IAuditService
    {
        /// <summary>
        /// E.g. "AUDIT (sitecore\admin): Login";        
        /// </summary>
        private const string AuditUnformatPattern1 = "AUDIT ({0}): {1}";

        /// <summary>
        /// E.g. AUDIT (sitecore\admin): Restore: archive: recyclebin, id: e232f06d-ac70-4912-908e-91b284991cab
        /// </summary>
        private const string AuditUnformatPattern2 = "AUDIT ({0}): {1}: archive: recyclebin, id: {2}";
        
        /// <summary>
        /// E.g. "AUDIT (sitecore\admin): Save item: master:/sitecore/content/Home, language: en, version: 3, id: {110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}"
        /// </summary>
        private const string AuditUnformatPattern3 = "AUDIT ({0}): {1}: {2}:{3}, language: {4}, version: {5}, id: {6}";
        
        /// <summary>
        /// E.g. "AUDIT (sitecore\admin): Rename item : master:/sitecore/content/Home/P1 D New, language: en, version: 1, id: {54CFD505-A8B2-4A8A-A0C4-A358D9C21D06} to P1 D New Test"
        /// </summary>
        private const string AuditUnformatPattern4 = "AUDIT ({0}): {1}: {2}:{3}, language: {4}, version: {5}, id: {6} to {7}";
        
        /// <summary>
        /// E.g. AUDIT (sitecore\admin): Execute workflow command. Item: master:/sitecore/content/Home/KP Demo, language: ar-SY, version: 2, id: {C4311D97-3651-4776-AAED-82CF4C16034F}, command: /sitecore/system/Workflows/Sample Workflow/Awaiting Approval/Reject, previous state: Awaiting Approval, next state: /sitecore/system/Workflows/Sample Workflow/Draft, user: sitecore\admin
        /// </summary>
        private const string AuditUnformatPattern5 = "AUDIT ({0}): {1}. Item: {2}:{3}, language: {4}, version: {5}, id: {6}, command: {7}, previous state: {8}, next state: {9}, user: {10}";

        private readonly ILog auditLog;

        /// <summary>
        /// Initializes a new instance of the <see cref="AuditService"/> class.
        /// </summary>
        public AuditService()
        {
            this.auditLog = LogManager.GetLogger(typeof(AuditService));
        }

        /// <summary>
        /// This audit captures user name and message
        /// </summary>
        /// <param name="username">The user name, domain should be included</param>
        /// <param name="message">The message associated with user</param>
        public void Audit(string username, string message)
        {
            this.auditLog.Info(string.Format(AuditUnformatPattern1, username, message));
        }

        /// <summary>
        /// The Audit of a recycling archive action of an item
        /// </summary>
        /// <param name="username">The user name, domain should be included</param>
        /// <param name="recycleAction">The recycle action name e.g. Restore</param>
        /// <param name="id">Item id</param>
        public void Audit(string username, string recycleAction, string id)
        {
            this.auditLog.Info(string.Format(AuditUnformatPattern2, username, recycleAction, id));
        }

        /// <summary>
        /// The action of an item
        /// </summary>
        /// <param name="username">The user name, domain should be included</param>
        /// <param name="action">The action name taken</param>
        /// <param name="db">The database it occured in</param>
        /// <param name="path">The item path</param>
        /// <param name="language">The Item's language</param>
        /// <param name="version">The version of the item</param>
        /// <param name="id">The id of the item</param>
        public void Audit(string username, string action, string db, string path, string language, string version, string id)
        {
            this.auditLog.Info(string.Format(AuditUnformatPattern3, username, action, db, path, language, version, id));
        }

        /// <summary>
        /// Need to add
        /// </summary>
        /// <param name="message">String Message</param>
        public void Audit(string message)
        {
            this.auditLog.Info(string.Format("AUDIT ({0}): {1}. Item: {2}:{3}, language: {4}, version: {5}, id: {6}, command: {7}, previous state: {8}, next state: {9}, user: {10}", new object[] { "sitecore\admin", "my command", "master", "/sitecore/content/Home/KP Demo", "ar-SY", "9", "{C4311D97-3651-4776-AAED-82CF4C16034F}", "/sitecore/system/Workflows/Sample Workflow/Awaiting Approval/Reject", "Awaiting Approval", "/sitecore/system/Workflows/Sample Workflow/Draft", "sitecore\admin" }));
        }

        /// <summary>
        /// Need to add
        /// </summary>
        /// <param name="auditObj">String Message</param>
        public void Audit(AuditMetadata auditObj)
        {
            // Record Miscellaneous (uses rename)
            if (!string.IsNullOrEmpty(auditObj.Miscellaneous))
            {
                this.auditLog.Info(string.Format(AuditUnformatPattern4, auditObj.UserName, auditObj.Action, auditObj.Db, auditObj.Path, auditObj.Language, auditObj.Version, auditObj.Id.ToUpper(), auditObj.Miscellaneous));
            }
            else
            {
                // Generic Item editing
                this.auditLog.Info(string.Format(AuditUnformatPattern3, auditObj.UserName, auditObj.Action, auditObj.Db, auditObj.Path, auditObj.Language, auditObj.Version, auditObj.Id.ToString()));
            }
        }
    }
}
