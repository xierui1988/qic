﻿namespace Qic.Sc.Services.Components
{
    using System;

    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// The editorial link service.
    /// </summary>
    public class DocumentLinkService : ILinkService
    {
        private readonly IContentRepository contentRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorialLinkService"/> class.
        /// </summary>
        public DocumentLinkService(IContentRepository contentRepository)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            this.contentRepository = contentRepository;
        }

        /// <summary>
        /// The generate a relative path for editorial.
        /// </summary>
        /// <param name="idOrPath">
        /// The id or path.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GenerateUrl(string idOrPath)
        {
            var document = this.contentRepository.GetEntity<IDocument>(idOrPath);
            return document == null ? null : document.AplicationRelativeUrl().ToLower();
        }
    }
}
