﻿namespace Qic.Sc.Services.Components
{
    using System;
    using System.Linq;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Interfaces.Crm;

    using Sitecore.Modules.EmailCampaign.Messages;
    using Sitecore.SecurityModel;

    internal class DocumentBaseEmailDispatchService : BaseEmailDispatchService
    {
        private readonly IContentRepository repository;
        private readonly ICrmService crmService;
        private readonly IAuditService auditService;
        private readonly ILoggerService loggerService;

        public DocumentBaseEmailDispatchService(IMasterContentRepository repository, ICrmService crmService, IAuditService auditService, ILoggerService loggerService)
        {
            this.repository = repository as IContentRepository;
            if (repository == null)
            {
                throw new ArgumentNullException("repository");
            }
            if (crmService == null)
            {
                throw new ArgumentNullException("crmService");
            }
            if (auditService == null)
            {
                throw new ArgumentNullException("auditService");
            }
            if (loggerService == null)
            {
                throw new ArgumentNullException("loggerService");
            }
            this.crmService = crmService;
            this.auditService = auditService;
            this.loggerService = loggerService;
        }

        public void Dispatch(string idOrPath)
        {
            using (new SecurityDisabler())
            {
                var document = this.repository.GetEntity<IDocument>(idOrPath);
                if (document == null)
                {
                    throw new ApplicationException("Can't find report document with ID: " + idOrPath);
                }

                this.SendDocument(document);
            }
        }

        private string ResolveTemplateId(IDocument report)
        {
            // Invoice - by report code or report name
            var invoiceCodes = this.crmService.GetReportTypes().GetInvoiceCodes();

            if (invoiceCodes != null && invoiceCodes.Any())
            {
                if (invoiceCodes.Contains(report.ReportCode))
                {
                    return Constants.Content.ClientPortal.Configuration.Notifications.Templates.ReportNotifications.InvoiceNotification.ID;
                }
            }

            // Product
            return !string.IsNullOrEmpty(report.ProductCode)
                       ? Constants.Content.ClientPortal.Configuration.Notifications.Templates.ReportNotifications.ProductNotification.ID
                       : Constants.Content.ClientPortal.Configuration.Notifications.Templates.ReportNotifications.ReportNotificaton.ID;
        }

        private void SendDocument(IDocument document)
        {
            // Resolve the template/stencil item to use as a base of the report notification
            // message. Check if report code specific message is configured
            var templateItemId = this.ResolveTemplateId(document);
            var isInvoice = templateItemId
                            == Constants.Content.ClientPortal.Configuration.Notifications.Templates.ReportNotifications
                                   .InvoiceNotification.ID;

            // Create and Dispatch message that links back to the report.
            var message = CreateMessage(templateItemId);
            this.SendPersonalisedDocumentMessages(message, document, isInvoice);
        }

        private void SendPersonalisedDocumentMessages(MessageItem template, IDocument document, bool isInvoice)
        {
            var entityCode = document.GetEntityCode();
            var reportCode = document.ReportCode;

            // If Invoice - Then all access records (onlyNotifications = false)
            // If not invoice - then just access records with Email Notification turned on (onlyNotifications = true)
            var userList = this.crmService.GetReportAccessForEmailBy(entityCode, reportCode, isInvoice);

            foreach (var access in userList)
            {
                try
                {
                    var message = template.Clone() as MessageItem;

                    message.To = access.EmailAddress;
                    message.CustomPersonTokens.Clear();

                    message.CustomPersonTokens.Add(Constants.FirstNameToken, access.FirstName ?? access.FullName);
                    message.CustomPersonTokens.Add(Constants.ClientNameToken, access.AccountName);
                    message.CustomPersonTokens.Add(Constants.ReportNameToken, document.ReportName);
                    message.CustomPersonTokens.Add(Constants.ReportDateToken, document.ReportDate.ToString(Constants.DocumentDateFormat));
                    message.CustomPersonTokens.Add(Constants.DocumentNameToken, document.DocumentName);
                    message.CustomPersonTokens.Add(Constants.ProductNameToken, document.ProductName ?? string.Empty);
                    message.CustomPersonTokens.Add(Constants.ContactOwnerToken, access.ContactOwner ?? string.Empty);

                    if (message.ManagerRoot != null && message.ManagerRoot.Settings != null)
                    {
                        message.CustomPersonTokens.Add(
                            Constants.DocumentLinkToken,
                            message.ManagerRoot.Settings.BaseURL + document.AplicationRelativeUrl());
                    }

                    if (message.Body == null)
                    {
                        message.Body = message.GetMessageBody();
                    }

                    var aduitAction = SitecoreUserAction.UserNotification;
                    if (!BaseEmailDispatchService.SendEmail(message))
                    {
                        aduitAction = SitecoreUserAction.FailedUserNotification;
                    }

                    this.auditService.Audit(new AuditMetadata
                    {
                        UserName = access.EmailAddress,
                        Action = EnumUtils<SitecoreUserAction>.GetDescription(aduitAction),
                        Db = Constants.MasterDb,
                        Path = string.Format("{0}{1}", Constants.ContentPath, document.EntityContentPath),
                        Id = string.Format(Constants.ItemIDFormat, document.EntityId),
                        Language = document.EntityLanguage.ToString(),
                        Version = document.EntityVersion,
                        Miscellaneous = access.AccountName
                    });
                }
                catch (Exception ex)
                {
                    this.loggerService.Error(string.Format("Email send failed: {0} ({1})", document.DocumentName, access.EmailAddress), ex);
                    this.auditService.Audit(new AuditMetadata
                    {
                        UserName = access.EmailAddress,
                        Action = EnumUtils<SitecoreUserAction>.GetDescription(SitecoreUserAction.FailedUserNotification),
                        Db = Constants.MasterDb,
                        Path = string.Format("{0}{1}", Constants.ContentPath, document.EntityContentPath),
                        Id = string.Format(Constants.ItemIDFormat, document.EntityId),
                        Language = document.EntityLanguage.ToString(),
                        Version = document.EntityVersion,
                        Miscellaneous = access.AccountName
                    });
                }
            }
        }
    }
}
