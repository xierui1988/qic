﻿namespace Qic.Sc.Services.Components.Decorators
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Glass.Mapper.Sc.Fields;

    using Qic.Sc.Entities.UserDefined.Components.Site.Configuration.MenuItem;
    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// Decorator class for IMenuItem
    /// </summary>
    public partial class ActiveMenuEntityDecorator : IMenuItem
    {
        private readonly IMenuItem target;
        private readonly Guid[] activeItems;
        private readonly IContentRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ActiveMenuEntityDecorator"/> class.
        /// </summary>
        /// <param name="repository">
        /// The repository.
        /// </param>
        /// <param name="target">
        /// Underlying decorated menu item.
        /// </param>
        /// <param name="activeItems">
        /// The target.
        /// </param>
        /// <exception cref="ActiveMenuEntityDecorator">
        /// Thrown if any of the arguments is null.
        /// </exception>
        public ActiveMenuEntityDecorator(IContentRepository repository, IMenuItem target, Guid[] activeItems)
        {
            if (activeItems == null)
            {
                throw new ArgumentNullException("activeItems");
            }

            if (target == null)
            {
                throw new ArgumentNullException("target");
            }

            if (repository == null)
            {
                throw new ArgumentNullException("repository");
            }

            this.target = target;
            this.activeItems = activeItems;
            this.repository = repository;
        }

        /// <summary>
        /// Gets the sub items for a menu item.
        /// </summary>
        public IEnumerable<IMenuItem> SubItems
        {
            get
            {
                if (this.target.SubItems == null)
                {
                    return null;
                }
                return this.target.SubItems.Select(item => new ActiveMenuEntityDecorator(this.repository, item, this.activeItems));
            }
        }

        /// <summary>
        /// Ensures menu item always has a name.
        /// </summary>
        public string TitleOrName
        {
            get
            {
                return this.Title ?? this.EntityName;
            }
        }

        /// <summary>
        /// Is this menu item within the page lineage
        /// </summary>
        public bool IsActive
        {
            get
            {
                return this.Link != null && this.Link.Type == LinkType.Internal && this.activeItems.Any(ai => ai == this.Link.TargetId);
            }
        }

        /// <summary>
        /// This method returns the Type that Menu Item's Internal
        /// link points to.
        /// </summary>
        /// <typeparam name="TEntity">Type that Internal Link points to </typeparam>
        /// <returns>Requested Type e.g. IContent </returns>
        public TEntity GetLinkedEntity<TEntity>() where TEntity : class
        {
            if (this.Link == null || this.Link.Type != LinkType.Internal || this.Link.TargetId == Guid.Empty)
            {
                return null;
            }

            return this.repository.GetEntity<TEntity>(this.Link.TargetId.ToString());
        }
    }
}