﻿namespace Qic.Sc.Services.Components.Decorators
{
    using System;

    using Qic.Sc.Interfaces.Components;

    internal partial class PortalWebSecurityServiceTracingDecorator : TracingBaseDecorator, IPortalWebSecurityService
    {
        private readonly IPortalWebSecurityService webSecurity;

        public PortalWebSecurityServiceTracingDecorator(IPortalWebSecurityService webSecurity)
        {
            if (webSecurity == null)
            {
                throw new ArgumentNullException("webSecurity");
            }
            this.webSecurity = webSecurity;
        }
    }
}