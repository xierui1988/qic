﻿namespace Qic.Sc.Services.Components.Decorators
{
    using System;

    using Qic.Sc.Interfaces.Components;

    internal partial class PortalEmailServiceTracingDecorator : TracingBaseDecorator, IPortalEmailService
    {
        private readonly IPortalEmailService portalEmailService;

        public PortalEmailServiceTracingDecorator(IPortalEmailService portalEmailService)
        {
            if (portalEmailService == null)
            {
                throw new ArgumentNullException("portalEmailService");
            }
            this.portalEmailService = portalEmailService;
        }
    }
}
