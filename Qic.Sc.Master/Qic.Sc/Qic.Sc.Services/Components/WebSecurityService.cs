﻿namespace Qic.Sc.Services.Components
{
    using System.Collections.Generic;
    using System.Linq;

    using Newtonsoft.Json;

    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Services.Core;

    using Sitecore.Caching;
    using Sitecore.Security.Accounts;
    using System;
    using System.Configuration;
    using System.Security.Principal;
    using System.Web;
    using System.Web.Security;

    using Constants = Qic.Sc.Entities.Constants;

    /// <summary>
    /// Web Security
    /// </summary>
    public class WebSecurityService : IWebSecurityService
    {
        /// <summary>
        ///  The number of days before a document automatically transitions from new to old.
        /// </summary>
        private const int MaxAgeInDaysBeforeDocumentNoLongerNew = 30;

        private readonly MembershipProvider loginProvider;

        private readonly IContentRepository contentRepository;

        /// <summary>
        /// Initializes a new instance of the MembershipProvider
        /// </summary>
        public WebSecurityService(MembershipProvider loginProvider, IContentRepository contentRepository)
        {
            if (loginProvider == null)
            {
                throw new ArgumentNullException("loginProvider");
            }
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            this.loginProvider = loginProvider;
            this.contentRepository = contentRepository;
        }

        /// <summary>
        /// Verifies if whether provided password is valid.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <returns>
        /// true if the user the password is valid.
        /// </returns>
        public bool IsValidPassword(string username, string password)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            if (string.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("password");
            }

            // This is a good place to clear user profile cache to ensure
            // we force it to be re-read from CRM during authentication.
            CacheManager.ClearUserProfileCache(username);

            return this.loginProvider.ValidateUser(username, password);
        }

        /// <summary>
        /// The is administrator level user.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <returns>
        /// True if the user is an administrator.
        /// </returns>
        public bool IsAdmin(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            var contact = User.FromName(username, true);
            if (contact == null)
            {
                throw new ApplicationException("Unknown user name.");
            }

            bool isAdmin;
            return bool.TryParse(contact.Profile["IsAdminLogin"], out isAdmin) && isAdmin;
        }

        /// <summary>
        /// Resets a password by using a password reset token.
        /// </summary>
        /// <param name="username">User name</param>
        /// <param name="newPassword">New Password</param>
        public void ResetPassword(string username, string newPassword)
        {
            if (string.IsNullOrEmpty(newPassword))
            {
                throw new ArgumentNullException("newPassword");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            var contact = User.FromName(username, true);
            if (contact == null)
            {
                throw new ApplicationException("Unknown user name.");
            }

            // Create a temporary password, then force reset to the one provided by the user.
            var resetComplete = this.loginProvider.ChangePassword(
                username,
                this.loginProvider.ResetPassword(username, null),
                newPassword);

            if (!resetComplete)
            {
                throw new ApplicationException("Underlying provider rejected password change.");
            }
        }

        /// <summary>
        /// The get user meta data for specified user.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <returns>
        /// The <see cref="UserMetadata"/>.
        /// </returns>
        public UserMetadata GetUserMetadataFor(string username)
        {
            var metadata = new UserMetadata();

            if (username == null)
            {
                throw new ArgumentNullException("username");
            }

            if (!User.Exists(username))
            {
                throw new ApplicationException("Invalid/unknown user name: " + username);
            }

            var user = User.FromName(username, true);
            var userMetadata = user.Profile[Constants.ProfileMetadataField];
            if (!string.IsNullOrEmpty(userMetadata))
            {
                try
                {
                    metadata.Favourites = JsonConvert.DeserializeObject<UserMetadataFavourites>(userMetadata);
                    metadata.Favourites.Items = this.RemoveNonExistingFavourites(metadata.Favourites.Items);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Corrupt favourites user meta data.", ex);
                }
            }

            var userReadMetadata = user.Profile[Constants.ReoprtMetadataField];
            if (!string.IsNullOrEmpty(userReadMetadata))
            {
                try
                {
                    metadata.ReadDocuments = JsonConvert.DeserializeObject<UserMetadataReadDocuments>(userReadMetadata);
                }
                catch (Exception ex)
                {
                    throw new ApplicationException("Corrupt favourites user meta data.", ex);
                }
            }

            metadata.ReadDocuments.SetRecentMaxAge(this.GetRecentDocumentMaxAgeInDaysFor(username));
            return metadata;
        }

        private List<Guid> RemoveNonExistingFavourites(ICollection<Guid> favourites)
        {
            if (favourites == null || !favourites.Any())
            {
                return new List<Guid>();
            }

            using (var search = this.contentRepository.GetSearchService())
            {
                return
                    search.GetDescendantsQueryable<DocumentEntity>(Constants.Content.ClientPortal.Documents.ItemID.Guid)
                        .Where(doc => favourites.Contains(doc.EntityId))
                        .Select(d => d.EntityId)
                        .ToList();
            }
        }

        /// <summary>
        /// The get active portal user name.
        /// </summary>
        /// <returns>
        /// First name for portal user
        /// </returns>
        public string GetActiveUserDisplayName()
        {
            var username = this.GetCurrentAuthenticatedUsername();
            if (string.IsNullOrEmpty(username))
            {
                throw new ApplicationException("Unknown user.");
            }

            var contact = User.FromName(username, true);
            if (contact == null)
            {
                throw new ApplicationException("Unknown user name.");
            }

            return contact.Profile[Constants.CrmSyncFirstName];
        }

        /// <summary>
        /// The updated user meta data for.
        /// </summary>
        /// <param name="username">
        /// The user name.
        /// </param>
        /// <param name="metadata">
        /// The meta data.
        /// </param>
        public void UpdatedUserMetadataFor(string username, UserMetadata metadata)
        {
            if (username == null)
            {
                throw new ArgumentNullException("username");
            }

            if (!User.Exists(username))
            {
                throw new ApplicationException("Invalid/unknown user name: " + username);
            }

            try
            {
                var user = User.FromName(username, true);
                var favorites = JsonConvert.SerializeObject(metadata.Favourites);
                if (favorites.Length > 4000)
                {
                    throw new ApplicationException("Failed to save favourite, exceeded max field length");
                }

                var read = JsonConvert.SerializeObject(metadata.ReadDocuments);
                if (read.Length > 50000)
                {
                    throw new ApplicationException("Failed to save read document, exceeded max field length");
                }

                user.Profile[Constants.ProfileMetadataField] = favorites;
                user.Profile[Constants.ReoprtMetadataField] = read;
                user.Profile.Save();
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed to update user meta data.", ex);
            }
        }

        /// <summary>
        /// The get current user name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetCurrentAuthenticatedUsername()
        {
            var identity = this.GetActiveUser();
            return identity == null ? null : identity.Identity.Name;
        }

        /// <summary>
        /// Authenticates the XRM credentials.
        /// </summary>
        /// <returns>
        /// The <see cref="bool" />.
        /// </returns>
        public bool AuthenticateXrmCredentials(string id)
        {
            return id == ConfigurationManager.ConnectionStrings[Constants.XrmUiCredentials].ConnectionString;
        }

        /// <summary>
        /// The get active user.
        /// </summary>
        /// <returns>
        /// The <see cref="System.Security.Principal.IPrincipal"/>.
        /// </returns>
        public IPrincipal GetActiveUser()
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new InvalidOperationException("Can't resolve user identity, HTTP context is empty.");
            }

            var currentIdentity = context.User;
            if (currentIdentity != null)
            {
                if (currentIdentity.Identity.IsAuthenticated)
                {
                    // Do not change identity if its already authenticated.
                    return currentIdentity;
                }

                // To force sitecore to re-create identity we need
                // to clear identity of the current context.
                context.User = null;
            }

            var sitecoreIdentity = Sitecore.Security.Authentication.AuthenticationManager.GetActiveUser();
            if (context.User == null)
            {
                // Restore context if sitecore did not set the user identity.
                context.User = sitecoreIdentity;
            }

            return sitecoreIdentity ?? context.User;
        }

        /// <summary>
        /// Get the maximum age of a document before its no-longer recent.
        /// </summary>
        private int GetRecentDocumentMaxAgeInDaysFor(string username)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            var contact = User.FromName(username, true);
            if (contact == null)
            {
                throw new ApplicationException("Unknown user name.");
            }

            int age;
            if (int.TryParse(contact.Profile["MaxRecentDocumentAge"], out age))
            {
                return age;
            }

            return MaxAgeInDaysBeforeDocumentNoLongerNew;
        }
    }
}