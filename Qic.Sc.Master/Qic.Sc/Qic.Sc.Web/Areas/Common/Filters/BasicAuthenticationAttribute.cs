﻿namespace Qic.Sc.Web.Areas.Common.Filters
{
    using System;
    using System.Configuration;
    using System.Net.Http.Headers;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Text;
    using System.Web.Mvc;
    using System.Web.Mvc.Filters;

    /// <summary>
    /// The basic authorize attribute.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class BasicAuthenticationAttribute : ActionFilterAttribute, IAuthenticationFilter
    {
        private const string CredentialType = "Basic for MVC";
        private readonly string credentials;

        /// <summary>
        /// Initializes a new instance of the <see cref="BasicAuthenticationAttribute"/> class.
        /// </summary>
        /// <param name="configurationName">
        /// The name of the connections sting that holds the user name password.
        /// Format (user-name:password)
        /// </param>
        public BasicAuthenticationAttribute(string configurationName)
        {
            if (configurationName == null)
            {
                throw new ArgumentNullException("configurationName");
            }
            this.credentials = ConfigurationManager.ConnectionStrings[configurationName].ConnectionString;
        }

        /// <summary>
        /// Gets or sets the realm.
        /// </summary>
        public string Realm { get; set; }

        /// <summary>
        /// The on authentication.
        /// </summary>
        public void OnAuthentication(AuthenticationContext context)
        {
            var request = context.HttpContext.Request;
            AuthenticationHeaderValue authorization;
            if (!AuthenticationHeaderValue.TryParse(request.Headers["Authorization"], out authorization))
            {
                context.Result = new HttpUnauthorizedResult("Missing credentials, no headers");
                return;
            }

            if (authorization.Scheme != "Basic")
            {
                // No authentication was attempted (for this authentication method).
                // Do not set either Principal (which would indicate success) or ErrorResult (indicating an error).
                context.Result = new HttpUnauthorizedResult("Missing credentials, not basic");
                return;
            }

            if (string.IsNullOrEmpty(authorization.Parameter))
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                context.Result = new HttpUnauthorizedResult("Missing credentials, empty");
                return;
            }

            Tuple<string, string> userNameAndPasword = ExtractUserNameAndPassword(authorization.Parameter);

            if (userNameAndPasword == null)
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                context.Result = new HttpUnauthorizedResult("Invalid credentials");
                return;
            }

            var username = userNameAndPasword.Item1;
            var password = userNameAndPasword.Item2;

            if (this.credentials != username + ":" + password)
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                context.Result = new HttpUnauthorizedResult("Invalid username or password");
            }
            else
            {
                // Authentication was attempted and succeeded. Set Principal to the authenticated user.
                context.Principal = new ClaimsPrincipal(new GenericIdentity(username, CredentialType));
            }
        }

        /// <summary>
        /// The on authentication challenge.
        /// </summary>
        /// <param name="filterContext">
        /// The filter context.
        /// </param>
        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            var user = filterContext.HttpContext.User;
            if (user != null && user.Identity.IsAuthenticated && user.Identity.AuthenticationType == CredentialType
                && this.credentials.StartsWith(user.Identity.Name))
            {
                return;
            }

            filterContext.HttpContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", this.Realm));
            filterContext.Result = new HttpUnauthorizedResult();
        }

        private static Tuple<string, string> ExtractUserNameAndPassword(string authorizationParameter)
        {
            byte[] credentialBytes;

            try
            {
                credentialBytes = Convert.FromBase64String(authorizationParameter);
            }
            catch (FormatException)
            {
                return null;
            }

            // The currently approved HTTP 1.1 specification says characters here are ISO-8859-1.
            // However, the current draft updated specification for HTTP 1.1 indicates this encoding is infrequently
            // used in practice and defines behavior only for ASCII.
            var encoding = Encoding.ASCII;

            // Make a writable copy of the encoding to enable setting a decoder fallback.
            encoding = (Encoding)encoding.Clone();

            // Fail on invalid bytes rather than silently replacing and continuing.
            encoding.DecoderFallback = DecoderFallback.ExceptionFallback;
            string decodedCredentials;

            try
            {
                decodedCredentials = encoding.GetString(credentialBytes);
            }
            catch (DecoderFallbackException)
            {
                return null;
            }

            if (string.IsNullOrEmpty(decodedCredentials))
            {
                return null;
            }

            var colonIndex = decodedCredentials.IndexOf(':');
            if (colonIndex == -1)
            {
                return null;
            }

            var userName = decodedCredentials.Substring(0, colonIndex);
            var password = decodedCredentials.Substring(colonIndex + 1);
            return new Tuple<string, string>(userName, password);
        }
    }
}