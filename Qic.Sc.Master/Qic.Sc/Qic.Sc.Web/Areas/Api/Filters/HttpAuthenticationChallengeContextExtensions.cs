﻿namespace Qic.Sc.Web.Areas.Api.Filters
{
    using System;
    using System.Net.Http.Headers;
    using System.Web.Http.Filters;

    using Qic.Sc.Web.Areas.Api.Results;

    /// <summary>
    /// The http authentication challenge context extensions.
    /// From: http://www.asp.net/web-api/overview/security/authentication-filters
    /// </summary>
    public static class HttpAuthenticationChallengeContextExtensions
    {
        /// <summary>
        /// The challenge with.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="scheme">
        /// The scheme.
        /// </param>
        public static void ChallengeWith(this HttpAuthenticationChallengeContext context, string scheme)
        {
            ChallengeWith(context, new AuthenticationHeaderValue(scheme));
        }

        /// <summary>
        /// The challenge with.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="scheme">
        /// The scheme.
        /// </param>
        /// <param name="parameter">
        /// The parameter.
        /// </param>
        public static void ChallengeWith(this HttpAuthenticationChallengeContext context, string scheme, string parameter)
        {
            ChallengeWith(context, new AuthenticationHeaderValue(scheme, parameter));
        }

        /// <summary>
        /// The challenge with.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        /// <param name="challenge">
        /// The challenge.
        /// </param>
        public static void ChallengeWith(this HttpAuthenticationChallengeContext context, AuthenticationHeaderValue challenge)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            context.Result = new AddChallengeOnUnauthorizedResult(challenge, context.Result);
        }
    }
}
