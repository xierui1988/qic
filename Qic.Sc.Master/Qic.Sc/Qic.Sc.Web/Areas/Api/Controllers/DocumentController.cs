﻿namespace Qic.Sc.Web.Areas.Api.Controllers
{
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;

    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;

    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Web.Areas.Api.Filters;

    /// <summary>
    /// The test controller.
    /// </summary>
    public class DocumentController : ApiController
    {
        private readonly IReportService reportService;
        private readonly ILoggerService loggerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentController"/> class.
        /// </summary>
        public DocumentController(IReportService reportService, ILoggerService loggerService)
        {
            if (reportService == null)
            {
                throw new ArgumentNullException("reportService");
            }
            if (loggerService == null)
            {
                throw new ArgumentNullException("loggerService");
            }
            this.reportService = reportService;
            this.loggerService = loggerService;
        }

        /// <summary>
        /// The get id.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        [IdentityBasicAuthentication]
        [HttpPost]
        public async Task<HttpResponseMessage> Upload()
        {
            var response = new HttpResponseMessage(HttpStatusCode.Created) { ReasonPhrase = "Report created" };
            try
            {
                var parts = await this.Request.Content.ReadAsMultipartAsync();
                if (parts.Contents.Count != 0)
                {
                    var part = parts.Contents[0];
                    var filename = part.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                    using (var filestream = await part.ReadAsStreamAsync())
                    {
                        var reportName = ReportNameMetadata.ParseFileName(filename);
                        reportName.FileStream = filestream;
                        this.reportService.CreateReport(reportName);
                    }
                }
            }
            catch (ArgumentException ae)
            {
                this.loggerService.Error("Upload failed.", ae);

                response = this.Request.CreateErrorResponse(HttpStatusCode.BadRequest, ae.Message);
                response.ReasonPhrase = ae.Message;
            }
            catch (InvalidOperationException ioe)
            {
                this.loggerService.Error("Upload failed.", ioe);

                response = this.Request.CreateErrorResponse(HttpStatusCode.Conflict, ioe.Message);
                response.ReasonPhrase = ioe.Message;
            }
            catch (ApplicationException ioe)
            {
                this.loggerService.Error("Upload failed.", ioe);

                response = this.Request.CreateErrorResponse(HttpStatusCode.Conflict, ioe.Message);
                response.ReasonPhrase = ioe.Message;
            }
            catch (Exception ex)
            {
                this.loggerService.Error("Upload failed.", ex);

                response = this.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
                response.ReasonPhrase = ex.Message;
            }

            return response;
        }
    }
}