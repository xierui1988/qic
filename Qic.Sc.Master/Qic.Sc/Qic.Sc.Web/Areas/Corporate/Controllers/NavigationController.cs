﻿using System.Collections.Generic;
using Qic.Sc.Entities.UserDefined.Components.Site.Configuration.MenuItem;

namespace Qic.Sc.Web.Areas.Corporate.Controllers
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;

    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Web.Controllers;
    using Qic.Sc.Web.Areas.Corporate.Model;
    using Qic.Sc.Entities;

    /// <summary>
    /// The navigation controller.
    /// </summary>
    public partial class NavigationController : RenderingController
    {
        private readonly INavigationService navigationService;
        private readonly IWebSecurityService webSecurity;
        private readonly IConfigurationService configurationService;
        private readonly ILoggerService loggerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationController"/> class.
        /// </summary>
        /// <param name="loggerService">
        /// The logger service.
        /// </param>
        /// <param name="navigationService">
        /// The navigation service.
        /// </param>
        /// <param name="webSecurity">
        /// The web security
        /// </param>
        /// <param name="configurationService">
        /// The configuration service.
        /// </param>
        public NavigationController(ILoggerService loggerService, INavigationService navigationService, IWebSecurityService webSecurity, IConfigurationService configurationService)
            : base(loggerService)
        {
            if (loggerService == null)
            {
                throw new ArgumentNullException("loggerService");
            }
            if (navigationService == null)
            {
                throw new ArgumentNullException("navigationService");
            }
            if (webSecurity == null)
            {
                throw new ArgumentNullException("webSecurity");
            }
            if (configurationService == null)
            {
                throw new ArgumentNullException("configurationService");
            }
            this.navigationService = navigationService;
            this.webSecurity = webSecurity;
            this.configurationService = configurationService;
            this.loggerService = loggerService;
        }

        /// <summary>
        /// Renders a menu specified via data source in a context of an item.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult MegaMenu(string renderingDataSource, Guid? renderingContextItem, string contextSite)
        {
            var menu = this.navigationService.GetMenu(renderingDataSource, renderingContextItem);
            return this.View(new NavigationModel()
            {
                Menu = menu,
                ContextSite = contextSite
            });
        }

        /// <summary>
        /// Renders a menu specified via data source in a context of an item.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult FooterMenu(string renderingDataSource, Guid? renderingContextItem, string contextSite)
        {
            var menu = this.navigationService.GetMenu(renderingDataSource, renderingContextItem);
            return this.View(new NavigationModel()
                {
                    Menu = menu,
                    ContextSite = contextSite
                });
        }

        /// <summary>
        /// Renders menu items found in under the node of the tree identified by the
        /// data source as links.
        /// </summary>
        public virtual ActionResult Legal(string renderingDataSource, string contextSite)
        {
            var menuItems = this.navigationService.GetMenu(renderingDataSource);
            if (menuItems == null)
            {
                throw new ApplicationException(
                    "Can't resolve (missing/deleted) menu items using provided data source: " + renderingDataSource);
            }

            return this.View(new NavigationModel { Menu = menuItems, ContextSite = contextSite });
        }

        /// <summary>
        /// Renders locations menu.
        /// </summary>
        public virtual ActionResult Locations(string renderingDataSource, string contextSite)
        {
            var menuItems = this.navigationService.GetMenu(renderingDataSource);
            if (menuItems == null)
            {
                throw new ApplicationException(
                    "Can't resolve (missing/deleted) menu items using provided data source: " + renderingDataSource);
            }

            return this.View(new NavigationModel { Menu = menuItems, ContextSite = contextSite });
        }

        /// <summary>
        /// Renders Top Menu
        /// </summary>
        /// <param name="renderingDataSource">Data Source</param>
        /// <param name="renderingContextItem">Data Context</param>
        /// <param name="displayPortalLink">Display the Portal Link</param>
        /// <param name="contextSite">Context of the Site</param>
        /// <returns>Renders Top Menu View</returns>
        public virtual ActionResult TopMenu(string renderingDataSource, Guid? renderingContextItem, bool displayPortalLink = true, string contextSite = null)
        {
            string userFirstname;
            var menu = this.GetTopMenuItems(renderingDataSource, renderingContextItem, displayPortalLink, out userFirstname);
            menu = menu.Where(x => x.EntityId != Constants.Content.Corporate.Configuration.Navigation.TopMenu.QIC.ItemID.Guid);

            return this.View(new TopNavModel { Menu = menu, UserFirstName = userFirstname, ContextSite = contextSite });
        }

        /// <summary>
        /// Renders Top Menu
        /// </summary>
        /// <param name="renderingDataSource">Data Source</param>
        /// <param name="renderingContextItem">Data Context</param>
        /// <param name="displayPortalLink">Display the Portal Link</param>
        /// <param name="contextSite">Context of the Site</param>
        /// <returns>Renders Top Menu View</returns>
        public virtual ActionResult MegaTopMenu(string renderingDataSource, Guid? renderingContextItem, bool displayPortalLink = true, string contextSite = null)
        {
            string userFirstname;
            var menu = this.GetTopMenuItems(renderingDataSource, renderingContextItem, displayPortalLink, out userFirstname);

            return this.View(new TopNavModel { Menu = menu, UserFirstName = userFirstname, ContextSite = contextSite });
        }

        /// <summary>
        /// Get Menu Links for the top menu
        /// </summary>
        private IEnumerable<IMenuItem> GetTopMenuItems(string renderingDataSource, Guid? renderingContextItem, bool displayPortalLink, out string userFirstname)
        {
            var menu = this.navigationService.GetMenu(renderingDataSource, renderingContextItem);
            userFirstname = this.GetUserDisplayName();

            if (!displayPortalLink)
            {
                menu = menu.Where(x => x.EntityId != Constants.Content.Corporate.Configuration.Navigation.TopMenu.PortalLogin.ItemID.Guid);
            }
            return menu;
        }

        /// <summary>
        /// Renders Top Menu
        /// </summary>
        /// <param name="renderingDataSource">Data Source</param>
        /// <param name="renderingContextItem">Data Context</param>
        /// <returns>Renders Top Menu View</returns>
        public virtual ActionResult SideMenu(string renderingDataSource, Guid? renderingContextItem)
        {
            var menu = this.navigationService.GetMenu(renderingDataSource, renderingContextItem);
            return this.View(menu);
        }

        /// <summary>
        /// (1) Check we have a user name cookie and use the name.
        /// (2) Gets the user name of the authenticated user and set the cookie.
        /// (3) Otherwise null.
        /// </summary>
        public string GetUserDisplayName()
        {
            var cookie = this.Request.Cookies.Get(Entities.Constants.QicUserNameCookie);
            if (cookie != null)
            {
                // Get the user name from the cookie
                return cookie.Value;
            }

            try
            {
                var user = this.webSecurity.GetActiveUser();
                if (user.Identity.IsAuthenticated)
                {
                    var name = this.webSecurity.GetActiveUserDisplayName();

                    this.Response.Cookies.Add(
                        new HttpCookie(Entities.Constants.QicUserNameCookie)
                            {
                                Domain = this.configurationService.GetGlobalSetting("UserInformtionCookieDomain"),
                                Value = name,
                                HttpOnly = true,
                                Shareable = true
                            });
                }
            }
            catch (Exception ex)
            {
                this.loggerService.Error("Failed get authenticated user and set user name cookie.", ex);
            }

            return null;
        }
    }
}