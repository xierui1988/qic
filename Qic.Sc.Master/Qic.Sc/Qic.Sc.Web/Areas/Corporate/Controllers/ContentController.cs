﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Qic.Sc.Entities;
using Qic.Sc.Entities.UserDefined.Components.Pages.Pagecontent;
using Qic.Sc.Entities.UserDefined.Components.Pages.Pageheadline;
using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Disclaimer;
using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Pagedisclaimer;
using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Property;
using Qic.Sc.Entities.UserDefined.QIC.Corporate.Pages.GREListingPage;
using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;
using Qic.Sc.Interfaces.Components;
using Qic.Sc.Interfaces.Core;
using Qic.Sc.Web.Areas.Corporate.Model;
using Qic.Sc.Web.Controllers;

namespace Qic.Sc.Web.Areas.Corporate.Controllers
{
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.Editorial;
    using Qic.Sc.Services.Core;

    using Sitecore.ContentSearch.Linq;

    /// <summary>
    /// The content controller.
    /// </summary>
    public partial class ContentController : RenderingController
    {
        private readonly IContentRepository contentRepository;
        private readonly IEmailService emailService;
        private readonly IConfigurationService configurationService;
        private readonly ILoggerService loggerService;   

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentController"/> class.
        /// </summary>
        public ContentController(
            IContentRepository contentRepository,
            IEmailService emailService,
            IConfigurationService configurationService,
            ILoggerService loggerService)
            : base(loggerService)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            if (emailService == null)
            {
                throw new ArgumentNullException("emailService");
            }
            if (configurationService == null)
            {
                throw new ArgumentNullException("configurationService");
            }
            if (loggerService == null)
            {
                throw new ArgumentNullException("loggerService");
            }

            this.contentRepository = contentRepository;
            this.emailService = emailService;
            this.configurationService = configurationService;
            this.loggerService = loggerService;
        }

        /// <summary>
        /// Renders page content
        /// </summary>
        /// <param name="renderingDataSource">
        ///     The page to render.
        /// </param>
        /// <param name="renderingContextItem">
        ///     If page to render if null will user current context item.
        /// </param>
        /// <param name="isRaw">
        ///     Do not wrap content, return as is. Otherwise use default wrapping markup.
        /// </param>
        public virtual ActionResult Index(string renderingDataSource, string renderingContextItem, bool isRaw = false)
        {
            var content = this.contentRepository.GetEntity<IPageContent, IPageHeadline>(renderingDataSource ?? renderingContextItem);
            if (content == null)
            {
                return this.HttpNotFound("Page not found");
            }

            if (isRaw)
            {
                return this.View(MVC.Corporate.Content.Views.ViewNames.Raw);
            }

            return this.View(new ContentModel { Headline = ((IPageHeadline)content).Headline, Content = content.Content });
        }

        /// <summary>
        /// Renders raw HTML.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult ContactUs(string renderingContextItem, bool complete = false)
        {
            if (complete)
            {
                var content = this.contentRepository.GetEntity<IPageContent, IPageHeadline>(renderingContextItem);
                return this.View(
                    MVC.Corporate.Content.Views.Index,
                    new ContentModel { Headline = ((IPageHeadline)content).Headline, Content = content.Content });
            }

            return this.View(new ContactUsModel());
        }

        /// <summary>
        /// Send Contact Message
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        [HttpPost]
        public virtual ActionResult ContactUs(ContactUsModel model)
        {
            var confirmMessage = new List<string>();
            if (ModelState.IsValid && model.TermsAndConditions && !model.PreferredMethodOfContact ? !string.IsNullOrEmpty(model.Email) : !string.IsNullOrEmpty(model.PhoneNumber))
            {
                var message = new MailMessage();
                message.From = new MailAddress(this.configurationService.GetGlobalSetting("ContactFromEmailAddress"));
                message.To.Add(new MailAddress(this.configurationService.GetGlobalSetting("QICEmailAddress")));
                message.Subject = "Contact Us Enquiry";
                message.Body =
                    string.Format(
                        "Contact: {0}\nCompany: {1}\nPreferred Method Of Contact: {2}\nEmail: {3}\nPhone: {4}\n\nMessage:\n{5}",
                        (model.Name ?? "-"), (model.Company ?? "-"), !model.PreferredMethodOfContact ? "Email." : "Phone.",
                        (model.Email ?? "-"), (model.PhoneNumber ?? "-"), (model.ContactMessage ?? "-"));

                try
                {
                    this.emailService.SendMessageDirectly(message);
                    return
                        this.Redirect(
                            this.Request.RawUrl
                            + (this.Request.QueryString.Count == 0 ? "?complete=true" : "&complete=true"));
                }
                catch (Exception ex)
                {
                    this.loggerService.Error("Contact us as email send failed.", ex);
                    confirmMessage.Add("Unable to send email.");
                }
                    
                return this.View(new ContactUsModel{ Messages = confirmMessage });
            }

            confirmMessage.Add("Information provided was not sufficient or incorrect to submit your query. Please check that you have entered all information correctly and that you have agreed to the terms and conditions.");
            return this.View(new ContactUsModel { Messages = confirmMessage });
        }

        /// <summary>
        /// Search result
        /// </summary>
        /// <param name="searchQuery">
        /// String that is being Searched
        /// </param>
        /// <param name="page">
        /// Page Number.
        /// </param>
        /// <param name="take">
        /// Page Size.
        /// </param>
        /// <returns>
        /// View of Search Results
        /// </returns>
        public virtual ActionResult Search(string searchQuery, int page = 1, int take = 10)
        {
            IEnumerable<IFeature> results = null;
            int total = 0;

            if (!string.IsNullOrWhiteSpace(searchQuery))
            {
                using (var search = this.contentRepository.GetSearchService())
                {
                    var query =
                        from feature in // Only search site content
                            search.GetDescendantsQueryable<EditorialEntity>(Constants.Content.Corporate.Home.ItemID.Guid)
                        where
                            feature.Content.Like(searchQuery, 10) || feature.Headline.Like(searchQuery, 10)
                            || feature.PullQuote.Like(searchQuery, 10) || feature.Description.Like(searchQuery, 10)
                        select feature;

                    total = query.Count();
                    results =
                        query.Skip(page <= 1 ? 0 : (page - 1) * take)
                            .Take(take)
                            .ToArray()
                            .Select(r => this.contentRepository.MapEntity(r)).ToArray();
                }
            }

            const string PageUrlFormat = "{0}?searchQuery={1}&page={2}";
            var count = (int)Math.Floor(total / (double)take);
            var path = this.Url.ForItem(Constants.Content.Corporate.Home.Search.ItemID.Guid);
            var prev = page > 1 ? string.Format(PageUrlFormat, path, this.Server.UrlEncode(searchQuery), page - 1) : null;
            var next = count > page ? string.Format(PageUrlFormat, path, this.Server.UrlEncode(searchQuery), page + 1) : null;

            return
                this.View(
                    new SearchModel
                        {
                            SearchQuery = searchQuery,
                            Page = page,
                            PageCount = count,
                            ResultsCount = total,
                            SearchResults = results,
                            Previous = prev,
                            Next = next
                        });
        }

        /// <summary>
        /// The disclaimer.
        /// </summary>
        [HttpPost]
        public virtual ActionResult Disclaimer()
        {
            HttpCookie myCookie = new HttpCookie(Constants.QicDisclaimerCookie);
            myCookie.HttpOnly = true;
            myCookie.Value = "true";
            Response.Cookies.Add(myCookie);

            return new EmptyResult();
        }

        /// <summary>
        /// The disclaimer.
        /// </summary>
        public virtual ActionResult Disclaimer(string renderingContextitem)
        {
            var isDisclaimer = this.contentRepository.GetEntity<IPageDisclaimer>(renderingContextitem);

            IDisclaimer model = null;

            if (isDisclaimer != null && isDisclaimer.DisclaimerRequired && (Request.Cookies[Constants.QicDisclaimerCookie] == null || Request.Cookies[Constants.QicDisclaimerCookie].Value != "true"))
            {
                model = this.contentRepository.GetEntity<IDisclaimer>(Constants.Content.Corporate.Configuration.Disclaimer.ID);
            }

            return this.View(model);
        }

        /// <summary>
        /// The attachment.
        /// </summary>
        public virtual ActionResult Attachment(string renderingContextitem)
        {
            var editorial = this.contentRepository.GetEntity<IEditorial>(renderingContextitem);
            if (editorial != null)
            {
                var attachmentModel = new AttachmentModel
                                          {
                                              Url = editorial.File.Src,
                                              Text = editorial.GetDisplayNameOrName()
                                          };

                return this.View(attachmentModel);
            }

            return this.Content("<!-- NO ATTACHMENT -->");
        }

        /// <summary>
        /// The property.
        /// </summary>
        public virtual ActionResult PropertyList(string renderingDataSource, string renderingContextitem)
        {
            var propertyContainer = this.contentRepository.GetEntity<GREListingPageEntity>(renderingDataSource ?? renderingContextitem);

            if (propertyContainer != null)
            {
                return this.View(propertyContainer.Properties);
            }

            return this.Content("<!-- NO PROPERTY -->");
        }
    }
}