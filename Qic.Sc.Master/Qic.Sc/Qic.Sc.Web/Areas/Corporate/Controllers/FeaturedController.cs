﻿using Qic.Sc.Web.Areas.Corporate.Model;

namespace Qic.Sc.Web.Areas.Corporate.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.CallToAction;
    using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Carousel;
    using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Featured;
    using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.GlobalFeature;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Web.Controllers;

    using Sitecore.Rules.Conditions.SiteConditions;

    /// <summary>
    /// The featured controller.
    /// </summary>
    public partial class FeaturedController : RenderingController
    {
        private readonly IContentRepository contentRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="FeaturedController"/> class.
        /// </summary>
        /// <param name="contentRepository">
        /// The content repository.
        /// </param>
        public FeaturedController(IContentRepository contentRepository)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            this.contentRepository = contentRepository;
        }

        /// <summary>
        /// The carousel.
        /// </summary>
        public virtual ActionResult Carousel(string renderingDataSource, string renderingContextItem, int count = 10)
        {
            var slides = this.contentRepository.GetEntity<ICarousel>(renderingDataSource ?? renderingContextItem);
            if (slides == null)
            {
                throw new ApplicationException("Carousel data source is missing: " + renderingDataSource);
            }

            return this.View(slides.SlideEntities.Take(count));
        }

        /// <summary>
        /// Side by side hero features.
        /// </summary>
        public virtual ActionResult SideBySideHero(string renderingDataSource, string renderingContextItem, int count = 2)
        {
            var featured = this.GetFeatureContainer(renderingDataSource ?? renderingContextItem);
            return this.View(new FeatureModel { Count = count, Feature = featured });
        }

        /// <summary>
        /// The stacked.
        /// </summary>
        public virtual ActionResult Stacked(string renderingDataSource, string formatdate = "dd MMM yyyy", int count = 20, string sitename = null)
        {
            var featured = this.GetFeatureContainer(renderingDataSource);
            return this.View(featured.ToStackedPanelModelWithBlockNews(this.Url, count, formatdate, sitename));
        }

        /// <summary>
        /// The stacked with hero.
        /// </summary>
        public virtual ActionResult HeroStacked(string renderingDataSource, string dateformat = "dd MMM yyyy", int count = 6, string sitename = null)
        {
            var data = this.contentRepository.GetEntity<FeaturedGroupingEntity>(renderingDataSource);
            return
                this.View(
                    new StackedHeroModel
                        {
                            Panels =
                                data.FeatureGroups.Select(
                                    f => f.ToStackedPanelModelWithNewsItems(this.Url, count, dateformat, sitename))
                        });
        }

        /// <summary>
        /// Simple featured item.
        /// </summary>
        public virtual ActionResult Simple(string renderingDataSource, string renderingContextItem, string dateformat, int count = 0, bool showlink = true, int introHeight = 0, string sitename = null)
        {
            var featured = this.GetFeatureContainer(renderingDataSource ?? renderingContextItem);

            if (count == 0)
            {
                var model = featured.ToStackedPanelModelWithBlockNews(this.Url, 1, dateformat, sitename);
                model.IsShowReadMoreEnabled = true;
                model.IntroHeight = introHeight;

                return this.View(model);
            }

            return this.View(
                MVC.Corporate.Featured.Views.SimplePanel, 
                new StackedHeroModel
                {
                    Panels = featured.FeatureEntities.Take(count).Select(
                    f =>
                        new StackedPanelModel
                            {
                                HeroImage = f.HeroImage.Src,
                                IsShowAllEnabled = false,
                                IsShowReadMoreEnabled = false,
                                Title = f.Title,
                                IntroHeight = introHeight,
                                NewsBlockList = new[]
                                {
                                    new StackedPanelItemModel
                                    {
                                        Date = f.PublicationDate,
                                        DateFormat = dateformat,
                                        Intro = f.PullQuote,
                                        Link = showlink ? this.Url.ForItem(f.EntityId) : null,
                                        Title = f.Headline
                                    }
                                }
                            })
                }
            );
        }

        /// <summary>
        /// Hero featured items
        /// </summary>
        public virtual ActionResult Hero(string renderingDataSource, string renderingContextItem, int skip = 0, int count = 20)
        {
            var heros = this.GetFeatureContainer(renderingDataSource ?? renderingContextItem);
            return this.View(heros.FeatureEntities.Skip(skip).Take(count));
        }

        /// <summary>
        /// The small hero.
        /// </summary>
        public virtual ActionResult SmallHero(string renderingDataSource, string renderingContextItem, string dateformat = "dd MMM yyyy", int skip = 0, int count = 20, bool showlink = true)
        {
            var heros = this.GetFeatureContainer(renderingDataSource ?? renderingContextItem);
            return
                this.View(
                    heros.EditorialEntities.Skip(skip).Take(count).Select(
                        h =>
                        new SmallHeroModel
                            {
                                Title = h.PullQuote,
                                SubTitle = h.Headline,
                                Content = h.Content,
                                Date = h.PublicationDate,
                                DateFormat = dateformat,
                                Link = showlink ? this.Url.ForItem(h.EntityId) : null,
                                HeroImage = h.HeroImage
                            }));
        }

        /// <summary>
        /// The banner.
        /// </summary>
        public virtual ActionResult Banner(string renderingDataSource, string renderingContextItem, bool renderlink = true, int height = 0)
        {
            var feature = this.GetOneFeature(renderingDataSource ?? renderingContextItem);
            return this.View(new BannerModel { Feature = feature, IsLinkVisible = renderlink, Height = height });
        }

        /// <summary>
        /// The call to action.
        /// </summary>
        public virtual ActionResult CallToAction(string renderingDataSource, int maxDropDownLinks = 7)
        {
            var cta = this.contentRepository.GetEntity<ICallToAction>(renderingDataSource);
            if (cta == null)
            {
                throw new ApplicationException("Missing call to action data source: " + renderingDataSource);
            }

            return
                this.View(
                    new CallToActionModel
                        {
                            Banner = cta.PullQuote,
                            DropDownLinks = cta.FeatureEntities.Take(maxDropDownLinks)
                        });
        }

        private IGlobalFeature GetFeatureContainer(string datasource)
        {
            if (datasource == null)
            {
                throw new ArgumentNullException("datasource", "Rendering is missing a data source.");
            }

            var featured = this.contentRepository.GetEntity<IGlobalFeature>(datasource);
            if (featured == null)
            {
                throw new ApplicationException("Failed to read featured content from data source: " + datasource);
            }
            return featured;
        }

        private IFeature GetOneFeature(string renderingDataSource)
        {
            if (renderingDataSource == null)
            {
                throw new ArgumentNullException("renderingDataSource", "Rendering is missing a data source.");
            }

            var featured = this.contentRepository.GetEntity<IFeature>(renderingDataSource);
            if (featured == null)
            {
                throw new ApplicationException("Failed to read feature content from data source: " + renderingDataSource);
            }
            return featured;
        }
    }
}