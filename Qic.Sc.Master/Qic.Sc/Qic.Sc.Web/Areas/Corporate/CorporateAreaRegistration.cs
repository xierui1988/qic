﻿namespace Qic.Sc.Web.Areas.Corporate
{
    using System.Web.Mvc;

    /// <summary>
    /// The corporate area registration.
    /// </summary>
    public class CorporateAreaRegistration : AreaRegistration
    {
        /// <summary>
        /// Gets the name of the area to register.
        /// </summary>
        /// <returns>
        /// The name of the area to register.
        /// </returns>
        public override string AreaName
        {
            get
            {
                return "Corporate";
            }
        }

        /// <summary>
        /// Registers an area in an ASP.NET MVC application using the specified area's context information.
        /// </summary>
        /// <param name="context">Encapsulates the information that is required in order to register the area.</param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "Corporate_default",
                "Corporate/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}