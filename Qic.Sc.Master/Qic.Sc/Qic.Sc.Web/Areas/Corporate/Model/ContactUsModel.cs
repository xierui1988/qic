﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Qic.Sc.Web.Areas.Corporate.Model
{
    /// <summary>
    /// Cuntact Us Model
    /// </summary>
    public class ContactUsModel
    {
        /// <summary>
        /// Contact's Name
        /// </summary>
        [Required]
        [Display(Name = "Name")]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        /// <summary>
        /// Contact's Company
        /// </summary>
        [Required]
        [Display(Name = "Company")]
        [DataType(DataType.Text)]
        public string Company { get; set; }

        /// <summary>
        /// Preferred Method Of Contact - True for Phone, False for Email (Default)
        /// </summary>
        [Required]
        [Display(Name = "Preferred Method Of Contact")]
        public bool PreferredMethodOfContact { get; set; }

        /// <summary>
        /// Email Address
        /// </summary>
        ////[RequiredIfTrue("PreferredMethodOfContact", ErrorMessage = "Must enter a valid email address.")]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        /// <summary>
        /// Phone Number
        /// </summary>
        ////[RequiredIfFalse("PreferredMethodOfContact", ErrorMessage = "Must enter a valid phone number.")]
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"/^\+[1-9]{1,4}[0-9]{10,14}$/", ErrorMessage = "Please specify a valid phone number. Eg. +61783274823")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Contact Message
        /// </summary>
        [Required(ErrorMessage = "This field is required")]
        [Display(Name = "Message")]
        [DataType(DataType.Text)]
        public string ContactMessage { get; set; }

        /// <summary>
        /// Terms And Conditions
        /// </summary>
        [Required(ErrorMessage = "You must accept the terms and conditions.")]
        [Display(Name = "I accept the terms and conditions")]
        public bool TermsAndConditions { get; set; }

        /// <summary>
        /// System Messages
        /// </summary>
        public List<string> Messages { get; set; }
    }
}