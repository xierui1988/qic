﻿using Qic.Sc.Entities.Metadata;

using System.Collections.Generic;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;

namespace Qic.Sc.Web.Areas.Portal.Models
{
    using System;
    using System.Linq;

    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;

    /// <summary>
    /// The document list filter model.
    /// </summary>
    public class DocumentListFilterModel : ReportFilterMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentListFilterModel"/> class.
        /// </summary>
        public DocumentListFilterModel()
        {
            this.Accounts = new List<FilterOptionModel>();
            this.Products = new List<FilterOptionModel>();
            this.HoldingAccounts = new List<FilterOptionModel>();
            this.Reports = new List<FilterOptionModel>();
        }

        /// <summary>
        /// Gets or sets the accounts.
        /// </summary>
        public List<FilterOptionModel> Accounts { get; set; }

        /// <summary>
        /// Gets or sets the products.
        /// </summary>
        public List<FilterOptionModel> Products { get; set; }

        /// <summary>
        /// Gets or sets the holding accounts.
        /// </summary>
        public List<FilterOptionModel> HoldingAccounts { get; set; }

        /// <summary>
        /// Gets or sets the reports.
        /// </summary>
        public List<FilterOptionModel> Reports { get; set; }

        /// <summary>
        /// Enumuration of the types of reports.
        /// </summary>
        public ReportType ReportType { get; set; }

        /// <summary>
        /// The set filter options from.
        /// </summary>
        public DocumentListFilterModel SetFilterOptionsFrom(ICollection<ReportAccessMetadata> access)
        {
            if (access == null || !access.Any())
            {
                return this;
            }

            this.Accounts.AddRange(
                (from a in access
                 where a.AccountCode != null
                group a by a.AccountCode
                    into grp
                    select new FilterOptionModel { Id = grp.Key, Name = grp.Max(a => a.AccountName) }).OrderBy(a => a.Name));

            this.Reports.AddRange(
                (from a in access
                 where a.ReportCode != null
                group a by a.ReportCode
                    into grp
                    select new FilterOptionModel { Id = grp.Key, Name = grp.Max(a => a.ReportName) }).OrderBy(a => a.Name));

            this.HoldingAccounts.AddRange(
                (from a in access
                 where a.HoldingAccountCode != null
                group a by a.HoldingAccountCode
                    into grp
                    select new FilterOptionModel { Id = grp.Key, Name = grp.Max(a => a.HoldingAccountName) }).OrderBy(a => a.Name));

            this.Products.AddRange(
                (from a in access
                 where a.ProductCode != null
                group a by a.ProductCode
                    into grp
                    select new FilterOptionModel { Id = grp.Key, Name = grp.Max(a => a.ProductName) }).OrderBy(a => a.Name));

            return this;
        }
    }

    /// <summary>
    /// The filter option.
    /// </summary>
    public class FilterOptionModel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public string Id { get; set; }
    }

    /// <summary>
    /// Document Model
    /// </summary>
    public class DocumentListModel
    {
        /// <summary>
        /// Gets or sets the filter.
        /// </summary>
        public DocumentListFilterModel Filter { get; set; }

        /// <summary>
        /// Gets or sets the take.
        /// </summary>
        public int Take { get; set; }

        /// <summary>
        /// Gets or sets the skip.
        /// </summary>
        public int Skip { get; set; }

        /// <summary>
        /// Gets or sets the total.
        /// </summary>
        public int Total { get; set; }

        /// <summary>
        /// Gets or sets the report group.
        /// </summary>
        public string ReportGroup { get; set; }

        /// <summary>
        /// Filter criteria for related reports 
        /// </summary>
        public FilterCriteriaModel FilterCriteria { get; set; }

        /// <summary>
        /// Gets or sets the documents.
        /// </summary>
        public IEnumerable<DocumentModel> Documents { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is filter required.
        /// </summary>
        public bool IsFilterRequired { get; set; }
        
        /// <summary>
        /// search term on the model
        /// </summary>
        public string Q { get; set; }
        
        /// <summary>
        /// A string to represent the cutoff date for recent documents.
        /// </summary>
        public string CutoffDate { get; set; }

        /// <summary>
        /// Count for the users favourites
        /// </summary>
        public int FavouriteCount { get; set; }
    }

    /// <summary>
    /// The document model.
    /// </summary>
    public class DocumentModel
    {
        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string RelativeUrl { get; set; }

        /// <summary>
        /// Gets or sets the file name.
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the report name.
        /// </summary>
        public string ReportName { get; set; }

        /// <summary>
        /// Gets or sets the report Id.
        /// </summary>
        public Guid ReportId { get; set; }

        /// <summary>
        /// Gets or sets Document Type
        /// </summary>
        public string DocumentType { get; set; }

        /// <summary>
        /// Gets or Sets the Visibility of the report.
        /// </summary>
        public bool IsVisible { get; set; }

        /// <summary>
        /// Gets or Sets the Is Recent .
        /// </summary>
        public bool IsRecent { get; set; }

        /// <summary>
        /// Gets or Sets the indication of a new report.
        /// </summary>
        public bool IsNew { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is favourite.
        /// </summary>
        public bool IsFavourite { get; set; }

        /// <summary>
        /// Gets or sets the report date.
        /// </summary>
        public string ReportDate { get; set; }

        /// <summary>
        /// Gets or sets the publish date.
        /// </summary>
        public string PublishedDate { get; set; }

        /// <summary>
        /// The from document.
        /// </summary>
        /// <param name="document">
        /// The document.
        /// </param>
        /// <param name="relativeUrl">
        /// Application relative path to the document.
        /// </param>
        /// <param name="accounts">
        /// Accounts filter.
        /// </param>
        /// <returns>
        /// The <see cref="DocumentModel"/>.
        /// </returns>
        public DocumentModel FromDocument(IDocument document, string relativeUrl, ICollection<string> accounts)
        {
            this.FileName = document.DocumentName;
            this.ReportName = document.ReportName;
            this.Name = document.GetAccountNames(accounts);
            this.ReportDate = document.ReportDate.ToString("dd MMM yyyy");
            this.DocumentType = document.ReportTypeName;
            this.PublishedDate = document.PublishedDate.HasValue && document.PublishedDate != new DateTime() ?
                document.PublishedDate.Value.ToString("dd MMM yyyy") + " " + document.PublishedDate.Value.ToShortTimeString() : 
                string.Empty;
            this.IsFavourite = document.IsFavorite;
            this.IsNew = document.IsUnreadRecent;
            this.IsVisible = true;
            this.ReportId = document.EntityId;
            this.RelativeUrl = relativeUrl;

            return this;
        }

        /// <summary>
        /// Froms the document.
        /// </summary>
        /// <param name="document">The document.</param>
        /// <param name="relativeUrl">The relative URL.</param>
        /// <returns>The <see cref="DocumentModel"/>.</returns>
        public DocumentModel FromDocument(IForm document, string relativeUrl)
        {
            this.FileName = document.EntityDisplayName ?? document.EntityName;
            this.ReportName = document.EntityName;
            this.Name = document.AccountName;
            this.ReportDate = document.PublishedDate.HasValue && document.PublishedDate != new DateTime()
                ? document.PublishedDate.Value.ToString("dd MMM yyyy") + " " +
                  document.PublishedDate.Value.ToShortTimeString()
                : string.Empty;
            this.PublishedDate = this.ReportDate;
            this.IsFavourite = document.IsFavorite;
            this.IsNew = document.IsUnreadRecent;
            this.IsVisible = true;
            this.ReportId = document.EntityId;
            this.RelativeUrl = relativeUrl;

            return this;
        }
    }

    /// <summary>
    /// Report Type Enum
    /// </summary>
    public enum ReportType
    {
        All,
        Recent,
        Favourite
    }
}