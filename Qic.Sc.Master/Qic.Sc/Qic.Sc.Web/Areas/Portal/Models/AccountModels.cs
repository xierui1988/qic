﻿namespace Qic.Sc.Web.Areas.Portal.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// Username Model
    /// </summary>
    public class UserModel : AccountModelBase
    {
        /// <summary>
        /// User's username
        /// </summary>
        [Required]
        [Display(Name = "Email Address")]
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }

        /// <summary>
        /// User's first name
        /// </summary>
        [DataType(DataType.Text)]
        public string FirstName { get; set; }

        /// <summary>
        /// User's last name
        /// </summary>
        [DataType(DataType.Text)]
        public string LastName { get; set; }

        /// <summary>
        /// User's username
        /// </summary>
        [DataType(DataType.Text)]
        public string FullName { get; set; }

        /// <summary>
        /// User's username
        /// </summary>
        [DataType(DataType.Text)]
        public string Company { get; set; }

        /// <summary>
        /// User's username
        /// </summary>
        [DataType(DataType.EmailAddress)]
        public string AlternativeEmail { get; set; }
    }

    /// <summary>
    /// Account Model
    /// </summary>
    public interface IAccountModelBase
    {
        /// <summary>
        /// System Message
        /// </summary>
        List<string> Messages { get; set; }
    }

    /// <summary>
    /// Account Model
    /// </summary>
    public class AccountModelBase : IAccountModelBase
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AccountModelBase"/> class.
        /// </summary>
        public AccountModelBase()
        {
            this.Messages = new List<string>();
        }

        /// <summary>
        /// System Message
        /// </summary>
        public List<string> Messages { get; set; }
    }

    /// <summary>
    /// Login Model
    /// </summary>
    public class AccountLoginModel : AccountModelBase
    {
        /// <summary>
        /// User's Password
        /// </summary>
        [Required]
        public UserModel User { get; set; }

        /// <summary>
        /// User's Password
        /// </summary>
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the return URL.
        /// </summary>
        public string ReturnUrl { get; set; }
    }

    /// <summary>
    /// Change Password
    /// </summary>
    public class ChangePasswordModel : AccountModelBase
    {
        /// <summary>
        /// New Password
        /// </summary>
        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [RegularExpression(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$", ErrorMessage = "Minimum password complexity strength requires one digit, one lower and upper case letter, and at least 8 characters.")]
        [DataType(DataType.Password)]
        [Display(Name = "New Password")]
        public string NewPassword { get; set; }

        /// <summary>
        /// Confirming New Password
        /// </summary>
        [DataType(DataType.Password)]
        [Display(Name = "Confirm")]
        [Compare("NewPassword", ErrorMessage = "The new password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        /// <summary>
        /// A relative path for a redirect after successful password change.
        /// </summary>
        public string ReturnUrl { get; set; }

        /// <summary>
        /// A reset token.
        /// </summary>
        public string Token { get; set; }

        /// <summary>
        /// Token time-stamp.
        /// </summary>
        public string Timestamp { get; set; }

        /// <summary>
        /// User name.
        /// </summary>
        public UserModel User { get; set; }
    }

    /// <summary>
    /// The send password notification model.
    /// </summary>
    public class SendPasswordNotificationModel : AccountModelBase
    {
        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the identifier.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is successful.
        /// </summary>
        public bool IsSuccessful { get; set; }
    }

    /// <summary>
    /// The send password notification model.
    /// </summary>
    public enum SendPasswordNotificationType
    {
        Reset,
        Welcome,
    }
}