﻿namespace Qic.Sc.Web.Areas.Portal.Models
{
    /// <summary>
    /// Dashboard Model
    /// </summary>
    public class DashboardModels
    {
        /// <summary>
        /// Cut off Date
        /// </summary>
        public string CutoffDate { get; set; }

        /// <summary>
        /// Favourite Count
        /// </summary>
        public int FavouriteCount { get; set; }
    }
}