﻿namespace Qic.Sc.Web.Areas.Portal.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// Library navigation model.
    /// </summary>
    public class LibraryNavigationModel
    {
        /// <summary>
        /// Paths to library folders. Derived from report groups.
        /// </summary>
        public IEnumerable<LibraryFolderModel> Folders { get; set; }

        /// <summary>
        /// Path to the profile page.
        /// </summary>
        public string ProfilePath { get; set; }
        
        /// <summary>
        /// the count of Recent Unread document count
        /// </summary>
        public string RecentCount { get; set; }
    }

    /// <summary>
    /// Library folder model.
    /// </summary>
    public class LibraryFolderModel
    {
        /// <summary>
        /// The name of the library folder.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Path to the page that renders the forlder.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// indicate if the folder is active
        /// </summary>
        public bool Active { get; set; }
        
        /// <summary>
        ///  indicates the order of the folders
        /// </summary>
        public int CrmFolderOrder { get; set; }
    }
}