﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qic.Sc.Web.Areas.Portal.Models
{
    /// <summary>
    /// Search term model
    /// </summary>
    public class SearchTermModel
    {
        /// <summary>
        /// The search term
        /// </summary>
        public string Q { get; set; }
    }
}