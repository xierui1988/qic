﻿using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;

namespace Qic.Sc.Web.Areas.Portal.Controllers
{
    using System.Web.Mvc.Filters;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;

    using System;
    using System.Linq;
    using System.Web.Mvc;

    /// <summary>
    /// The report controller.
    /// </summary>
    public partial class DownloadController : Controller
    {
        private readonly IContentRepository contentRepository;
        private readonly IWebSecurityService webSecurityService;
        private readonly IPortalWebSecurityService portalWebSecurityService;
        private readonly IAuditService auditService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportController"/> class.
        /// </summary>
        public DownloadController(IReportService reportService, IContentRepository contentRepository, IWebSecurityService webService, IPortalWebSecurityService portalWebSecurityService, IAuditService auditService, ILinkService linkService)
        {
            if (reportService == null)
            {
                throw new ArgumentNullException("reportService");
            }
            if (webService == null)
            {
                throw new ArgumentNullException("webService");
            }
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            if (auditService == null)
            {
                throw new ArgumentNullException("auditService");
            }
            if (linkService == null)
            {
                throw new ArgumentNullException("auditService");
            }
            this.contentRepository = contentRepository;
            this.webSecurityService = webService;
            this.auditService = auditService;
            this.portalWebSecurityService = portalWebSecurityService;
        }

        /// <summary>
        /// Accepts a Get of the a report file. 
        /// </summary>
        [Authorize]
        public virtual ActionResult Download(Guid? documentId, string fileName, bool download = false)
        {
            var user = this.webSecurityService.GetCurrentAuthenticatedUsername();
            var doc = this.contentRepository.GetEntity<DocumentBlobEntity>(documentId.ToString());

            if (documentId == null || doc == null)
            {
                return this.HttpNotFound();
            }

            var reportlist = this.portalWebSecurityService.GetReportAccessFor(user).ToArray();

            if (!this.webSecurityService.IsAdmin(user))
            {
                if (!reportlist.Any(x => x.GetEntityCode() == doc.GetEntityCode() && x.ReportCode == doc.ReportCode))
                {
                    var downloadAccessDeniedUrl = this.Url.ForItem(Constants.Content.ClientPortal.Home.DownloadAccessDenied.ItemID.Guid).ToLower().Replace(Constants.Content.ClientPortal.Home.Path.ToLower(), string.Empty);
                    return this.Redirect(downloadAccessDeniedUrl);
                }
            }

            this.auditService.Audit(new AuditMetadata
            {
                UserName = this.webSecurityService.GetCurrentAuthenticatedUsername(),
                Action = EnumUtils<SitecoreUserAction>.GetDescription(SitecoreUserAction.DocumentDownload),
                Db = Constants.WebDb,
                Path = string.Format("{0}{1}", Constants.ContentPath, doc.EntityContentPath),
                Language = doc.EntityLanguage.Name,
                Version = doc.EntityVersion,
                Id = string.Format(Constants.ItemIDFormat, doc.EntityId.ToString()),
                Miscellaneous = doc.GetAccountNames(reportlist.GetAccessibleAccountCodes())
            });

            // Flag document as no longer new. Save if needed.
            var userModel = this.webSecurityService.GetUserMetadataFor(this.webSecurityService.GetCurrentAuthenticatedUsername());
            if (userModel.ReadDocuments.MarkRecentDocumentAsRead(doc.EntityId, doc.PublishedDate.GetValueOrDefault()))
            {
                this.webSecurityService.UpdatedUserMetadataFor(this.webSecurityService.GetCurrentAuthenticatedUsername(), userModel);
            }

            if (download)
            {
                this.Response.Headers.Add("Content-Disposition", "attachment");
            }

            var extension = (FileExtensionToMimeMap)Enum.Parse(typeof(FileExtensionToMimeMap), doc.Extension, true);
            var mimeType = EnumUtils<FileExtensionToMimeMap>.GetDescription(extension);
            return new FileStreamResult(doc.Blob, mimeType);
        }

        /// <summary>
        /// Accepts a Get of the a report file. 
        /// </summary>
        [Authorize]
        public virtual ActionResult DownloadForm(Guid? documentId, string fileName, bool download = false)
        {
            var user = this.webSecurityService.GetCurrentAuthenticatedUsername();
            var doc = this.contentRepository.GetEntity<FormBlobEntity>(documentId.ToString());

            if (documentId == null || doc == null)
            {
                return this.HttpNotFound();
            }

            var reportlist = this.portalWebSecurityService.GetReportAccessFor(user);
            if (reportlist.All(x => x.AccountCode != doc.AccountCode))
            {
                var downloadAccessDeniedUrl = this.Url.ForItem(Constants.Content.ClientPortal.Home.DownloadAccessDenied.ItemID.Guid).ToLower().Replace(Constants.Content.ClientPortal.Home.Path.ToLower(), string.Empty);
                return this.Redirect(downloadAccessDeniedUrl);
            }

            this.auditService.Audit(new AuditMetadata
            {
                UserName = this.webSecurityService.GetCurrentAuthenticatedUsername(),
                Action = EnumUtils<SitecoreUserAction>.GetDescription(SitecoreUserAction.DocumentDownload),
                Db = Constants.WebDb,
                Path = string.Format("{0}{1}", Constants.ContentPath, doc.EntityContentPath),
                Language = doc.EntityLanguage.Name,
                Version = doc.EntityVersion,
                Id = string.Format(Constants.ItemIDFormat, documentId.ToString()),
                Miscellaneous = doc.AccountName
            });

            // Flag document as no longer new. Save if needed.
            var userModel = this.webSecurityService.GetUserMetadataFor(this.webSecurityService.GetCurrentAuthenticatedUsername());
            if (userModel.ReadDocuments.MarkRecentDocumentAsRead(doc.EntityId, doc.PublishedDate.GetValueOrDefault()))
            {
                this.webSecurityService.UpdatedUserMetadataFor(this.webSecurityService.GetCurrentAuthenticatedUsername(), userModel);
            }

            if (download)
            {
                this.Response.Headers.Add("Content-Disposition", "attachment");
            }

            var extension = (FileExtensionToMimeMap)Enum.Parse(typeof(FileExtensionToMimeMap), doc.Extension);
            var mimeType = EnumUtils<FileExtensionToMimeMap>.GetDescription(extension);
            return new FileStreamResult(doc.Blob, mimeType);
        }

        /// <summary>
        /// Called when authorization occurs.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnAuthentication(AuthenticationContext filterContext)
        {
            // Try getting a user based on HTTP context. Sitecore will only
            // try to set identity if it resolves a site. Since this is vanilla
            // MVC controller we need to force user resolution.
            filterContext.Principal = this.webSecurityService.GetActiveUser();
        }

        /// <summary>
        /// Called when authorization challenge occurs.
        /// </summary>
        /// <param name="filterContext">Information about the current request and action.</param>
        protected override void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
            if (filterContext.HttpContext.User == null || !filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                var loginUrl = this.Url.ForItem(Constants.Content.ClientPortal.Home.Login.ItemID.Guid);
                if (loginUrl.StartsWith(Constants.Content.ClientPortal.Home.Path, StringComparison.InvariantCultureIgnoreCase))
                {
                    loginUrl = loginUrl.Substring(Constants.Content.ClientPortal.Home.Path.Length);
                }

                // Unauthenticated users should be authenticated first. Direct them to login page.
                filterContext.Result =
                    new RedirectResult(
                        string.Format("{0}?returnUrl={1}", loginUrl, this.Server.UrlEncode(this.Request.RawUrl)));
            }
        }
    }
}
