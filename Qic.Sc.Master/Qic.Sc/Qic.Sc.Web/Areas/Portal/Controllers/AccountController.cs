﻿namespace Qic.Sc.Web.Areas.Portal.Controllers
{
    using System;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Security;

    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Interfaces.Crm;
    using Qic.Sc.Web.Areas.Portal.Models;
    using Qic.Sc.Web.Controllers;

    using Sitecore.StringExtensions;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Pages.Alertpage;
    using Qic.Sc.Entities;

    /// <summary>
    /// Account Controller
    /// </summary>
    public partial class AccountController : RenderingController
    {
        private readonly IWebSecurityService webSecurity;
        private readonly IPortalWebSecurityService portalWebSecurity;
        private readonly IEmailService emailService;
        private readonly ITokenService tokenService;
        private readonly ILoggerService loggerService;
        private readonly IContentRepository contentRepository;
        private readonly IConfigurationService configurationService;
        private readonly ISiteService siteService;

        /// <summary>
        /// Account Controller Constructor
        /// </summary>
        public AccountController(IWebSecurityService webSecurity, IPortalWebSecurityService portalWebSecurity, IEmailService emailService, ICrmService crmService, ITokenService tokenService, ILoggerService loggerService, IContentRepository contentRepository, IConfigurationService configurationService, ISiteService siteService)
            : base(loggerService)
        {
            if (webSecurity == null)
            {
                throw new ArgumentNullException("webSecurity");
            }
            if (portalWebSecurity == null)
            {
                throw new ArgumentNullException("portalWebSecurity");
            }
            if (emailService == null)
            {
                throw new ArgumentNullException("emailService");
            }
            if (crmService == null)
            {
                throw new ArgumentNullException("crmService");
            }
            if (tokenService == null)
            {
                throw new ArgumentNullException("tokenService");
            }
            if (loggerService == null)
            {
                throw new ArgumentNullException("loggerService");
            }
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            if (configurationService == null)
            {
                throw new ArgumentNullException("configurationService");
            }
            if (siteService == null)
            {
                throw new ArgumentNullException("siteService");
            }

            this.webSecurity = webSecurity;
            this.portalWebSecurity = portalWebSecurity;
            this.emailService = emailService;
            this.tokenService = tokenService;
            this.loggerService = loggerService;
            this.contentRepository = contentRepository;
            this.configurationService = configurationService;
            this.siteService = siteService;
        }

        /// <summary>
        /// User Login
        /// </summary>
        public virtual ActionResult Login(string returnUrl, string authenicatedUserRedirectUrl)
        {
            if (!string.IsNullOrEmpty(authenicatedUserRedirectUrl) && User.Identity.IsAuthenticated)
            {
                return this.Redirect(authenicatedUserRedirectUrl);
            }

            return this.View(new AccountLoginModel { ReturnUrl = returnUrl });
        }

        /// <summary>
        /// User Login
        /// </summary>
        [HttpPost]
        public virtual ActionResult Login(AccountLoginModel model)
        {
            if (!this.ModelState.IsValid)
            {
                return this.View(model);
            }

            model.Password = model.Password.Trim();
            model.User.UserName = model.User.UserName.Trim();

            var domainQaulifiedUserName = string.Format("{0}\\{1}", Constants.CrmDomain, model.User.UserName);
            if (this.webSecurity.IsValidPassword(domainQaulifiedUserName, model.Password))
            {
                var access = this.portalWebSecurity.GetReportAccessFor(domainQaulifiedUserName);
                if (access == null || !access.Any())
                {
                    // The user does exist, however has no access privileges
                    // Access denied!
                    return this.Redirect(this.Url.ForItem(Constants.Content.ClientPortal.Home.Login.AccessDenied.ItemID.Guid));
                }

                FormsAuthentication.SetAuthCookie(domainQaulifiedUserName, createPersistentCookie: false);
                return this.Redirect(model.ReturnUrl ?? "/");
            }

            model.Messages.Add("Unable to login, unknown email address or password.");
            return this.View(model);
        }

        /// <summary>
        /// Log out
        /// </summary>
        /// <returns>URL Action</returns>
        public virtual ActionResult LogOff()
        {
            string[] myCookies = Request.Cookies.AllKeys;
            foreach (string cookie in myCookies)
            {
                var httpCookie = Response.Cookies[cookie];
                if (httpCookie != null)
                {
                    httpCookie.Expires = DateTime.Now.AddDays(-1);
                }

                if (cookie != Constants.QicUserNameCookie)
                {
                    continue;
                }
                var replaceSharedCookie = new HttpCookie(Constants.QicUserNameCookie)
                {
                    Expires = DateTime.Now.AddDays(-1), // make it expire yesterday
                    Name = Constants.QicUserNameCookie,
                    Value = null,
                    Domain = this.configurationService.GetGlobalSetting("UserInformtionCookieDomain"),
                    HttpOnly = true,
                    Shareable = true
                };
                Response.Cookies.Add(replaceSharedCookie); // overwrite it
            }

            Session.Abandon();

            return
                this.Redirect(this.GenerateRelativeUrl(Constants.Content.ClientPortal.Home.ItemID.Guid));
        }

        /// <summary>
        /// Set Password
        /// </summary>
        /// <returns>URL Action</returns>
        [AllowAnonymous]
        public virtual ActionResult ResetPassword()
        {
            return this.View(new UserModel());
        }

        /// <summary>
        /// Initiate password reset.
        /// </summary>
        [HttpPost]
        public virtual ActionResult ResetPassword(string returnUrl, UserModel model, bool isNew = false)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    this.SendNotification(model.UserName, isNew);
                    return this.Redirect(returnUrl);
                }
                catch (Exception ex)
                {
                    const string Message = "We are unable to complete your request at this time.  Please contact QIC Client Services on clientservices@qic.com for further assistance.";
                    this.loggerService.Error(Message, ex);
                    model.Messages.Add(Message);
                }
            }

            // Add errors from the form fields.
            model.Messages.AddRange(
                this.ModelState.Values.Where(e => e.Errors.Count > 0).SelectMany(e => e.Errors).Select(e => e.ErrorMessage));

            return this.View(model);
        }

        /// <summary>
        /// Change password.
        /// </summary>
        public virtual ActionResult ChangePassword(string user, string tk, string ts, string expiredUrl)
        {
            if (!this.ValidateToken(user, tk, ts))
            {
                return this.Redirect(expiredUrl);
            }

            return
                this.View(
                    new ChangePasswordModel { Token = tk, Timestamp = ts, User = new UserModel { UserName = user } });
        }

        /// <summary>
        /// Set Password
        /// </summary>
        [HttpPost]
        public virtual ActionResult ChangePassword(string returnUrl, string expiredUrl, ChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (!this.ValidateToken(model.User.UserName, model.Token, model.Timestamp))
                {
                    // Token is no longer valid.
                    return this.Redirect(expiredUrl);
                }

                try
                {
                    // Initiate password reset.
                    this.webSecurity.ResetPassword(string.Format("{0}\\{1}", Constants.CrmDomain, model.User.UserName), model.NewPassword);
                    return this.Redirect(returnUrl);
                }
                catch (Exception ex)
                {
                    const string Message =
                        "A error occurred during password reset, please retry. "
                        + "If the error persist contact QIC for assistance.";

                    model.Messages.Add(Message);
                    this.loggerService.Error(Message, ex);
                }
            }

            // Add model errors.
            model.Messages.AddRange(
                this.ModelState.Values.Where(e => e.Errors.Count > 0)
                    .SelectMany(e => e.Errors)
                    .Select(e => e.ErrorMessage));

            // Error
            return this.View(model);
        }

        /// <summary>
        /// User welcome controller
        /// </summary>
        public virtual ActionResult Welcome()
        {
            var welcome = new WelcomeModel();
            welcome.FirstName = this.webSecurity.GetActiveUserDisplayName();

            var alertsItemHolder = this.contentRepository.GetEntity<AlertsEntity>(Constants.Content.ClientPortal.Home.Alerts.ID);

            if (alertsItemHolder == null)
            {
                throw new ArgumentNullException(Constants.Content.ClientPortal.Home.Alerts.Path);
            }

            welcome.Alerts = alertsItemHolder.AlertPages.ToArray();
            welcome.Notifications = welcome.Alerts.Count();

            return this.View(welcome);
        }

        /// <summary>
        /// Sends the password notification.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="email">The email.</param>
        public virtual ActionResult SendPasswordNotification(string id, string email)
        {
            if (!this.webSecurity.AuthenticateXrmCredentials(id))
            {
                return new HttpUnauthorizedResult("Invalid credentials");
            }

            return this.View(new SendPasswordNotificationModel { UserName = email, Id = id });
        }

        /// <summary>
        /// The send password rest notification.
        /// </summary>
        [HttpPost]
        public virtual ActionResult SendPasswordNotification(SendPasswordNotificationModel model, SendPasswordNotificationType emailType, string userName)
        {
            if (!this.webSecurity.AuthenticateXrmCredentials(model.Id))
            {
                return new HttpUnauthorizedResult("Invalid credentials");
            }
            try
            {
                this.SendNotification(userName, emailType == SendPasswordNotificationType.Welcome);
                model.Messages.Add("Email notification has been sent.");
                model.IsSuccessful = true;
                return this.View(model);
            }
            catch (Exception ex)
            {
                model.Messages.Add(ex.Message);
                this.loggerService.Error("Failed to send email notification.", ex);
                return this.View(model);
            }
        }

        private void SendNotification(string userName, bool isWelcome)
        {
            try
            {
                // Generate rest token an link.
                var timesamp = DateTime.UtcNow;
                var localPath = this.Url.ForItem(Constants.Content.ClientPortal.Home.Login.ChangePassword.ItemID.Guid);
                var baseUri = new Uri(this.siteService.GetCanonicalUri(), localPath);

                var token = HttpUtility.UrlEncode(this.tokenService.GenerateToken(userName, timesamp));
                var resetUrl = string.Format("{0}/?tk={1}&ts={2}&user={3}", baseUri, token, timesamp.Ticks, userName);
                var domainUserName = string.Format("{0}\\{1}", Constants.CrmDomain, userName);

                // Send reset notification.
                if (isWelcome)
                {
                    this.emailService.SendWelcomeNotification(domainUserName, resetUrl);
                }
                else
                {
                    this.emailService.SendPasswordResetNotification(domainUserName, resetUrl);
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed to send notification for: " + userName, ex);
            }
        }

        /// <summary>
        /// Account Controls
        /// </summary>
        /// <returns>Return Page</returns>
        public virtual ActionResult AccountControl()
        {
            if (User.Identity.IsAuthenticated)
            {
                return this.View(new Corporate.Model.TopNavModel { UserFirstName = this.webSecurity.GetActiveUserDisplayName() ?? string.Empty });
            }
            return this.View();
        }

        /// <summary>
        /// Validate the token
        /// </summary>
        /// <param name="user">The user</param>
        /// <param name="tk">The Token</param>
        /// <param name="ts">The Timestamp</param>
        /// <returns>Return if ok or not</returns>
        private bool ValidateToken(string user, string tk, string ts)
        {
            try
            {
                var timestamp = Convert.ToInt64(ts);
                return this.tokenService.ValidateToken(tk, user, new DateTime(timestamp, DateTimeKind.Utc));
            }
            catch (Exception ex)
            {
                // Token service error, potentially valid token validation failed.
                // Behave as if the token is invalid.
                var message = "Token validation error: User({0}), Token({1}), Timestamp({2})".FormatWith(tk, user, ts);
                this.loggerService.Error(message, ex);
                return false;
            }
        }
    }
}
