﻿using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Qic.Sc.Entities;
using Qic.Sc.Interfaces.Components;
using Qic.Sc.Interfaces.Core;
using Qic.Sc.Web.Areas.Corporate.Model;

namespace Qic.Sc.Web.Areas.Portal.Controllers
{
    /// <summary>
    /// Navigation Controller
    /// </summary>
    public partial class NavigationController : Controller
    {
        private readonly INavigationService navigationService;
        private readonly IWebSecurityService webSecurity;
        private readonly IConfigurationService configurationService;
        private readonly ILoggerService loggerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NavigationController"/> class.
        /// </summary>
        public NavigationController(INavigationService navigationService, IWebSecurityService webSecurity, IConfigurationService configurationService, ILoggerService loggerService)
        {
            if (navigationService == null)
            {
                throw new ArgumentNullException("navigationService");
            }
            if (webSecurity == null)
            {
                throw new ArgumentNullException("webSecurity");
            }
            if (configurationService == null)
            {
                throw new ArgumentNullException("configurationService");
            }
            if (loggerService == null)
            {
                throw new ArgumentNullException("loggerService");
            }
            this.navigationService = navigationService;
            this.webSecurity = webSecurity;
            this.configurationService = configurationService;
            this.loggerService = loggerService;
        }

        /// <summary>
        /// Renders Top Menu
        /// </summary>
        /// <param name="renderingDataSource">Data Source</param>
        /// <param name="renderingContextItem">Data Context</param>
        /// <param name="contextSite">Context of the Site</param>
        /// <returns>Renders Top Menu View</returns>
        public virtual ActionResult TopMenu(string renderingDataSource, Guid? renderingContextItem, string contextSite = null)
        {
            var menu = this.navigationService.GetMenu(renderingDataSource, renderingContextItem);
            menu = menu.Where(x => x.EntityId != Constants.Content.Corporate.Configuration.Navigation.TopMenu.PortalLogin.ItemID.Guid);
            //This sets the cookie if signed
            this.GetUserDisplayName();

            return this.View(new NavigationModel { Menu = menu, ContextSite = contextSite });
        }

        /// <summary>
        /// (1) Check we have a user name cookie and use the name.
        /// (2) Gets the user name of the authenticated user and set the cookie.
        /// (3) Otherwise null.
        /// </summary>
        public string GetUserDisplayName()
        {
            var cookie = this.Request.Cookies.Get(Entities.Constants.QicUserNameCookie);
            if (cookie != null)
            {
                // Get the user name from the cookie
                return cookie.Value;
            }

            try
            {
                var user = this.webSecurity.GetActiveUser();
                if (user.Identity.IsAuthenticated)
                {
                    var name = this.webSecurity.GetActiveUserDisplayName();

                    this.Response.Cookies.Add(
                        new HttpCookie(Entities.Constants.QicUserNameCookie)
                        {
                            Domain = this.configurationService.GetGlobalSetting("UserInformtionCookieDomain"),
                            Value = name,
                            HttpOnly = true,
                            Shareable = true
                        });
                }
            }
            catch (Exception ex)
            {
                this.loggerService.Error("Failed get authenticated user and set user name cookie.", ex);
            }

            return null;
        }

        /// <summary>
        /// Renders a menu specified via data source in a context of an item.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult MegaMenu(string renderingDataSource, Guid? renderingContextItem, string contextSite)
        {
            var menu = this.navigationService.GetMenu(renderingDataSource, renderingContextItem);
            return this.View(new NavigationModel()
            {
                Menu = menu,
                ContextSite = contextSite
            });
        }

        /// <summary>
        /// Renders Top Menu
        /// </summary>
        /// <param name="renderingDataSource">Data Source</param>
        /// <param name="renderingContextItem">Data Context</param>
        /// <param name="contextSite">Context of the Site</param>
        /// <returns>Renders Top Menu View</returns>
        public virtual ActionResult MegaTopMenu(string renderingDataSource, Guid? renderingContextItem, string contextSite = null)
        {
            var menu = this.navigationService.GetMenu(renderingDataSource, renderingContextItem);
            menu = menu.Where(x => x.EntityId != Constants.Content.Corporate.Configuration.Navigation.TopMenu.PortalLogin.ItemID.Guid);

            return this.View(new NavigationModel { Menu = menu, ContextSite = contextSite });
        }
    }
}