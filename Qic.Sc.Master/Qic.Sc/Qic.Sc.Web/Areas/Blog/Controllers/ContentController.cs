﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using Qic.Sc.Entities;
using Qic.Sc.Entities.UserDefined.Components.Pages.Pagecontent;
using Qic.Sc.Interfaces.Components;
using Qic.Sc.Interfaces.Core;
using Qic.Sc.Web.Areas.Blog.Model;
using Qic.Sc.Web.Controllers;

namespace Qic.Sc.Web.Areas.Blog.Controllers
{
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.Editorial;
    using Qic.Sc.Services.Core;

    using Sitecore.ContentSearch.Linq;
    using Qic.Sc.Entities.UserDefined.QIC.Blog.Data.BlogArticle;

    /// <summary>
    /// The content controller.
    /// </summary>
    public partial class ContentController : Controller
    {
        private readonly IContentRepository contentRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentController"/> class.
        /// </summary>
        public ContentController(
            IContentRepository contentRepository)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }

            this.contentRepository = contentRepository;
        }
                
        /// <summary>
        /// Search content
        /// </summary>
        /// <param name="searchQuery">Search query</param>
        /// <returns>Search results</returns>
        public virtual ActionResult Search(string searchQuery)
        {
            IEnumerable<IBlogArticle> results = null;
            int total = 0;

            if (!string.IsNullOrWhiteSpace(searchQuery))
            {
                using (var search = this.contentRepository.GetSearchService())
                {
                    var query =
                        from article in // Only search site content
                            search.GetDescendantsQueryable<BlogArticleEntity>(Constants.Content.Blog.Home.ItemID.Guid)
                        where
                            article.Article.Like(searchQuery) || article.Title.Like(searchQuery) || article.Description.Like(searchQuery)
                        select article;

                    total = query.Count();

                    var test = query.ToList();

                    results = query.ToArray();
                }
            }

            return
                this.View(
                    new SearchModel
                    {
                        SearchQuery = searchQuery,
                        ResultsCount = total,
                        SearchResults = results,
                    });
        }
    }
}