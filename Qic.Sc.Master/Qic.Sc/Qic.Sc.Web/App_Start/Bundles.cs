﻿using System.Diagnostics.CodeAnalysis;

namespace Links
{
    [SuppressMessage("StyleCop.CSharp.NamingRules", "*", Justification = "Naming specified by T4MVC")]
    public static partial class Bundles
    {
        public static partial class Scripts
        {
            /// <summary>
            /// The name of script bundle for client portal.
            /// The path is kept as source to ensure relative reference work.
            /// </summary>
            public const string Portal = "~/content/portal/js/site.js";
            public const string Corporate = "~/content/corporate/js/site.js";
        }

        public static partial class Styles
        {
            /// <summary>
            /// The name of script bundle for client portal.
            /// The path is kept as source to ensure relative reference work.
            /// </summary>
            public const string Portal = "~/content/portal/css/site.css";
            public const string Corporate = "~/content/corporate/css/site.css";
        }
    }
}