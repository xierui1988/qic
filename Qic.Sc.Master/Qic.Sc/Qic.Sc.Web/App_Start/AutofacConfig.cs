﻿using System.Web;

using Qic.Sc.Web;

[assembly: PreApplicationStartMethod(typeof(AutofacConfig), "Configure")]

namespace Qic.Sc.Web
{
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Mvc;

    using Autofac;
    using Autofac.Integration.Mvc;
    using Autofac.Integration.WebApi;

    /// <summary>
    /// Autofac Dependency Injection configuration.
    /// </summary>
    public static class AutofacConfig
    {
        /// <summary>
        /// Configure IoC container for the application.
        /// </summary>
        public static void Configure()
        {
            var builder = new ContainerBuilder();

            builder.RegisterAssemblyModules(typeof(Qic.Sc.Services.Module).Assembly); // Register services module
            builder.RegisterControllers(Assembly.GetExecutingAssembly()); // Register controllers
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly()); // Register API controllers

            var container = builder.Build();

            // Expose our container to MVC infrastructure
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            GlobalConfiguration.Configuration.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            // Expose container to Platform code
            Qic.Sc.Platform.ServiceLocator.SetLocator(container);
        }
    }
}
