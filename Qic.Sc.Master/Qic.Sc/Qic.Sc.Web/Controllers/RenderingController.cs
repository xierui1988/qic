﻿namespace Qic.Sc.Web.Controllers
{
    using System;
    using System.Web.Mvc;

    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// Abstract controller providing basic rendering functionality.
    /// </summary>
    public abstract partial class RenderingController : Controller, IRenderingController
    {
        private ILoggerService loggerService;

        /// <summary>
        /// Initializes a new instance of the <see cref="Qic.Sc.Web.RenderingController"/> class.
        /// </summary>
        protected RenderingController(ILoggerService loggerService)
        {
            if (loggerService == null)
            {
                throw new ArgumentNullException("loggerService");
            }

            this.loggerService = loggerService;
        }

        /// <summary>
        /// Returns an instance of the <see cref="T:System.Web.Mvc.HttpNotFoundResult"/> class.
        /// </summary>
        /// <returns>
        /// An instance of the <see cref="T:System.Web.Mvc.HttpNotFoundResult"/> class.
        /// </returns>
        /// <param name="statusDescription">The status description.</param>
        protected override HttpNotFoundResult HttpNotFound(string statusDescription)
        {
            return new HttpContentNotFoundResult(statusDescription);
        }

        #region Error Handling

        /// <summary>
        /// Implementation specific behavior.
        /// Refer to the implementation for details on how exceptions are handled.
        /// </summary>
        /// <param name="filterContext">
        /// Exception context.
        /// </param>
        public void OnRenderingException(object filterContext)
        {
            try
            {
                this.OnException((ExceptionContext) filterContext);
            }
            catch (InvalidCastException ex)
            {
                throw new ArgumentException("Expected " + typeof(ExceptionContext).FullName, "filterContext", ex);
            }
        }

        /// <summary>
        /// The on exception is invoked from the error handling pipeline to
        /// allow controller to react to any unhanded errors on any of its actions.
        /// </summary>
        /// <param name="filterContext">
        /// The exception context.
        /// </param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {
                var message = string.Format(
                    "Action: {0}, Controller: {1}",
                    filterContext.RouteData.Values["action"],
                    filterContext.RouteData.Values["controller"]);

                if (this.loggerService == null)
                {
                    // We can't enforce implementation of constructor by hiding the default
                    // constructor as it interferes with T4MVC. Default to system logger.
                    // This way if the developer chooses not to have a logger in the controller
                    // we still catch errors via system logger.
                    this.loggerService = DependencyResolver.Current.GetService<ILoggerService>();
                }

                this.loggerService.Error(
                    "An unexpected error occurred in a rendering controller: " + message,
                    filterContext.Exception);

                // Render some generic into the page to leave a clue that a rendering should
                // have been there.
                filterContext.Result = new ContentResult { Content = string.Format("<!-- Rendering ERROR: {0} -->", message) };
                filterContext.ExceptionHandled = true;
            }
        }
        #endregion
    }
}