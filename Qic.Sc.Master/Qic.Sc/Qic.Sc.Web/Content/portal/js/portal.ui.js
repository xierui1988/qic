﻿!(function($, qic) {
    var alerttemplate = '';

    // Modal dialog ui
    function showModal(title, message) {
        var popupClass = "popupClass-model";
        var modelId = popupClass + ($(popupClass).length + 1);

        $('body').append('<div id="' + modelId + '" class="' + popupClass +'"><div class="container"><div class="modal show bs-example-modal-lg in" tabindex="-1" role="dialog" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button><h4 class="modal-title">'+ title +'</h4></div><div class="modal-body">'+ message +'</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Accept</button></div></div></div></div></div></div>');

        $('#' + modelId + ' button').click(function () {
            $('#' + modelId + ' container modal').modal('hide');
            $('#' + modelId).remove();
        });
    }

    // Expose public UI methods
    qic.ui = {
        modal: showModal
    };

})(jQuery, qic);