﻿using System;

namespace Qic.Sc.Entities.Metadata
{
    using global::System.Collections.Generic;

    /// <summary>
    /// The report filter and sort meta data.
    /// </summary>
    public class ReportFilterAndSortMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFilterAndSortMetadata"/> class.
        /// </summary>
        public ReportFilterAndSortMetadata()
        {
            // Defaults
            this.Sort = new ReportSortMetadata();
            this.Filters = new ReportFilterMetadata();
        }

        /// <summary>
        /// Gets the filters.
        /// </summary>
        public ReportFilterMetadata Filters { get; set; }

        /// <summary>
        /// Gets the sort.
        /// </summary>
        public ReportSortMetadata Sort { get; set; }
    }

    /// <summary>
    /// The report filter meta-data.
    /// </summary>
    public class ReportFilterMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportFilterMetadata"/> class.
        /// </summary>
        public ReportFilterMetadata()
        {
            this.ProductCodes = new List<string>();
            this.AccountCodes = new List<string>();
            this.HoldingAccountCodes = new List<string>();
            this.ReportCodes = new List<string>();
        }

        /// <summary>
        /// Gets or sets the report codes.
        /// </summary>
        public List<string> ReportCodes { get; set; }

        /// <summary>
        /// Gets or sets the account codes.
        /// </summary>
        public List<string> AccountCodes { get; set; }

        /// <summary>
        /// Gets or sets the product codes.
        /// </summary>
        public List<string> ProductCodes { get; set; }

        /// <summary>
        /// Gets or sets the holding account codes.
        /// </summary>
        public List<string> HoldingAccountCodes { get; set; }

        /// <summary>
        /// Gets or sets the report date from.
        /// </summary>
        public DateTime ReportDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the report date to.
        /// </summary>
        public DateTime ReportDateTo { get; set; }

        /// <summary>
        /// Gets or sets the publish date from.
        /// </summary>
        public DateTime PublishDateFrom { get; set; }

        /// <summary>
        /// Gets or sets the publish date to.
        /// </summary>
        public DateTime PublishDateTo { get; set; }
    }

    /// <summary>
    /// The report sort metadata.
    /// </summary>
    public class ReportSortMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportSortMetadata"/> class.
        /// </summary>
        public ReportSortMetadata()
        {
            // Default sort
            this.SortBy = ReportSortByMetadata.PublishDate;
            this.SortOrder = ReportSortOrderMetadata.Descending;
        }

        /// <summary>
        /// Gets or sets the sort by.
        /// </summary>
        public ReportSortByMetadata SortBy { get; set; }

        /// <summary>
        /// Gets or sets the sort order.
        /// </summary>
        public ReportSortOrderMetadata SortOrder { get; set; }
    }

    /// <summary>
    /// The report sort by meta data.
    /// </summary>
    public enum ReportSortByMetadata
    {
        DocumentName,
        ReportName,
        ReportDate,
        PublishDate,
    }

    /// <summary>
    /// The report sort order meta data.
    /// </summary>
    public enum ReportSortOrderMetadata
    {
        Accending,
        Descending,
    }
}
