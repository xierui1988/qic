﻿namespace Qic.Sc.Entities.Metadata
{
    /// <summary>
    /// Audit's metadata
    /// </summary>
    public class AuditMetadata
    {
        /// <summary>
        /// User Name (SCUser Column) Format: sitecore\admin
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// The Action Name (SCAction) E.g. Execute Workflow command
        /// </summary>
        public string Action { get; set; }

        /// <summary>
        /// Sitecore Database (not recorded) E.g. master
        /// Used to get the Site name of the item using ItemId
        /// </summary>
        public string Db { get; set; }

        /// <summary>
        /// Sitecore Item Full Path (ScPath) E.g: /sitecore/content/Home/Test item
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Sitecore Language (ScLanguage) the language of the item: E.g: en
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// Sitecore Item version number (ScItemVersion) E.g: 1
        /// </summary>
        public int Version { get; set; }

        /// <summary>
        /// Sitecore Item Id (ScItemId) E.g: {110D559F-DEA5-42EA-9C1C-8A5DF7E70EF9}
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Miscellanous Information such as the New name of the item (ScMisc)
        /// </summary>
        public string Miscellaneous { get; set; }
    }
}
