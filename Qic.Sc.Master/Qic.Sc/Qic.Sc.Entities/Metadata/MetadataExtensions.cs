﻿namespace Qic.Sc.Entities.Metadata
{
    using global::System.Collections.Generic;
    using global::System.Linq;

    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;

    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.Web;

    /// <summary>
    /// The report access meta data extensions.
    /// </summary>
    public static class MetadataExtensions
    {
        /// <summary>
        /// Gets distinct accessible report codes.
        /// </summary>
        public static ICollection<string> GetAccessibleReportCodes(this IEnumerable<ReportAccessMetadata> self)
        {
            if (self == null)
            {
                return null;
            }

            return self.Select(a => a.ReportCode).Distinct().ToArray();
        }

        /// <summary>
        /// The get accessible account codes.
        /// </summary>
        public static ICollection<string> GetAccessibleAccountCodes(this IEnumerable<ReportAccessMetadata> self)
        {
            if (self == null)
            {
                return null;
            }

            return self.Select(a => a.AccountCode).Distinct().ToArray();
        }

        /// <summary>
        /// The get invoice codes.
        /// </summary>
        public static ICollection<string> GetInvoiceCodes(this IEnumerable<ReportTypeMetadata> self)
        {
            if (self == null)
            {
                return null;
            }

            return self.Where(r => r.ReportTypeId == 909410005 /* CRM Option set value that represent invoices. */).Select(r => r.ReportCode).ToArray();
        }

        /// <summary>
        /// Trims report groups based on accessible report types.
        /// </summary>
        /// <param name="reports">report groups</param>
        /// <param name="accessibleReportCodes">accessible report code filter</param>
        /// <returns>List of Report Groups</returns>
        public static IEnumerable<ReportTypeMetadata> GetAccessibleReportTypes(this IEnumerable<ReportTypeMetadata> reports, ICollection<string> accessibleReportCodes)
        {
            if (reports == null)
            {
                return null;
            }

            if (accessibleReportCodes == null || !accessibleReportCodes.Any())
            {
                return reports; // Don't trim.
            }

            return reports.Where(r => accessibleReportCodes.Contains(r.ReportCode));
        }

        /// <summary>
        /// The get report group URL safe name.
        /// </summary>
        public static string ToUrlSafeName(this ReportTypeMetadata self)
        {
            return self == null ? null : self.ReportTypeName.Replace(" ", "-").ToLower();
        }

        /// <summary>
        /// The is matching URL safe name.
        /// </summary>
        public static bool IsMatchingUrlSafeName(this ReportTypeMetadata self, string urlSafeName)
        {
            if (self == null)
            {
                return false;
            }

            return self.ToUrlSafeName() == urlSafeName;
        }

        /// <summary>
        /// Gets distinct accessible report codes.
        /// </summary>
        public static ICollection<string> GetSecurityCodes(this IEnumerable<ReportAccessMetadata> self)
        {
            if (self == null)
            {
                return null;
            }

            return self.Select(a => a.GetSecurityCode()).Distinct().ToArray();
        }

        /// <summary>
        /// Generates the Security Code from Report Access
        /// </summary>
        /// <param name="access">The Report Access Metadata</param>
        /// <returns>The Security Code</returns>
        public static string GetSecurityCode(this ReportAccessMetadata access)
        {
            if (access == null)
            {
                return null;
            }

            return GetSecurityCode(access.GetEntityCode(), access.ReportCode, access.AccountCode);
        }

        /// <summary>
        /// Generates the Secuirty Code from a Document Item
        /// </summary>
        /// <param name="item">A Document Item</param>
        /// <returns>Returns a security code if its a document item</returns>
        public static string[] GetSecurityCode(this Item item)
        {
            if (item == null)
            {
                return null;
            }

            if (item.TemplateID != IDocumentConstants.TemplateId)
            {
                return null;
            }

            var acocuntCodes = item.GetAccountCodes();
            var entityCode = item.GetEntityCode();
            var reportCode = item.GetReportCode();

            return acocuntCodes.Select(code => GetSecurityCode(entityCode, reportCode, code)).ToArray();
        }

        /// <summary>
        /// Document thumb print is Entity Code (Account or Holding or Product codes)
        /// concatenated with report code and report date.
        /// </summary>
        /// <returns>Document thumb print</returns>
        public static string GetDocumentThumbprint(this Item item)
        {
            if (item == null)
            {
                return null;
            }

            if (!item.Fields.Contains(IDocumentConstants.ReportDateFieldId))
            {
                return null;
            }

            DateField reportDateField = item.Fields[IDocumentConstants.ReportDateFieldId];

            var entityCode = item.GetEntityCode();
            var reportCode = item.GetReportCode();
            var reportDate = reportDateField.DateTime;

            return GetDocumentThumbprint(entityCode, reportCode, reportDate);
        }

        /// <summary>
        /// Document thumb print is Entity Code (Account or Holding or Product codes)
        /// concatenated with report code and report date.
        /// </summary>
        /// <returns>Document thumb print</returns>
        public static string GetDocumentThumbprint(string entityCode, string reportCode, global::System.DateTime reportDate)
        {
            return string.Format("{0}{1}{2:yyyyMMdd}", entityCode, reportCode, reportDate);
        }

        /// <summary>
        /// Gets the entity code of this access
        /// </summary>
        /// <param name="access">The CRM report Access</param>
        /// <returns>The entityCode</returns>
        public static string GetEntityCode(this ReportAccessMetadata access)
        {
            if (!string.IsNullOrEmpty(access.ProductCode))
            {
                return access.ProductCode;
            }

            if (!string.IsNullOrEmpty(access.HoldingAccountCode))
            {
                return access.HoldingAccountCode;
            }

            return access.AccountCode;
        }

        private static string GetEntityCode(this Item item)
        {
            string productCode = item.GetProductCode();
            if (!string.IsNullOrEmpty(productCode))
            {
                return productCode;
            }

            string holdingAccountCode = item.GetHoldingAccountCode();
            if (!string.IsNullOrEmpty(holdingAccountCode))
            {
                return holdingAccountCode;
            }

            return item.GetAccountCodes().FirstOrDefault();
        }

        /// <summary>
        /// The get security code based on (Account|Holding Account|Product) Codes and report code.
        /// </summary>
        /// <param name="entityCode">
        /// The entity code.
        /// </param>
        /// <param name="reportCode">
        /// The report code.
        /// </param>
        /// <param name="accountCode">
        /// The account code.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetSecurityCode(string entityCode, string reportCode, string accountCode)
        {
            return string.Format("{0}{1}{2}", entityCode, reportCode, accountCode).ToLower();
        }

        private static ICollection<string> GetAccountCodes(this Item item)
        {
            var accountListField = item.Fields[IDocumentConstants.AccountListFieldId];
            if (accountListField != null)
            {
                var rawValue = accountListField.Value;
                var nameValueCollection = WebUtil.ParseUrlParameters(rawValue);

                return nameValueCollection.AllKeys.Where(x => !string.IsNullOrEmpty(x)).ToArray();
            }

            return new[] { string.Empty };
        }

        /// <summary>
        /// Gets the Account Names (Client Names) from the Document items' Account List
        /// </summary>
        /// <param name="documentItem">The reference item</param>
        /// <returns>A comma separated list of Account Names</returns>
        public static string GetAccountNamesFromDocumentItem(this Item documentItem)
        {
            if (documentItem.Fields.Contains(IDocumentConstants.AccountListFieldId))
            {
                var rawValue = documentItem.Fields[IDocumentConstants.AccountListFieldId].Value;
                var accountList = WebUtil.ParseUrlParameters(rawValue);

                return string.Join(Constants.ClientListSeparator, accountList.AllKeys.Select(accountList.Get));
            }

            if (documentItem.Fields.Contains(IFormConstants.AccountNameFieldId))
            {
                return documentItem.Fields[IFormConstants.AccountNameFieldId].Value ?? string.Empty;
            }

            return string.Empty;
        }

        private static string GetHoldingAccountCode(this Item item)
        {
            var holdingAccountField = item.Fields[IDocumentConstants.HoldingAccountCodeFieldId];
            if (holdingAccountField != null)
            {
                return holdingAccountField.Value;
            }

            return string.Empty;
        }

        private static string GetProductCode(this Item item)
        {
            var productCodeField = item.Fields[IDocumentConstants.ProductCodeFieldId];
            if (productCodeField != null)
            {
                return productCodeField.Value;
            }

            return string.Empty;
        }

        private static string GetReportCode(this Item item)
        {
            var reportCodeField = item.Fields[IDocumentConstants.ReportCodeFieldId];
            if (reportCodeField != null)
            {
                return reportCodeField.Value;
            }

            return string.Empty;
        }
    }
}
