﻿namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.ReportGroup
{
    using Glass.Mapper.Sc.Configuration.Attributes;

    using global::System.Collections.Generic;

    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Report;

    public partial interface IReportGroup
    {
        /// <summary>
        /// Gets the sub items for a menu item.
        /// </summary>
        [SitecoreChildren(IsLazy = true)]
        IEnumerable<IReport> ReportEntities { get; }
    }
}
