﻿using System.Collections.Generic;

namespace Qic.Sc.Entities.UserDefined.Components.Site.Configuration.MenuItem
{
    using Glass.Mapper.Sc.Configuration.Attributes;

    public partial interface IMenuItem
    {
        /// <summary>
        /// Gets the sub items for a menu item.
        /// </summary>
        [SitecoreChildren(IsLazy = true)]
        IEnumerable<IMenuItem> SubItems { get; }

        /// <summary>
        /// Gets whether a menu item is active.
        /// </summary>
        bool IsActive { get; }

        /// <summary>
        /// Get linked entity.
        /// </summary>
        /// <typeparam name="TEntity">Type of the linked entity.</typeparam>
        /// <returns>
        /// Get a linked entity if one can be resolved or null.
        /// </returns>
        TEntity GetLinkedEntity<TEntity>() where TEntity : class;
    }
}
