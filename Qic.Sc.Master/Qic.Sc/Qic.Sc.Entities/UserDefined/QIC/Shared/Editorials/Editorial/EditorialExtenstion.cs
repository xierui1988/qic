﻿namespace Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.Editorial
{
    /// <summary>
    /// The editorial extensions.
    /// </summary>
    public static class EditorialExtensions
    {
        /// <summary>
        /// The to relative path.
        /// </summary>
        public static string ToRelativePath(this IEditorial self)
        {
            if (self == null)
            {
                return null;
            }

            var kowledgeHubSiteRelativePath =
                Constants.Content.Corporate.Home.KnowledgeCentre.Path.Substring(
                    Constants.Content.Corporate.Home.Path.Length).Replace(" ", "-");
            return
                string.Format(
                    "{0}/{1}-{2:yyyyMMdd}",
                    kowledgeHubSiteRelativePath,
                    self.EntityName.Replace(" ", "-"),
                    self.PublicationDate).ToLower();
        }
    }
}
