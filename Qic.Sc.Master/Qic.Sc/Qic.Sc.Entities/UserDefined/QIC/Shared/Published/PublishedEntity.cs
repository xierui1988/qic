﻿using Glass.Mapper.Sc.Configuration.Attributes;
using Sitecore.ContentSearch;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qic.Sc.Entities.UserDefined.QIC.Shared.Published
{
    public partial class PublishedEntity
    {
        /// <summary>
        /// Gets or sets the published date
        /// </summary>
        [SitecoreField("Published Date")]
        public DateTime? PublishedDate { get; set; }

        /// <summary>
        /// Gets or sets the index published date.
        /// A kludge to work around the nullable date time. This field will
        /// be read back form index.
        /// </summary>
        [IndexField("published date")]
        [SitecoreIgnore]
        public DateTime IndexPublishedDate { get; set; }
    }
}
