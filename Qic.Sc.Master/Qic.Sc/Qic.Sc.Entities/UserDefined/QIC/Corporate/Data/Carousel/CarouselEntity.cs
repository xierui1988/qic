﻿namespace Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Carousel
{
    using Glass.Mapper.Sc.Configuration;
    using Glass.Mapper.Sc.Configuration.Attributes;

    using global::System.Collections.Generic;

    using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;

    public partial class CarouselEntity
    {
        /// <summary>
        /// Gets or sets the slide entities.
        /// </summary>
        [SitecoreField(ICarouselConstants.SlidesFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        public IEnumerable<IFeature> SlideEntities { get; set; }
    }
}
