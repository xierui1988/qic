﻿using Glass.Mapper.Sc.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Pages.Alertpage
{
    /// <summary>
    /// The ALerts in Client Portal
    /// </summary>
    public class AlertsEntity
    {
        /// <summary>
        /// Gets the sub items in Alerts that has a list of pages.
        /// </summary>
        [SitecoreChildren(IsLazy = true)]
        public IEnumerable<IAlertPage> AlertPages { get; set; }
    }
}
