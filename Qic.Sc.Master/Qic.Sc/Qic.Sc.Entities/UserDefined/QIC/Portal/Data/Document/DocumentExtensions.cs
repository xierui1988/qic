﻿using System.Web;
using System.Linq;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document
{
    using global::System.Collections.Generic;

    /// <summary>
    /// The document extensions.
    /// </summary>
    public static class DocumentExtensions
    {
        /// <summary>
        /// This is an extension method that generates the link for client portal
        /// </summary>
        /// <param name="document">The document that it needs to generate documents for.</param>
        /// <returns>The client portal download link</returns>
        public static string AplicationRelativeUrl(this IDocument document)
        {
            return string.Format(
                "/portal/report/download/{0}/{1}",
                document.EntityId.ToString("N"),
                HttpUtility.UrlPathEncode(string.Format("{0}.{1}", document.DocumentName, document.Extension)));
        }

        /// <summary>
        /// The Entity Name
        /// </summary>
        /// <param name="document">The document that requires it</param>
        /// <returns>Its entity name</returns>
        public static string GetEntityCode(this IDocument document)
        {
            string holdingAccountCode = document.HoldingAccountCode;
            string productCode = document.ProductCode;

            // If product code exists, then its a product code access
            if (!string.IsNullOrEmpty(productCode))
            {
                return productCode;
            }

            // If Holding Account Exists, then its a Holding Account code
            if (!string.IsNullOrEmpty(holdingAccountCode))
            {
                return holdingAccountCode;
            }

            // Check the Name Value List, and get the Account Name (this should only be 1 when its an Account or Holding Account)
            if (document.AccountList.Count > 0)
            {
                return document.AccountList.GetKey(0);
            }

            // Emulate that Account Code is an empty string - worst case scenario
            return string.Empty;
        }

        /// <summary>
        /// This method returns one or more Account Names (Clients) from the Account List
        /// </summary>
        /// <param name="document">The document</param>
        /// <param name="accountCodesFilter">
        /// Accounts filter.
        /// </param>
        /// <returns>A comma space separated list of if more than 1 Account</returns>
        public static string GetAccountNames(this IDocument document, ICollection<string> accountCodesFilter = null)
        {
            var accountList = document.AccountList;

            // Get account names, optionally filter account by code
            var names = accountCodesFilter != null && accountCodesFilter.Any()
                            ? accountList.AllKeys.Where(accountCodesFilter.Contains).Select(accountList.Get)
                            : accountList.AllKeys.Select(accountList.Get);

            // Gets the List of Account Names, sorts them alphabetically, and joins them to a single string, 
            // comma separated
            return string.Join(Constants.ClientListSeparator, names.OrderBy(accountName => accountName));
        }
    }
}
