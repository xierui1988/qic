﻿using Glass.Mapper.Sc.Configuration.Attributes;
using System;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document
{
    using Sitecore.ContentSearch;

    public partial class DocumentEntity
    {
        /// <summary>
        /// Gets or sets a value indicating whether is favorite.
        /// </summary>
        public virtual bool IsFavorite { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the document is new.
        /// </summary>
        public virtual bool IsUnreadRecent { get; set; }

        /// <summary>
        /// Gets or sets the published date
        /// </summary>
        [SitecoreField("Published Date")]
        public DateTime? PublishedDate { get; set; }

        /// <summary>
        /// Gets or sets the index published date.
        /// A kludge to work around the nullable date time. This field will
        /// be read back form index.
        /// </summary>
        [IndexField("published date")]
        [SitecoreIgnore]
        public DateTime IndexPublishedDate { get; set; }

        /// <summary>
        /// Gets the Account Codes from Account List
        /// SitecoreIgnore - Ignores it for Database
        /// </summary>
        [IndexField("qicaccountcodes")]
        [SitecoreIgnore]
        public string IndexAccountCodes { get; set; }

        /// <summary>
        /// Gets the QIC Security Value
        /// SitecoreIgnore - Ignores it for Database
        /// </summary>
        [IndexField("qicsecurity")]
        [SitecoreIgnore]
        public string Security { get; set; }

        /// <summary>
        /// Gets or sets the thumb print. A combination of field that uniquely identifies
        /// the document in the index.
        /// </summary>
        [SitecoreIgnore]
        [IndexField("qicdocumentthumbprint")]
        public string Thumbprint { get; set; }

        /// <summary>
        /// Get or sets the report type name.
        /// </summary>
        public string ReportTypeName { get; set; }
    }
}
