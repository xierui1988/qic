﻿using System;
using System.IO;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form
{
    public partial interface IForm
    {
        /// <summary>
        /// Gets or sets a value indicating whether is favorite.
        /// </summary>
        bool IsFavorite { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is an unread recent document.
        /// </summary>
        bool IsUnreadRecent { get; set; }

        /// <summary>
        /// Gets or sets the published date
        /// </summary>
        [SitecoreField("Published Date")]
        DateTime? PublishedDate { get; set; }
    }
}
