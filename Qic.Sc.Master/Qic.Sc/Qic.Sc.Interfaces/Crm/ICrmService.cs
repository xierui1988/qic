﻿using Qic.Sc.Entities;

namespace Qic.Sc.Interfaces.Crm
{
    using System.Collections.Generic;

    using Qic.Sc.Entities.Metadata;

    /// <summary>
    /// The CRM service interface.
    /// </summary>
    public interface ICrmService
    {
        /// <summary>
        /// Gets report meta-data.
        /// </summary>
        IEnumerable<ReportMetadata> GetReportMetadata(EntityCodeType entityCodeType, string entityCode, string reportCode);

        /// <summary>
        /// Gets client meta-data
        /// </summary>
        /// <param name="userName">
        ///     User name, an email address.
        /// </param>
        IEnumerable<ReportAccessMetadata> GetUserReportAccess(string userName);

        /// <summary>
        /// The get super user report access.
        /// </summary>
        /// <returns>
        /// Report access to all reports in the system.
        /// </returns>
        IEnumerable<ReportAccessMetadata> GetSuperUserReportAccess();

        /// <summary>
        /// Gets the list of users that are subscribed to receive access of these reports
        /// </summary>
        /// <param name="entityCode">
        /// The code of the entity its associated to
        /// </param>
        /// <param name="reportCode">
        /// The code of the report
        /// </param>
        /// <param name="ignoreNotificationPreference">
        /// The ignore notification preference, ignores email preference flag.
        /// </param>
        /// <returns>
        /// Returns a list of User's Metadata
        /// </returns>
        IEnumerable<ReportAccessMetadata> GetReportAccessForEmailBy(string entityCode, string reportCode, bool ignoreNotificationPreference = false);

        /// <summary>
        /// Set the new Update Report Access
        /// </summary>
        void UpdateUserReportAccessNotifications(string username, IEnumerable<ReportAccessMetadata> reportAccessChange);

        /// <summary>
        /// The get report types.
        /// </summary>
        IEnumerable<ReportTypeMetadata> GetReportTypes();
    }
}