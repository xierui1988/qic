﻿namespace Qic.Sc.Interfaces.Components
{
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;

    using System.Collections.Generic;

    /// <summary>
    /// The ReportService interface.
    /// </summary>
    public interface IReportService
    {
        /// <summary>
        /// Creates a report in the content repository using the meta data and the stream provided.
        /// </summary>
        /// <param name="reportObj">
        /// ReportNameMetadata Object.
        /// </param>
        void CreateReport(ReportNameMetadata reportObj);

        /// <summary>
        /// Gets a list of documents that match a criteria.
        /// </summary>
        /// <param name="username">
        /// The username.
        /// </param>
        /// <param name="filterAndSort">
        /// Filter and sort instructions.
        /// </param>
        /// <param name="skip">
        /// Number of records to skip.
        /// </param>
        /// <param name="take">
        /// Maximum number of record to return.
        /// </param>
        /// <param name="total">
        /// Total number of records that matched.
        /// </param>
        /// <returns>
        /// A list of documents.
        /// </returns>
        IEnumerable<IDocument> GetDocumentsByFilterAndSort(string username, ReportFilterAndSortMetadata filterAndSort, int skip, int take, out int total);

        /// <summary>
        /// Gets a list of favourite documents for the user. The user is taken from the first
        /// access meta data record.
        /// </summary>
        /// <param name="username">
        ///     The user name.
        /// </param>
        /// <param name="reportCodes">
        ///     Filter by report codes provided. Provide an empty collection or null to ignore filtering.
        /// </param>
        /// <param name="skip">
        ///     Number of records to skip.
        /// </param>
        /// <param name="take">
        ///     Maximum number of record to return.
        /// </param>
        /// <param name="total">
        ///     Total number of records that matched.
        /// </param>
        /// <returns>
        /// A list of documents.
        /// </returns>
        IEnumerable<IDocument> GetFavouriteDocumentsBy(string username, ICollection<string> reportCodes, int skip, int take, out int total);

        /// <summary>
        /// Gets the list of recent documents for the user. The user
        /// is taken from the first access meta data record.
        /// </summary>
        /// <param name="username">
        ///     User name.
        /// </param>
        /// <param name="reportCodes">
        ///     Filter by report codes, ignored if null or empty.
        /// </param>
        /// <param name="skip">
        ///     The skip.
        /// </param>
        /// <param name="take">
        ///     The take.
        /// </param>
        /// <param name="total">
        ///     The total.
        /// </param>
        /// <returns>
        /// Matching documents.
        /// </returns>
        IEnumerable<IDocument> GetRecentDocumentsBy(string username, ICollection<string> reportCodes, int skip, int take, out int total);

        /// <summary>
        /// get the number of Recent Documents
        /// </summary>
        /// <param name="username"> user name</param>
        /// <returns></returns>
        int GetRecentDocumentsCount(string username);

        /// <summary>
        /// Get Documents based on Document Name
        /// </summary>
        IEnumerable<IDocument> SearchDocumentsBy(string username, ReportFilterAndSortMetadata filter, string searchText, int skip, int take, out int total);
    }
}