﻿using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;

namespace Qic.Sc.Interfaces.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Qic.Sc.Entities.Metadata;

    /// <summary>
    /// The FormService interface
    /// </summary>
    public interface IFormService
    {
        /// <summary>
        /// Creates a form in the content repository using the meta data and the stream provided.
        /// </summary>
        /// <param name="formName">
        /// FormNameMetadata Object.
        /// </param>
        void CreateForm(FormMetadata formName);

        IFormBlob GetForm(string idOrPath);

        IEnumerable<IForm> GetFormsBy(string username, int skip, int take, out int total);
    }
}
