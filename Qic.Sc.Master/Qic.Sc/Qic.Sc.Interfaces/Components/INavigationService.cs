﻿namespace Qic.Sc.Interfaces.Components
{
    using System;
    using System.Collections.Generic;

    using Qic.Sc.Entities.UserDefined.Components.Site.Configuration.MenuItem;

    /// <summary>
    /// The NavigationService interface.
    /// </summary>
    public interface INavigationService
    {
        /// <summary>
        /// Get menu items under a specified item. IRems will have active flag set
        /// based on the context item provided. Any menu item that points at the
        /// context item or one of its parents will be active.
        /// </summary>
        /// <param name="idOrPath">
        ///     Id or a path to an item which children are returned as menu items.
        /// </param>
        /// <param name="contextItemId">
        ///     Id or a path to the current context item use to resolve active menu items.
        ///     If this parameter is not provided menu items will not provide an implementation
        ///     for the <see cref="IMenuItem.IsActive"/> flag.
        /// </param>
        /// <returns>
        /// A collection of menu items under the specified node, or null if none found.
        /// </returns>
        IEnumerable<IMenuItem> GetMenu(string idOrPath, Guid? contextItemId = null);
    }
}