﻿using System.Net.Mail;

namespace Qic.Sc.Interfaces.Components
{
    using System;

    /// <summary>
    /// The email service interface.
    /// </summary>
    public interface IEmailService
    {
        /// <summary>
        /// The send welcome notification. Email will have a password reset token.
        /// </summary>
        /// <param name="userName">
        ///     The user name.
        /// </param>
        /// <param name="resetToken"></param>
        void SendWelcomeNotification(string userName, string resetToken);

        /// <summary>
        /// The send password reset notification. Email will have a password reset token.
        /// </summary>
        /// <param name="userName">
        ///     The user name.
        /// </param>
        /// <param name="resetToken"></param>
        void SendPasswordResetNotification(string userName, string resetToken);

        /// <summary>
        /// Send message.  Will be sent to qic email.
        /// </summary>
        /// <param name="message">Message to be sent</param>
        void SendMessageDirectly(MailMessage message);
    }
}