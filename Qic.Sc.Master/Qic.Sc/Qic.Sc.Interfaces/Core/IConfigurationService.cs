﻿namespace Qic.Sc.Interfaces.Core
{
    /// <summary>
    /// System configuration infrastructure interface.
    /// </summary>
    public interface IConfigurationService
    {
        /// <summary>
        /// Resolve global configuration name space at runtime.
        /// </summary>
        /// <returns>Global configuration name space.</returns>
        string GetGlobalConfigurationNamespace();

        /// <summary>
        /// Resolve current site configuration name space at runtime.
        /// </summary>
        /// <returns>Current site configuration name space. Current site name is resolved at runtime.</returns>
        string GetSiteConfigurationNamespace();

        /// <summary>
        /// Get site level setting. A site level setting is resolved by prefixing
        /// site name space <see cref="GetSiteConfigurationNamespace"/> to the <paramref name="name"/>
        /// to create the full setting name.
        /// </summary>
        /// <param name="name">Setting name to retrieve.</param>
        /// <param name="default">Default value if setting is not configured.</param>
        /// <returns>Returns configuration setting or default value if setting is not found.</returns>
        string GetGlobalSetting(string name, string @default);

        /// <summary>
        /// Get site level setting. A site level setting is resolved by prefixing
        /// site name space <see cref="GetSiteConfigurationNamespace"/> to the <paramref name="name"/>
        /// to create the full setting name.
        /// NOTE: This method should throw an error is a setting is not found.
        /// </summary>
        /// <param name="name">Setting name to retrieve.</param>
        /// <returns>Returns configuration setting or throw an exception if setting is not found.</returns>
        string GetGlobalSetting(string name);

        /// <summary>
        /// Get site level setting. A site level setting is resolved by prefixing
        /// global name space <see cref="GetSiteConfigurationNamespace"/> to the <paramref name="name"/>
        /// to create the full setting name.
        /// </summary>
        /// <param name="name">Setting name to retrieve.</param>
        /// <param name="default">Default value if setting is not configured.</param>
        /// <returns>Returns configuration setting or default value if setting is not found.</returns>
        string GetSiteSetting(string name, string @default);

        /// <summary>
        /// Get site level setting. A site level setting is resolved by prefixing
        /// global name space <see cref="GetSiteConfigurationNamespace"/> to the <paramref name="name"/>
        /// to create the full setting name.
        /// NOTE: This method should raise an exception is the setting is not found.
        /// </summary>
        /// <param name="name">Setting name to retrieve.</param>
        /// <returns>Returns configuration setting or throws an exception if setting is not found.</returns>
        string GetSiteSetting(string name);
    }
}
