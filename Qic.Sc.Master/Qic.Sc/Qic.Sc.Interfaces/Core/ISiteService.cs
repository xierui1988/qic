﻿namespace Qic.Sc.Interfaces.Core
{
    using System;

    /// <summary>
    /// Contextual site service.
    /// </summary>
    public interface ISiteService
    {
        /// <summary>
        /// The get content path for the site.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetContentPath();

        /// <summary>
        /// The get site name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        string GetSiteName();

        /// <summary>
        /// Gets the path or ID of the 404 item for the site.
        /// </summary>
        /// <returns>
        /// Configured path or null if not configured.
        /// </returns>
        string Get404Item();

        /// <summary>
        /// Gets configured target host name.
        /// </summary>
        string GetTargetHostName();

        /// <summary>
        /// The get canonical URL.
        /// </summary>
        Uri GetCanonicalUri();

        /// <summary>
        /// Get if SSL is required for the site.
        /// </summary>
        bool IsSslRequired();
    }
}