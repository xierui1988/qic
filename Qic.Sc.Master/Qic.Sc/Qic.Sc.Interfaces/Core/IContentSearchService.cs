﻿using System;
using System.Linq;

namespace Qic.Sc.Interfaces.Core
{
    /// <summary>
    /// The ContentSearchService interface.
    /// </summary>
    public interface IContentSearchService : IDisposable
    {
        /// <summary>
        /// Create a query against a default search index. For current context.
        /// Context is defined by the database of the current site.
        /// </summary>
        /// <typeparam name="TEntity">
        /// Type of entities to retrieve from the index.
        /// </typeparam>
        /// <returns>
        /// A query that can be executed against a search index.
        /// </returns>
        IQueryable<TEntity> GetQueryable<TEntity>();
    }
}
