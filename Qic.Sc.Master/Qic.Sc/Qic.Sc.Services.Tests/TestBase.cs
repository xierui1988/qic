﻿namespace Qic.Sc.Services.Tests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Moq;
    using Qic.Sc.Entities.UserDefined.Components.Site.Configuration;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Interfaces.Crm;
    using System;

    /// <summary>
    /// Base functionality for Unit Tests.
    /// </summary>
    [TestClass]
    public abstract class TestBase
    {
        private static bool isInitialised;
        protected Mock<IContentRepository> MockContentRepository { get; set; }
        protected Mock<IContentRepository> MockMasterContentRepository { get; set; }
        protected Mock<ILoggerService> MockLoggerService { get; set; }
        protected Mock<IConfigurationService> MockConfigurationService { get; set; }
        protected Mock<IContentSearchService> MockContentSearchService { get; set; }
        protected Mock<ICrmService> MockCrmService { get; set; }
        protected Mock<IWebSecurityService> MockWebSecurityService { get; set; }
        protected Mock<IPortalWebSecurityService> MockPortalWebSecurityService { get; set; }
        protected Mock<IAuditService> MockAuditService { get; set; }

        public static void InitialiseOnce()
        {
            if (isInitialised)
            {
                return;
            }

            isInitialised = true;
        }

        [TestInitialize]
        public virtual void Setup()
        {
            InitialiseOnce();
            this.MockContentRepository = new Mock<IContentRepository>();
            this.MockMasterContentRepository = new Mock<IContentRepository>();
            this.MockLoggerService = new Mock<ILoggerService>();
            this.MockConfigurationService = new Mock<IConfigurationService>();
            this.MockContentSearchService = new Mock<IContentSearchService>();
            this.MockCrmService = new Mock<ICrmService>();
            this.MockWebSecurityService = new Mock<IWebSecurityService>();
            this.MockPortalWebSecurityService = new Mock<IPortalWebSecurityService>();
            this.MockAuditService = new Mock<IAuditService>();
        }
    }
}
