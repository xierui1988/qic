﻿namespace Qic.Sc.Platform
{
    using Qic.Sc.Platform.Mvc;

    using Sitecore.Mvc.Configuration;
    using Sitecore.Mvc.Presentation;

    /// <summary>
    /// Platform initialization.
    /// </summary>
    public static class Startup
    {
        /// <summary>
        /// Platform initialization entry point. Executed very early in the application
        /// life cycle. Add platform initialization code here.
        /// <remarks>
        /// This method is guaranteed to run once per application domain.
        /// </remarks>
        /// </summary>
        public static void Initialize()
        {
            // Register our custom MVC value providers. Order is important. Providers will be tried
            // in the order they are registered. The first to resolve a value will take precedence.
            RenderingParametersValueProviderFactory.Register();
            RenderingRouteDataValueProviderFactory.Register();

            // BUG FIX: A work around a partial implementation of the cache support
            // BUG FIX: by Sitecore 7.2 to add support for ClearOnIndexUpdate cache
            // BUG FIX: flag. Should be removed once fixed by Sitecore.
            // BUG FIX: Replacing broken implementation with our custom extended
            // BUG FIX: implementation.
            MvcSettings.RegisterObject(() => (XmlBasedRenderingParser) new XmlBasedRenderingParserWithFullCacheSupport());
        }
    }
}
