﻿using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;

namespace Qic.Sc.Platform.ContentSearch.ComputedFields
{
    /// <summary>
    /// This class is used for displaying readable workflow states
    /// http://www.sitecore.net/learn/blogs/technical-blogs/sitecore-7-development-team/posts/2013/10/adding-a-workflow-state-facet.aspx
    /// </summary>
    public class WorkflowState : IComputedIndexField
    {
        /// <summary>
        /// Gets or sets the field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the return type.
        /// </summary>
        public string ReturnType { get; set; }

        /// <summary>The compute field value.</summary>
        /// <param name="indexable">The indexable.</param>
        /// <returns>The <see cref="object" />.</returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
            {
                return null;
            }

            var database = item.Database;

            if (database == null)
            {
                return null;
            }

            if (database.WorkflowProvider == null)
            {
                return null;
            }

            var wf = database.WorkflowProvider.GetWorkflow(item);
            if (wf == null)
            {
                return null;
            }

            var state = wf.GetState(item);
            return state == null ? null : state.DisplayName;
        }
    }
}
