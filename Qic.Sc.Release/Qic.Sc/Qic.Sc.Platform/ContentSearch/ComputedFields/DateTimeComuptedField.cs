﻿namespace Qic.Sc.Platform.ContentSearch.ComputedFields
{
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Published;

    using Sitecore.ContentSearch;
    using Sitecore.ContentSearch.ComputedFields;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;

    using IDocumentConstants = Entities.UserDefined.QIC.Portal.Data.Document.IDocumentConstants;
    using IPublished = Entities.UserDefined.QIC.Shared.Published.IPublished;

    /// <summary>
    /// The sortable date computed index field.
    /// </summary>
    public class PublishDateTimeComuptedField : IComputedIndexField
    {
        /// <summary>
        /// Gets or sets the field name.
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// Gets or sets the return type.
        /// </summary>
        public string ReturnType { get; set; }

        /// <summary>
        /// Compute field value.
        /// </summary>
        /// <param name="indexable">
        /// Context passed in by the platform.
        /// </param>
        /// <returns>
        /// Computed value to be added to the index, or null if can't be computed.
        /// </returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;
            if (item == null)
            {
                return null;
            }

            // NOTE: Do not convert to var funky implicit type conversion in progress.
            DateField dateField = item.Fields[IPublishedConstants.PublishedDateFieldName] ?? item.Fields[IDocumentConstants.ReportDateFieldName];

            // Format publish date.
            return (dateField == null ? item.Statistics.Updated : dateField.DateTime).ToString("yyyyMMddHHmmss");
        }
    }
}
