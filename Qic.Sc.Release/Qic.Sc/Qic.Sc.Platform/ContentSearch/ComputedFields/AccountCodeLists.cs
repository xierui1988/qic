﻿using Qic.Sc.Entities;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.ComputedFields;
using Sitecore.Data.Items;
using Sitecore.Web;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Qic.Sc.Platform.ContentSearch.ComputedFields
{
    /// <summary>
    /// Responsible for getting all the Account Codes
    /// </summary>
    public class AccountCodeLists : IComputedIndexField
    {
        /// <summary>
        /// The Index Field Name
        /// </summary>
        public string FieldName { get; set; }

        /// <summary>
        /// The Return Type needed to implement IComputed Index Field
        /// </summary>
        public string ReturnType { get; set; }

        /// <summary>
        /// Get the Account Codes of Documents
        /// </summary>
        /// <param name="indexable">The Item being Indexed</param>
        /// <returns>The Account Codes</returns>
        public object ComputeFieldValue(IIndexable indexable)
        {
            Item item = indexable as SitecoreIndexableItem;

            if (item == null)
            {
                return null;
            }

            if (item.Fields[IDocumentConstants.AccountListFieldId] != null)
            {
                string rawValue = item[IDocumentConstants.AccountListFieldId];
                NameValueCollection nameValueCollection = WebUtil.ParseUrlParameters(rawValue);

                var codes = nameValueCollection.AllKeys.Where(x => !string.IsNullOrEmpty(x)).Distinct().ToList();

                return string.Join(" ", codes);
            }

            return null;
        }
    }
}
