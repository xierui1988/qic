﻿namespace Qic.Sc.Platform.Events
{
    using System;

    using Qic.Sc.Entities;

    using Sitecore.Data;
    using Sitecore.Data.Fields;
    using Sitecore.Data.Items;
    using Sitecore.Data.Managers;
    using Sitecore.Diagnostics;
    using Sitecore.Events;
    using Sitecore.SecurityModel;

    /// <summary>
    /// Provide functionality for filing items based on a date field. Intended to be used within a Sitecore event.
    /// </summary>
    public class MoveToBase
    {
        /*-------------------------------------------------------------------------------------------------------------------------------------------------------
         * SynchronizedCollection to solve the infinte loop which may end up throwing stack-overflow!
         * If the collection contains an item, exit the loop immediately. Otherwise, add item to the collection, perform business logicm(create date folders)
         * and then remove the item from the collection
         * Reference:
         * http://www.sitecore.net/Learn/Blogs/Technical-Blogs/John-West-Sitecore-Blog/Posts/2010/11/Intercepting-Item-Updates-with-Sitecore.aspx
         * *-------------------------------------------------------------------------------------------------------------------------------------------------------
         */

        /// <summary>
        /// The Root Folder that an item will be added under, and needs category folders
        /// </summary>
        public string DestinationItemID { get; set; }

        /// <summary>
        /// The Item type/Template items that need to be in folder structure.
        /// </summary>
        public string MatchedTemplateID { get; set; }

        /// <summary>
        /// The Folder Template that will be created as folder structure
        /// </summary>
        public string FolderTemplateID { get; set; }

        /// <summary>
        /// The Field that will create the folder structure from
        /// </summary>
        public string DateField { get; set; }

        /// <summary>
        /// If set to true, then it will create the Year, Month, as well as day folder
        /// </summary>
        public bool RequiresDayFolder { get; set; }

        /// <summary>
        /// Called when [item saved].
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        public void OnItemSaved(object sender, EventArgs args)
        {
            //SiteContext site = Sitecore.Context.Site;
            Log.Debug("Hitting OnItemSaved event on MoveToYearMonthFolder class");

            Item savedItem = this.GetItemSaved(args);
            Database db = savedItem.Database;

            Item destination = null;
            ID categorizableID = null;
            TemplateItem folderItem = null;
            DateField folderDate = null;

            //item saved is called when an item is published (because the item is being saved in the web database)
            //when this happens, we don't want our code to move the item anywhere, so escape out of this function.
            Sitecore.Diagnostics.Log.Debug("-- checking if this is the master database");
            if ((Sitecore.Context.Job != null && Sitecore.Context.Job.Category.Equals(Constants.PublishJobName, StringComparison.OrdinalIgnoreCase))
                || string.Compare(db.Name, Constants.MasterDb, true) != 0)
            {
                Log.Debug(string.Format("-- this is the {0} database, returning", db.Name));
                return;
            }

            try
            {
                this.ValidateParameters(db, savedItem, out destination, out categorizableID, out folderItem, out folderDate);
            }
            catch (Exception e)
            {
                Log.Error("Failed Validation, therefore exiting", e, this);
                return;
            }

            // checking if the saved item is the defined item
            // Consider writing a ... method that looks through all its ancestors
            if (!(savedItem.TemplateID == categorizableID || this.HasBaseTemplate(savedItem, categorizableID)))
            {
                Log.Debug(string.Format("Saved Item template ID: {0} does not match MatchedTemplateID: {1} or descends from it", savedItem.TemplateID, this.MatchedTemplateID), this);
                return;
            }

            // Checking that is is in the Destination ID path
            if (!savedItem.Paths.FullPath.Contains(destination.Paths.FullPath))
            {
                Log.Debug(string.Format("Saved Item Path: {0} does not match Destination Item Path: {1}", savedItem.Paths.FullPath, destination.Paths.FullPath), this);
                return;
            }

            Log.Debug("-- this is the master database");
            // Check to see if the item is an ArticleItem, and make sure it's not the Standard Values child of the template or a wildcard (!)
            if (this.HasBaseTemplate(savedItem, categorizableID))
            {
                //create the security disabler object
                using (new SecurityDisabler())
                {
                    try
                    {
                        this.MoveItemToRequiredLocation(folderDate, destination, db, savedItem, this.RequiresDayFolder, folderItem);
                    }
                    catch (Exception ex)
                    {
                        Sitecore.Diagnostics.Log.Error(string.Format("Error occured while moving item ID {0}", savedItem.ID), ex);
                    }
                }
            }

            Sitecore.Diagnostics.Log.Debug("Leaving OnItemSaved event");
        }

        private void ValidateParameters(Database db, Item savedItem, out Item destination, out ID categorizableID, out TemplateItem folderItem, out DateField folderDate)
        {
            Assert.IsNotNullOrEmpty(this.DestinationItemID, "DestinationID");
            Assert.IsNotNullOrEmpty(this.MatchedTemplateID, "MatchedTemplateID");
            Assert.IsNotNullOrEmpty(this.FolderTemplateID, "FolderTemplateID");
            Assert.IsNotNullOrEmpty(this.DateField, "DateField");

            destination = db.GetItem(this.DestinationItemID);
            Assert.IsNotNull(destination, string.Format("DestinationID {0} could not be found in DB {1}", this.DestinationItemID, db.Name));

            categorizableID = ID.Parse(this.MatchedTemplateID);

            folderItem = db.GetTemplate(this.FolderTemplateID);
            Assert.IsNotNull(folderItem, string.Format("Could not get folder template item: {0}", this.FolderTemplateID), this);

            // checking if the saved item is the defined item
            if (!this.HasBaseTemplate(savedItem, categorizableID))
            {
                folderDate = null;
                return;
            }

            folderDate = savedItem.Fields[this.DateField];
            Assert.IsNotNull(folderDate, string.Format("Saved Item ID: {0} does not match Field: {1}", savedItem.ID, this.DateField));
        }

        /// <summary>
        /// Extracts the item Parameter in EventArgs
        /// </summary>
        /// <param name="args">The Event Args in saved Item event</param>
        /// <returns>The Item in the Event Args</returns>
        protected Item GetItemSaved(EventArgs args)
        {
            return Event.ExtractParameter(args, 0) as Item;
        }

        /// <summary>
        /// Creates a filing path based on a day.
        /// </summary>
        /// <param name="bucket">The bucket under which to create paths</param>
        /// <param name="fldDate">The date from which to create the path.</param>
        /// <param name="folderTemplate">The folder template to create.</param>
        /// <returns>The newly created filing path.</returns>
        protected Item FilingPathToDay(Item bucket, DateField fldDate, TemplateItem folderTemplate)
        {
            string year;
            string month;
            string day;
            if (fldDate.DateTime > DateTime.MinValue)
            {
                DateTime date = fldDate.DateTime;
                year = date.Year.ToString();
                month = date.Month.ToString().PadLeft(2, '0');
                day = date.Day.ToString().PadLeft(2, '0');
            }
            else
            {
                year = DateTime.Now.Year.ToString();
                month = DateTime.Now.Month.ToString().PadLeft(2, '0');
                day = DateTime.Now.Day.ToString().PadLeft(2, '0');
            }

            return this.FilingPath(bucket, folderTemplate, year, month, day);
        }

        /// <summary>
        /// Creates a filing path based on a month
        /// </summary>
        /// <param name="bucket">The bucket under which to create paths</param>
        /// <param name="fldDate">The date from which to create the path.</param>
        /// <param name="folderTemplate">The folder template to create.</param>
        /// <returns>The newly created filing path.</returns>
        protected Item FilingPathToMonth(Item bucket, DateField fldDate, TemplateItem folderTemplate)
        {
            string year;
            string month;
            if (fldDate.DateTime > DateTime.MinValue)
            {
                DateTime date = fldDate.DateTime;
                year = date.Year.ToString();
                month = date.Month.ToString().PadLeft(2, '0');
            }
            else
            {
                year = DateTime.Now.Year.ToString();
                month = DateTime.Now.Month.ToString().PadLeft(2, '0');
            }

            return this.FilingPath(bucket, folderTemplate, year, month, string.Empty);
        }

        /// <summary>
        /// Creates a filing path.
        /// </summary>
        /// <param name="bucket">The bucket under which to create paths</param>
        /// <param name="folderTemplate">The folder template to create.</param>
        /// <param name="year">The year.</param>
        /// <param name="month">The month.</param>
        /// <param name="day">The day.</param>
        /// <returns>The newly created filing path.</returns>
        protected Item FilingPath(Item bucket, TemplateItem folderTemplate, string year, string month, string day)
        {
            // Create the folder structure if it doesn't exist
            Item yearFolder = bucket.Children[year];
            if (yearFolder == null)
            {
                yearFolder = bucket.Add(year, folderTemplate);
            }

            Item monthFolder = yearFolder.Children[month];
            if (monthFolder == null)
            {
                monthFolder = yearFolder.Add(month, folderTemplate);
            }
            var returnFolder = monthFolder;

            //only create the day folder if the day var is not empty
            if (!string.IsNullOrEmpty(day))
            {
                Item dayFolder = monthFolder.Children[day];
                if (dayFolder == null)
                {
                    dayFolder = monthFolder.Add(day, folderTemplate);
                }
                returnFolder = dayFolder;
            }
            return returnFolder;
        }

        /// <summary>
        /// Moves the news item to required location.
        /// </summary>
        /// <param name="fldDate">The FLD date.</param>
        /// <param name="articleBucket">The article bucket.</param>
        /// <param name="db">The db.</param>
        /// <param name="createdItem">The created item.</param>
        protected void MoveItemToRequiredLocation(DateField fldDate, Item articleBucket, Database db, Item createdItem, bool includeDayFolder, TemplateItem folderTemplateType)
        {
            //first move the news item to either the month or day folder as required
            if (includeDayFolder)
            {
                this.MoveNewsItemToDayFolder(fldDate, articleBucket, db, createdItem, folderTemplateType);
            }
            else
            {
                this.MoveNewsItemToMonthFolder(fldDate, articleBucket, db, createdItem, folderTemplateType);
            }
        }

        /// <summary>
        /// Moves the news item to day folder.
        /// </summary>
        /// <param name="fldDate">The FLD date.</param>
        /// <param name="articleBucket">The article bucket.</param>
        /// <param name="db">The db.</param>
        /// <param name="createdItem">The created item.</param>
        protected void MoveNewsItemToDayFolder(DateField fldDate, Item articleBucket, Database db, Item createdItem, TemplateItem folderTemplateType)
        {
            Item path = this.FilingPathToDay(articleBucket, fldDate, folderTemplateType);
            this.MoveItemToPath(path, createdItem);
        }

        /// <summary>
        /// Moves the news item to month folder.
        /// </summary>
        /// <param name="fldDate">The FLD date.</param>
        /// <param name="articleBucket">The article bucket.</param>
        /// <param name="db">The db.</param>
        /// <param name="createdItem">The created item.</param>
        protected void MoveNewsItemToMonthFolder(DateField fldDate, Item articleBucket, Database db, Item createdItem, TemplateItem folderTemplateType)
        {
            Item path = this.FilingPathToMonth(articleBucket, fldDate, folderTemplateType);
            this.MoveItemToPath(path, createdItem);
        }

        /// <summary>
        /// Moves the news item to path.
        /// </summary>
        /// <param name="path">The path.</param>
        /// <param name="createdItem">The created item.</param>
        protected void MoveItemToPath(Item path, Item createdItem)
        {
            //only move the item if its not already there
            if (!path.Paths.FullPath.Equals(createdItem.Parent.Paths.FullPath, StringComparison.OrdinalIgnoreCase))
            {
                createdItem.MoveTo(path);
            }
        }

        /// <summary>
        /// Checks to see if the given Item is same or descends from given templateID
        /// </summary>
        /// <param name="item"> item in context </param>
        /// <param name="templateId"> Template ID to compare to </param>
        /// <returns> true or false </returns>
        protected bool HasBaseTemplate(Item item, ID templateId)
        {
            if (item.Name.Equals(Constants.StandardValues, StringComparison.OrdinalIgnoreCase))
            {
                return false;
            }

            var itemTemplateId = TemplateManager.GetTemplate(item);
            return itemTemplateId != null && (item.TemplateID == templateId || itemTemplateId.DescendsFrom(templateId));
        }
    }
}
