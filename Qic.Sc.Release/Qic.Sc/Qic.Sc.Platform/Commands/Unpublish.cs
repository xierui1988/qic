﻿using System;
using System.Linq;
using Autofac;
using Qic.Sc.Entities;
using Qic.Sc.Entities.Metadata;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;
using Qic.Sc.Interfaces.Components;
using Sitecore;
using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Publishing;
using Sitecore.SecurityModel;
using Sitecore.Shell.Framework.Commands;
using Sitecore.Web.UI.Sheer;
using Constants = Qic.Sc.Entities.Constants;

namespace Qic.Sc.Platform.Commands
{
    /// <summary>
    /// Used for commands in search
    /// </summary>
    public class Unpublish : Command
    {
        /// <summary>
        /// Gets called when the command is requested to execute
        /// </summary>
        /// <param name="context">The context of the trigger</param>
        public override void Execute(CommandContext context)
        {
            Assert.ArgumentNotNull(context, "context");
            
            if (context.Items.Length == 0)
            {
                return;
            }
            // Only get the Sitecore Items
            Item[] items = context.Items.Where(x => x.TemplateID == IDocumentConstants.TemplateId).ToArray();
            
            bool isDocumentType = true;
            if (items == null || items.Length == 0)
            {
                isDocumentType = false;
                items = context.Items.Where(x => x.TemplateID == IFormConstants.TemplateId).ToArray();
            }
            // Remove all the non published documents
            items = items.Where(x => x[Constants.WorkflowState] == Constants.System.Workflows.QICPortalPublishingWorkflow.Published.ItemID.ToString()).ToArray();
            
            if (items.Length > Constants.UnpublishDocumentsAlertThreshold)
            {
                var response = SheerResponse.Alert(string.Format("You are about to unpublish more than {0} {1}. The operation will be aborted.", Constants.UnpublishDocumentsAlertThreshold, isDocumentType ? "documents" : "forms"));
            }
            else
            {
                this.UnpublishDocuments(items, isDocumentType);
            }
        }

        private void UnpublishDocuments(Item[] items, bool isDocumentType)
        {
            using (var scope = ServiceLocator.BeginScopeFor(Constants.MasterDb))
            {
                using (new SecurityDisabler())
                {
                    var webDb = Database.GetDatabase(Constants.WebDb);

                    var auditService = scope.Resolve<IAuditService>();

                    foreach (var item in items)
                    {
                        // Double checking that its not will not unpublish document items
                        if (isDocumentType)
                        {
                            if (item.TemplateID != IDocumentConstants.TemplateId)
                            {
                                continue;
                            }
                        }
                        else
                        {
                            if (item.TemplateID != IFormConstants.TemplateId)
                            {
                                continue;
                            }
                        }

                        this.ChangeWorkflowState(item);
                        this.Publish(item.Database, webDb, item);

                        auditService.Audit(new AuditMetadata()
                        {
                            UserName = Context.User.Profile.UserName,
                            Action = isDocumentType ? EnumUtils<SitecoreUserAction>.GetDescription(SitecoreUserAction.UnpublishedDocument) : EnumUtils<SitecoreUserAction>.GetDescription(SitecoreUserAction.UnpublishedForm),
                            Db = item.Database.Name,
                            Path = item.Paths.FullPath,
                            Language = item.Language.Name,
                            Version = item.Version.Number,
                            Id = item.ID.ToString(),
                            Miscellaneous = item.GetAccountNamesFromDocumentItem()
                        });
                    }
                }
            }
        }

        private void ChangeWorkflowState(Item item)
        {
            Assert.IsNotNull(item, "item");

            using (new EditContext(item, true, false))
            {
                item[Constants.WorkflowState] = Constants.System.Workflows.QICPortalPublishingWorkflow.Unpublished.ItemID.ToString();
            }
        }

        private void Publish(Database source, Database target, Item item)
        {
            Assert.IsNotNull(item, "Item");

            var publishOptions = new PublishOptions(source, target, PublishMode.SingleItem, item.Language, DateTime.Now);
            var publisher = new Publisher(publishOptions);
            publisher.Options.PublishRelatedItems = false;
            publisher.Options.RootItem = item;

            // Publish children as well?
            publisher.Options.Deep = true;

            // Do the publish!
            publisher.Publish();
        }
    }
}
