﻿namespace Qic.Sc.Platform.Pipelines.Mvc
{
    using Qic.Sc.Platform.Mvc;

    using Sitecore.Mvc.Extensions;
    using Sitecore.Mvc.Pipelines.Response.RenderRendering;

    /// <summary>
    /// BUG: This is an extension to the bug in sitecore 7.2
    /// BUG: ClearOnUndexUpdate cache flag is never set for MVC rendering.
    /// </summary>
    public class FixForOnIndexUpdateCacheFlag : RenderRenderingProcessor
    {
        /// <summary>
        /// Add ClearOnIndexUpdare cache key if it has been set for the rendering.
        /// </summary>
        /// <param name="args">
        /// Pipeline arguments passed in by the platform.
        /// </param>
        public override void Process(RenderRenderingArgs args)
        {
            if (args.Cacheable
                && args.Rendering[XmlBasedRenderingParserWithFullCacheSupport.ClearOnIndexUpdatePropertyName].ToBool())
            {
                // NOTE: Code was derived from the ASP.NET Forms implementation
                //       via a decompile of (Sitecore.Web.UI.WebControl.GetCacheKey).
                args.CacheKey += "_#index";
            }
        }
    }
}
