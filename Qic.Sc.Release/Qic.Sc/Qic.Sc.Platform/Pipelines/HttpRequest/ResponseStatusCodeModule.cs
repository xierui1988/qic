﻿namespace Qic.Sc.Platform.Pipelines.HttpRequest
{
    using System.Web;

    /// <summary>
    /// The response status code module. Part of a platform level
    /// 404 and other custom status code handling infrastructure.
    /// </summary>
    public class ResponseStatusCodeModule : IHttpModule
    {
        /// <summary>
        /// Register request life cycle hooks to manage custom HTTP
        /// response codes.
        /// </summary>
        /// <param name="context">
        /// Current application context passed to the module by the platform.
        /// </param>
        public void Init(HttpApplication context)
        {
            // Set response status code if it was requested via context
            // or URL rewrite.
            context.PreSendRequestHeaders += (sender, args) =>
            {
                var app = (HttpApplication)sender;
                var statusCode = app.Context.GetSitecoreResponseStatusCode();
                if (statusCode != null)
                {
                    app.Context.SetSitecoreResponseStatusCodeChanged();

                    app.Context.Response.StatusCode = statusCode.Value;
                    app.Context.Response.TrySkipIisCustomErrors = true;
                }
            };

            // Enforce content type for request we have changed status code for.
            // Forces all responses where we have set a custom response code to have
            // text/html content type.
            context.PreSendRequestContent += (sender, args) =>
            {
                var app = (HttpApplication)sender;
                if (app.Context.GetSitecoreResponseStatusCodeChanged().GetValueOrDefault())
                {
                    app.Context.Response.ContentType = "text/html";
                }
            };
        }

        /// <summary>
        /// The dispose. Dose nothing in the current implementation.
        /// </summary>
        public void Dispose()
        {
        }
    }
}
