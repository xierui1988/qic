﻿namespace Qic.Sc.Platform.Pipelines.Initialize
{
    using System;

    using Qic.Sc.Platform;

    using Sitecore.Pipelines;

    /// <summary>
    /// Sitecore initialization hook. This processor is invoked from a Sitecore initialization
    /// pipeline.
    /// </summary>
    public class InitializePlatform
    {
        /// <summary>
        /// Initialise platform functionality on the first Sitecore request.
        /// </summary>
        /// <param name="args">Arguments to the pipeline.</param>
        public void Process(PipelineArgs args)
        {
            // Synchronizes access and ensures initialization is done per application domain.
            this.RunOnce(Startup.Initialize);
        }

        #region Run once infrastructure
        private static readonly object Sync = new object();

        private static bool initialized;

        private void RunOnce(Action runonce)
        {
            if (initialized)
            {
                return;
            }

            lock (Sync)
            {
                if (initialized)
                {
                    return;
                }

                initialized = true;
                runonce();
            }
        }

        #endregion
    }
}
