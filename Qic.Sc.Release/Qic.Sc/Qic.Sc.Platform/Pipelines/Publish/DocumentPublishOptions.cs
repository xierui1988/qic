﻿namespace Qic.Sc.Platform.Pipelines.Publish
{
    using System;
    using System.Collections.Generic;

    using Sitecore.Data;
    using Sitecore.Globalization;
    using Sitecore.Publishing;

    /// <summary>
    /// The document publish options.
    /// </summary>
    public class DocumentPublishOptions : PublishOptions
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DocumentPublishOptions"/> class.
        /// </summary>
        public DocumentPublishOptions(Database sourceDatabase, Database targetDatabase, PublishMode mode, Language language, DateTime publishDate)
            : base(sourceDatabase, targetDatabase, mode, language, publishDate)
        {
        }

        /// <summary>
        /// Gets or sets the document ids.
        /// </summary>
        public IDictionary<Guid, DocumentPublishResult> Documents { get; set; }
    }
}
