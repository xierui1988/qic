﻿using Sitecore.Data;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Publishing;
using Sitecore.Publishing.Pipelines.Publish;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Qic.Sc.Platform.Pipelines.Publish
{
    /// <summary>
    /// This class is intended to ensure that all the items that were not published will be published
    /// </summary>
    public class AddUnpublishedParentsToQueue : PublishProcessor
    {
        /// <summary>
        /// This will get called every publishing action
        /// </summary>
        /// <param name="context">The publishing context information</param>
        public override void Process(PublishContext context)
        {
            Assert.ArgumentNotNull(context, "context");

            // Full publish will take care of itself, as all items will be evaluated
            // for single and incremental check parent are published.
            if (context.PublishOptions.Mode == PublishMode.SingleItem || context.PublishOptions.Mode == PublishMode.Incremental)
            {
                var parents = this.GetUnpublishedParents(context.PublishOptions, context.Queue);

                // (Language item is first) Adding after language item
                context.Queue.Insert(1, parents);
            }
        }

        private IEnumerable<PublishingCandidate> GetUnpublishedParents(PublishOptions publishOptions, List<IEnumerable<PublishingCandidate>> publishQueue)
        {
            List<PublishingCandidate> parentQueueList = new List<PublishingCandidate>();

            foreach (IEnumerable<PublishingCandidate> current in publishQueue)
            {
                foreach (PublishingCandidate current2 in current)
                {
                    // Get the item that is being published
                    Item item = publishOptions.SourceDatabase.GetItem(current2.ItemId, publishOptions.Language);

                    // Parent List
                    List<ID> parentList = new List<ID>();

                    if (item != null)
                    {
                        // Get all the Parent Id
                        this.GetParentIds(item, publishOptions.TargetDatabase, parentList);

                        // Reverse the list to get the highest parent in the tree first
                        parentList.Reverse(0, parentList.Count);

                        PublishOptions candidateOptions = new PublishOptions(
                            publishOptions.SourceDatabase,
                            publishOptions.TargetDatabase,
                            publishOptions.Mode,
                            publishOptions.Language,
                            publishOptions.FromDate);

                        candidateOptions.Deep = false;

                        parentQueueList.AddRange(
                            from id in parentList
                            select new PublishingCandidate(id, candidateOptions));
                    }
                }
            }

            return parentQueueList;
        }

        private IEnumerable<ID> GetParentIds(Item item, Database targetDatabase, List<ID> parentIds)
        {
            // Return the same list
            if (item == null)
            {
                return parentIds;
            }

            Item parent = item.Parent;

            if (parent == null)
            {
                return parentIds;
            }

            Item targetItem = targetDatabase.GetItem(parent.Uri.ToDataUri());

            // Check if it exists and if it was already placed in the list, and it hasn't been modified
            if (targetItem == null || targetItem.Statistics.Revision == parent.Statistics.Revision)
            {
                if (!parentIds.Contains(parent.ID))
                {
                    parentIds.Add(parent.ID);

                    // Recursive call up the tree
                    return this.GetParentIds(parent, targetDatabase, parentIds);
                }
            }

            return parentIds;
        }
    }
}