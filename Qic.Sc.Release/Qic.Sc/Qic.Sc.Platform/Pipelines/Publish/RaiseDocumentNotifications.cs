﻿namespace Qic.Sc.Platform.Pipelines.Publish
{
    using System;

    using Autofac;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Interfaces.Components;

    using Sitecore.Data.Items;
    using Sitecore.Diagnostics;
    using Sitecore.Publishing;
    using Sitecore.Publishing.Pipelines.PublishItem;

    /// <summary>
    /// The raise document notifications.
    /// </summary>
    public class RaiseDocumentNotifications : PublishItemProcessor
    {
        /// <summary>
        /// Invoked by the pipeline.
        /// </summary>
        public override void Process(PublishItemContext context)
        {
            if (context == null)
            {
                return; // Not enough context
            }

            var item = context.VersionToPublish;
            if (item == null)
            {
                return; // No item
            }

            var result = context.Result;
            if (result == null)
            {
                return; // result unknown
            }

            var options = context.PublishContext.PublishOptions as DocumentPublishOptions;
            if (options == null)
            {
                return; // No publishing options, publish was not triggers from a workflow.
            }

            if (item.TemplateID != IDocumentConstants.TemplateId)
            {
                if (options.Documents.ContainsKey(item.ID.Guid))
                {
                    var optionDoc = options.Documents[item.ID.Guid];
                    if (result.Operation == PublishOperation.Created)
                    {
                        // Not a document but is going through the same workflow commands
                        // could be a 'Form' or any other new type of document should be audited.
                        optionDoc.Action = SitecoreUserAction.FormPublish;
                    }

                    this.Audit(item, optionDoc.Action);
                }
                return; // Not a document
            }

            if (!options.Documents.ContainsKey(item.ID.Guid))
            {
                this.Audit(item, SitecoreUserAction.DocumentNotPublishedAction);
                Log.Error("Unexpected document detected during publishing: " + item.Paths.FullPath, this);
                return;
            }

            var document = options.Documents[item.ID.Guid];
            if (result.Operation != PublishOperation.Created)
            {
                document.Action = SitecoreUserAction.DocumentNotPublishedAction;
                this.Audit(item, document.Action);
                return; // Document not published, skipped or updated (should be unpublished first)
            }

            this.SendNotification(item, item[IDocumentConstants.IsNotificationRequiredFieldId] == "1", document);
        }

        private void SendNotification(Item item, bool notify, DocumentPublishResult result)
        {
            using (var scope = ServiceLocator.BeginScopeFor(Constants.MasterDb))
            {
                try
                {
                    if (notify)
                    {
                        var emailService = scope.Resolve<IPortalEmailService>();
                        emailService.SendReportNotification(item.ID.Guid);

                        result.Action = SitecoreUserAction.DocumentPublishSendEmailAction;
                    }
                    else
                    {
                        result.Action = SitecoreUserAction.DocumentPublishNoNotificationAction;
                    }
                }
                catch (Exception e)
                {
                    result.Action = SitecoreUserAction.DocumentNotPublishedAction;

                    Log.Error(
                        "Send Notification Email Action threw an error whilst trying to create an email message",
                        e,
                        this);
                }

                this.Audit(item, result.Action);
            }
        }

        private void Audit(Item item, SitecoreUserAction action)
        {
            new AuditMetadata
                {
                    Action = EnumUtils<SitecoreUserAction>.GetDescription(action),
                    UserName = Sitecore.Context.User.Name,
                    Miscellaneous = item.GetAccountNamesFromDocumentItem()
                }.FromItem(item).Audit();
        }
    }
}
