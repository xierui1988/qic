﻿namespace Qic.Sc.Platform.Mvc
{
    using System;
    using System.Collections.Generic;
    using System.Web;

    using Qic.Sc.Web;

    /// <summary>
    /// Rendering page level script context. Flushes
    /// </summary>
    public class RenderingScriptContext : IDisposable
    {
        private readonly Action<RenderingScriptContext> record;

        /// <summary>
        /// Initializes a new instance of the <see cref="RenderingScriptContext"/> class.
        /// </summary>
        /// <param name="record">
        /// Callback to flush context. Used to record script for cache integration.
        /// </param>
        /// <param name="scriptStore">
        /// Script store for the context.
        /// </param>
        public RenderingScriptContext(Action<RenderingScriptContext> record, IList<string> scriptStore)
        {
            if (record == null)
            {
                throw new ArgumentNullException("record");
            }
            this.record = record;
            this.ScriptStore = scriptStore;
        }

        /// <summary>
        /// Gets page level the script store.
        /// </summary>
        public IList<string> ScriptStore { get; private set; }

        /// <summary>
        /// Flushes/promotes rendering script store to the overall request store.
        /// Combines it with the script from other renderings so it can be added to the page.
        /// </summary>
        public void Dispose()
        {
            this.record(this);
            HttpContext.Current.RegisterPageScript(this.ScriptStore);
        }
    }
}
