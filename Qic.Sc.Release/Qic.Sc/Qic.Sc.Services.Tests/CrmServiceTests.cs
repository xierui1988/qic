﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xrm.Sdk;

using Qic.Sc.Interfaces.Crm;
using Qic.Sc.Services.Crm;

namespace Qic.Sc.Services.Tests
{
    [TestClass]
    public class CrmServiceTests : TestBase
    {
        [TestInitialize]
        public override void Setup()
        {
            base.Setup();
        }

        private ICrmService CreateCRMService(IOrganizationService org)
        {
            return new CrmService(org);
        }

        //[TestMethod]
        //[ExpectedException(typeof(ArgumentException))]
        //public void FailCreateReportNullEntityName()
        //{
        //    ICrmService crmService = this.CreateCRMService(new Mock<IOrganizationService>().Object);
        //}
    }
}
