﻿<?xml version="1.0" encoding="utf-8"?>
<configuration>
  <configSections>
    <section name="sitecore" type="Sitecore.Configuration.ConfigReader, Sitecore.Kernel" />
    <section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, Sitecore.Logging" />
    <section name="glimpse" type="Glimpse.Core.Configuration.Section, Glimpse.Core" />
  </configSections>
  <connectionStrings configSource="App_Config\ConnectionStrings.config" />
  <appSettings>
    <add key="EmailReminder.FromAddress" value="name@server.net" />
    <!-- NetSpell directory -->
    <add key="DictionaryFolder" value="/sitecore/shell/controls/rich text editor/Dictionaries/" />
    <add key="Lucene.Net.FSDirectory.class" value="Sitecore.Search.FSDirectory, Sitecore.Kernel" />
    <!-- disable glimpse async support - https://github.com/Glimpse/Glimpse/issues/632#issuecomment-30934627 -->
    <add key="Glimpse:DisableAsyncSupport" value="true" />
    <!-- Enable tracing of services to Glimpse -->
    <add key="Qic.Sc.PerformanceTracingEnabled" value="true" />
    <add key="Qic.Sc.ContactFromEmailAddress" value="no-reply@qic.com" />
    <add key="Qic.Sc.QICEmailAddress" value="sample@example.com" />
    <add key="Qic.Sc.OperationsEmailAddress" value="sample@example.com" />
    <add key="Qic.Sc.UserInformtionCookieDomain" value="cpqic.local" />
    <add key="Qic.Sc.ResetTokenTimeouInMinutes" value="30" />
    <!--qic@qic.xom-->
  </appSettings>
  <sitecore database="SqlServer">
    <sc.variable name="smtpUser" value="#{SitecoreSmtpUser}" />
    <sc.variable name="smtpPassword" value="#{SitecoreSmtpPassword}" />
    <!-- Include full sitecore configuration -->
    <sc.include file="/App_Config/Sitecore.config" />
  </sitecore>
  <log4net>
    <!-- LOGGING SETTINGS
         The file element defines the location of the log files. This location must
         be the same as the setting in LogFolder. The file element is a relative or
         absolute path that always uses slashes (/) as separators. A valid file
         element for a relative path would be:

           <file value="/data/logs/log.{date}.{processid}.txt"/>

         A valid element for an absolute path would be:

           <file value="C:/inetpub/wwwroot/data/logs/log.{date}.{processid}.txt"/>

         The macros supported are:

           {date}: Replaced with the current date (in the format yyyyMMdd)
           {time}: Replaced with the current time (in the format HHmmss)
           {processid}: Replaced with the current Windows process id

         For further information refer to the Log4Net documentation.
    -->
    <appender name="LogFileAppender" type="log4net.Appender.SitecoreLogFileAppender, Sitecore.Logging">
      <file value="$(dataFolder)/logs/log.{date}.txt" />
      <appendToFile value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%4t %d{ABSOLUTE} %-5p %m%n" />
      </layout>
      <encoding value="utf-8" />
    </appender>
    <appender name="WebDAVLogFileAppender" type="log4net.Appender.SitecoreLogFileAppender, Sitecore.Logging">
      <file value="$(dataFolder)/logs/WebDAV.log.{date}.txt" />
      <appendToFile value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%4t %d{ABSOLUTE} %-5p %m%n" />
      </layout>
      <encoding value="utf-8" />
    </appender>
    <appender name="SearchLogFileAppender" type="log4net.Appender.SitecoreLogFileAppender, Sitecore.Logging">
      <file value="$(dataFolder)/logs/Search.log.{date}.txt" />
      <appendToFile value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%4t %d{ABSOLUTE} %-5p %m%n" />
      </layout>
      <encoding value="utf-8" />
    </appender>
    <appender name="CrawlingLogFileAppender" type="log4net.Appender.SitecoreLogFileAppender, Sitecore.Logging">
      <file value="$(dataFolder)/logs/Crawling.log.{date}.txt" />
      <appendToFile value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%4t %d{ABSOLUTE} %-5p %m%n" />
      </layout>
      <encoding value="utf-8" />
    </appender>
    <appender name="PublishingLogFileAppender" type="log4net.Appender.SitecoreLogFileAppender, Sitecore.Logging">
      <file value="$(dataFolder)/logs/Publishing.log.{date}.txt" />
      <appendToFile value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%4t %d{ABSOLUTE} %-5p %m%n" />
      </layout>
      <encoding value="utf-8" />
    </appender>
    <appender name="SCBasicsAuditTrailDBAppender" type="SCBasics.AuditTrail.Appender.SitecoreDatabaseLogAppender, SCBasics.AuditTrail">
      <!-- Filter items where message field start with 'AUDIT....' -->
      <filter type="log4net.Filter.StringMatchFilter">
        <regexToMatch value="^AUDIT" />
      </filter>

      <!-- Deny all other items -->
      <filter type="log4net.Filter.DenyAllFilter" />
      <!--his is because logs are buffered until a given buffersize is reached.
      The default value is 512, but you can lower this by modifying the following line of code-->
      <bufferSize value="1" />
      <param name="ConnectionType" value="System.Data.SqlClient.SqlConnection, System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" />
      <!--Double SLASH is Very Important-->
      <!--<param name="CommandText" value="INSERT INTO Log ([Date],[Thread],[Level],[Logger],[Message]) VALUES (@log_date, @thread, @log_level, @logger, @message)" />-->
      <param name="CommandText" value="INSERT INTO Logs ([Date],[Thread],[Level],[Logger],[Message],[Exception], [SCUser], [SCAction], [SCItemPath], [SCLanguage] , [SCVersion] , [SCItemId], [SiteName],[SCMisc]) VALUES (@log_date, @thread, @log_level, @logger, @message, @exception, @scuser,  @scaction, @scitempath, @sclanguage, @scversion, @scitemid, @sitename, @scmisc)" />
      <param name="Parameter">
        <param name="ParameterName" value="@log_date" />
        <param name="DbType" value="DateTime" />
        <param name="Layout" type="log4net.Layout.PatternLayout">
          <param name="ConversionPattern" value="%d{yyyy'-'MM'-'dd HH':'mm':'ss'.'fff}" />
        </param>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@thread" />
        <param name="DbType" value="String" />
        <param name="Size" value="255" />
        <param name="Layout" type="log4net.Layout.PatternLayout">
          <param name="ConversionPattern" value="%t" />
        </param>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@log_level" />
        <param name="DbType" value="String" />
        <param name="Size" value="50" />
        <param name="Layout" type="log4net.Layout.PatternLayout">
          <param name="ConversionPattern" value="%p" />
        </param>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@logger" />
        <param name="DbType" value="String" />
        <param name="Size" value="255" />
        <param name="Layout" type="log4net.Layout.PatternLayout">
          <param name="ConversionPattern" value="%c" />
        </param>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@message" />
        <param name="DbType" value="String" />
        <param name="Size" value="4000" />
        <param name="Layout" type="log4net.Layout.PatternLayout">
          <param name="ConversionPattern" value="%m" />
        </param>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@exception" />
        <dbType value="String" />
        <size value="2000" />
        <layout type="log4net.Layout.ExceptionLayout" />
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@scuser" />
        <dbType value="String" />
        <size value="255" />
        <layout type="SCBasics.AuditTrail.Log4NetLayout.PropertyLayout,SCBasics.AuditTrail">
          <param name="PropertyName" value="scuser" />
        </layout>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@scaction" />
        <dbType value="String" />
        <size value="255" />
        <layout type="SCBasics.AuditTrail.Log4NetLayout.PropertyLayout,SCBasics.AuditTrail">
          <param name="PropertyName" value="scaction" />
        </layout>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@scitempath" />
        <dbType value="String" />
        <size value="255" />
        <layout type="SCBasics.AuditTrail.Log4NetLayout.PropertyLayout,SCBasics.AuditTrail">
          <param name="PropertyName" value="scitempath" />
        </layout>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@sclanguage" />
        <dbType value="String" />
        <size value="255" />
        <layout type="SCBasics.AuditTrail.Log4NetLayout.PropertyLayout,SCBasics.AuditTrail">
          <param name="PropertyName" value="sclanguage" />
        </layout>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@scversion" />
        <dbType value="String" />
        <size value="255" />
        <layout type="SCBasics.AuditTrail.Log4NetLayout.PropertyLayout,SCBasics.AuditTrail">
          <param name="PropertyName" value="scversion" />
        </layout>
      </param>      
      <param name="Parameter">
        <param name="ParameterName" value="@scitemid" />
        <dbType value="String" />
        <size value="38" />
        <layout type="SCBasics.AuditTrail.Log4NetLayout.PropertyLayout,SCBasics.AuditTrail">
          <param name="PropertyName" value="scitemid" />
        </layout>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@sitename" />
        <dbType value="String" />
        <size value="255" />
        <layout type="SCBasics.AuditTrail.Log4NetLayout.PropertyLayout,SCBasics.AuditTrail">
          <param name="PropertyName" value="sitename" />
        </layout>
      </param>
      <param name="Parameter">
        <param name="ParameterName" value="@scmisc" />
        <dbType value="String" />
        <size value="100000" />
        <layout type="SCBasics.AuditTrail.Log4NetLayout.PropertyLayout,SCBasics.AuditTrail">
          <param name="PropertyName" value="scmisc" />
        </layout>
      </param>
    </appender>
    <root>
      <priority value="INFO" />
      <appender-ref ref="LogFileAppender" />
      <appender-ref ref="SCBasicsAuditTrailDBAppender" />
    </root>
    <logger name="Sitecore.Diagnostics.WebDAV" additivity="false">
      <level value="INFO" />
      <appender-ref ref="WebDAVLogFileAppender" />
    </logger>
    <logger name="Sitecore.Diagnostics.Search" additivity="false">
      <level value="INFO" />
      <appender-ref ref="SearchLogFileAppender" />
    </logger>
    <logger name="Sitecore.Diagnostics.Crawling" additivity="false">
      <level value="INFO" />
      <appender-ref ref="CrawlingLogFileAppender" />
    </logger>
    <logger name="Sitecore.Diagnostics.Publishing" additivity="false">
      <level value="INFO" />
      <appender-ref ref="PublishingLogFileAppender" />
    </logger>
  </log4net>
  <!-- SYSTEM.WEBSERVER
       This section is a ASP.NET configuration section when running in Integrated Mode on IIS7.
  -->
  <system.webServer>
    <modules runAllManagedModulesForAllRequests="true">
      <remove name="WebDAVModule" />
      <add name="Glimpse" type="Glimpse.AspNet.HttpModule, Glimpse.AspNet" preCondition="integratedMode" />
      <add type="Sitecore.Web.RewriteModule, Sitecore.Kernel" name="SitecoreRewriteModule" />
      <add type="Sitecore.Nexus.Web.HttpModule,Sitecore.Nexus" name="SitecoreHttpModule" />
      <add type="Sitecore.Resources.Media.UploadWatcher, Sitecore.Kernel" name="SitecoreUploadWatcher" />
      <add type="Sitecore.IO.XslWatcher, Sitecore.Kernel" name="SitecoreXslWatcher" />
      <add type="Sitecore.IO.LayoutWatcher, Sitecore.Kernel" name="SitecoreLayoutWatcher" />
      <add type="Sitecore.Configuration.ConfigWatcher, Sitecore.Kernel" name="SitecoreConfigWatcher" />
      <remove name="Session" />
      <add name="Session" type="System.Web.SessionState.SessionStateModule" preCondition="" />
      <add type="Sitecore.Analytics.RobotDetection.Media.MediaRequestSessionModule, Sitecore.Analytics.RobotDetection" name="MediaRequestSessionModule" />
      <add type="Sitecore.Web.HttpModule,Sitecore.Kernel" name="SitecoreHttpModuleExtensions" />
      <add name="SitecoreAntiCSRF" type="Sitecore.Security.AntiCsrf.SitecoreAntiCsrfModule, Sitecore.Security.AntiCsrf" />
      <!-- NOTE QIC: Custom response codes for sitecore pages. Used to serve content managed 404 pages. -->
      <add name="ResponseStatusCode" type="Qic.Sc.Platform.Pipelines.HttpRequest.ResponseStatusCodeModule, Qic.Sc.Platform" />
    </modules>
    <handlers>
      <add name="WebDAVRoot" path="*" verb="OPTIONS,PROPFIND" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" resourceType="Unspecified" preCondition="classicMode,runtimeVersionv4.0,bitness32" />
      <add name="WebDAVRoot64" path="*" verb="OPTIONS,PROPFIND" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" resourceType="Unspecified" preCondition="classicMode,runtimeVersionv4.0,bitness64" />
      <add verb="*" path="sitecore_webDAV.ashx" type="Sitecore.Resources.Media.WebDAVMediaRequestHandler, Sitecore.Kernel" name="Sitecore.WebDAVMediaRequestHandler" />
      <add verb="*" path="sitecore_media.ashx" type="Sitecore.Resources.Media.MediaRequestHandler, Sitecore.Kernel" name="Sitecore.MediaRequestHandler" />
      <add verb="*" path="sitecore_xaml.ashx" type="Sitecore.Web.UI.XamlSharp.Xaml.XamlPageHandlerFactory, Sitecore.Kernel" name="Sitecore.XamlPageRequestHandler" />
      <add verb="*" path="sitecore_icon.ashx" type="Sitecore.Resources.IconRequestHandler, Sitecore.Kernel" name="Sitecore.IconRequestHandler" />
      <add verb="*" path="sitecore_feed.ashx" type="Sitecore.Shell.Feeds.FeedRequestHandler, Sitecore.Kernel" name="Sitecore.FeedRequestHandler" />
      <add verb="*" path="sitecore_handlers.ashx" type="Sitecore.Web.CustomHandlerFactory, Sitecore.Kernel" name="Sitecore.GenericHandler" />
      <add verb="*" path="sitecore_device_simulation.ashx" type="Sitecore.Shell.DeviceSimulation.SimulationRequestHandler, Sitecore.Kernel" name="Sitecore.SimulationRequestHandler" />
      <add name="Telerik_Web_UI_DialogHandler_aspx" verb="*" preCondition="integratedMode" path="Telerik.Web.UI.DialogHandler.aspx" type="Telerik.Web.UI.DialogHandler" />
      <add name="Telerik_Web_UI_SpellCheckHandler_axd" verb="*" preCondition="integratedMode" path="Telerik.Web.UI.SpellCheckHandler.axd" type="Telerik.Web.UI.SpellCheckHandler" />
      <add name="Telerik_Web_UI_WebResource_axd" verb="*" preCondition="integratedMode" path="Telerik.Web.UI.WebResource.axd" type="Telerik.Web.UI.WebResource" />
      <add verb="*" name="Sitecore.SpeakJS64" path="*/speak/v1/*/*.js" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" resourceType="Unspecified" preCondition="classicMode,runtimeVersionv4.0,bitness64" />
      <add verb="*" name="Sitecore.SpeakJS32" path="*/speak/v1/*/*.js" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" resourceType="Unspecified" preCondition="classicMode,runtimeVersionv4.0,bitness32" />
      <add verb="*" name="Sitecore.SpeakClassic64" path="sitecore_speak.ashx" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" resourceType="Unspecified" preCondition="classicMode,runtimeVersionv4.0,bitness64" />
      <add verb="*" name="Sitecore.SpeakClassic32" path="sitecore_speak.ashx" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" resourceType="Unspecified" preCondition="classicMode,runtimeVersionv4.0,bitness32" />
      <add verb="*" path="sitecore_speak.ashx" type="Sitecore.Resources.Scripts.ScriptHandler, Sitecore.Speak.Client" name="Sitecore.Speak" />
      <add name="Glimpse" path="glimpse.axd" verb="GET" type="Glimpse.AspNet.HttpHandler, Glimpse.AspNet" preCondition="integratedMode" />
    </handlers>
    <validation validateIntegratedModeConfiguration="false" />
    <security>
      <requestFiltering>
        <requestLimits maxAllowedContentLength="524288000" />
      </requestFiltering>
    </security>
    <!-- NOTE QIC: When all else fails (i.e. Sitecore, APS.NET), IIS errors will be used. -->
    <httpErrors errorMode="Custom" existingResponse="Auto">
      <remove statusCode="404" />
      <error statusCode="404" path="Content\Common\404.html" responseMode="File" />
      <remove statusCode="500" />
      <error statusCode="500" path="Content\Common\500.html" responseMode="File" />
    </httpErrors>
    <!-- NOTE QIC: Remove unnecessary HTTP headers which leak version information -->
    <httpProtocol>
      <customHeaders>
        <clear />
        <add name="X-UA-Compatible" value="IE=Edge" />
      </customHeaders>
    </httpProtocol>
    <staticContent>
      <mimeMap fileExtension=".woff2" mimeType="application/font-woff2" />
    </staticContent>
  </system.webServer>
  <!--
    For a description of web.config changes see http://go.microsoft.com/fwlink/?LinkId=235367.

    The following attributes can be set on the <httpRuntime> tag.
      <system.Web>
        <httpRuntime targetFramework="4.5.2" />
      </system.Web>
  -->
  <system.web>
    <!-- Continue to run Sitecore without script validations -->
    <pages validateRequest="false">
      <controls>
        <add tagPrefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Kernel" />
        <add tagPrefix="asp" namespace="System.Web.UI" assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add tagPrefix="asp" namespace="System.Web.UI.WebControls" assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add tagPrefix="sc" namespace="Sitecore.Web.UI.WebControls" assembly="Sitecore.Analytics" />
      </controls>
      <namespaces>
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Linq" />
        <add namespace="System.Collections.Generic" />
      </namespaces>
    </pages>
    <!-- Glimpse: This can be commented in to add additional data to the Trace tab when using WebForms
        <trace writeToDiagnosticsTrace="true" enabled="true" pageOutput="false"/> -->
    <httpModules>
      <add type="Sitecore.Web.RewriteModule, Sitecore.Kernel" name="SitecoreRewriteModule" />
      <add type="Sitecore.Nexus.Web.HttpModule,Sitecore.Nexus" name="SitecoreHttpModule" />
      <add type="Sitecore.Resources.Media.UploadWatcher, Sitecore.Kernel" name="SitecoreUploadWatcher" />
      <add type="Sitecore.IO.XslWatcher, Sitecore.Kernel" name="SitecoreXslWatcher" />
      <add type="Sitecore.IO.LayoutWatcher, Sitecore.Kernel" name="SitecoreLayoutWatcher" />
      <add type="Sitecore.Configuration.ConfigWatcher, Sitecore.Kernel" name="SitecoreConfigWatcher" />
      <add type="Sitecore.Analytics.RobotDetection.Media.MediaRequestSessionModule, Sitecore.Analytics.RobotDetection" name="MediaRequestSessionModule" />
      <add type="Sitecore.Web.HttpModule,Sitecore.Kernel" name="SitecoreHttpModuleExtensions" />
      <add name="SitecoreAntiCSRF" type="Sitecore.Security.AntiCsrf.SitecoreAntiCsrfModule, Sitecore.Security.AntiCsrf" />
    </httpModules>
    <httpHandlers>
      <add verb="*" path="sitecore_webDAV.ashx" type="Sitecore.Resources.Media.WebDAVMediaRequestHandler, Sitecore.Kernel" />
      <add verb="*" path="sitecore_media.ashx" type="Sitecore.Resources.Media.MediaRequestHandler, Sitecore.Kernel" />
      <add verb="*" path="sitecore_xaml.ashx" type="Sitecore.Web.UI.XamlSharp.Xaml.XamlPageHandlerFactory, Sitecore.Kernel" />
      <add verb="*" path="sitecore_icon.ashx" type="Sitecore.Resources.IconRequestHandler, Sitecore.Kernel" />
      <add verb="*" path="sitecore_feed.ashx" type="Sitecore.Shell.Feeds.FeedRequestHandler, Sitecore.Kernel" />
      <add verb="*" path="sitecore_handlers.ashx" type="Sitecore.Web.CustomHandlerFactory, Sitecore.Kernel" />
      <add verb="*" path="sitecore_device_simulation.ashx" type="Sitecore.Shell.DeviceSimulation.SimulationRequestHandler, Sitecore.Kernel" />
      <add verb="*" path="Telerik.Web.UI.DialogHandler.aspx" type="Telerik.Web.UI.DialogHandler" />
      <add verb="*" path="Telerik.Web.UI.SpellCheckHandler.axd" type="Telerik.Web.UI.SpellCheckHandler" />
      <add verb="*" path="Telerik.Web.UI.WebResource.axd" type="Telerik.Web.UI.WebResource" />
      <add verb="*" path="sitecore_speak.ashx" type="Sitecore.Resources.Scripts.ScriptHandler, Sitecore.Speak.Client" />
    </httpHandlers>
    <membership defaultProvider="sitecore" hashAlgorithmType="SHA1">
      <providers>
        <clear />
        <add name="sitecore" type="Sitecore.Security.SitecoreMembershipProvider, Sitecore.Kernel" realProviderName="switcher" providerWildcard="%" raiseEvents="true" />
        <add name="crm" type="CRMSecurityProvider.CRMMembershipProvider, CRMSecurityProvider" readOnly="false" connectionStringName="CRMConnString" applicationName="sitecore" minRequiredPasswordLength="8" maxInvalidPasswordAttempts="5" minRequiredNonalphanumericCharacters="0" requiresUniqueEmail="false" passwordFieldName="qic_portalticket" autoCreatePasswordField="true" />
        <add name="sql" type="System.Web.Security.SqlMembershipProvider" connectionStringName="core" applicationName="sitecore" minRequiredPasswordLength="1" minRequiredNonalphanumericCharacters="0" requiresQuestionAndAnswer="false" requiresUniqueEmail="false" maxInvalidPasswordAttempts="256" />
        <add name="switcher" type="Sitecore.Security.SwitchingMembershipProvider, Sitecore.Kernel" applicationName="sitecore" mappings="switchingProviders/membership" />
      </providers>
    </membership>
    <roleManager defaultProvider="sitecore" enabled="true">
      <providers>
        <clear />
        <add name="sitecore" type="Sitecore.Security.SitecoreRoleProvider, Sitecore.Kernel" realProviderName="switcher" raiseEvents="true" />
        <add name="crm" type="CRMSecurityProvider.CRMRoleProvider, CRMSecurityProvider" connectionStringName="CRMConnString" readOnly="false" />
        <add name="sql" type="System.Web.Security.SqlRoleProvider" connectionStringName="core" applicationName="sitecore" />
        <add name="switcher" type="Sitecore.Security.SwitchingRoleProvider, Sitecore.Kernel" applicationName="sitecore" mappings="switchingProviders/roleManager" />
      </providers>
    </roleManager>
    <profile defaultProvider="switcher" enabled="true" inherits="Sitecore.Security.UserProfile, Sitecore.Kernel">
      <providers>
        <clear />
        <add name="sql" type="System.Web.Profile.SqlProfileProvider" connectionStringName="core" applicationName="sitecore" />
        <add name="crm" type="CRMSecurityProvider.CRMProfileProvider, CRMSecurityProvider" connectionStringName="CRMConnString" readOnly="false" />
        <add name="switcher" type="Sitecore.Security.SwitchingProfileProvider, Sitecore.Kernel" applicationName="sitecore" mappings="switchingProviders/profile" />
      </providers>
      <properties>
        <clear />
        <add type="System.String" name="IsGroupLogin" customProviderData="crm|qic_isgrouplogin" />
        <add type="System.String" name="IsAdminLogin" customProviderData="crm|qic_isqicuser" />
        <add type="System.String" name="UserMetadata" customProviderData="crm|qic_portalmetadata" />
        <add type="System.String" name="ReoprtMetadata" customProviderData="crm|qic_portalreportmetadata" />
        <add type="System.String" name="FirstName" customProviderData="crm|firstname" />
        <add type="System.String" name="MaxRecentDocumentAge" customProviderData="crm|qic_maximumrecentdocumentageint" />
        <add type="System.String" name="SC_UserData" />
      </properties>
    </profile>
    <!--  DYNAMIC DEBUG COMPILATION
          Set compilation debug="true" to enable ASPX debugging.  Otherwise, setting this value to
          false will improve runtime performance of this application.
          Set compilation debug="true" to insert debugging symbols (.pdb information)
          into the compiled page. Because this creates a larger file that executes
          more slowly, you should set this value to true only when debugging and to
          false at all other times. For more information, refer to the documentation about
          debugging ASP .NET files.
    -->
    <compilation defaultLanguage="c#" debug="false" targetFramework="4.5.2">
      <assemblies>
        <add assembly="System.Web.Abstractions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Routing, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Data.Linq, Version=4.0.0.0, Culture=neutral, PublicKeyToken=B77A5C561934E089" />
        <add assembly="System.Web.Helpers, Version=3.0.0.0, Culture=neutral,PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Mvc, Version=5.1.0.0, Culture=neutral,PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.WebPages, Version=3.0.0.0, Culture=neutral,PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.WebPages.Razor, Version=3.0.0.0, Culture=neutral,PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Http, Version=5.1.0.0, Culture=neutral,PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Web.Http.WebHost, Version=5.1.0.0, Culture=neutral,PublicKeyToken=31BF3856AD364E35" />
        <add assembly="System.Net.Http.Formatting, Version=5.1.0.0, Culture=neutral,PublicKeyToken=31BF3856AD364E35" />
      </assemblies>
    </compilation>
    <!--  CUSTOM ERROR MESSAGES
          Set customError mode values to control the display of user-friendly
          error messages to users instead of error details (including a stack trace):

          "On" Always display custom (friendly) messages
          "Off" Always display detailed ASP.NET error information.
          "RemoteOnly" Display custom (friendly) messages only to users not running
          on the local Web server. This setting is recommended for security purposes, so
          that you do not display application detail information to remote clients.

          NOTE QIC:
              The ___rewrite_statuscode is a trigger for our custom http module
              to ensure correct status code is returned. We expect to server
              status pages as errors.
    -->
    <customErrors mode="RemoteOnly" defaultRedirect="Content/common/500.html?___rewrite_statuscode=500" redirectMode="ResponseRewrite" allowNestedErrors="true">
      <error statusCode="404" redirect="Content/common/404.html?___rewrite_statuscode=404" />
      <error statusCode="500" redirect="Content/common/500.html?___rewrite_statuscode=500" />
    </customErrors>
    <!--  AUTHENTICATION
          This section sets the authentication policies of the application. Possible modes are "Windows", "Forms",
          "Passport" and "None"
    -->
    <authentication mode="None">
      <forms name=".ASPXAUTH" timeout="60" cookieless="UseCookies" slidingExpiration="true" />
    </authentication>
    <!--  IDENTITY
          If this setting is true, aspnet will run in the security context of the IIS authenticated
          user (ex. IUSR_xxx).
          If false, aspnet will run in the security context of the default ASPNET user.
    -->
    <identity impersonate="false" />
    <!--  APPLICATION-LEVEL TRACE LOGGING
          Application-level tracing enables trace log output for every page within an application.
          Set trace enabled="true" to enable application trace logging.  If pageOutput="true", the
          trace information will be displayed at the bottom of each page.  Otherwise, you can view the
          application trace log by browsing the "trace.axd" page from your web application
          root.
    -->
    <trace enabled="false" requestLimit="50" pageOutput="false" traceMode="SortByTime" localOnly="true" />
    <!--  SESSION STATE SETTINGS
          By default ASP .NET uses cookies to identify which requests belong to a particular session.
          If cookies are not available, a session can be tracked by adding a session identifier to the URL.
          To disable cookies, set sessionState cookieless="true".

          Note that Sitecore does not support cookieless sessions
          <sessionState mode="InProc" cookieless="false" timeout="20"/>
          <sessionState mode="StateServer" stateConnectionString="tcpip=127.0.0.1:42424" sqlConnectionString="data source=127.0.0.1;user id=sa;password=" cookieless="false" timeout="20"/>
    -->
    <sessionState mode="InProc" cookieless="false" timeout="60">
      <providers>
        <add name="mongo" type="Sitecore.SessionProvider.MongoDB.MongoSessionStateProvider, Sitecore.SessionProvider.MongoDB" sessionType="Standard" connectionStringName="session" pollingInterval="2" compression="true" />
        <add name="mssql" type="Sitecore.SessionProvider.Sql.SqlSessionStateProvider, Sitecore.SessionProvider.Sql" sessionType="Standard" connectionStringName="session" pollingInterval="2" compression="true" />
      </providers>
    </sessionState>
    <!--  GLOBALIZATION
          This section sets the globalization settings of the application.
    -->
    <globalization requestEncoding="utf-8" responseEncoding="utf-8" />
    <!--
      httpRuntime Attributes:
        executionTimeout="[seconds]" - time in seconds before request is automatically timed out
        maxRequestLength="[KBytes]" - KBytes size of maximum request length to accept
        useFullyQualifiedRedirectUrl="[true|false]" - Fully qualifiy the URL for client redirects
        minFreeThreads="[count]" - minimum number of free thread to allow execution of new requests
        minLocalRequestFreeThreads="[count]" - minimum number of free thread to allow execution of new local requests
        appRequestQueueLimit="[count]" - maximum number of requests queued for the application

        If you change the maxRequestLength setting, you should also change the Media.MaxSizeInDatabase setting.
        Media.MaxSizeInDatabase should always be less than maxRequestLength.
      -->
    <httpRuntime maxRequestLength="512000" executionTimeout="600" enableKernelOutputCache="false" relaxedUrlToFileSystemMapping="true" enableVersionHeader="false" />
  </system.web>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="Lucene.Net" publicKeyToken="85089178b9ac3181" />
        <bindingRedirect oldVersion="0.0.0.0-2.9.4.0" newVersion="3.0.3.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Newtonsoft.Json" publicKeyToken="30ad4fe6b2a6aeed" />
        <bindingRedirect oldVersion="0.0.0.0-6.0.0.0" newVersion="6.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages.Razor" publicKeyToken="31bf3856ad364e35" xmlns="urn:schemas-microsoft-com:asm.v1" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" xmlns="urn:schemas-microsoft-com:asm.v1" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http" publicKeyToken="31bf3856ad364e35" xmlns="urn:schemas-microsoft-com:asm.v1" />
        <bindingRedirect oldVersion="0.0.0.0-5.1.0.0" newVersion="5.1.0.0" xmlns="urn:schemas-microsoft-com:asm.v1" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Http.WebHost" publicKeyToken="31bf3856ad364e35" xmlns="urn:schemas-microsoft-com:asm.v1" />
        <bindingRedirect oldVersion="1.0.0.0-5.0.0.0" newVersion="5.1.0.0" xmlns="urn:schemas-microsoft-com:asm.v1" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Net.Http.Formatting" publicKeyToken="31bf3856ad364e35" xmlns="urn:schemas-microsoft-com:asm.v1" />
        <bindingRedirect oldVersion="0.0.0.0-5.1.0.0" newVersion="5.1.0.0" xmlns="urn:schemas-microsoft-com:asm.v1" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Antlr3.Runtime" publicKeyToken="eb42632606e9261f" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.2" newVersion="3.5.0.2" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-1.5.2.14234" newVersion="1.5.2.14234" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Castle.Core" publicKeyToken="407dd0808d44fbdc" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.3.0.0" newVersion="3.3.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Razor" publicKeyToken="31bf3856ad364e35" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Castle.Windsor" publicKeyToken="407dd0808d44fbdc" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.3.0.0" newVersion="3.3.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Autofac" publicKeyToken="17863af14b0044da" culture="neutral" />
        <bindingRedirect oldVersion="0.0.0.0-3.5.0.0" newVersion="3.5.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-3.0.0.0" newVersion="3.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-5.1.0.0" newVersion="5.1.0.0" />
      </dependentAssembly>
      <!--Below section fixes issue for Sitecore CRM Connector Module -->
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Xrm.Sdk" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-7.0.0.0" newVersion="7.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="Microsoft.Crm.Sdk.Proxy" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-7.0.0.0" newVersion="7.0.0.0" />
      </dependentAssembly>
      <!-- Email campaign manager requirements -->
      <dependentAssembly>
        <assemblyIdentity name="Telerik.Web.UI" culture="neutral" publicKeyToken="121fae78165ba3d4" />
        <!--<bindingRedirect oldVersion="0.0.0.0-2012.2.607.35" newVersion="2014.1.403.45" />-->
        <bindingRedirect oldVersion="0.0.0.0-2012.2.607.35" newVersion="2012.2.607.35" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <system.serviceModel>
    <bindings>
      <basicHttpBinding>
        <binding name="SitecoreApplicationCenter" closeTimeout="00:01:00" openTimeout="00:01:00" receiveTimeout="00:20:00" sendTimeout="00:05:00" allowCookies="false" bypassProxyOnLocal="false" hostNameComparisonMode="StrongWildcard" maxBufferSize="65536" maxBufferPoolSize="524288" maxReceivedMessageSize="65536" messageEncoding="Text" textEncoding="utf-8" transferMode="Buffered" useDefaultWebProxy="true">
          <readerQuotas maxDepth="32" maxStringContentLength="8192" maxArrayLength="16384" maxBytesPerRead="4096" maxNameTableCharCount="16384" />
        </binding>
      </basicHttpBinding>
    </bindings>
  </system.serviceModel>
  <!-- Configure logging to go to Glimpse trace listener. -->
  <system.diagnostics>
    <sources>
      <source name="Sc.Web" switchName="sourceSwitch" switchType="System.Diagnostics.SourceSwitch">
        <listeners>
          <add name="GlimpseListener" />
        </listeners>
      </source>
    </sources>
    <switches>
      <add name="sourceSwitch" value="Verbose" />
    </switches>
    <sharedListeners>
      <add name="GlimpseListener" type="Glimpse.Core.TraceListener, Glimpse.Core" />
    </sharedListeners>
  </system.diagnostics>
  <glimpse defaultRuntimePolicy="On" endpointBaseUri="~/Glimpse.axd">
    <!-- 
          For more information on how to configure Glimpse, please visit http://getglimpse.com/Help/Configuration
          or access {your site}/Glimpse.axd for even more details and a Configuration Tool to support you. 
      -->
    <runtimePolicies>
      <ignoredTypes>
        <add type="Glimpse.AspNet.Policy.LocalPolicy, Glimpse.AspNet" />
      </ignoredTypes>
      <uris>
        <add regex=".*/sitecore.*" />
        <add regex=".*/Tools.*" />
        <add regex=".*/speak.*" />
        <add regex=".*\?.*sc_mode=edit.*" />
        <add regex=".*\?.*sc_mode=preview.*" />
      </uris>
    </runtimePolicies>
  </glimpse>
</configuration>