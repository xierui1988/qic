﻿namespace Qic.Sc.Web.Areas.Common.Controllers
{
    using System;
    using System.Web.Mvc;

    using Qic.Sc.Entities.UserDefined.Components.Html.Plainhtml;
    using Qic.Sc.Entities.UserDefined.Components.Pages.Pagecontent;
    using Qic.Sc.Entities.UserDefined.Components.Pages.Pageheadline;
    using Qic.Sc.Entities.UserDefined.Components.Pages.Pagemetadata;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Web.Areas.Common.Models;
    using Qic.Sc.Web.Controllers;

    /// <summary>
    /// The HTML controller.
    /// </summary>
    public partial class ContentController : RenderingController
    {
        private readonly IContentRepository contentRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="ContentController"/> class.
        /// </summary>
        /// <param name="contentRepository">
        /// The content repository.
        /// </param>
        /// <param name="loggerService">
        /// The logger Service.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If any of the arguments is null.
        /// </exception>
        public ContentController(IContentRepository contentRepository, ILoggerService loggerService) : base(loggerService)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            this.contentRepository = contentRepository;
        }

        /// <summary>
        /// Renders raw HTML.
        /// </summary>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult Html(string renderingDataSource)
        {
            var html = this.contentRepository.GetEntity<IPlainHtml>(renderingDataSource);
            if (html == null)
            {
                throw new ApplicationException("Data source for HTML rendering can't be resolved: " + renderingDataSource);
            }

            return this.View(html);
        }

        /// <summary>
        /// Render page content as raw HTML as is.
        /// </summary>
        /// <param name="renderingDataSource">
        /// The rendering data source.
        /// </param>
        /// <param name="renderingContextItem">
        /// The rendering context item.
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult Page(string renderingDataSource, string renderingContextItem)
        {
            var pageContent = this.contentRepository.GetEntity<IPageContent>(
                string.IsNullOrEmpty(renderingDataSource) ? renderingContextItem : renderingDataSource);

            if (pageContent == null && string.IsNullOrEmpty(renderingDataSource))
            {
                return this.HttpNotFound("Content not found");
            }

            if (pageContent == null)
            {
                throw new ApplicationException(
                    "Specified data source can't be resolved (missing/deleted): " + renderingDataSource);
            }

            return this.View(pageContent);
        }

        /// <summary>
        /// Renders the heading as read via the data source or from context item.
        /// </summary>
        /// <param name="renderingDataSource">
        /// The rendering data source.
        /// </param>
        /// <param name="renderingContextItem">
        /// The rendering context item.
        /// </param>
        /// <param name="tag">
        /// The tag, if specified will be used as the heading tag. Default to "h1".
        /// </param>
        /// <returns>
        /// The <see cref="ActionResult"/>.
        /// </returns>
        public virtual ActionResult Heading(string renderingDataSource, string renderingContextItem, string tag)
        {
            var pageHeading = this.contentRepository.GetEntity<IPageHeadline>(
                string.IsNullOrEmpty(renderingDataSource) ? renderingContextItem : renderingDataSource);

            if (pageHeading == null && string.IsNullOrEmpty(renderingDataSource))
            {
                return this.HttpNotFound("Content not found");
            }

            if (pageHeading == null)
            {
                throw new ApplicationException(
                    "Specified data source can't be resolved (missing/deleted): " + renderingDataSource);
            }

            if (string.IsNullOrEmpty(tag))
            {
                tag = "h1";
            }

            return this.View(new HeadingModel{ Heading = pageHeading.Headline, Tag = tag });
        }

        /// <summary>
        /// The title and description.
        /// </summary>
        public virtual ActionResult TitleAndDescription(string renderingContextItem)
        {
            var meta = this.contentRepository.GetEntity<IPageMetadata>(renderingContextItem);
            if (meta == null)
            {
                // This should never happen!
                throw new ApplicationException("Context is empty.");
            }

            return this.View(new MetadataModel { PageMetadata = meta, TitleSuffix = "QIC" });
        }
    }
}