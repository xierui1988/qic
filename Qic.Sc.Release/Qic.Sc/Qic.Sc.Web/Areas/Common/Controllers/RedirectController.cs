﻿namespace Qic.Sc.Web.Areas.Common.Controllers
{
    using System;
    using System.Web.Mvc;

    using Qic.Sc.Entities.UserDefined.Components.Pages.Redirect;
    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// Controller to render basic page content.
    /// </summary>
    public partial class RedirectController : Controller
    {
        private readonly IContentRepository contentRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="RedirectController"/> class.
        /// </summary>
        public RedirectController(IContentRepository contentRepository)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            this.contentRepository = contentRepository;
        }

        /// <summary>
        /// This action is to redirect sitecore item to other external websites or internal resources (e.g. PDF).
        /// </summary>
        public virtual ActionResult RedirectTo()
        {
            var entity = this.contentRepository.GetEntity<IRedirect>(Sitecore.Context.Item.ID.ToString());
            if (entity != null)
            {
                return this.Redirect(Url.ContentLink(entity.RedirectTo));
            }

            return this.HttpNotFound();
        }
    }
}