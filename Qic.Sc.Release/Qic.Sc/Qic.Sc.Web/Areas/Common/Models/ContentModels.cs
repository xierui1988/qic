﻿namespace Qic.Sc.Web.Areas.Common.Models
{
    using Qic.Sc.Entities.UserDefined.Components.Pages.Pagemetadata;

    /// <summary>
    /// The heading view model.
    /// </summary>
    public class HeadingModel
    {
        /// <summary>
        /// Gets or sets the text of the heading.
        /// </summary>
        public string Heading { get; set; }

        /// <summary>
        /// Gets or sets the wrapping tag (e.g. H1, H2 etc.).
        /// </summary>
        public string Tag { get; set; }
    }

    /// <summary>
    /// Page meta data model.
    /// </summary>
    public class MetadataModel
    {
        /// <summary>
        /// Gets or sets the page meta data.
        /// </summary>
        public IPageMetadata PageMetadata { get; set; }

        /// <summary>
        /// Gets or sets the title suffix.
        /// </summary>
        public string TitleSuffix { get; set; }
    }
}