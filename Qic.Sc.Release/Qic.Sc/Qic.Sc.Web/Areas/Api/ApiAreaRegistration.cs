﻿using System.Web.Mvc;

namespace Qic.Sc.Web.Areas.Api
{
    using System.Web.Http;

    /// <summary>
    /// The api area registration.
    /// </summary>
    public class ApiAreaRegistration : AreaRegistration 
    {
        /// <summary>
        /// Gets the area name.
        /// </summary>
        public override string AreaName 
        {
            get 
            {
                return "Api";
            }
        }

        /// <summary>
        /// Area registration routine.
        /// </summary>
        /// <param name="context">
        /// The context.
        /// </param>
        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.Routes.MapHttpRoute(
                "DefaultApiRoute",
                "api/{controller}/{action}/{id}",
                new { id = RouteParameter.Optional });
        }
    }
}