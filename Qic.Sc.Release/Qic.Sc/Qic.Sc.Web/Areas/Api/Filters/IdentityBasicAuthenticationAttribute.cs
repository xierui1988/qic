﻿namespace Qic.Sc.Web.Areas.Api.Filters
{
    using System.Configuration;
    using System.Security.Principal;
    using System.Threading;
    using System.Threading.Tasks;

    using ClaimsPrincipal = System.Security.Claims.ClaimsPrincipal;

    /// <summary>
    /// The identity basic authentication attribute.
    /// From: http://www.asp.net/web-api/overview/security/authentication-filters
    /// </summary>
    public class IdentityBasicAuthenticationAttribute : BasicAuthenticationAttribute
    {
        /// <summary>
        /// The authenticate async.
        /// </summary>
        /// <param name="userName">
        /// The user name.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="cancellationToken">
        /// The cancellation token.
        /// </param>
        /// <returns>
        /// The <see cref="Task"/>.
        /// </returns>
        protected override async Task<IPrincipal> AuthenticateAsync(string userName, string password, CancellationToken cancellationToken)
        {
            return
                await Task.Run(() => this.AuthenticateConfiguredUploadPrincipal(userName, password), cancellationToken);
        }

        private IPrincipal AuthenticateConfiguredUploadPrincipal(string username, string password)
        {
            var configuredCredentials = ConfigurationManager.ConnectionStrings["DocumentUploadCredentials"];
            if (configuredCredentials == null || string.IsNullOrEmpty(configuredCredentials.ConnectionString))
            {
                return null;
            }

            if (configuredCredentials.ConnectionString != username + ":" + password)
            {
                return null;
            }

            return new ClaimsPrincipal(new GenericIdentity(username));
        }
    }
}