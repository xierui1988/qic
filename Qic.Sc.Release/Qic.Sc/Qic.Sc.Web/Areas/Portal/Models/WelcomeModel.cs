﻿using Qic.Sc.Entities.UserDefined.QIC.Portal.Pages.Alertpage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qic.Sc.Web.Areas.Portal.Models
{
    /// <summary>
    /// The information to information needed to display in the service
    /// </summary>
    public class WelcomeModel
    {
        /// <summary>
        /// The User's First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Alert Messages that are needed to display
        /// </summary>
        public IEnumerable<IAlertPage> Alerts { get; set; }

        /// <summary>
        /// The Amount of New Document Notifications that the user can see
        /// </summary>
        public int Notifications { get; set; }
    }
}