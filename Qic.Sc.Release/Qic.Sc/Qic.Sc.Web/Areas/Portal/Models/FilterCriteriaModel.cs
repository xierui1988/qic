﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qic.Sc.Web.Areas.Portal.Models
{
    /// <summary>
    /// Filter criteria model
    /// </summary>
    public class FilterCriteriaModel
    {
        /// <summary>
        /// Account ID
        /// </summary>
        public string AccountFilterId { get; set; }
        
        /// <summary>
        /// Account holding ID
        /// </summary>
        public string AccountHoldingFilterId { get; set; }
       
        /// <summary>
        /// Product ID
        /// </summary>
        public string ProductFilterId { get; set; }
       
        /// <summary>
        /// Report ID
        /// </summary>
        public string ReportId { get; set; }

        /// <summary>
        /// Report date from
        /// </summary>
        public string ReportDateFrom { get; set; }

        /// <summary>
        /// Report date To
        /// </summary>
        public string ReportDateTo { get; set; }

        /// <summary>
        /// Report published date from
        /// </summary>
        public string PublishedDateFrom { get; set; }

        /// <summary>
        /// Report published date from
        /// </summary>
        public string PublishedDateTo { get; set; }

        /// <summary>
        /// Gets or sets the report group.
        /// </summary>
        public string ReportGroup { get; set; }

        /// <summary>
        /// Gets or sets the Search Term.
        /// </summary>
        public string SearchTerm { get; set; }
    }
}