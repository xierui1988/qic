﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Qic.Sc.Web.Areas.Portal.Models
{
    /// <summary>
    /// The Form Upload model
    /// </summary>
    public class FormUploadModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="FormUploadModel"/> class.
        /// </summary>
        public FormUploadModel()
        {
            this.Messages = new List<string>();
        }

        /// <summary>
        /// Gets or sets the upload type.
        /// </summary>
        public DocumentUploadType UploadType { get; set; }

        /// <summary>
        /// Gets or sets the name of the account.
        /// </summary>
        /// <value>
        /// The name of the account.
        /// </value>
        public string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the account code.
        /// </summary>
        /// <value>
        /// The account code.
        /// </value>
        public string AccountCode { get; set; }

        /// <summary>
        /// Gets or sets the messages.
        /// </summary>
        public List<string> Messages { get; set; }

        /// <summary>
        /// Gets or sets the Form date.
        /// </summary>
        [DataType(DataType.Date)]
        public DateTime FormDate
        {
            get
            {
                try
                {
                    return new DateTime(this.FormDateYear, this.FormDateMonth, this.FormDateDay);
                }
                catch
                {
                    return DateTime.Now;
                }
            }
        }

        /// <summary>
        /// Gets or sets the form date day.
        /// </summary>
        public int FormDateDay { get; set; }

        /// <summary>
        /// Gets or sets the form date month.
        /// </summary>
        public int FormDateMonth { get; set; }

        /// <summary>
        /// Gets or sets the report date year.
        /// </summary>
        public int FormDateYear { get; set; }

        /// <summary>
        /// Id used for authentication
        /// </summary>
        public string Id { get; set; }
    }
}