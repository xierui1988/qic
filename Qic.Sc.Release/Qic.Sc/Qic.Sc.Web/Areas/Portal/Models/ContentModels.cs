﻿using Qic.Sc.Entities.UserDefined.Components.Pages.Pagecontent;

namespace Qic.Sc.Web.Areas.Portal.Models
{
    /// <summary>
    /// Content Model
    /// </summary>
    public class ContentModels
    {
        /// <summary>
        /// Page Meta Data
        /// </summary>
        public IPageContent PageMetadata { get; set; }

        /// <summary>
        /// Title for page (if any)
        /// </summary>
        public string Title { get; set; }
    }
}