﻿namespace Qic.Sc.Web.Areas.Portal.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;

    using Newtonsoft.Json;

    using Qic.Sc.Web.Areas.Portal.Models;
    using Qic.Sc.Web.Controllers;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Entities.Metadata;
    using System.Collections.Generic;
    using System.Globalization;

    /// <summary>
    /// The library controller.
    /// </summary>
    [Authorize]
    public partial class LibraryController : RenderingController
    {
        private readonly IReportService reportService;
        private readonly IFormService formService;
        private readonly IWebSecurityService webSecurity;
        private readonly IPortalWebSecurityService portalWebSecurity;

        /// <summary>
        /// Initializes a new instance of the <see cref="LibraryController"/> class.
        /// </summary>
        public LibraryController(IWebSecurityService webSecurityService, IPortalWebSecurityService portalWebSecurity, IReportService reportService, IFormService formService)
        {
            if (webSecurityService == null)
            {
                throw new ArgumentNullException("webSecurityService");
            }
            if (reportService == null)
            {
                throw new ArgumentNullException("reportService");
            }
            if (formService == null)
            {
                throw new ArgumentNullException("formService");
            }
            this.webSecurity = webSecurityService;
            this.portalWebSecurity = portalWebSecurity;
            this.reportService = reportService;
            this.formService = formService;
        }

        /// <summary>
        /// Library navigation
        /// </summary>
        public virtual ActionResult Navigation(string profilePath)
        {
            var userName = this.webSecurity.GetCurrentAuthenticatedUsername();
            var accessibleReports = this.portalWebSecurity.GetReportAccessFor(userName).GetAccessibleReportCodes();
            var folders = Enumerable.Empty<LibraryFolderModel>();
            var recentCount = "0";

            if (accessibleReports.Any())
            {
                var groupedReportMetadata = (from reportType in this.portalWebSecurity.GetReportTypeMetadataFor(accessibleReports)
                                             group reportType by reportType.ReportTypeId
                                                 into g
                                                 select g.First());

                var path = this.Request.Url.LocalPath.Split('/').Last();

                folders = groupedReportMetadata.Select(
                                   r => new LibraryFolderModel
                                   {
                                       Path = r.ToUrlSafeName(),
                                       Title = r.ReportTypeName,
                                       Active = path.Equals(r.ToUrlSafeName()),
                                       CrmFolderOrder = r.CrmIndexOrder,
                                   }).OrderBy(x => x.CrmFolderOrder);

                var count = this.GetUnreadRecentCount();
                recentCount = count > 99 ? "99+" : count.ToString();
            }

            // Build navigation model
            return this.View(new LibraryNavigationModel
            {
                Folders = folders,
                ProfilePath = profilePath,
                RecentCount = recentCount
            });
        }

        /// <summary>
        /// Profile view.
        /// </summary>
        public virtual ActionResult MyProfile()
        {
            var reportAccess = this.portalWebSecurity.GetProfileReportAccessFor(this.webSecurity.GetCurrentAuthenticatedUsername()).ToList();
            var invoices = this.portalWebSecurity.GetAllReportTypes().GetInvoiceCodes();
            if (invoices != null && invoices.Any())
            {
                reportAccess = reportAccess.Where(access => !invoices.Contains(access.ReportCode)).ToList();
            }

            var result = new ProfileModel
            {
                FullName = Sitecore.Context.User.Profile.FullName,
                Email = Sitecore.Context.User.Profile.Email,
                ContactOwner = reportAccess.Select(x => x.ParentAccount).FirstOrDefault(),
                ReportAccess = reportAccess
            };

            return this.View(result);
        }

        /// <summary>
        /// The my profile post back handler.
        /// </summary>
        [HttpPost]
        public virtual ActionResult MyProfile(ProfileModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model", "No Profile Model was provided");
            }

            this.portalWebSecurity.UpdateUserReportAccessNotifications(
                this.webSecurity.GetCurrentAuthenticatedUsername(),
                model.ReportAccess);

            return this.MyProfile();
        }

        /// <summary>
        /// Document list view.
        /// </summary>
        public virtual ActionResult DocumentList(string id, int take = 20, ReportType type = ReportType.All)
        {
            // Get access rights for the current user. Trim to requested report group id.
            var userName = this.webSecurity.GetCurrentAuthenticatedUsername();
            var isAdmin = this.webSecurity.IsAdmin(userName);
            var userModel = this.webSecurity.GetUserMetadataFor(userName);
            var favCount = userModel.Favourites != null && userModel.Favourites.Items.Any() ? userModel.Favourites.Items.Count : 0;
            var access = this.TrimAccessToReportGroup(id, this.portalWebSecurity.GetReportAccessFor(userName).ToArray());

            // Check for empty access and return empty model
            if (!access.Any())
            {
                return
                    this.View(
                        new DocumentListModel
                            {
                                Filter = new DocumentListFilterModel(),
                                Documents = Enumerable.Empty<DocumentModel>(),
                                Take = take,
                                Total = 0,
                                ReportGroup = id,
                                FavouriteCount = favCount
                            });
            }

            // Create filtering options based on user access. These drive the filter control
            // ensuring the user can only select options accessible based on access rights and
            // current report group/type.
            var filter = new DocumentListFilterModel().SetFilterOptionsFrom(access);

            // At the lease we should ensure the filter is applied based on accessible
            // report codes.
            filter.ReportCodes = access.GetAccessibleReportCodes().ToList();

            // Get security trimmed list of documents for the current user.
            int total;
            var documents = this.reportService.GetDocumentsByFilterAndSort(userName, new ReportFilterAndSortMetadata { Filters = filter }, 0, take, out total);
            var cutoffDate = userModel.ReadDocuments.CutOffDate;
            var daysCutOff = (DateTime.Now - cutoffDate).Days;

            return this.View(new DocumentListModel
            {
                Filter = filter,
                Documents = documents.Select(d => new DocumentModel().FromDocument(d, this.Url.ForItem(d.EntityId), isAdmin ? null : access.GetAccessibleAccountCodes())),
                Take = take,
                Total = total,
                ReportGroup = id,
                CutoffDate = daysCutOff.ToString(),
                FavouriteCount = favCount
            });
        }

        /// <summary>
        /// This is partial response to update the model list based on
        /// posted back model.
        /// </summary>
        [HttpPost]
        public virtual ActionResult DocumentList(DocumentListModel model)
        {
            // Get access rights for the current user. Trim to requested report group id.
            var userName = this.webSecurity.GetCurrentAuthenticatedUsername();
            var isAdmin = this.webSecurity.IsAdmin(userName);
            var userModel = this.webSecurity.GetUserMetadataFor(userName);
            var favCount = userModel.Favourites != null && userModel.Favourites.Items.Any() ? userModel.Favourites.Items.Count : 0;
            var access = this.TrimAccessToReportGroup(model.ReportGroup, this.portalWebSecurity.GetReportAccessFor(userName).ToArray());

            // Check for empty access and return empty model
            if (!access.Any())
            {
                return
                    this.View(
                        new DocumentListModel
                        {
                            Filter = model.Filter,
                            Documents = Enumerable.Empty<DocumentModel>(),
                            Take = model.Take,
                            Total = 0,
                            ReportGroup = model.ReportGroup,
                            FavouriteCount = favCount
                        });
            }

            if (model.ReportGroup == null)
            {
                return this.Content(JsonConvert.SerializeObject(this.FormListDataModel(null, model.Take, model.Skip)), "application/json");
            }
            // Convert filter control view model into an actual filter
            FilterMapping(model);

            // We should always ensure report code filter is present an is withing current
            // report group/type.
            if (!model.Filter.ReportCodes.Any())
            {
                model.Filter.ReportCodes = access.GetAccessibleReportCodes().ToList();
            }

            int total;
            IEnumerable<Entities.UserDefined.QIC.Portal.Data.Document.IDocument> documents;

            switch (model.Filter.ReportType)
            {
                case ReportType.Recent:
                    documents =
                        this.reportService.GetRecentDocumentsBy(
                            userName,
                            access.GetAccessibleReportCodes(),
                            model.Skip,
                            model.Take,
                            out total);
                    break;

                case ReportType.Favourite:
                    documents =
                        this.reportService.GetFavouriteDocumentsBy(
                            userName,
                            access.GetAccessibleReportCodes(),
                            model.Skip,
                            model.Take,
                            out total);
                    break;

                default:
                    // If search paging criteria
                    if (model.FilterCriteria != null && !string.IsNullOrEmpty(model.FilterCriteria.SearchTerm))
                    {
                        documents = this.reportService.SearchDocumentsBy(
                           userName,
                           new ReportFilterAndSortMetadata { Filters = model.Filter },
                           model.FilterCriteria.SearchTerm,
                           model.Skip,
                           model.Take,
                           out total);
                    }
                    else
                    {
                        documents = this.reportService.GetDocumentsByFilterAndSort(
                            userName,
                            new ReportFilterAndSortMetadata { Filters = model.Filter },
                            model.Skip,
                            model.Take,
                            out total);
                    }
                    break;
            }

            // Create filtering portions based on the access of the current user.
            model.Documents = documents.Select(d => new DocumentModel().FromDocument(d, this.Url.ForItem(d.EntityId), isAdmin ? null : access.GetAccessibleAccountCodes()));
            model.Total = total;

            return this.Content(JsonConvert.SerializeObject(model), "application/json");
        }

        private static void FilterMapping(DocumentListModel model)
        {
            if (model.FilterCriteria != null)
            {
                if (model.FilterCriteria.ReportGroup != null)
                {
                    model.ReportGroup = model.FilterCriteria.ReportGroup;
                }

                if (model.FilterCriteria.AccountFilterId != null)
                {
                    model.Filter.AccountCodes.Add(model.FilterCriteria.AccountFilterId);
                }
                if (model.FilterCriteria.AccountHoldingFilterId != null)
                {
                    model.Filter.HoldingAccountCodes.Add(model.FilterCriteria.AccountHoldingFilterId);
                }

                if (model.FilterCriteria.ProductFilterId != null)
                {
                    model.Filter.ProductCodes.Add(model.FilterCriteria.ProductFilterId);
                }

                if (model.FilterCriteria.ReportId != null)
                {
                    model.Filter.ReportCodes.Add(model.FilterCriteria.ReportId);
                }

                if (model.FilterCriteria.ReportDateFrom != null)
                {
                    model.Filter.ReportDateFrom = DateTime.ParseExact((model.FilterCriteria.ReportDateFrom), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                if (model.FilterCriteria.ReportDateTo != null)
                {
                    model.Filter.ReportDateTo = DateTime.ParseExact((model.FilterCriteria.ReportDateTo), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                if (model.FilterCriteria.PublishedDateFrom != null)
                {
                    model.Filter.PublishDateFrom = DateTime.ParseExact((model.FilterCriteria.PublishedDateFrom), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }

                if (model.FilterCriteria.PublishedDateTo != null)
                {
                    model.Filter.PublishDateTo = DateTime.ParseExact((model.FilterCriteria.PublishedDateTo), "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
            }
        }

        /// <summary>
        /// update logic for Favourites
        /// </summary>
        [HttpPost]
        public virtual ActionResult UpdateFavourites(DocumentListModel model)
        {
            if (model == null)
            {
                throw new ArgumentNullException("model");
            }

            var username = this.webSecurity.GetCurrentAuthenticatedUsername();
            var userModel = this.webSecurity.GetUserMetadataFor(username);
            var itemsToAdd = model.Documents.Aggregate(0, (current, item) => !item.IsFavourite ? current + 1 : current - 1);

            if (userModel.Favourites != null && userModel.Favourites.Items != null && (userModel.Favourites.Items.Count + itemsToAdd) <= Qic.Sc.Entities.Constants.NumberOfFavourites)
            {
                foreach (var item in model.Documents)
                {
                    // Update favourites based on the posted data
                    bool isFavourite = false;
                    if (!userModel.Favourites.Items.Contains(item.ReportId))
                    {
                        // Add to favourite
                        userModel.Favourites.Items.Add(item.ReportId);
                        isFavourite = true;
                    }
                    else
                    {
                        // Remove favourite
                        userModel.Favourites.Items.Remove(item.ReportId);
                    }
                    item.IsFavourite = isFavourite;
                }

                this.webSecurity.UpdatedUserMetadataFor(username, userModel);
            }

            model.FavouriteCount = userModel.Favourites == null || userModel.Favourites.Items == null
                                       ? 0
                                       : userModel.Favourites.Items.Count;

            return this.Content(JsonConvert.SerializeObject(model), "application/json");
        }

        /// <summary>
        /// update the options to be selected for filters.
        /// </summary>
        /// <param name="model">JSON model of string Id</param>
        /// <returns>a new filter criteria</returns>
        [HttpPost]
        public virtual ActionResult UpdateFilterCriteria(FilterCriteriaModel model)
        {
            // Get access rights for the current user. Trim to requested report group id.
            var access = this.TrimAccessToReportGroup(
                model.ReportGroup,
                this.portalWebSecurity.GetReportAccessFor(this.webSecurity.GetCurrentAuthenticatedUsername()).ToArray());

            IEnumerable<ReportAccessMetadata> matchingReports = new List<ReportAccessMetadata>();

            if (model.AccountFilterId != null)
            {
                matchingReports = access.Where(x => x.AccountCode == model.AccountFilterId);
            }

            if (model.AccountHoldingFilterId != null)
            {
                matchingReports = access.Where(x => x.HoldingAccountCode == model.AccountHoldingFilterId);
            }

            if (model.ProductFilterId != null)
            {
                matchingReports = access.Where(x => x.ProductCode == model.ProductFilterId);
            }

            if (model.ReportId != null)
            {
                matchingReports = access.Where(x => x.ReportCode == model.ReportId);
            }

            // Create filtering options based on user access.
            var filter = new DocumentListFilterModel().SetFilterOptionsFrom(matchingReports == null || !matchingReports.Any() ? access.ToArray() : matchingReports.ToArray());

            return this.Content(JsonConvert.SerializeObject(filter), "application/json");
        }

        /// <summary>
        /// Used for the Home Control in home page
        /// </summary>
        public virtual ActionResult Dashboard()
        {
            var userName = this.webSecurity.GetCurrentAuthenticatedUsername();
            var userModel = this.webSecurity.GetUserMetadataFor(userName);
            var cutoffDate = userModel.ReadDocuments.CutOffDate;

            var model = new DashboardModels()
            {
                CutoffDate = (DateTime.Now - cutoffDate).Days.ToString(),
                FavouriteCount =
                    userModel.Favourites != null && userModel.Favourites.Items.Any()
                        ? userModel.Favourites.Items.Count
                        : 0
            };

            return this.View(model);
        }

        /// <summary>
        /// Get Dashboard related reports.
        /// </summary>
        /// <returns> json response of relevent report.</returns>
        [HttpPost]
        public virtual ActionResult Dashboard(DocumentListModel model, ICollection<string> getAccessibleReportCodes, ICollection<string> getAccessibleReportCodes1)
        {
            // Get access rights for the current user. Trim to requested report group id.
            var userName = this.webSecurity.GetCurrentAuthenticatedUsername();
            var isAdmin = this.webSecurity.IsAdmin(userName);
            var access = this.TrimAccessToReportGroup(
                model.ReportGroup,
                this.portalWebSecurity.GetReportAccessFor(userName).ToArray());

            int total = 0;
            IEnumerable<Entities.UserDefined.QIC.Portal.Data.Document.IDocument> documents =
                Enumerable.Empty<Entities.UserDefined.QIC.Portal.Data.Document.IDocument>();

            switch (model.Filter.ReportType)
            {
                case ReportType.Recent:
                    documents =
                        this.reportService.GetRecentDocumentsBy(
                            userName, getAccessibleReportCodes1, model.Skip,
                            model.Take,
                            out total);
                    break;

                case ReportType.Favourite:
                    documents =
                        this.reportService.GetFavouriteDocumentsBy(
                            userName, getAccessibleReportCodes, model.Skip,
                            model.Take,
                            out total);
                    break;
            }

            // Create filtering portions based on the access of the current user.
            model.Documents = documents.Select(d => new DocumentModel().FromDocument(d, this.Url.ForItem(d.EntityId), isAdmin ? null : access.GetAccessibleAccountCodes()));
            model.Total = total;

            var cutoffDate = this.webSecurity.GetUserMetadataFor(userName).ReadDocuments.CutOffDate;
            model.CutoffDate = (DateTime.Now - cutoffDate).Days.ToString();
            return this.Content(JsonConvert.SerializeObject(model), "application/json");
        }

        /// <summary>
        /// Form list.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="take">The take.</param>
        /// <param name="skip">The skip</param>
        /// <param name="type">The type.</param>
        /// <returns>Action Result</returns>
        public virtual ActionResult FormList(string id, int take = 20, int skip = 0, ReportType type = ReportType.All)
        {
            return this.View(this.FormListDataModel(id, take, skip, type));
        }

        /// <summary>
        /// Form list Data Model Generator.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="take">The take.</param>
        /// <param name="skip">The skip</param>
        /// <param name="type">The type.</param>
        /// <returns>Action Result</returns>
        private DocumentListModel FormListDataModel(string id, int take = 20, int skip = 0, ReportType type = ReportType.All)
        {
            int total;
            var forms = this.formService.GetFormsBy(
                this.webSecurity.GetCurrentAuthenticatedUsername(), skip, take, out total);

            return new DocumentListModel
            {
                Documents = forms.Select(f => new DocumentModel().FromDocument(f, this.Url.ForItem(f.EntityId))),
                Take = take,
                Skip = skip,
                Total = total,
                ReportGroup = id
            };
        }

        private ReportAccessMetadata[] TrimAccessToReportGroup(string reportGroupName, ReportAccessMetadata[] access)
        {
            if (reportGroupName == null || access == null || !access.Any())
            {
                // Do not trim. Nothing to be done.
                return access;
            }

            // Get a list of reports in the report group based on id as requested.
            var accessibleReportsWithingGroup =
                this.portalWebSecurity.GetReportTypeMetadataFor(access.GetAccessibleReportCodes())
                    .Where(r => r.IsMatchingUrlSafeName(reportGroupName))
                    .Select(r => r.ReportCode)
                    .ToArray();

            // Use these reports as the minimal set of report filters. This should be a subset
            // of reports the user otherwise have access to.
            if (!accessibleReportsWithingGroup.Any())
            {
                // Do not trim. Group name does not match any configured.
                return access;
            }

            // Reduce to only reports in the requested report group.
            return access.Where(a => accessibleReportsWithingGroup.Contains(a.ReportCode)).ToArray();
        }

        private int GetUnreadRecentCount()
        {
            return this.reportService.GetRecentDocumentsCount(this.webSecurity.GetCurrentAuthenticatedUsername());
        }
    }
}