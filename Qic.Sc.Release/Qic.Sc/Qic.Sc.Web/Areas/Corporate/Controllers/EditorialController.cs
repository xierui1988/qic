﻿namespace Qic.Sc.Web.Areas.Corporate.Controllers
{
    using System;
    using System.Web.Mvc;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.UserDefined.Components.Pages.Pagemetadata;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.Editorial;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;
    using Qic.Sc.Interfaces.Core;
    using Qic.Sc.Web.Areas.Common.Models;
    using Qic.Sc.Web.Areas.Corporate.Model;
    using Qic.Sc.Web.Controllers;

    using Action = Antlr.Runtime.Misc.Action;

    /// <summary>
    /// The editorial controller.
    /// </summary>
    public partial class EditorialController : RenderingController
    {
        private readonly IContentRepository contentRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorialController"/> class.
        /// </summary>
        public EditorialController(IContentRepository contentRepository)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            this.contentRepository = contentRepository;
        }

        /// <summary>
        /// Editorial content.
        /// </summary>
        public virtual ActionResult Index(string name, string date)
        {
            var content = this.contentRepository.GetEntity<IEditorial>(this.GetEditorialPath(name, date));
            if (content == null)
            {
                return this.HttpNotFound();
            }

            return this.View(
                MVC.Corporate.Content.Views.Index,
                new ContentModel { Content = content.Content, Headline = content.Headline });
        }

        /// <summary>
        /// The meta data.
        /// </summary>
        /// <param name="name">
        /// The name.
        /// </param>
        /// <param name="date">
        /// The date.
        /// </param>
        /// <returns>
        /// The <see cref="Action"/>.
        /// </returns> 
        public virtual ActionResult Metadata(string name, string date)
        {
            var metadata = this.contentRepository.GetEntity<IPageMetadata>(this.GetEditorialPath(name, date));
            if (metadata == null)
            {
                return this.HttpNotFound();
            }

            return this.View(
                MVC.Common.Content.Views.TitleAndDescription,
                new MetadataModel { PageMetadata = metadata, TitleSuffix = "QIC" });
        }

        /// <summary>
        /// The banner.
        /// </summary>
        public virtual ActionResult Banner(string name, string date)
        {
            var feature = this.contentRepository.GetEntity<IFeature>(this.GetEditorialPath(name, date));
            if (feature == null)
            {
                return this.HttpNotFound();
            }

            return this.View(
                MVC.Corporate.Featured.Views.Banner,
                new BannerModel { Feature = feature, IsLinkVisible = false });
        }

        /// <summary>
        /// The attachment.
        /// </summary>
        public virtual ActionResult Attachment(string name, string date)
        {
            var editorial = this.contentRepository.GetEntity<IEditorial>(this.GetEditorialPath(name, date));
            return this.View(
                MVC.Corporate.Content.Views.Attachment,
                new AttachmentModel { Url = editorial.File.Src, Text = editorial.GetDisplayNameOrName() });
        }

        private string GetEditorialPath(string name, string date)
        {
            if (string.IsNullOrEmpty(name) || string.IsNullOrEmpty(date))
            {
                throw new ApplicationException("Editorial can't be resolved based on the name and date provided.");
            }

            if (date.Length < 6)
            {
                throw new ApplicationException("Editorial can't be resolved based on the date provided.");
            }

            var year = date.Substring(0, 4);
            var month = date.Substring(4, 2);

            return string.Format(
                "{0}/{1}/{2}/{3}",
                Constants.Content.Corporate.Home.KnowledgeCentre.Wildcard.Path,
                year,
                month,
                name.Replace("-", " "));
        }
    }
}