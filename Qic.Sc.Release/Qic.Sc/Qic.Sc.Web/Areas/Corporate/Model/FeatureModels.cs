﻿namespace Qic.Sc.Web.Areas.Corporate.Model
{
    using System;

    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;

    using Glass.Mapper.Sc.Fields;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.GlobalFeature;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;

    /// <summary>
    /// The feature model extensions.
    /// </summary>
    public static class FeatureModelExtensions
    {
        /// <summary>
        /// The to news block stacked panel model.
        /// </summary>
        public static StackedPanelModel ToStackedPanelModelWithBlockNews(this IGlobalFeature featured, UrlHelper url, int count, string dateformat, string sitename)
        {
            var model = ToStackedPanelModel(featured, url);
            if (model != null)
            {
                model.NewsBlockList = featured.FeatureEntities.Take(count).Select(f => f.ToStackedPanelItemModel(url, dateformat, sitename));
            }

            return model;
        }

        /// <summary>
        /// The to news block stacked panel model.
        /// </summary>
        public static StackedPanelModel ToStackedPanelModelWithNewsItems(this IGlobalFeature featured, UrlHelper url, int count, string dateformat, string sitename)
        {
            var model = ToStackedPanelModel(featured, url);
            if (model != null)
            {
                model.IsShowAllEnabled = true;
                model.NewsBlockList = featured.FeatureEntities.Take(1).Select(f => f.ToStackedPanelItemModel(url, dateformat, sitename));
                model.NewsItemList = featured.FeatureEntities.Skip(1).Take(count).Select(f => f.ToStackedPanelItemModel(url, null, sitename));
            }

            return model;
        }

        /// <summary>
        /// The to stacked panel model.
        /// </summary>
        /// <param name="featured">
        /// The featured.
        /// </param>
        /// <param name="url">
        /// The url.
        /// </param>
        /// <returns>
        /// The <see cref="StackedPanelModel"/>.
        /// </returns>
        public static StackedPanelModel ToStackedPanelModel(this IGlobalFeature featured, UrlHelper url)
        {
            if (featured == null)
            {
                return null;
            }

            var model = new StackedPanelModel
                            {
                                Title = featured.GetDisplayNameOrName(),
                                TitleLink = url.ContentLink(featured.Link, Constants.CorporateSiteName)
                            };

            if (featured.Image != null && !string.IsNullOrEmpty(featured.Image.Src))
            {
                model.HeroImage = featured.Image.Src;
            }

            return model;
        }

        /// <summary>
        /// The to stacked panel item model.
        /// </summary>
        public static StackedPanelItemModel ToStackedPanelItemModel(this IFeature feature, UrlHelper url, string formatdate, string sitename)
        {
            if (feature == null)
            {
                return null;
            }

            return new StackedPanelItemModel
                       {
                           Title = feature.Title,
                           Link = url.ForItem(feature.EntityId, sitename),
                           Date = feature.PublicationDate,
                           Intro = feature.PullQuote,
                           DateFormat = formatdate
                       };
        }
    }

    /// <summary>
    /// Feature Model
    /// </summary>
    public class FeatureModel
    {
        /// <summary>
        /// Stack Title
        /// </summary>
        public IGlobalFeature Feature { get; set; }

        /// <summary>
        /// Number to Display
        /// </summary>
        public int Count { get; set; }
    }

    /// <summary>
    /// The banner model.
    /// </summary>
    public class BannerModel
    {
        /// <summary>
        /// Gets or sets the feature.
        /// </summary>
        public IFeature Feature { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is link visible.
        /// </summary>
        public bool IsLinkVisible { get; set; }

        /// <summary>
        /// Gets or sets the height of the banner.
        /// </summary>
        public int Height { get; set; }
    }

    /// <summary>
    /// Feature Model
    /// </summary>
    public class CallToActionModel
    {
        /// <summary>
        /// Banner for Action
        /// </summary>
        public string Banner { get; set; }

        /// <summary>
        /// Drop Down Links
        /// </summary>
        public IEnumerable<IFeature> DropDownLinks { get; set; }
    }

    /// <summary>
    /// Stack Hero Model
    /// </summary>
    public class StackedHeroModel
    {
        /// <summary>
        /// Gets or sets the panels.
        /// </summary>
        public IEnumerable<StackedPanelModel> Panels { get; set; }
    }

    /// <summary>
    /// The stacked panel model.
    /// </summary>
    public class StackedPanelModel
    {
        /// <summary>
        /// Gets or sets the hero image.
        /// Set to null if not needed.
        /// </summary>
        public string HeroImage { get; set; }

        /// <summary>
        /// Gets or sets the heading.
        /// Set to null if not needed.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the heading link.
        /// Set to null if not needed.
        /// </summary>
        public string TitleLink { get; set; }

        /// <summary>
        /// Gets or sets the news block list.
        /// Set to null if not needed.
        /// </summary>
        public IEnumerable<StackedPanelItemModel> NewsBlockList { get; set; }

        /// <summary>
        /// Gets or sets the news item list.
        /// Set to null if not needed.
        /// </summary>
        public IEnumerable<StackedPanelItemModel> NewsItemList { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is show all enabled.
        /// </summary>
        public bool IsShowAllEnabled { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is show read more enabled.
        /// </summary>
        public bool IsShowReadMoreEnabled { get; set; }

        /// <summary>
        /// Gets or sets the intro height.
        /// </summary>
        public int IntroHeight { get; set; }
    }

    /// <summary>
    /// The stacked panel item model.
    /// </summary>
    public class StackedPanelItemModel
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the intro.
        /// </summary>
        public string Intro { get; set; }

        /// <summary>
        /// Gets or sets the date format.
        /// </summary>
        public string DateFormat { get; set; }
    }

    /// <summary>
    /// The small hero model.
    /// </summary>
    public class SmallHeroModel
    {
        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the sub title.
        /// </summary>
        public string SubTitle { get; set; }

        /// <summary>
        /// Gets or sets the date.
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Gets or sets the date format.
        /// </summary>
        public string DateFormat { get; set; }

        /// <summary>
        /// Gets or sets the content.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Gets or sets the link.
        /// </summary>
        public string Link { get; set; }

        /// <summary>
        /// Gets or sets the hero image.
        /// </summary>
        public Image HeroImage { get; set; }
    }
}