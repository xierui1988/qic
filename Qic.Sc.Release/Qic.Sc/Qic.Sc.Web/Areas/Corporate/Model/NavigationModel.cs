﻿using Qic.Sc.Entities.UserDefined.Components.Site.Configuration.MenuItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Qic.Sc.Web.Areas.Corporate.Model
{
    /// <summary>
    /// Navigation Model that holds Menu and context site
    /// </summary>
    public class NavigationModel
    {
        /// <summary>
        /// The context site name it should point to
        /// </summary>
        public string ContextSite { get; set; }

        /// <summary>
        /// The Menus
        /// </summary>
        public IEnumerable<IMenuItem> Menu { get; set; }
    }

    /// <summary>
    /// Top Navigation Model
    /// </summary>
    public class TopNavModel : NavigationModel
    {
        /// <summary>
        /// Portal user's First Name
        /// </summary>
        public string UserFirstName { get; set; }
    }
}