﻿namespace Qic.Sc.Web.Areas.Corporate.Model
{
    /// <summary>
    /// Attachment Model
    /// </summary>
    public class AttachmentModel
    {
        /// <summary>
        /// Gets or sets the URL.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }
    }
}