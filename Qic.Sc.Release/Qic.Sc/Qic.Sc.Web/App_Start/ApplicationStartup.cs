﻿using Qic.Sc.Web;

using WebActivatorEx;

[assembly: PreApplicationStartMethod(typeof(ApplicationStartup), "PreStartup")]
[assembly: PostApplicationStartMethod(typeof(ApplicationStartup), "PostStartup")]

namespace Qic.Sc.Web
{
    using System.Diagnostics.CodeAnalysis;
    using System.Net.Http.Formatting;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;

    using Links;

    /// <summary>
    /// Global application start up.
    /// </summary>
    public class ApplicationStartup
    {
        /// <summary>
        /// Called during application startup. Before the Application_Start method
        /// of Global.aspx
        /// </summary>
        public static void PreStartup()
        {
        }

        /// <summary>
        /// Called during application startup. After the Application_Start method
        /// of Global.aspx.
        /// </summary>
        public static void PostStartup()
        {
            // Register our MVC application
            AreaRegistration.RegisterAllAreas();

            // Register WEB API
            GlobalConfiguration.Configure(RegisterApi);

            // remove MVC HTTP headers
            MvcHandler.DisableMvcResponseHeader = true;

            // Register Optimization bundles - JS bundling / minification
            RegisterBundles(BundleTable.Bundles);
        }

        /// <summary>
        /// Web API initialization.
        /// </summary>
        private static void RegisterApi(HttpConfiguration http)
        {
            GlobalConfiguration.Configuration.MapHttpAttributeRoutes();
            GlobalConfiguration.Configuration.Formatters.Clear();
            GlobalConfiguration.Configuration.Formatters.Add(new JsonMediaTypeFormatter());
        }

        /// <summary>
        /// Register resource bundles.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.ReadabilityRules", "SA1123:DoNotPlaceRegionsWithinElements", Justification = "Reviewed. Suppression is OK here.")]
        private static void RegisterBundles(BundleCollection bundles)
        {
            #region www.qic.com

            bundles.Add(
                new ScriptBundle(Bundles.Scripts.Corporate).IncludeT4MVC(
                    Content.common.jquery_dotdotdot_js,
                    Content.corporate.js.jquery_cycle2_min_js,
                    Content.corporate.js.jquery_cycle2_swipe_min_js,
                    Content.corporate.js.main_js,
                    Content.corporate.js.DOTCOM_js));

            bundles.Add(
                new StyleBundle(Bundles.Styles.Corporate).IncludeT4MVC(
                    Content.corporate.css.bootstrap_css,
                    Content.corporate.css.app_css,
                    Content.corporate.css.custom_css,
                    Content.corporate.css.ellipses_css
                ));

            #endregion

            #region portal.qic.com

            bundles.Add(
                new ScriptBundle(Bundles.Scripts.Portal).IncludeT4MVC(              
                    Content.portal.js.bootstrap_datepicker_min_js,
                    Content.portal.js.main_js,
                    Content.portal.js.portal_ui_js));

            bundles.Add(
                new StyleBundle(Bundles.Styles.Portal).IncludeT4MVC(
                    Content.portal.css.bootstrap_datepicker3_min_css,
                    Content.portal.css.bootstrap_css,
                    Content.portal.css.app_css
                ));

            #endregion
        }
    }
}