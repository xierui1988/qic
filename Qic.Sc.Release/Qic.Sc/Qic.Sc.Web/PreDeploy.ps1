Write-Host "Stopping IIS."
iisreset.exe /stop

$InstallFolder = $OctopusParameters["Octopus.Action.Package.CustomInstallationDirectory"]

Write-Host "Cleaning temporary folders, before the backup ..."
Remove-Item -Recurse -Force "$($InstallFolder)temp\*"
Remove-Item -Recurse -Force "$($InstallFolder)App_Data\MediaCache\*"

$ZipWeb = "$($InstallFolder.Trim('\\'))-$(Get-Date -format "yyyyMMdd-HHmm").zip"
Write-Host "Creating a backup of the website: $ZipWeb ..."

Add-Type -Assembly "System.IO.Compression.FileSystem" ;
[System.IO.Compression.ZipFile]::CreateFromDirectory($InstallFolder, $ZipWeb)
