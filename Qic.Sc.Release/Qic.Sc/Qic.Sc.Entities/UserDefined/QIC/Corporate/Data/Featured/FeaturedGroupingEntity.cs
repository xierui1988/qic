﻿using System.Collections.Generic;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Featured
{
    using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.GlobalFeature;

    /// <summary>
    /// The report grouping.
    /// </summary>
    public class FeaturedGroupingEntity
    {
        /// <summary>
        /// Gets the sub items for a menu item.
        /// </summary>
        [SitecoreChildren(IsLazy = true)]
        public IEnumerable<IGlobalFeature> FeatureGroups { get; set; }
    }
}
