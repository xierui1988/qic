﻿using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Property;

namespace Qic.Sc.Entities.UserDefined.QIC.Corporate.Pages.GREListingPage
{
    using Glass.Mapper.Sc.Configuration;
    using Glass.Mapper.Sc.Configuration.Attributes;

    using global::System.Collections.Generic;

    using Qic.Sc.Entities.UserDefined.QIC.Corporate.Data.Features;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Editorials.Editorial;
    using Qic.Sc.Entities.UserDefined.QIC.Shared.Feature;
    public partial class GREListingPageEntity
    {
        /// <summary>
        /// Gets or sets the feature entities.
        /// </summary>
        [SitecoreField(IFeaturesConstants.FeaturesFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        public IEnumerable<IFeature> FeatureEntities { get; set; }

        /// <summary>
        /// Feature Entities
        /// </summary>
        [SitecoreField(IFeaturesConstants.FeaturesFieldName, Setting = SitecoreFieldSettings.RichTextRaw)]
        public virtual IEnumerable<IEditorial> EditorialEntities { get; set; }

        /// <summary>
        /// Gets or sets the sub items for a menu item.
        /// </summary>
        [SitecoreChildren(IsLazy = true)]
        public virtual IEnumerable<IProperty> Properties { get; set; }
    }
}
