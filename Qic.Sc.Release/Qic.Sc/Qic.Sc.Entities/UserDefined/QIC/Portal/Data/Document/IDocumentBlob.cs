﻿namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document
{
    using Glass.Mapper.Sc.Configuration;
    using Glass.Mapper.Sc.Configuration.Attributes;

    using global::System;
    using global::System.IO;

    using Qic.Sc.Entities.System.Media.Versioned.File;

    /// <summary>
    /// The document with file interface.
    /// </summary>
    public interface IDocumentBlob : IDocument
    {
        /// <summary>
        /// Gets or sets the blob.
        /// </summary>
        [SitecoreField("Blob")]
        Stream Blob { get; set; }
    }
}
