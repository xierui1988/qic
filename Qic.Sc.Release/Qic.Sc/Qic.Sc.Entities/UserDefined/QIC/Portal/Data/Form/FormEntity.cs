﻿using System;
using System.IO;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form
{
    public partial class FormEntity
    {
        /// <summary>
        /// Gets or sets a value indicating whether is favorite.
        /// </summary>
        public bool IsFavorite { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is an unread recent document.
        /// </summary>
        public bool IsUnreadRecent { get; set; }

        /// <summary>
        /// Gets or sets the published date
        /// </summary>
        [SitecoreField("Published Date")]
        public DateTime? PublishedDate { get; set; }
    }
}
