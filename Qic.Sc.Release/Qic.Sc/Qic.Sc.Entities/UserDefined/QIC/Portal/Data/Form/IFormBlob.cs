﻿using System.IO;
using Glass.Mapper.Sc.Configuration.Attributes;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form
{
    public partial interface IFormBlob : IForm
    {
        /// <summary>
        /// Gets or sets the blob.
        /// </summary>
        [SitecoreField("Blob")]
        Stream Blob { get; set; }
    }
}
