﻿using System.Web;
using System.Linq;

namespace Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form
{
    using global::System;
    using global::System.Collections.Generic;

    /// <summary>
    /// The document extensions.
    /// </summary>
    public static class FormExtensions
    {
        /// <summary>
        /// This is an extension method that generates the link for client portal
        /// </summary>
        /// <param name="document">The document that it needs to generate documents for.</param>
        /// <returns>The client portal download link</returns>
        public static string AplicationRelativeUrl(this IForm document)
        {
            return string.Format(
                "/portal/report/downloadform/{0}/{1}",
                document.EntityId.ToString("N"),
                HttpUtility.UrlPathEncode(string.Format("{0}.{1}", Uri.EscapeDataString(document.GetDisplayNameOrName()), document.Extension)));
        }
    }
}
