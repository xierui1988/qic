﻿using System;
using System.Collections.Generic;

namespace Qic.Sc.Entities.UserDefined.Components.Site.Configuration.MenuItem
{
    using Glass.Mapper.Sc.Configuration.Attributes;

    public partial class MenuItemEntity
    {
        /// <summary>
        /// Gets or sets the sub items for a menu item.
        /// </summary>
        [SitecoreChildren(IsLazy = true)]
        public virtual IEnumerable<IMenuItem> SubItems { get; set; }

        /// <summary>
        /// Gets whether a menu item is active.
        /// </summary>
        public virtual bool IsActive
        {
            get
            {
                throw new NotImplementedException("MenuItemEntity is not initialized with the IsActive property.");
            }
        }

        /// <summary>
        /// Get linked entity.
        /// </summary>
        /// <typeparam name="TEntity">Type of the linked entity.</typeparam>
        /// <returns>Get a linked entity if one can be resolved or null.</returns>
        public TEntity GetLinkedEntity<TEntity>() where TEntity : class
        {
            throw new NotImplementedException();
        }
    }
}
