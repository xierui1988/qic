﻿namespace Qic.Sc.Entities.Metadata
{
    using global::System;
    using global::System.Linq;
    using global::System.Collections.Generic;
    using global::System.Runtime.Serialization;

    using Newtonsoft.Json;

    /// <summary>
    /// User Data
    /// </summary>
    public class UserMetadata
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserMetadata"/> class.
        /// </summary>
        public UserMetadata()
        {
            this.Favourites = new UserMetadataFavourites();
            this.ReadDocuments = new UserMetadataReadDocuments();
        }

        /// <summary>
        /// Gets or sets the favourites.
        /// </summary>
        public UserMetadataFavourites Favourites { get; set; }

        /// <summary>
        /// Gets or sets the read documents.
        /// </summary>
        public UserMetadataReadDocuments ReadDocuments { get; set; }
    }

    /// <summary>
    /// The user meta data favourites.
    /// </summary>
    public class UserMetadataFavourites
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserMetadata"/> class.
        /// </summary>
        public UserMetadataFavourites()
        {
            this.Items = new List<Guid>();
        }

        /// <summary>
        /// The user's saved Favorites.
        /// </summary>
        public List<Guid> Items { get; set; }
    }

    /// <summary>
    /// The user meta data read documents.
    /// </summary>
    public class UserMetadataReadDocuments
    {
        private int maxAgeInDaysBeforeNoLongerRecent;

        /// <summary>
        /// The date from which documents should not longer be new,
        /// whether read or not.
        /// </summary>
        public DateTime CutOffDate
        {
            get
            {
                return DateTime.UtcNow.AddDays(-(this.maxAgeInDaysBeforeNoLongerRecent));
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UserMetadata"/> class.
        /// </summary>
        public UserMetadataReadDocuments()
        {
            this.Items = new List<UserMetadataDocument>();
        }

        /// <summary>
        /// The document's read by the user.
        /// </summary>
        public List<UserMetadataDocument> Items { get; set; }

        /// <summary>
        /// Determine whether a document is new based on previously read documents and published date.
        /// </summary>
        /// <param name="documentId">The document id.</param>
        /// <param name="publishedDate">The published date.</param>
        /// <returns>True of the document is new.</returns>
        public bool IsUnreadRecentDocument(Guid documentId, DateTime publishedDate)
        {
            if (publishedDate < this.CutOffDate)
            {
                return false;
            }

            return !this.Items.Any(readDoc => readDoc.Id == documentId);
        }

        /// <summary>
        /// Flags a document as no longer new (aka read).
        /// </summary>
        /// <param name="documentId">Document id.</param>
        /// <param name="publishedDate">The published date.</param>
        /// <returns>True if the meta data has been changed and should be saved.</returns>
        public bool MarkRecentDocumentAsRead(Guid documentId, DateTime publishedDate)
        {
            if (publishedDate < this.CutOffDate || this.Items.Any(readDoc => readDoc.Id == documentId))
            {
                // The publish date is older then cut off date
                // or the document is already read.
                return false;
            }

            this.Items.Add(new UserMetadataDocument { Id = documentId, PublishedDate = publishedDate });
            return true;
        }

        /// <summary>
        /// The set recent document max age.
        /// </summary>
        /// <param name="age">
        /// The age.
        /// </param>
        public void SetRecentMaxAge(int age)
        {
            this.maxAgeInDaysBeforeNoLongerRecent = age;
        }

        /// <summary>
        /// Ensure we have initialized collections in the event that null is saved
        /// into profile.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnDeserialized]
        public void OnDeserialized(StreamingContext context)
        {
            if (this.Items == null)
            {
                this.Items = new List<UserMetadataDocument>();
            }
        }

        /// <summary>
        /// Trim data which isn't necessary.
        /// </summary>
        /// <param name="context">The streaming context.</param>
        [OnSerializing]
        public void OnSerializing(StreamingContext context)
        {
            this.Items = this.Items.Where(d => d.PublishedDate >= this.CutOffDate).ToList();
        }
    }

    /// <summary>
    /// Represents a document which a user has read. 
    /// </summary>
    public class UserMetadataDocument
    {
        /// <summary>
        /// The document id.
        /// </summary>
        [JsonProperty("I")]
        public Guid Id { get; set; }

        /// <summary>
        /// The published date.
        /// </summary>
        [JsonProperty("D")]
        public DateTime PublishedDate { get; set; }
    }
}