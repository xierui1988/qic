﻿namespace Qic.Sc.Web.Tests
{
    using System.Web;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    using Moq;

    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// Base functionality for Sitecore web tests.
    /// </summary>
    public abstract class TestBase
    {
        protected Mock<IContentRepository> MockContentRepository { get; set; }
        protected Mock<ILoggerService> MockLogger { get; set; }
        protected Mock<HttpContextBase> MockHttpContext { get; set; }
        protected Mock<HttpRequestBase> MockRequest { get; set; }
        protected Mock<IContentSearchService> MockContentSearchService { get; set; }

        private static bool isInitialised;

        public static void InitialiseOnce()
        {
            if (isInitialised)
            {
                return;
            }

            isInitialised = true;

            // Initialize routing tables
            // RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        [TestInitialize]
        public virtual void Setup()
        {
            InitialiseOnce();

            this.MockContentRepository = new Mock<IContentRepository>();
            this.MockLogger = new Mock<ILoggerService>();
            this.MockRequest = new Mock<HttpRequestBase>();
            this.MockHttpContext = new Mock<HttpContextBase>();
            this.MockContentSearchService = new Mock<IContentSearchService>();
            this.MockHttpContext.Setup(c => c.Request).Returns(this.MockRequest.Object);
        }
    }
}
