﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xrm.Sdk;
using Qic.Sc.Entities;
using Qic.Sc.Interfaces.Crm;

namespace Qic.Sc.Services.Crm
{
    using Microsoft.Xrm.Sdk.Messages;
    using Microsoft.Xrm.Sdk.Metadata;

    using Qic.Sc.Entities.Metadata;

    /// <summary>
    /// The crm service.
    /// </summary>
    public class CrmService : ICrmService
    {
        private readonly IOrganizationService organisationService;

        /// <summary>
        /// Initializes a new instance of the <see cref="CrmService"/> class.
        /// </summary>
        /// <param name="organisationService">
        /// The organisation service.
        /// </param>
        public CrmService(IOrganizationService organisationService)
        {
            if (organisationService == null)
            {
                throw new ArgumentNullException("organisationService");
            }
            this.organisationService = organisationService;
        }

        /// <summary>
        /// Get Report Data from CRM
        /// </summary>
        /// <param name="entityCodeType">
        /// File type. Either Account, Product or Client
        /// </param>
        /// <param name="entityCode">
        /// Code for the particular file type
        /// </param>
        /// <param name="reportCode">
        /// Code of the report
        /// </param>
        /// <returns>
        /// Report Data
        /// </returns>
        public IEnumerable<ReportMetadata> GetReportMetadata(EntityCodeType entityCodeType, string entityCode, string reportCode)
        {
            if (entityCode == null)
            {
                throw new ArgumentNullException("entityCode");
            }

            if (reportCode == null)
            {
                throw new ArgumentNullException("reportCode");
            }

            switch (entityCodeType)
            {
                case EntityCodeType.AccountCode:
                    return this.GetAccountReportMetadata(entityCode, reportCode);
                case EntityCodeType.HoldingAccountCode:
                    return this.GetHoldingAccountReportMetadata(entityCode, reportCode);
                case EntityCodeType.ProductCode:
                    return this.GetProductReportMetadata(entityCode, reportCode);
                default:
                    throw new ApplicationException("Unknown entity code type.");
            }
        }

        /// <summary>
        /// Get Client Data
        /// </summary>
        /// <param name="holdingAccountCode"> Client Code</param>
        /// <param name="reportCode">Report code</param>
        /// <returns>Report Metadata</returns>
        private IEnumerable<ReportMetadata> GetHoldingAccountReportMetadata(string holdingAccountCode, string reportCode)
        {
            using (var crmContext = new CRMServiceContext(this.organisationService))
            {
                // Determine whether holding account code and report code combination
                // exists and retrieve user friendly names for report and holding account.
                var result = (from access in crmContext.qic_holdingaccountreportaccessSet
                              where access.qic_CodeHoldingAccount == holdingAccountCode && access.qic_ReportCode == reportCode && access.statecode == qic_holdingaccountreportaccessState.Active
                              select new ReportMetadata
                              {
                                  AccountCode = access.qic_AccountCode,
                                  AccountName = access.qic_Account.GetEntityNameOrDefault(),
                                  HoldingAccountCode = access.qic_CodeHoldingAccount,
                                  HoldingAccountName = access.qic_NameHoldingAccount,
                                  ReportName = access.qic_ReportName.GetEntityNameOrDefault(),
                                  ReportCode = access.qic_ReportCode
                              }).ToArray();

                if (!result.Any())
                {
                    throw new ApplicationException(string.Format("Either EntityCode({0}) is not in CRM Or ReportCode({1}) is not in CRM Or ReportCode({1}) is not mapped to EntityCode({0})", holdingAccountCode, reportCode));
                }

                if (result.Count() > 1)
                {
                    throw new ApplicationException(string.Format("More than one record in CRM for Entity Code({0}).", holdingAccountCode));
                }
                return result;
            }
        }

        /// <summary>
        /// Get Account data
        /// </summary>
        /// <param name="accountCode">Account Code</param>
        /// <param name="reportCode">Report Code</param>
        /// <returns>Report Metadata</returns>
        private IEnumerable<ReportMetadata> GetAccountReportMetadata(string accountCode, string reportCode)
        {
            using (var crmContext = new CRMServiceContext(this.organisationService))
            {
                var result = (from access in crmContext.qic_accountreportaccessSet
                              where access.qic_CodeAccount == accountCode && access.qic_ReportCode == reportCode && access.statecode == qic_accountreportaccessState.Active
                              select new ReportMetadata
                              {
                                  AccountName = access.qic_NameAccount,
                                  AccountCode = access.qic_CodeAccount,
                                  ReportName = access.qic_ReportName.Name,
                                  ReportCode = access.qic_ReportCode
                              }).ToArray();

                if (!result.Any())
                {
                    throw new ApplicationException(string.Format("Either EntityCode({0}) is not in CRM Or ReportCode({1}) is not in CRM Or ReportCode({1}) is not mapped to EntityCode({0})", accountCode, reportCode));
                }

                if (result.Count() > 1)
                {
                    throw new ApplicationException(string.Format("More than one record in CRM for Entity Code({0}).", accountCode));
                }

                return result;
            }
        }

        /// <summary>
        /// Get Product Data
        /// </summary>
        /// <param name="productCode">Product Code</param>
        /// <param name="reportCode">Report Code</param>
        /// <returns>Report Metadata</returns>
        private IEnumerable<ReportMetadata> GetProductReportMetadata(string productCode, string reportCode)
        {
            using (var crmContext = new CRMServiceContext(this.organisationService))
            {
                var result = (from access in crmContext.qic_productreportaccessSet
                              where access.qic_ProductCode == productCode && access.qic_ReportCode == reportCode && access.statecode == qic_productreportaccessState.Active
                              select new ReportMetadata
                              {
                                  AccountName = access.qic_AccountId.Name,
                                  AccountCode = access.qic_AccountCode,
                                  ProductName = access.qic_ProductId.Name,
                                  ProductCode = access.qic_ProductCode,
                                  ReportName = access.qic_ReportNameId.Name,
                                  ReportCode = access.qic_ReportCode
                              }).ToArray();

                if (!result.Any())
                {
                    throw new ApplicationException(string.Format("Either EntityCode({0}) is not in CRM Or ReportCode({1}) is not in CRM Or ReportCode({1}) is not mapped to EntityCode({0})", productCode, reportCode));
                }

                return result;
            }
        }

        /// <summary>
        /// Gets the User's report metadata
        /// </summary>
        /// <param name="userName">
        ///     User name of the client/user (e.g. email address).
        /// </param>
        public IEnumerable<ReportAccessMetadata> GetUserReportAccess(string userName)
        {
            using (var crmContext = new CRMServiceContext(this.organisationService))
            {
                // Get the Reports that the user wants to receive
                // 1) Get the report that are mapped to that user
                // 2) checks that contacts, reportAccess and *portal Access* are all Active
                return (from reportAccess in crmContext.qic_reportaccessSet
                        join contact in crmContext.ContactSet on reportAccess.qic_contact_qic_reportaccess.Id equals
                            contact.Id
                        join portalAccess in crmContext.qic_portalaccessSet on reportAccess.qic_PorId.Id equals
                            portalAccess.Id
                        where
                            contact.EMailAddress1 == userName && reportAccess.statecode == qic_reportaccessState.Active
                            && contact.StateCode == ContactState.Active
                            && portalAccess.statecode == qic_portalaccessState.Active
                        select
                            new ReportAccessMetadata
                                {
                                    ID = reportAccess.Id,
                                    AccountName = reportAccess.qic_AccountId.GetEntityNameOrDefault(),
                                    AccountCode = reportAccess.qic_AccountCode,
                                    HoldingAccountName = reportAccess.qic_HoldingAccount.GetEntityNameOrDefault(),
                                    HoldingAccountCode = reportAccess.qic_HoldingAccountCode,
                                    ProductName = reportAccess.qic_Product.GetEntityNameOrDefault(),
                                    ProductCode = reportAccess.qic_ProductCode,
                                    ReportName = reportAccess.qic_ReportName,
                                    ReportCode = reportAccess.qic_ReportCode,
                                    ContactOwner = contact.OwnerId.GetEntityNameOrDefault(),
                                    EmailNotification = reportAccess.qic_EmailNotification.GetValueOrDefault(),
                                    EmailAddress = userName,
                                    FullName = contact.FullName,
                                    FirstName = contact.FirstName,
                                    ParentAccount = contact.ParentCustomerId.GetEntityNameOrDefault()
                                })
                    .ToArray();
            }
        }

        /// <summary>
        /// The get super user report access.
        /// </summary>
        /// <returns>
        /// Report access to all reports in the system.
        /// </returns>
        public IEnumerable<ReportAccessMetadata> GetSuperUserReportAccess()
        {
            using (var crm = new CRMServiceContext(this.organisationService))
            {
                // Super user has access to all configured reports in the system.
                return (from reportAccess in crm.qic_reportaccessSet
                        join portalAccess in crm.qic_portalaccessSet on reportAccess.qic_PorId.Id equals portalAccess.Id
                        join contact in crm.ContactSet on reportAccess.qic_ContactId.Id equals contact.Id
                        where
                            reportAccess.statecode == qic_reportaccessState.Active
                            && portalAccess.statecode == qic_portalaccessState.Active
                        select
                            new ReportAccessMetadata
                                {
                                    ID = reportAccess.Id,
                                    AccountName = reportAccess.qic_AccountId.GetEntityNameOrDefault(),
                                    AccountCode = reportAccess.qic_AccountCode,
                                    HoldingAccountName = reportAccess.qic_HoldingAccount.GetEntityNameOrDefault(),
                                    HoldingAccountCode = reportAccess.qic_HoldingAccountCode,
                                    ProductName = reportAccess.qic_Product.GetEntityNameOrDefault(),
                                    ProductCode = reportAccess.qic_ProductCode,
                                    ReportName = reportAccess.qic_ReportName,
                                    ReportCode = reportAccess.qic_ReportCode,
                                    ContactOwner = contact.OwnerId.GetEntityNameOrDefault(),
                                    EmailAddress = contact.EMailAddress1,
                                    FirstName = contact.FirstName,
                                    FullName = contact.FullName,
                                    EmailNotification = reportAccess.qic_EmailNotification.GetValueOrDefault(),
                                    ParentAccount = contact.ParentCustomerId.GetEntityNameOrDefault()
                                }).ToArray();
            }
        }

        /// <summary>
        /// Gets the list of users that are subscribed to receive access of these reports
        /// </summary>
        /// <param name="entityCode">The code of the entity its associated to</param>
        /// <param name="reportCode">The code of the report</param>
        /// <param name="ignoreNotificationPreference">
        ///     Get report access ignoring email notification flag, otherwise only access recored that
        ///     have notification flag set to 'true' will be returned.
        /// </param>
        /// <returns>Returns a list of User's Metadata</returns>
        public IEnumerable<ReportAccessMetadata> GetReportAccessForEmailBy(string entityCode, string reportCode, bool ignoreNotificationPreference = false)
        {
            using (var crmContext = new CRMServiceContext(this.organisationService))
            {
                // Queries all reports in Access Table that meet:
                // 1) Report Code - for the reprt
                // 2) Entity Code that is associated with (Account or Holding Account or Product)
                // 3) (Optional) a filtered list of user that want an email notification
                // 4) checks that contacts, reportAccess and *portal Access* are all Active
                return (from reportAccess in crmContext.qic_reportaccessSet
                        join contact in crmContext.ContactSet on reportAccess.qic_ContactId.Id equals contact.Id
                        join portalAccess in crmContext.qic_portalaccessSet on reportAccess.qic_PorId.Id equals
                            portalAccess.Id
                        where
                            reportAccess.qic_ReportCode == reportCode
                            && ((reportAccess.qic_AccountCode == entityCode && reportAccess.qic_ProductCode == null
                                 && reportAccess.qic_HoldingAccountCode == null)
                                || reportAccess.qic_HoldingAccountCode == entityCode
                                || reportAccess.qic_ProductCode == entityCode)
                            && (reportAccess.qic_EmailNotification == true
                                || reportAccess.qic_EmailNotification != ignoreNotificationPreference)
                            && portalAccess.statecode == qic_portalaccessState.Active
                            && contact.StateCode == ContactState.Active
                            && reportAccess.statecode == qic_reportaccessState.Active
                        select
                            new ReportAccessMetadata
                                {
                                    ID = reportAccess.Id,
                                    AccountName = reportAccess.qic_AccountId.GetEntityNameOrDefault(),
                                    AccountCode = reportAccess.qic_AccountCode,
                                    HoldingAccountName =
                                        reportAccess.qic_HoldingAccount.GetEntityNameOrDefault(),
                                    HoldingAccountCode = reportAccess.qic_HoldingAccountCode,
                                    ProductName = reportAccess.qic_Product.GetEntityNameOrDefault(),
                                    ProductCode = reportAccess.qic_ProductCode,
                                    ReportName = reportAccess.qic_ReportName,
                                    ReportCode = reportAccess.qic_ReportCode,
                                    ContactOwner = contact.OwnerId.GetEntityNameOrDefault(),
                                    EmailAddress = contact.EMailAddress1,
                                    FirstName = contact.FirstName,
                                    FullName = contact.FullName,
                                    EmailNotification = reportAccess.qic_EmailNotification.GetValueOrDefault(),
                                    ParentAccount = contact.ParentCustomerId.GetEntityNameOrDefault()
                                })
                    .ToArray();
            }
        }

        /// <summary>
        /// Set the new Update Report Access
        /// </summary>
        /// <param name="username">The user name or email</param>
        /// <param name="reportAccessChanges">The report Access entities</param>
        public void UpdateUserReportAccessNotifications(string username, IEnumerable<ReportAccessMetadata> reportAccessChanges)
        {
            foreach (var accessChange in reportAccessChanges)
            {
                // Creating a reference to the qic report access record
                var reportAccessChange = new qic_reportaccess { Id = accessChange.ID, qic_EmailNotification = accessChange.EmailNotification };
                this.organisationService.Update(reportAccessChange);
            }
        }

        /// <summary>
        /// The get report types.
        /// </summary>
        public IEnumerable<ReportTypeMetadata> GetReportTypes()
        {
            using (var crm = new CRMServiceContext(this.organisationService))
            {
                var counter = 0;
                var request = new RetrieveOptionSetRequest { Name = "qic_reporttype" };
                var response = (RetrieveOptionSetResponse)crm.Execute(request);
                var optionset = (OptionSetMetadata)response.OptionSetMetadata;
               
                var options = new Dictionary<int, KeyValuePair<int, OptionMetadata>>();

                foreach (var option in optionset.Options)
                {
                    options.Add(
                        option.Value.GetValueOrDefault(),
                        new KeyValuePair<int, OptionMetadata>(counter++, option));
                }

                // Report type/group is a property of the report name
                // load all report names.
                var query = from report in crm.qic_reportnameSet
                            where report.statecode == qic_reportnameState.Active
                            select
                                new ReportTypeMetadata
                                    {
                                        ReportCode = report.qic_Code,
                                        ReportTypeId = report.qic_reporttype.Value,
                                        ReportTypeName = options[report.qic_reporttype.Value].Value.Label.UserLocalizedLabel.Label,
                                        CrmIndexOrder = options[report.qic_reporttype.Value].Key
                                    };

                // Augment report names with the human readable report type title.
                return query.ToArray();
            }
        }
    }
}
