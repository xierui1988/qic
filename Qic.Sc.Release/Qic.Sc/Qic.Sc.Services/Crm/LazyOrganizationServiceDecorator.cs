﻿namespace Qic.Sc.Services.Crm
{
    using System;
    using System.Collections.Specialized;

    using Microsoft.Xrm.Client;
    using Microsoft.Xrm.Client.Configuration;
    using Microsoft.Xrm.Client.Services;
    using Microsoft.Xrm.Sdk;
    using Microsoft.Xrm.Sdk.Query;

    internal partial class LazyOrganizationServiceDecorator : IDisposable, IOrganizationService, IInitializable
    {
        private readonly CrmConnection conection;
        private OrganizationService lazy;

        public LazyOrganizationServiceDecorator(CrmConnection conection)
        {
            this.conection = conection;
        }

        public void Dispose()
        {
            // In some scenarios when CRM connection can't be made.
            // Underlying implementation tries to establish connection during dispose.
            // This causes exception at clean up, suppress the error.
            if (this.lazy != null)
            {
                try
                {
                    var intance = this.lazy;
                    this.lazy = null;

                    intance.Dispose();
                }
                catch
                {
                }
            }
        }

        public Guid Create(Entity entity)
        {
            return this.EnsureInstance().Create(entity);
        }

        public Entity Retrieve(string entityName, Guid id, ColumnSet columnSet)
        {
            return this.EnsureInstance().Retrieve(entityName, id, columnSet);
        }

        public void Update(Entity entity)
        {
            this.EnsureInstance().Update(entity);
        }

        public void Delete(string entityName, Guid id)
        {
            this.EnsureInstance().Delete(entityName, id);
        }

        public OrganizationResponse Execute(OrganizationRequest request)
        {
            return this.EnsureInstance().Execute(request);
        }

        public void Associate(string entityName, Guid entityId, Relationship relationship, EntityReferenceCollection relatedEntities)
        {
            this.EnsureInstance().Associate(entityName, entityId, relationship, relatedEntities);
        }

        public void Disassociate(
            string entityName,
            Guid entityId,
            Relationship relationship,
            EntityReferenceCollection relatedEntities)
        {
            this.EnsureInstance().Disassociate(entityName, entityId, relationship, relatedEntities);
        }

        public EntityCollection RetrieveMultiple(QueryBase query)
        {
            return this.EnsureInstance().RetrieveMultiple(query);
        }

        public void Initialize(string name, NameValueCollection config)
        {
            this.EnsureInstance().Initialize(name, config);
        }

        private OrganizationService EnsureInstance()
        {
            return this.lazy ?? (this.lazy = new OrganizationService(this.conection));
        }
    }
}
