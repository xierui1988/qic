// <auto-generated />
#pragma warning disable 1591
#region T4Decorator

using System;
using System.Diagnostics;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace Qic.Sc.Services.Components
{
    [GeneratedCode("T4Decorator", "1.0"), DebuggerNonUserCode]
    internal partial class ReportServiceWithUserMetadataDecorator
    {
        public void CreateReport(Qic.Sc.Entities.Metadata.ReportNameMetadata reportObj)
        {
            this.reportService.CreateReport(reportObj);
        }

    }
}
#endregion T4Decorator
#pragma warning restore 1591
