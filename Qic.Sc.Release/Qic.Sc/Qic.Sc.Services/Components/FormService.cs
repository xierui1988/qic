﻿using System;
using System.Collections.Generic;
using System.Linq;

using Qic.Sc.Entities;
using Qic.Sc.Entities.Metadata;
using Qic.Sc.Entities.UserDefined.Components.Site.Configuration;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Form;
using Qic.Sc.Interfaces.Components;
using Qic.Sc.Interfaces.Core;
using Qic.Sc.Services.Core;
using Sitecore.Data.Items;

namespace Qic.Sc.Services.Components
{
    using Sitecore.SecurityModel;

    /// <summary>
    /// Form Service
    /// </summary>
    public class FormService : IFormService
    {
        private readonly IContentRepository contentRepository;
        private readonly IContentRepository masterContentRepository;
        private readonly IWebSecurityService securityService;
        private readonly IPortalWebSecurityService portalWebSecurityService;
        private readonly IAuditService auditService;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public FormService(IContentRepository contentRepository, IMasterContentRepository masterContentRepository, IWebSecurityService securityService, IPortalWebSecurityService portalWebSecurityService, IAuditService auditService)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            if (securityService == null)
            {
                throw new ArgumentNullException("securityService");
            }
            if (portalWebSecurityService == null)
            {
                throw new ArgumentNullException("securityService");
            }
            if (masterContentRepository as IContentRepository == null)
            {
                throw new ArgumentNullException("masterContentRepository");
            }
            if (auditService == null)
            {
                throw new ArgumentNullException("auditService");
            }

            this.contentRepository = contentRepository;
            this.securityService = securityService;
            this.portalWebSecurityService = portalWebSecurityService;
            this.masterContentRepository = masterContentRepository as IContentRepository;
            this.auditService = auditService;
        }

        /// <summary>
        /// Creates a form in the content repository using the meta data and the stream provided.
        /// </summary>
        /// <param name="formObj">FormNameMetadata Object.</param>
        public void CreateForm(FormMetadata formObj)
        {
            var fileType = formObj.AccountCode.StartsWith(EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.HoldingAccountCode))
                ? EntityCodeType.HoldingAccountCode : (formObj.AccountCode.StartsWith(EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.ProductCode)) || formObj.AccountCode.StartsWith(EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.ProductCode)))
                ? EntityCodeType.ProductCode : EntityCodeType.AccountCode;

            if (fileType.Equals(EntityCodeType.None))
            {
                throw new ArgumentException(string.Format("Entity Code: {0} is invalid", formObj.AccountCode));
            }

            // no need to pull out metadata from CRM for form?
            //var reportMetadata = this.crmOnlineService.GetReportMetadata(fileType, formObj.AccountCode, formObj.AccountCode).ToArray();
            var document = new FormBlobEntity();
            document.AccountName = formObj.AccountName;
            document.AccountCode = formObj.AccountCode;
            document.EntityName = ItemUtil.ProposeValidItemName(GenerateFormName(formObj.AccountName, formObj.AccountCode, formObj.FormPublishedDate));
            document.EntityDisplayName = formObj.FileName;
            document.Blob = formObj.FileStream;
            document.PublishedDate = formObj.FormPublishedDate;
            document.Extension = formObj.FileExtension;
            document.MimeType = formObj.MimeType;

            // Populate the workflow Values
            document.Workflow = Constants.System.Workflows.QICPortalPublishingWorkflow.ItemID.Guid;
            document.WorkflowState = Constants.System.Workflows.QICPortalPublishingWorkflow.New.ItemID.ToString();
            document.DefaultWorkflow = Constants.System.Workflows.QICPortalPublishingWorkflow.ItemID.Guid;

            try
            {
                using (new SecurityDisabler())
                {
                    // NOTE: The only place to create documents is MASTER data base.
                    // NOTE: Make sure we calling the correct repository.
                    var parent =
                        this.masterContentRepository.GetEntity<IConfiguration>(
                            Constants.Content.ClientPortal.Documents.ID);

                    var uploadedDocument = this.masterContentRepository.CreateEntity(
                        parent,
                        document,
                        disableSecurity: true);

                    this.auditService.Audit(
                        new AuditMetadata
                            {
                                UserName = this.securityService.GetCurrentAuthenticatedUsername(),
                                Action =
                                    EnumUtils<SitecoreUserAction>.GetDescription(
                                        SitecoreUserAction.DocumentUpload),
                                Db = Constants.MasterDb,
                                Path =
                                    string.Format(
                                        "{0}{1}",
                                        Constants.ContentPath,
                                        uploadedDocument.EntityContentPath),
                                Id = string.Format(Constants.ItemIDFormat, uploadedDocument.EntityId),
                                Language = uploadedDocument.EntityLanguage.Name,
                                Version = uploadedDocument.EntityVersion,
                                Miscellaneous = formObj.AccountName
                            });
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed to save the form.", ex);
            }
        }

        /// <summary>
        /// Gets the form.
        /// </summary>
        /// <param name="idOrPath">The identifier or path.</param>
        /// <returns>Form blob</returns>
        public IFormBlob GetForm(string idOrPath)
        {
            var formItem = this.masterContentRepository.GetEntity<FormBlobEntity>(idOrPath);
            return formItem;
        }

        /// <summary>
        /// Gets the forms by.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <param name="total">The total.</param>
        /// <returns>collection of forms</returns>
        public IEnumerable<IForm> GetFormsBy(string username, int skip, int take, out int total)
        {
            var accessibelAccountCodes = this.portalWebSecurityService.GetReportAccessFor(username).GetAccessibleAccountCodes();
            if (accessibelAccountCodes == null || !accessibelAccountCodes.Any())
            {
                throw new ApplicationException("Failed to retrieve user mata data: " + username);
            }

            using (var search = this.contentRepository.GetSearchService())
            {
                // SELECT: All potential document
                var query = GetDocumentsQueryable(search);

                // WHERE: We only care about the favourite documents.
                query = query.Where(doc => accessibelAccountCodes.Contains(doc.AccountCode));

                // EXECUTE: Get the total count and data for a page.
                total = query.Count();
                return this.BackloadDocumentsFromRepository(query, skip, take);
            }
        }

        private static IQueryable<FormEntity> GetDocumentsQueryable(IContentSearchService search)
        {
            // SELECT: All documents under the document library.
            var query = search.GetDescendantsQueryable<FormEntity>(Constants.Content.ClientPortal.Documents.ItemID.Guid);

            // WHERE: Document library contains folder items as well as documents, filter by document.
            query = query.Where(d => d.EntityTemplateId == IFormConstants.TemplateId.Guid);
            return query;
        }

        private static string GenerateFormName(string accountName, string accountCode, DateTime reportDate)
        {
            var date = reportDate.ToString(Constants.DocumentDateFormat);
            var names = new[] { accountName, accountCode, date };
            return string.Join(Constants.DocumentNameSeparator, names);
        }

        private IEnumerable<IForm> BackloadDocumentsFromRepository(IQueryable<FormEntity> query, int skip, int take)
        {
            // EXECUTE: Read the results from the index, adjusted for current page.
            var results = query.Skip(skip).Take(take).ToArray();

            // Backfill any information not stored in the index from the database.
            return results.Select(d => this.contentRepository.MapEntity(d)).ToArray();
        }
    }
}
