﻿namespace Qic.Sc.Services.Components.Decorators
{
    using System;

    using Qic.Sc.Interfaces.Components;

    internal partial class ReportServiceTracingDecorator : TracingBaseDecorator, IReportService
    {
        private readonly IReportService reportService;

        public ReportServiceTracingDecorator(IReportService reportService)
        {
            if (reportService == null)
            {
                throw new ArgumentNullException("reportService");
            }
            this.reportService = reportService;
        }
    }
}
