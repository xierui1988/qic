﻿namespace Qic.Sc.Services.Components.Decorators
{
    using System;

    using Qic.Sc.Interfaces.Components;

    internal partial class WebSecurityServiceTracingDecorator : TracingBaseDecorator, IWebSecurityService
    {
        private readonly IWebSecurityService webSecurity;

        public WebSecurityServiceTracingDecorator(IWebSecurityService webSecurity)
        {
            if (webSecurity == null)
            {
                throw new ArgumentNullException("webSecurity");
            }
            this.webSecurity = webSecurity;
        }
    }
}