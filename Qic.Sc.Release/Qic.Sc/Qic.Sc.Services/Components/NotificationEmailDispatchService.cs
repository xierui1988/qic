﻿namespace Qic.Sc.Services.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Mail;

    using Qic.Sc.Entities;
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;
    using Qic.Sc.Interfaces.Crm;

    using Sitecore.ApplicationCenter.Applications;
    using Sitecore.Modules.EmailCampaign.Messages;
    using Sitecore.Security;
    using Sitecore.SecurityModel;

    /// <summary>
    /// The notification email dispatch service.
    /// </summary>
    public class NotificationEmailDispatchService : BaseEmailDispatchService
    {
        private readonly ICrmService crmService;
        private readonly IAuditService auditService;
        private readonly IWebSecurityService webSecurityService;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationEmailDispatchService"/> class.
        /// </summary>
        public NotificationEmailDispatchService(IAuditService auditService, ICrmService crmService, IWebSecurityService webSecurityService)
        {
            if (auditService == null)
            {
                throw new ArgumentNullException("auditService");
            }
            if (crmService == null)
            {
                throw new ArgumentNullException("crmService");
            }
            if (webSecurityService == null)
            {
                throw new ArgumentNullException("webSecurityService");
            }
            this.auditService = auditService;
            this.crmService = crmService;
            this.webSecurityService = webSecurityService;
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="email">
        /// The email.
        /// </param>
        /// <param name="resetToken">
        /// The reset token.
        /// </param>
        /// <param name="templateItemId">
        /// The template item id.
        /// </param>
        /// <exception cref="Exception">
        /// Thrown if email dispatch fails.
        /// </exception>
        public void Dispatch(string email, string resetToken, string templateItemId)
        {
            using (new SecurityDisabler())
            {
                var userAction = SitecoreUserAction.ChangePassword;
                if (templateItemId == Constants.Content.ClientPortal.Configuration.Notifications.Templates.UserNotifications.WelcomeNotification.ID)
                {
                    userAction = SitecoreUserAction.WelcomeNotification;
                }

                var clientNames = string.Empty;

                try
                {
                    var userReportAccess = this.crmService.GetUserReportAccess(email).ToArray();
                    var access = userReportAccess.FirstOrDefault();
                    var firsName = string.Empty;
                    if (access == null)
                    {
                        var userName = string.Format("{0}\\{1}", Constants.CrmDomain, email);
                        if (!this.webSecurityService.IsAdmin(userName))
                        {
                            throw new ApplicationException("User has no access rights: " + email);
                        }

                        var user = Sitecore.Security.Accounts.User.FromName(userName, false);
                        if (user != null)
                        {
                            firsName = user.Profile.FullName;
                        }
                    }
                    else
                    {
                        firsName = access.FirstName ?? access.FullName ?? string.Empty;
                    }

                    var clientList = new List<string>();

                    foreach (var accessReport in userReportAccess)
                    {
                        if (!clientList.Contains(accessReport.AccountName))
                        {
                            clientList.Add(accessReport.AccountName);
                        }
                    }

                    var template = CreateMessage(templateItemId);

                    // Send the email in the background
                    var message = template.Clone() as HtmlMail;

                    message.To = email;

                    message.CustomPersonTokens.Add("email", email);
                    message.CustomPersonTokens.Add(Constants.FirstNameToken, firsName);
                    message.CustomPersonTokens.Add(Constants.ResetToken, resetToken);

                    clientNames = string.Join(Constants.ClientListSeparator, clientList);
                    message.CustomPersonTokens.Add(Constants.ClientNameToken, clientNames);
                    message.CustomPersonTokens.Add(Constants.ContactOwnerToken, userReportAccess.Select(x => x.ContactOwner).FirstOrDefault() ?? string.Empty);

                    if (message.Body == null)
                    {
                        message.Body = message.GetMessageBody();
                    }

                    if (!BaseEmailDispatchService.SendEmail(message))
                    {
                        userAction = SitecoreUserAction.FailedUserNotification;
                    }
                }
                catch (Exception ex)
                {
                    userAction = SitecoreUserAction.FailedUserNotification;
                    throw new Exception("Failed to dispatch an email notification.", ex);
                }
                finally
                {
                    this.auditService.Audit(new AuditMetadata
                    {
                        UserName = email,
                        Id = Constants.Content.ClientPortal.Home.Login.ChangePassword.ItemID.ToString(),
                        Path = Constants.Content.ClientPortal.Home.Login.ChangePassword.Path,
                        Action = EnumUtils<SitecoreUserAction>.GetDescription(userAction),
                        Db = Constants.WebDb,
                        Language = "en",
                        Version = 1,
                        Miscellaneous = clientNames
                    });
                }
            }
        }

        /// <summary>
        /// The dispatch.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        public void Dispatch(MailMessage message)
        {
            var configuration = GetSmtpSettings();
            var cerdentials = new System.Net.NetworkCredential(
                configuration.UserName,
                configuration.Password,
                configuration.LoginDomain);

            var client = new SmtpClient
            {
                Host = configuration.Server,
                Port = configuration.Port,
                Credentials = cerdentials,
                EnableSsl = configuration.StartTLS
            };

            client.Send(message);
        }
    }
}
