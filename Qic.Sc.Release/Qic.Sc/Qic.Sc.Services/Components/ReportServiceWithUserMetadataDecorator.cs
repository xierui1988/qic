﻿namespace Qic.Sc.Services.Components
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
    using Qic.Sc.Interfaces.Components;

    /// <summary>
    /// The report service with user meta data decorator.
    /// Enriches document information with user specific meta data. Including favourites and is new indicators.
    /// </summary>
    internal partial class ReportServiceWithUserMetadataDecorator : IReportService
    {
        private readonly IReportService reportService;
        private readonly IWebSecurityService webSecurityService;
        private readonly IPortalWebSecurityService portalWebSecurityService;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportServiceWithUserMetadataDecorator"/> class.
        /// </summary>
        public ReportServiceWithUserMetadataDecorator(IReportService reportService, IWebSecurityService webSecurityService, IPortalWebSecurityService portalWebSecurityService)
        {
            if (reportService == null)
            {
                throw new ArgumentNullException("reportService");
            }
            if (webSecurityService == null)
            {
                throw new ArgumentNullException("webSecurityService");
            }
            if (portalWebSecurityService == null)
            {
                throw new ArgumentNullException("portalWebSecurityService");
            }
            this.portalWebSecurityService = portalWebSecurityService;
            this.reportService = reportService;
            this.webSecurityService = webSecurityService;
        }

        public IEnumerable<IDocument> GetDocumentsByFilterAndSort(string userName, ReportFilterAndSortMetadata filterAndSort, int skip, int take, out int total)
        {
            var documents =
                this.reportService.GetDocumentsByFilterAndSort(userName, filterAndSort, skip, take, out total)
                    .ToArray();

            return this.PersonaliseDocuments(documents);
        }

        public IEnumerable<IDocument> SearchDocumentsBy(string username, ReportFilterAndSortMetadata filter, string searchText, int skip, int take, out int total)
        {
            var documents =
                this.reportService.SearchDocumentsBy(username, filter, searchText, skip, take, out total)
                    .ToArray();

            return this.PersonaliseDocuments(documents);
        }

        public IEnumerable<IDocument> GetFavouriteDocumentsBy(string username, ICollection<string> reportCodes, int skip, int take, out int total)
        {
            var documents = this.reportService.GetFavouriteDocumentsBy(username, reportCodes, skip, take, out total).ToArray();
            return !documents.Any() ? documents : this.PersonaliseDocuments(documents);
        }

        public IEnumerable<IDocument> GetRecentDocumentsBy(string username, ICollection<string> reportCodes, int skip, int take, out int total)
        {
            var documents =
                this.reportService.GetRecentDocumentsBy(username, reportCodes, skip, take, out total)
                    .ToArray();

            return !documents.Any() ? documents : this.PersonaliseDocuments(documents);
        }

        private class ReportCodeType
        {
            public string ReportCode { get; set; }
            public string ReportTypeName { get; set; }
        }

        private IEnumerable<IDocument> PersonaliseDocuments(IEnumerable<IDocument> documents)
        {
            var metadata = this.webSecurityService.GetUserMetadataFor(this.webSecurityService.GetCurrentAuthenticatedUsername());
            var types = this.portalWebSecurityService.GetAllReportTypes().Select(x => new ReportCodeType{ ReportCode = x.ReportCode, ReportTypeName = x.ReportTypeName });

            return documents.Select(
                d =>
                    {
                        // augment documents with favourite indicator
                        d.IsFavorite = metadata.Favourites.Items.Contains(d.EntityId);

                        // augment document with unread indicator
                        d.IsUnreadRecent = metadata.ReadDocuments.IsUnreadRecentDocument(d.EntityId, d.PublishedDate.GetValueOrDefault());

                        // set report type name
                        d.ReportTypeName = types.Where(t => t.ReportCode == d.ReportCode).Select(t => t.ReportTypeName).FirstOrDefault();
                        return d;
                    });
        }

        public int GetRecentDocumentsCount(string username)
        {
            return this.reportService.GetRecentDocumentsCount(username);
        }
    }
}
