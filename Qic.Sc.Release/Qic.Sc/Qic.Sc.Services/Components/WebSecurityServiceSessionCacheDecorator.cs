﻿using System;

namespace Qic.Sc.Services.Components
{
    using System.Web;

    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Interfaces.Components;

    internal partial class WebSecurityServiceSessionCacheDecorator : IWebSecurityService
    {
        private const string UserMetadataCacheKey = "Qic.Sc.Services.Components.WebSecurityServiceSessionCacheDecorator.GetUserMetadataFor(string username)";
        private const string IsAdminCacheKey = "Qic.Sc.Services.Components.WebSecurityServiceSessionCacheDecorator.IsAdmin({0})";
        private readonly IWebSecurityService webSecurityService;

        public WebSecurityServiceSessionCacheDecorator(IWebSecurityService webSecurityService)
        {
            if (webSecurityService == null)
            {
                throw new ArgumentNullException("webSecurityService");
            }
            this.webSecurityService = webSecurityService;
        }

        public UserMetadata GetUserMetadataFor(string username)
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new InvalidOperationException("Can't resolve user identity, HTTP context is empty.");
            }

            var key = UserMetadataCacheKey + username;
            var metadata = context.Session[key] as UserMetadata;
            if (metadata == null)
            {
                metadata = this.webSecurityService.GetUserMetadataFor(username);
                context.Session[key] = metadata;
            }

            return metadata;
        }

        public void UpdatedUserMetadataFor(string username, UserMetadata metadata)
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new InvalidOperationException("Can't resolve user identity, HTTP context is empty.");
            }

            var key = UserMetadataCacheKey + username;
            context.Session[key] = metadata;

            this.webSecurityService.UpdatedUserMetadataFor(username, metadata);
        }

        public bool IsAdmin(string username)
        {
            var context = HttpContext.Current;
            if (context == null)
            {
                throw new InvalidOperationException("Can't resolve user identity, HTTP context is empty.");
            }

            var key = string.Format(IsAdminCacheKey, username);
            var isAdminObj = context.Session[key];
            if (isAdminObj == null)
            {
                isAdminObj = this.webSecurityService.IsAdmin(username);
                context.Session[key] = isAdminObj;
            }

            return (bool) isAdminObj;
        }
    }
}
