﻿using Qic.Sc.Entities;
using Qic.Sc.Entities.UserDefined.Components.Site.Configuration;
using Qic.Sc.Entities.UserDefined.QIC.Portal.Data.Document;
using Qic.Sc.Interfaces.Components;
using Qic.Sc.Interfaces.Core;
using Qic.Sc.Interfaces.Crm;

using System;
using Sitecore.SecurityModel;

namespace Qic.Sc.Services.Components
{
    using Qic.Sc.Entities.Metadata;
    using Qic.Sc.Services.Core;

    using Sitecore.Data.Items;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Linq;

    /// <summary>
    /// The report service implementation.
    /// </summary>
    public class ReportService : IReportService
    {
        private readonly ICrmService crmOnlineService;
        private readonly IContentRepository contentRepository;
        private readonly IContentRepository masterContentRepository;
        private readonly IWebSecurityService securityService;
        private readonly IPortalWebSecurityService portalSecurity;
        private readonly IAuditService auditService;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Object"/> class.
        /// </summary>
        public ReportService(
            ICrmService crmService,
            IContentRepository contentRepository,
            IMasterContentRepository masterContentRepository,
            IWebSecurityService securityService,
            IPortalWebSecurityService portalSecurity,
            IAuditService auditService)
        {
            if (crmService == null)
            {
                throw new ArgumentNullException("crmService");
            }
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            if (securityService == null)
            {
                throw new ArgumentNullException("securityService");
            }
            if (portalSecurity == null)
            {
                throw new ArgumentNullException("portalSecurity");
            }
            if (masterContentRepository as IContentRepository == null)
            {
                throw new ArgumentNullException("masterContentRepository");
            }
            if (auditService == null)
            {
                throw new ArgumentNullException("auditService");
            }

            this.crmOnlineService = crmService;
            this.contentRepository = contentRepository;
            this.securityService = securityService;
            this.portalSecurity = portalSecurity;
            this.masterContentRepository = masterContentRepository as IContentRepository;
            this.auditService = auditService;
        }

        /// <summary>
        /// Creates a report in the content repository using the meta data and the stream provided.
        /// </summary>
        public void CreateReport(ReportNameMetadata reportObj)
        {
            var fileType = reportObj.EntityCode.StartsWith(EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.HoldingAccountCode))
                ? EntityCodeType.HoldingAccountCode : (reportObj.EntityCode.StartsWith(EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.ProductCode)) || reportObj.EntityCode.StartsWith(EnumUtils<EntityCodeType>.GetDescription(EntityCodeType.ProductCode)))
                ? EntityCodeType.ProductCode : EntityCodeType.AccountCode;

            if (fileType.Equals(EntityCodeType.None))
            {
                throw new ArgumentException(string.Format("Entity Code: {0} is invalid", reportObj.EntityCode));
            }

            ReportMetadata[] reportMetadata;
            try
            {
                reportMetadata =
                    this.crmOnlineService.GetReportMetadata(fileType, reportObj.EntityCode, reportObj.ReportCode)
                        .ToArray();
            }
            catch (ArgumentException e)
            {
                throw e;
            }
            catch (ApplicationException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(
                    "Failed to save the report, unable to validate meta-data against CRM.",
                    ex);
            }

            string documentName = GenerateReportName(fileType, reportMetadata.FirstOrDefault(), reportObj.ReportDate);
            string itemName = ItemUtil.ProposeValidItemName(documentName);

            bool exists;

            // Catch all other exceptions, except Application Exception for Publishing
            var document = this.GetOrCreateDocumentBlob(
                reportObj.FileName,
                reportObj.ReportDate,
                reportObj.EntityCode,
                reportObj.ReportCode,
                out exists);

            document.EntityName = itemName;
            document.DocumentName = GenerateReportName(fileType, reportMetadata.FirstOrDefault(), reportObj.ReportDate);
            document.TibcoFilename = reportObj.FileName;
            document.ReportDate = reportObj.ReportDate;
            document.Extension = reportObj.FileExtension;
            document.Upload = DateTime.Now;
            document.MimeType = reportObj.MimeType;
            document.PublishedDate = null;

            if (document.Blob != null)
            {
                // Dispose of the blob object we're about to overwrite
                document.Blob.Dispose();
                document.Blob = null;
            }

            document.Blob = reportObj.FileStream;

            // Populate the workflow Values
            document.Workflow = Constants.System.Workflows.QICPortalPublishingWorkflow.ItemID.Guid;
            document.WorkflowState = Constants.System.Workflows.QICPortalPublishingWorkflow.New.ItemID.ToString();
            document.DefaultWorkflow = Constants.System.Workflows.QICPortalPublishingWorkflow.ItemID.Guid;

            if (reportObj.IsMigration)
            {
                document.WorkflowState = Constants.System.Workflows.QICPortalPublishingWorkflow.Published.ItemID.ToString();

                // Set a date for published documents
                document.PublishedDate = DateTime.Now;
            }

            // Override Published Date if provided
            if (reportObj.MigrationPublishedDate != null)
            {
                document.PublishedDate = reportObj.MigrationPublishedDate.GetValueOrDefault();
            }

            if (reportObj.IsNotificationRequired.HasValue)
            {
                document.IsNotificationRequired = reportObj.IsNotificationRequired.GetValueOrDefault();
            }

            // Populate Product information or Holding Account information
            switch (fileType)
            {
                case EntityCodeType.HoldingAccountCode:
                    document.HoldingAccountName = reportMetadata.FirstOrDefault().HoldingAccountName;
                    document.HoldingAccountCode = reportMetadata.FirstOrDefault().HoldingAccountCode;
                    break;
                case EntityCodeType.ProductCode:
                    document.ProductName = reportMetadata.FirstOrDefault().ProductName;
                    document.ProductCode = reportMetadata.FirstOrDefault().ProductCode;
                    break;
            }

            // Populate Account (Client) list
            NameValueCollection accountList = new NameValueCollection();

            foreach (var data in reportMetadata)
            {
                accountList.Add(data.AccountCode, data.AccountName);
            }

            document.AccountList = accountList;

            // Populate Report
            document.ReportName = reportMetadata.FirstOrDefault().ReportName;
            document.ReportCode = reportMetadata.FirstOrDefault().ReportCode;

            try
            {
                using (new SecurityDisabler())
                {
                    // NOTE: The only place to create documents is MASTER data base.
                    // NOTE: Make sure we calling the correct repository.
                    DocumentBlobEntity uploadedDocument = null;

                    if (exists)
                    {
                        this.masterContentRepository.SaveEntity(document);
                        uploadedDocument = document;
                    }
                    else
                    {
                        var parent =
                            this.masterContentRepository.GetEntity<IConfiguration>(
                                Constants.Content.ClientPortal.Documents.ID);
                        uploadedDocument = this.masterContentRepository.CreateEntity(parent, document);
                    }

                    this.auditService.Audit(new AuditMetadata
                    {
                        UserName = "system", // Original identity whether TIBCO or CRM user is lost so all uploads are as system.
                        Action = EnumUtils<SitecoreUserAction>.GetDescription(SitecoreUserAction.DocumentUpload),
                        Db = Constants.MasterDb,
                        Path = string.Format("{0}{1}", Constants.ContentPath, uploadedDocument.EntityContentPath),
                        Language = uploadedDocument.EntityLanguage.Name,
                        Version = uploadedDocument.EntityVersion,
                        Id = string.Format(Constants.ItemIDFormat, uploadedDocument.EntityId.ToString()),
                        Miscellaneous = string.Join(Constants.ClientListSeparator, reportMetadata.Select(x => x.AccountName).Distinct())
                    });
                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed to save the report.", ex);
            }
        }

        /// <summary>
        /// Gets a list of documents that match a criteria.
        /// </summary>
        /// <param name="username">
        /// The current logged in user
        /// </param>
        /// <param name="filterAndSort">
        /// Filter and sort instructions.
        /// </param>
        /// <param name="skip">
        /// Number of records to skip.
        /// </param>
        /// <param name="take">
        /// Maximum number of record to return.
        /// </param>
        /// <param name="total">
        /// Total number of records that matched.
        /// </param>
        /// <returns>
        /// A list of documents.
        /// </returns>
        public IEnumerable<IDocument> GetDocumentsByFilterAndSort(string username, ReportFilterAndSortMetadata filterAndSort, int skip, int take, out int total)
        {
            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            var reportAccess = this.portalSecurity.GetReportAccessFor(username);

            if (!reportAccess.Any())
            {
                total = 0;
                return Enumerable.Empty<IDocument>();
            }

            using (var search = this.contentRepository.GetSearchService())
            {
                // SELECT: For all documents in the library.
                var query = GetDocumentsQueryable(search);

                // SECURITY: CRITICAL CODE SECURITY IS APPLIED BASED ON ACCESS LIST PROVIDED
                query = this.ApplySecurity(query, username);

                // WHERE: Add where closes from the filter. Used to further reduce the set.
                query = FilterBy(query, filterAndSort.Filters);

                // ORDER BY: Add sorting instructions
                query = OrderBy(query, filterAndSort.Sort);

                // COUNT: Total number of matches.
                total = query.Count();

                // EXECUTE: Read a page and load missing fields from content repository
                return this.BackloadDocumentsFromRepository(query, skip, take);
            }
        }

        /// <summary>
        /// Gets a list of documents that match a criteria.
        /// </summary>
        /// <param name="username">The current logged in user</param>
        /// <param name="filter">Used for access Records</param>
        /// <param name="searchText">The text used to search</param>
        /// <param name="skip">Number of records to skip.</param>
        /// <param name="take">Maximum number of record to return.</param>
        /// <param name="total">Total number of records that matched.</param>
        /// <returns>A list of documents.</returns>
        public IEnumerable<IDocument> SearchDocumentsBy(string username, ReportFilterAndSortMetadata filter, string searchText, int skip, int take, out int total)
        {
            if (searchText == null)
            {
                throw new ArgumentNullException("searchText");
            }

            if (string.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException("username");
            }

            using (var search = this.contentRepository.GetSearchService())
            {
                // SELECT: For all documents in the library.
                var query = GetDocumentsQueryable(search);

                // SECURITY: CRITICAL CODE SECURITY IS APPLIED BASED ON ACCESS LIST PROVIDED
                query = this.ApplySecurity(query, username);

                // Fuzzy search
                query =
                    query.Where(
                        doc =>
                        doc.DocumentName.Contains(searchText));

                // ORDER BY: Add sorting instructions
                query = OrderBy(query, filter.Sort);

                // COUNT: Total number of matches.
                total = query.Count();

                // EXECUTE: Read a page and load missing fields from content repository
                return this.BackloadDocumentsFromRepository(query, skip, take);
            }
        }

        /// <summary>
        /// Gets a list of favourite documents for the user. The user is taken from the first
        /// access meta data record.
        /// </summary>
        /// <param name="username">
        ///     user name
        /// </param>
        /// <param name="reportCodes">
        ///     Filter by provided report codes, ignored if null or empty.
        /// </param>
        /// <param name="skip">
        ///     Number of records to skip.
        /// </param>
        /// <param name="take">
        ///     Maximum number of record to return.
        /// </param>
        /// <param name="total">
        ///     Total number of records that matched.
        /// </param>
        /// <returns>
        /// A list of documents.
        /// </returns>
        public IEnumerable<IDocument> GetFavouriteDocumentsBy(string username, ICollection<string> reportCodes, int skip, int take, out int total)
        {
            var metadata = this.securityService.GetUserMetadataFor(username);
            if (metadata == null)
            {
                throw new ApplicationException("Failed to retrieve user mata data: " + username);
            }

            var favourites = metadata.Favourites;

            if (!favourites.Items.Any())
            {
                total = 0;
                return Enumerable.Empty<IDocument>();
            }

            using (var search = this.contentRepository.GetSearchService())
            {
                // SELECT: All potential document
                var query = GetDocumentsQueryable(search);

                // SECURITY: CRITICAL CODE SECURITY IS APPLIED BASED ON ACCESS LIST PROVIDED
                query = this.ApplySecurity(query, username);

                // WHERE: Apply specific report code filter if provided.
                if (reportCodes != null && reportCodes.Any())
                {
                    query = query.Where(doc => reportCodes.Contains(doc.ReportCode));
                }

                // WHERE: We only care about the favourite documents.
                query = query.Where(doc => favourites.Items.Contains(doc.EntityId));

                // EXECUTE: Get the total count and data for a page.
                total = query.Count();
                return this.BackloadDocumentsFromRepository(query, skip, take);
            }
        }

        /// <summary>
        /// Gets the list of recent documents for the user. The user
        /// is taken from the first access meta data record.
        /// </summary>
        /// <param name="username">
        ///     The user name.
        /// </param>
        /// <param name="reportCodes">
        ///     Filter by the provided report codes, ignored if null or empty.
        /// </param>
        /// <param name="skip">
        ///     The skip.
        /// </param>
        /// <param name="take">
        ///     The take.
        /// </param>
        /// <param name="total">
        ///     The total.
        /// </param>
        /// <returns>
        /// The <see cref="System.Collections.Generic.IEnumerable{T}"/>.
        /// </returns>
        public IEnumerable<IDocument> GetRecentDocumentsBy(string username, ICollection<string> reportCodes, int skip, int take, out int total)
        {
            var metadata = this.securityService.GetUserMetadataFor(username);
            if (metadata == null)
            {
                throw new ApplicationException("Failed to retrieve user mata data: " + username);
            }

            var cutOffDate = metadata.ReadDocuments.CutOffDate;

            using (var search = this.contentRepository.GetSearchService())
            {
                // SELECT: All potential document
                var query = GetDocumentsQueryable(search);

                // SECURITY: CRITICAL CODE SECURITY IS APPLIED BASED ON ACCESS LIST PROVIDED
                query = this.ApplySecurity(query, username);

                // WHERE: Apply specific report code filter if provided.
                if (reportCodes != null && reportCodes.Any())
                {
                    query = query.Where(doc => reportCodes.Contains(doc.ReportCode));
                }

                // WHERE: Recent are documents published after a cutoff date per user.
                query = query.Where(doc => doc.IndexPublishedDate >= cutOffDate);

                // EXECUTE: Get the total count and data for a page.
                total = query.Count();
                return this.BackloadDocumentsFromRepository(query, skip, take);
            }
        }

        private static IQueryable<DocumentEntity> FilterBy(IQueryable<DocumentEntity> query, ReportFilterMetadata filters)
        {
            var date = new DateTime();
            if (filters.ReportDateFrom != date)
            {
                query = query.Where(doc => doc.ReportDate >= filters.ReportDateFrom);
            }

            if (filters.ReportDateTo != date)
            {
                query = query.Where(doc => doc.ReportDate <= filters.ReportDateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }

            if (filters.PublishDateFrom != date)
            {
                query = query.Where(doc => doc.IndexPublishedDate >= filters.PublishDateFrom);
            }

            if (filters.PublishDateTo != date)
            {
                query = query.Where(doc => doc.IndexPublishedDate <= filters.PublishDateTo.AddHours(23).AddMinutes(59).AddSeconds(59));
            }

            if (filters.AccountCodes != null && filters.AccountCodes.Any())
            {
                // Filter by selected account codes.
                query = query.Where(doc => filters.AccountCodes.Contains(doc.IndexAccountCodes));
            }

            if (filters.ProductCodes != null && filters.ProductCodes.Any())
            {
                // Filter by selected product codes.
                query = query.Where(doc => filters.ProductCodes.Contains(doc.ProductCode));
            }

            if (filters.HoldingAccountCodes != null && filters.HoldingAccountCodes.Any())
            {
                // Filter by selected holding account codes.
                query = query.Where(doc => filters.HoldingAccountCodes.Contains(doc.HoldingAccountCode));
            }

            if (filters.ReportCodes != null && filters.ReportCodes.Any())
            {
                // Filter by selected Report codes
                query = query.Where(doc => filters.ReportCodes.Contains(doc.ReportCode));
            }

            return query;
        }

        private static string GenerateReportName(EntityCodeType entityCodeType, ReportMetadata reportMetadata, DateTime reportDate)
        {
            if (entityCodeType.Equals(EntityCodeType.ProductCode))
            {
                return string.Concat(reportMetadata.ReportName, Constants.DocumentNameSeparator, reportMetadata.ProductName, Constants.DocumentNameSeparator, reportDate.ToString(Constants.DocumentDateFormat));
            }

            if (entityCodeType.Equals(EntityCodeType.HoldingAccountCode))
            {
                return string.Concat(reportMetadata.HoldingAccountName, Constants.DocumentNameSeparator, reportMetadata.ReportName, Constants.DocumentNameSeparator, reportDate.ToString(Constants.DocumentDateFormat));
            }

            return string.Concat(reportMetadata.ReportName, Constants.DocumentNameSeparator, reportDate.ToString(Constants.DocumentDateFormat));
        }

        private static IQueryable<DocumentEntity> OrderBy(IQueryable<DocumentEntity> query, ReportSortMetadata sort)
        {
            switch (sort.SortBy)
            {
                case ReportSortByMetadata.DocumentName:
                    return sort.SortOrder == ReportSortOrderMetadata.Accending
                               ? query.OrderBy(d => d.DocumentName)
                               : query.OrderByDescending(d => d.DocumentName);

                case ReportSortByMetadata.ReportName:
                    return sort.SortOrder == ReportSortOrderMetadata.Accending
                               ? query.OrderBy(d => d.ReportName)
                               : query.OrderByDescending(d => d.ReportName);

                case ReportSortByMetadata.ReportDate:
                    return sort.SortOrder == ReportSortOrderMetadata.Accending
                               ? query.OrderBy(d => d.ReportDate)
                               : query.OrderByDescending(d => d.ReportDate);

                case ReportSortByMetadata.PublishDate:
                    return sort.SortOrder == ReportSortOrderMetadata.Accending
                               ? query.OrderBy(d => d.IndexPublishedDate)
                               : query.OrderByDescending(d => d.IndexPublishedDate);

                default:
                    throw new ArgumentOutOfRangeException("sort", sort.SortBy, "Unknown sort order.");
            }
        }

        private static IQueryable<DocumentEntity> GetDocumentsQueryable(IContentSearchService search)
        {
            // SELECT: All documents under the document library.
            var query = search.GetDescendantsQueryable<DocumentEntity>(Constants.Content.ClientPortal.Documents.ItemID.Guid);

            // WHERE: Document library contains folder items as well as documents, filter by document.
            query = query.Where(d => d.EntityTemplateId == IDocumentConstants.TemplateId.Guid);
            return query;
        }

        private DocumentBlobEntity GetOrCreateDocumentBlob(string tibcoFilename, DateTime reportDate, string entityCode, string reportCode, out bool exists)
        {
            try
            {
                using (new SecurityDisabler())
                {
                    var indexedDocument = this.ReportExists(reportDate, entityCode, reportCode);

                    if (indexedDocument == null)
                    {
                        exists = false;
                        return new DocumentBlobEntity();
                    }

                    exists = true;

                    // Getting Entity rather than mapping to ensure I get the latest version to get the Workflow state
                    var document =
                        this.masterContentRepository.GetEntity<DocumentBlobEntity>(indexedDocument.EntityId.ToString());

                    if (document.WorkflowState
                        == Constants.System.Workflows.QICPortalPublishingWorkflow.Published.ItemID.ToString())
                    {
                        throw new ApplicationException(
                            string.Format(
                                "The report can not be loaded as a published version of the same report already exists for file name {0}. you must first unpublish the report in Sitecore before loading.",
                                tibcoFilename));
                    }

                    if (document.WorkflowState
                        == Constants.System.Workflows.QICPortalPublishingWorkflow.Unpublished.ItemID.ToString())
                    {
                        document = this.masterContentRepository.AddVersion(document, true);
                    }

                    return document;
                }
            }
            catch (ApplicationException e)
            {
                throw e;
            }
            catch (Exception ex)
            {
                throw new ApplicationException("Failed to save the report, unable to determine whether the report exists.", ex);
            }
        }

        private DocumentEntity ReportExists(DateTime reportDate, string entityCode, string reportCode)
        {
            using (var search = this.masterContentRepository.GetSearchService())
            {
                var thumbprint = MetadataExtensions.GetDocumentThumbprint(entityCode, reportCode, reportDate);

                var query =
                    search.GetDescendantsQueryable<DocumentEntity>(Constants.Content.ClientPortal.Documents.ItemID.Guid)
                        .Where(doc => doc.Thumbprint == thumbprint);

                return query.FirstOrDefault();
            }
        }

        private IQueryable<DocumentEntity> ApplySecurity(IQueryable<DocumentEntity> query, string username)
        {
            // If Admin user, then NO need to apply security
            if (this.securityService.IsAdmin(username))
            {
                return query;
            }

            // SECURITY: We will always filter by a entity and report.
            var access = this.portalSecurity.GetReportAccessFor(username).ToArray();
            if (!access.Any())
            {
                throw new ApplicationException("User has no active access recored: " + username);
            }

            // SECURITY: Filter based on document security field
            var accessibleReports = access.GetSecurityCodes();
            if (accessibleReports.Any())
            {
                query = query.Where(doc => accessibleReports.Contains(doc.Security));
            }

            // User is Admin, therefore should see everything
            return query;
        }

        private IEnumerable<IDocument> BackloadDocumentsFromRepository(IQueryable<DocumentEntity> query, int skip, int take)
        {
            // EXECUTE: Read the results from the index, adjusted for current page.
            var results = query.Skip(skip).Take(take).OrderByDescending(x => x.IndexPublishedDate).ToArray();

            // Backfill any information not stored in the index from the database.
            return results.Select(d => this.contentRepository.MapEntity(d)).ToArray();
        }

        /// <summary>
        ///  get the recent document count for the user
        /// </summary>
        /// <param name="username"> the user name</param>
        /// <returns> count of recent report</returns>
        public int GetRecentDocumentsCount(string username)
        {
            var metadata = this.securityService.GetUserMetadataFor(username);
            if (metadata == null)
            {
                throw new ApplicationException("Failed to retrieve user mata data: " + username);
            }

            var cutOffDate = metadata.ReadDocuments.CutOffDate;

            using (var search = this.contentRepository.GetSearchService())
            {
                // SELECT: All potential document
                var query = GetDocumentsQueryable(search);

                // SECURITY: CRITICAL CODE SECURITY IS APPLIED BASED ON ACCESS LIST PROVIDED
                query = this.ApplySecurity(query, username);

                // WHERE: Recent are documents published after a cutoff date per user.
                query = query.Where(doc => doc.IndexPublishedDate >= cutOffDate);

                // Get the read documents for this user
                var readDocumentsIds = metadata.ReadDocuments.Items.Select(x => x.Id).ToArray();
                if (!readDocumentsIds.Any())
                {
                    // No documents have been read get the full recent count.
                    return query.Count();
                }

                // Count only the unread documents.
                query = query.Where(doc => !readDocumentsIds.Contains(doc.EntityId));
                return query.Count();
            }
        }
    }
}
