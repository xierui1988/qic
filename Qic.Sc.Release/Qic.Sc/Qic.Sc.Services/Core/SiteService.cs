﻿namespace Qic.Sc.Services.Core
{
    using System;
    using System.Web;

    using Qic.Sc.Interfaces.Core;

    using Sitecore.Sites;

    /// <summary>
    /// A simple wrapper around Sitecore site.
    /// </summary>
    public class SiteService : ISiteService
    {
        private readonly SiteContext site;

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteService"/> class.
        /// From context.
        /// </summary>
        public SiteService()
        {
            this.site = Sitecore.Context.Site ?? GetSiteContext();
            if (this.site == null)
            {
                throw new ApplicationException("Sitecore site can't be resolved from context.");
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="SiteService"/> class.
        /// </summary>
        /// <param name="configuredName">
        /// The configured name.
        /// </param>
        public SiteService(string configuredName)
        {
            if (configuredName == null)
            {
                throw new ArgumentNullException("configuredName");
            }

            this.site = SiteContextFactory.GetSiteContext(configuredName);
            if (this.site == null)
            {
                throw new ArgumentException(
                    "Sitecore site can't be resolved for provided site name: " + configuredName,
                    "configuredName");
            }
        }

        /// <summary>
        /// The get content path for the site.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetContentPath()
        {
            return this.site.ContentStartPath;
        }

        /// <summary>
        /// The get site name.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetSiteName()
        {
            return this.site.Name;
        }

        /// <summary>
        /// Gets the path or ID of the 404 item for the site.
        /// </summary>
        public string Get404Item()
        {
            return this.site.Properties["notFoundItem"];
        }

        /// <summary>
        /// Gets the based canonical URL of the site.
        /// </summary>
        public string GetTargetHostName()
        {
            return this.site.Properties["targetHostName"];
        }

        /// <summary>
        /// The get canonical URL.
        /// </summary>
        public Uri GetCanonicalUri()
        {
            var scheme = this.site.SiteInfo.Scheme;
            if (string.IsNullOrEmpty(scheme))
            {
                scheme = Uri.UriSchemeHttp;
            }

            var host = this.site.TargetHostName;

            var port = this.site.SiteInfo.Port;
            if (port == 0)
            {
                port = -1; // Will be set to default for the scheme
            }

            return new UriBuilder(scheme, host, port).Uri;
        }

        /// <summary>
        /// Get if SSL is required for the site.
        /// </summary>
        /// <returns>
        /// boolean value if this site requires SSL
        /// </returns>
        public bool IsSslRequired()
        {
            var sslRequired = false;
            bool.TryParse(this.site.Properties["sslRequired"], out sslRequired);
            return sslRequired;
        }

        /// <summary>
        /// Runtime sitecore site context.
        /// </summary>
        private static SiteContext GetSiteContext()
        {
            var httpContext = HttpContext.Current;
            if (httpContext == null)
            {
                return null;
            }

            var requestUri = httpContext.Request.Url;
            return SiteContextFactory.GetSiteContext(requestUri.Host, httpContext.Request.FilePath.ToLower(), requestUri.Port);
        }
    }
}
