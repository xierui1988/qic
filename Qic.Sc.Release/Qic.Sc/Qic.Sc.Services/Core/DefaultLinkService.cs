﻿using Sitecore.Resources.Media;

namespace Qic.Sc.Services.Core
{
    using System;

    using Qic.Sc.Interfaces.Core;

    using Sitecore.Data.Items;

    /// <summary>
    /// The default link service.
    /// </summary>
    public class DefaultLinkService : ILinkService
    {
        private readonly IContentRepository contentRepository;
        private readonly ISiteService siteService;

        /// <summary>
        /// Initializes a new instance of the <see cref="DefaultLinkService"/> class.
        /// </summary>
        /// <param name="contentRepository">
        /// The content repository.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// If any of the arguments is null.
        /// </exception>
        public DefaultLinkService(IContentRepository contentRepository, ISiteService siteService)
        {
            if (contentRepository == null)
            {
                throw new ArgumentNullException("contentRepository");
            }
            if (siteService == null)
            {
                throw new ArgumentNullException("siteService");
            }
            this.contentRepository = contentRepository;
            this.siteService = siteService;
        }

        /// <summary>
        /// Generate URL for News
        /// </summary>
        /// <param name="idOrPath">Sitecore item id</param>
        /// <returns>Generated URL</returns>
        public string GenerateUrl(string idOrPath)
        {
            var item = this.contentRepository.GetEntity<Item>(idOrPath);
            return item == null
                       ? null
                       : this.GetItemUrl(item);
        }

        private string GetItemUrl(Item item)
        {
            if (item.Paths.IsMediaItem)
            {
                return MediaManager.GetMediaUrl(
                    item, new MediaUrlOptions()
                    {
                        LowercaseUrls = true,
                        AlwaysIncludeServerUrl = false,
                        IncludeExtension = false
                    }).ToLower();
            }

            return Sitecore.Links.LinkManager.GetItemUrl(
                item, new Sitecore.Links.UrlOptions
                {
                    LowercaseUrls = true,
                    Site =
                        Sitecore.Sites.SiteContext.GetSite(
                            this.siteService.GetSiteName()),
                    LanguageEmbedding = Sitecore.Links.LanguageEmbedding.Never,
                    EncodeNames = true,
                    AlwaysIncludeServerUrl = false,
                    AddAspxExtension = false,
                    ShortenUrls = true
                }).ToLower();
        }
    }
}
