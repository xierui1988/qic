﻿namespace Qic.Sc.Services.Core
{
    using System;
    using System.Collections.Generic;

    using Glass.Mapper.Sc;

    using Qic.Sc.Interfaces.Core;

    using Sitecore.ContentSearch;
    using Sitecore.Data;
    using Sitecore.Data.Managers;

    /// <summary>
    /// Implementation of the repository for Sitecore.
    /// </summary>
    public class SitecoreRepository : IContentRepository, IMasterContentRepository
    {
        private readonly ISitecoreService context;

        /// <summary>
        /// Initializes a new instance of the <see cref="SitecoreRepository"/> class.
        /// </summary>
        /// <param name="context">Sitecore context.</param>
        public SitecoreRepository(ISitecoreService context)
        {
            if (context == null)
            {
                throw new ArgumentNullException("context");
            }

            this.context = context;
        }

        /// <summary>
        /// Retrieve entity from content repository based on the 
        /// </summary>
        /// <param name="identity">
        /// Id or a path to the item used to load the model.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of entity to be retrieved.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/> that matches specified identity.
        /// Null if entity is not found.
        /// </returns>
        public TModel GetEntity<TModel>(string identity) where TModel : class
        {
            return this.context.GetItem<TModel>(identity);
        }

        /// <summary>
        /// Retrieve entity from content repository.
        /// </summary>
        /// <param name="identity">
        /// Id or a path to the item used to load the model.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of entity to be retrieved.
        /// </typeparam>
        /// <typeparam name="TModel1">
        /// Additional entity type to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/> that matches specified identity.
        /// Null if entity is not found.
        /// </returns>
        public TModel GetEntity<TModel, TModel1>(string identity)
            where TModel : class
            where TModel1 : class
        {
            return this.context.GetItemWithInterfaces<TModel, TModel1>(identity);
        }

        /// <summary>
        /// Retrieve entity from content repository.
        /// </summary>
        /// <param name="identity">
        /// Id or a path to the item used to load the model.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of entity to be retrieved.
        /// </typeparam>
        /// <typeparam name="TModel1">
        /// Additional entity type X to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <typeparam name="TModel2">
        /// Additional entity type Y to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/> that matches specified identity.
        /// Null if entity is not found.
        /// </returns>
        public TModel GetEntity<TModel, TModel1, TModel2>(string identity)
            where TModel : class
            where TModel1 : class
            where TModel2 : class
        {
            return this.context.GetItemWithInterfaces<TModel, TModel1, TModel2>(identity);
        }

        /// <summary>
        /// Retrieve entity from content repository.
        /// </summary>
        /// <param name="identity">
        /// Id or a path to the item used to load the model.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of entity to be retrieved.
        /// </typeparam>
        /// <typeparam name="TModel1">
        /// Additional entity type X to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <typeparam name="TModel2">
        /// Additional entity type Y to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <typeparam name="TModel3">
        /// Additional entity type Z to be retrieved at the same time.
        /// Cast original return value to access this model.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/> that matches specified identity.
        /// Null if entity is not found.
        /// </returns>
        public TModel GetEntity<TModel, TModel1, TModel2, TModel3>(string identity)
            where TModel : class
            where TModel1 : class
            where TModel2 : class
            where TModel3 : class
        {
            return this.context.GetItemWithInterfaces<TModel, TModel1, TModel2, TModel3>(identity);
        }

        /// <summary>
        /// Load missing information into the model.
        /// </summary>
        /// <param name="model">
        /// The model to be back-loaded.
        /// </param>
        /// <typeparam name="TModel">
        /// Type of the model.
        /// </typeparam>
        /// <returns>
        /// Same object as passed in after the its been back-loaded with missing
        /// information. Useful for chaining the calls.
        /// </returns>
        public TModel MapEntity<TModel>(TModel model)
        {
            this.context.Map(model);
            return model;
        }

        /// <summary>
        /// Get the parent
        /// </summary>
        /// <param name="childObject">
        /// The Id.
        /// </param>
        /// <typeparam name="TModel">
        /// Entity type to return for the parent.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/>.
        /// </returns>
        public TModel GetParent<TModel>(object childObject) where TModel : class
        {
            var item = this.context.ResolveItem(childObject);
            if (item == null || item.Parent == null)
            {
                return default(TModel);
            }
            return item.Parent.GlassCast<TModel>();
        }

        /// <summary>
        /// Get a specific ancestor identified by <paramref name="contextItem"/> the of a content
        /// entity identified by <paramref name="identity"/>.
        /// </summary>
        /// <param name="contextItem">
        /// The target.
        /// </param>
        /// <param name="identity">
        /// The identity.
        /// </param>
        /// <typeparam name="TModel">
        /// Entity type to return for the parent.
        /// </typeparam>
        /// <returns>
        /// The <see cref="TModel"/>.
        /// </returns>
        public TModel FindAncestorItemBy<TModel>(object contextItem, string identity) where TModel : class
        {
            var item = this.context.ResolveItem(contextItem);
            if (item == null)
            {
                return default(TModel);
            }

            ID id;
            var isId = ID.TryParse(identity, out id);

            var parent = item.Parent;
            while (parent != null)
            {
                if (isId && parent.ID == id)
                {
                    return parent.GlassCast<TModel>();
                }

                var template = TemplateManager.GetTemplate(parent);
                if (isId && template.DescendsFromOrEquals(id))
                {
                    return parent.GlassCast<TModel>();
                }

                if (parent.Paths.FullPath == identity || template.FullName == identity)
                {
                    return item.GlassCast<TModel>();
                }

                parent = parent.Parent; // walk up
            }

            return default(TModel);
        }

        /// <summary>
        /// Queries a single entity. If you know the identity of the entity use one
        /// of the GetEntity methods instead they are much more efficient.
        /// </summary>
        /// <typeparam name="TModel">The type of the entity to return.</typeparam>
        /// <param name="query">Query string</param>
        /// <param name="isLazy">Lazy load the result set.</param>
        /// <param name="inferType">Infer types of th result set items.</param>
        /// <returns>A single item that matches the query or null.</returns>
        public TModel QuerySingle<TModel>(string query, bool isLazy = false, bool inferType = false) where TModel : class
        {
            return this.context.QuerySingle<TModel>(query, isLazy, inferType);
        }

        /// <summary>
        /// Queries repository for the entities.
        /// </summary>
        /// <typeparam name="TModel">The type of entity to return.</typeparam>
        /// <param name="query">Query string</param>
        /// <param name="isLazy">Lazy load the result set.</param>
        /// <param name="inferType">Infer types of the items in the result set.</param>
        /// <returns>A sequence of entities that match the query or an empty set.</returns>
        public IEnumerable<TModel> Query<TModel>(string query, bool isLazy = false, bool inferType = false) where TModel : class
        {
            return this.context.Query<TModel>(query, isLazy, inferType);
        }

        /// <summary>
        /// The create entity.
        /// </summary>
        /// <param name="parent">
        ///     The parent entity.
        /// </param>
        /// <param name="newEntity">
        ///     The new entity.
        /// </param>
        /// <param name="disableSecurity">
        ///     It will allow the option to disable security (intended for Tibco webservice)
        ///     (Default: false)
        /// </param>
        /// <param name="updateStatistics">
        ///     Whether the operation should affect statistic (updated time-stamp and version).
        ///     (Default: true)
        /// </param>
        /// <param name="silent">
        ///     Whether the operation would NOT raise events.
        ///     (Default: false)
        /// </param>
        /// <returns>
        /// The <see cref="TEntity"/>.
        /// </returns>
        public TEntity CreateEntity<TEntity, TParentEntity>(TParentEntity parent, TEntity newEntity, bool disableSecurity = false, bool updateStatistics = true, bool silent = false)
            where TEntity : class
            where TParentEntity : class
        {
            if (disableSecurity)
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    return this.context.Create(parent, newEntity, updateStatistics, silent);
                }
            }

            return this.context.Create(parent, newEntity, updateStatistics, silent);
        }

        /// <summary>
        /// The save entity.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="disableSecurity">
        ///     It will allow the option to disable security (intended for Tibco webservice)
        ///     (Default: false)
        /// </param>
        /// <param name="updateStatistics">
        ///     Whether the operation should affect statistic (updated time-stamp and version).
        ///     (Default: true)
        /// </param>
        /// <param name="silent">
        ///     Whether the operation would NOT raise events.
        ///     (Default: false)
        /// </param>
        public void SaveEntity<TModel>(TModel entity, bool disableSecurity = false, bool updateStatistics = true, bool silent = false) where TModel : class
        {
            if (disableSecurity)
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    this.context.Save(entity, updateStatistics, silent);
                }
            }

            this.context.Save(entity, updateStatistics, silent);
        }

        /// <summary>
        /// The save entity.
        /// </summary>
        /// <param name="entity">
        /// The entity.
        /// </param>
        /// <param name="disableSecurity">
        ///     It will allow the option to disable security (intended for Tibco webservice)
        ///     (Default: false)
        /// </param>
        public TModel AddVersion<TModel>(TModel entity, bool disableSecurity = false) where TModel : class
        {
            if (entity == null)
            {
                throw new ArgumentException("entity");
            }

            if (disableSecurity)
            {
                using (new Sitecore.SecurityModel.SecurityDisabler())
                {
                    return this.context.AddVersion(entity);
                }
            }

            return this.context.AddVersion(entity);
        }

        /// <summary>
        /// Get a default search context based on provided identity.
        /// </summary>
        /// <returns>
        /// Context aware search service. Search is contextual to the database
        /// of the current site.
        /// </returns>
        public IContentSearchService GetSearchService()
        {
            // User the root sitecore item to derive search context.
            var item = this.context.Database.GetRootItem() ?? Sitecore.Context.Item;
            if (item == null)
            {
                throw new ApplicationException(
                    string.Format(
                        "Search context can't be resolved, current user has no access to the root "
                        + "item and context item is also null."));
            }

            return new SitecoreSearchService(ContentSearchManager.CreateSearchContext((SitecoreIndexableItem)item));
        }
    }
}
