﻿namespace Qic.Sc.Services.Core
{
    using System;
    using System.Configuration;

    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// A configuration service implementation that users Web.config as the
    /// configuration store. All configuration settings are name spaced to
    /// <see cref="GlobalConfigurationNamespace"/> compile time constant.
    /// </summary>
    public class ConfigurationService : IConfigurationService
    {
        private readonly ISiteService siteService;
        public const string GlobalConfigurationNamespace = "Qic.Sc";

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationService"/> class.
        /// </summary>
        /// <param name="siteService">
        /// The site service to resolve contextual site.
        /// </param>
        /// <exception cref="ArgumentNullException">
        /// Throw if site service is not provided.
        /// </exception>
        public ConfigurationService(ISiteService siteService)
        {
            if (siteService == null)
            {
                throw new ArgumentNullException("siteService");
            }
            this.siteService = siteService;
        }

        /// <summary>
        /// A compile time constant <see cref="GlobalConfigurationNamespace"/>.
        /// </summary>
        /// <returns>Global configuration name space.</returns>
        public string GetGlobalConfigurationNamespace()
        {
            return GlobalConfigurationNamespace;
        }

        /// <summary>
        /// A runtime value created by combining <see cref="GetGlobalConfigurationNamespace"/>
        /// and the name of the current site.
        /// </summary>
        /// <returns>Site level name space, resolved at runtime to the value of the current site.</returns>
        public string GetSiteConfigurationNamespace()
        {
            return string.Format("{0}.{1}", this.GetGlobalConfigurationNamespace(), this.siteService.GetSiteName());
        }

        /// <summary>
        /// Get site level setting. A site level setting is resolved by prefixing
        /// site name space <see cref="IConfigurationService.GetSiteConfigurationNamespace"/> to the <paramref name="name"/>
        /// to create the full setting name.
        /// </summary>
        /// <param name="name">Setting name to retrieve.</param>
        /// <param name="default">Default value if setting is not configured.</param>
        /// <returns>Returns configuration setting or default value if setting is not found.</returns>
        public string GetGlobalSetting(string name, string @default)
        {
            return
                GetApplicationSetting(
                    GetFullyQualifiedSettingName(this.GetGlobalConfigurationNamespace(), name)) ?? @default;
        }

        /// <summary>
        /// Get site level setting. A site level setting is resolved by prefixing
        /// site name space <see cref="IConfigurationService.GetSiteConfigurationNamespace"/> to the <paramref name="name"/>
        /// to create the full setting name.
        /// NOTE: This method should throw an error is a setting is not found.
        /// </summary>
        /// <param name="name">Setting name to retrieve.</param>
        /// <returns>Returns configuration setting or throw an exception if setting is not found.</returns>
        public string GetGlobalSetting(string name)
        {
            var value = this.GetGlobalSetting(name, null);
            if (value == null)
            {
                throw new ApplicationException(
                    string.Format(
                        "Missing mandatory configuration setting '{0}'.",
                        GetFullyQualifiedSettingName(this.GetGlobalConfigurationNamespace(), name)));
            }

            return value;
        }

        /// <summary>
        /// Get site level setting. A site level setting is resolved by prefixing
        /// global name space <see cref="IConfigurationService.GetSiteConfigurationNamespace"/> to the <paramref name="name"/>
        /// to create the full setting name.
        /// </summary>
        /// <param name="name">Setting name to retrieve.</param>
        /// <param name="default">Default value if setting is not configured.</param>
        /// <returns>Returns configuration setting or default value if setting is not found.</returns>
        public string GetSiteSetting(string name, string @default)
        {
            var @naespace = GetFullyQualifiedSettingName(this.GetGlobalConfigurationNamespace(), this.siteService.GetSiteName());
            return
                GetApplicationSetting(
                    GetFullyQualifiedSettingName(@naespace, name)) ?? @default;
        }

        /// <summary>
        /// Get site level setting. A site level setting is resolved by prefixing
        /// global name space <see cref="IConfigurationService.GetSiteConfigurationNamespace"/> to the <paramref name="name"/>
        /// to create the full setting name.
        /// NOTE: This method should raise an exception if the setting is not found.
        /// </summary>
        /// <param name="name">Setting name to retrieve.</param>
        /// <returns>Returns configuration setting or throws an exception if setting is not found.</returns>
        public string GetSiteSetting(string name)
        {
            var value = this.GetSiteSetting(name, null);
            if (value == null)
            {
                throw new ApplicationException(
                    string.Format(
                        "Missing mandatory configuration setting '{0}'.",
                        GetFullyQualifiedSettingName(this.GetSiteConfigurationNamespace(), name)));
            }

            return value;
        }

        #region Implementation

        private static string GetFullyQualifiedSettingName(string @namespace, string name)
        {
            return string.Format("{0}.{1}", @namespace, name);
        }

        private static string GetApplicationSetting(string name)
        {
            return ConfigurationManager.AppSettings[name];
        }

        #endregion
    }
}