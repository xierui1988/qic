﻿namespace Qic.Sc.Services.Core
{
    using System;
    using System.Diagnostics;

    using log4net;

    using Qic.Sc.Interfaces.Core;

    /// <summary>
    /// Provide basic logging functionality.
    /// </summary>
    public class LoggerService : ILoggerService
    {
        private readonly string name;
        private readonly ILog logger;
        private readonly TraceSource traceSource;

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggerService"/> class.
        /// </summary>
        /// <param name="context">Type name to be used as logging context.</param>
        public LoggerService(Type context)
            : this(context.FullName)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoggerService"/> class.
        /// </summary>
        /// <param name="name">The name of this logger.</param>
        public LoggerService(string name)
        {
            this.name = name;
            this.logger = LogManager.GetLogger(name);
            this.traceSource = new TraceSource("Sc.Web", SourceLevels.Warning); // Should match trace source configuration 'name'
        }

        /// <summary>
        /// Log debug message.
        /// </summary>
        /// <param name="message">
        /// The message to be logged. Assumes that calling a to string on the object
        /// will produce a well formated message. Intended to be the output of 
        /// <example> ILogger.Debug(string.Format("My message: {0}", errorCode)</example>
        /// </param>
        public void Debug(object message)
        {
            this.logger.Debug(message);
            this.traceSource.TraceEvent(TraceEventType.Verbose, 0, string.Format("{0}: {1}", this.name, message));
        }

        /// <summary>
        /// Log information message.
        /// </summary>
        /// <param name="message">
        /// The message to be logged. Assumes that calling a to string on the object
        /// will produce a well formated message. Intended to be the output of 
        /// <example> ILogger.Debug(string.Format("My message: {0}", errorCode)</example>
        /// </param>
        public void Info(object message)
        {
            this.logger.Info(message);
            this.traceSource.TraceEvent(TraceEventType.Information, 0, string.Format("{0}: {1}", this.name, message));
        }

        /// <summary>
        /// Log warning message.
        /// </summary>
        /// <param name="message">
        /// The message to be logged. Assumes that calling a to string on the object
        /// will produce a well formated message. Intended to be the output of 
        /// <example> ILogger.Debug(string.Format("My message: {0}", errorCode)</example>
        /// </param>
        public void Warn(object message)
        {
            this.logger.Warn(message);
            this.traceSource.TraceEvent(TraceEventType.Warning, 0, string.Format("{0}: {1}", this.name, message));
        }

        /// <summary>
        /// Log error message.
        /// </summary>
        /// <param name="message">
        /// The message to be logged. Assumes that calling a to string on the object
        /// will produce a well formated message. Intended to be the output of 
        /// <example> ILogger.Debug(string.Format("My message: {0}", errorCode)</example>
        /// </param>
        /// <param name="exception">
        /// An exception instance information or null.
        /// </param>
        public void Error(object message, Exception exception)
        {
            if (exception != null)
            {
                this.logger.Error(message, exception);
                this.traceSource.TraceEvent(
                    TraceEventType.Error,
                    0,
                    string.Format("{0}: {1}{2}{3}", this.name, message, Environment.NewLine, exception));
            }
            else
            {
                this.logger.Error(message);
                this.traceSource.TraceEvent(TraceEventType.Error, 0, string.Format("{0}: {1}", this.name, message));
            }
        }
    }
}
