﻿namespace Qic.Sc.Interfaces.Core
{
    /// <summary>
    /// Link Service 
    /// </summary>
    public interface ILinkService
    {
        /// <summary>
        /// Generate URL for News
        /// </summary>
        /// <param name="idOrPath">Sitecore item id</param>
        /// <returns>Generated URL</returns>
        string GenerateUrl(string idOrPath);
    }
}
