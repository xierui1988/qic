﻿namespace Qic.Sc.Interfaces.Core
{
    using System;

    /// <summary>
    /// The background service interface.
    /// </summary>
    public interface IBackgroundService
    {
        /// <summary>
        /// The run.
        /// </summary>
        /// <param name="task">
        /// The task.
        /// </param>
        void Run<T>(Action<T> task);

        /// <summary>
        /// The run.
        /// </summary>
        void Run<T1, T2, T3, T4>(Action<T1, T2, T3, T4> task);
    }
}
