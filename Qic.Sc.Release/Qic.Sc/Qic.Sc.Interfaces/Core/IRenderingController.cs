﻿namespace Qic.Sc.Interfaces.Core
{
    /// <summary>
    /// The RenderingController interface serves as a common interface
    /// to handle errors in renderings.
    /// </summary>
    public interface IRenderingController
    {
        /// <summary>
        /// The on rendering exception, tries to handle exception.
        /// </summary>
        /// <param name="exceptionContext">
        /// The exception context. (System.Web.MvcExceptionContext) is expected.
        /// </param>
        void OnRenderingException(object exceptionContext);
    }
}
