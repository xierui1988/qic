namespace Qic.Sc.Interfaces.Core
{
    using System;

    /// <summary>
    /// URL redirect service
    /// </summary>
    public interface IRedirecService
    {
        /// <summary>
        /// Resolve a permanent redirect for the specified URL.
        /// The rules are implementation specific.
        /// </summary>
        /// <param name="uri">
        /// Original Uri.
        /// </param>
        /// <returns>
        /// A redirect URL or null if no redirect required/exists.
        /// </returns>
        string PermanentRedirectFor(Uri uri);
    }
}