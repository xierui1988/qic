﻿namespace Qic.Sc.Interfaces.Core
{
    /// <summary>
    /// The content repository. Abstracts CRUD operations over content.
    /// Enables search capability over content. This one is tagged to a specific database.
    /// </summary>
    public interface IMasterContentRepository
    {
    }
}
