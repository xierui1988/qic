﻿using System;

namespace Qic.Sc.Interfaces.Core
{
    /// <summary>
    /// Interface for different types of loggers to be used in application
    /// </summary>
    public interface ILoggerService
    {
        /// <summary>
        /// Log debug message.
        /// </summary>
        /// <param name="message">
        /// The message to be logged. Assumes that calling a to string on the object
        /// will produce a well formated message. Intended to be the output of 
        /// <example> ILogger.Debug(string.Format("My message: {0}", errorCode)</example>
        /// </param>
        void Debug(object message);

        /// <summary>
        /// Log information message.
        /// </summary>
        /// <param name="message">
        /// The message to be logged. Assumes that calling a to string on the object
        /// will produce a well formated message. Intended to be the output of 
        /// <example> ILogger.Debug(string.Format("My message: {0}", errorCode)</example>
        /// </param>
        void Info(object message);

        /// <summary>
        /// Log warning message.
        /// </summary>
        /// <param name="message">
        /// The message to be logged. Assumes that calling a to string on the object
        /// will produce a well formated message. Intended to be the output of 
        /// <example> ILogger.Debug(string.Format("My message: {0}", errorCode)</example>
        /// </param>
        void Warn(object message);

        /// <summary>
        /// Log error message.
        /// </summary>
        /// <param name="message">
        /// The message to be logged. Assumes that calling a to string on the object
        /// will produce a well formated message. Intended to be the output of 
        /// <example> ILogger.Debug(string.Format("My message: {0}", errorCode)</example>
        /// </param>
        /// <param name="exception">
        /// An exception instance information or null.
        /// </param>
        void Error(object message, Exception exception);
    }
}
