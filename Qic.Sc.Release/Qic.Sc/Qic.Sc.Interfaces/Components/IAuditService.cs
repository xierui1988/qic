﻿using Qic.Sc.Entities.Metadata;
namespace Qic.Sc.Interfaces.Components
{
    /// <summary>
    /// The audit service interface.
    /// </summary>
    public interface IAuditService
    {
        /// <summary>
        /// The audit.
        /// </summary>
        /// <param name="message">
        /// The message.
        /// </param>
        void Audit(string message);

        /// <summary>
        /// Need to add
        /// </summary>
        /// <param name="message">String Message</param>
        void Audit(string username, string message);

        /// <summary>
        /// Need to add
        /// </summary>
        /// <param name="message">String Message</param>
        void Audit(string username, string recycleAction, string id);

        /// <summary>
        /// Need to add
        /// </summary>
        /// <param name="message">String Message</param>
        void Audit(string username, string action, string db, string path, string language, string version, string id);

        /// <summary>
        /// Need to add
        /// </summary>
        /// <param name="auditObj">String Message</param>
        void Audit(AuditMetadata auditObj);
    }
}