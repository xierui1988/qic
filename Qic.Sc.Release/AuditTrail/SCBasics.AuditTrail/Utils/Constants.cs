﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SCBasics.AuditTrail.Utils
{
    /// <summary>
    /// Constants class
    /// </summary>
    public class Constants
    {
        public const string AuditTrailConnectionString = "SCAuditTrailConnectionString";
    }
}
