USE [SCAuditTrail]
GO
/****** Object:  UserDefinedFunction [dbo].[ufnGetState]    Script Date: 4/1/2015 2:40:42 PM ******/

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'ufnGetState')
                    AND type IN ( N'FN' ) ) 
	DROP FUNCTION [dbo].[ufnGetState]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE function [dbo].[ufnGetState]( @text VARCHAR(MAX)) 
returns varchar(MAX) as

Begin

DECLARE @start INT
SELECT @start = CHARINDEX('|',@text)

DECLARE @returntext VARCHAR(MAX)
IF (@start=0)
Begin
set @returntext=@text
End

Else

Begin

declare @end int
select @end=len(@text)

declare @lastpart int
select @lastpart=charindex('|',REVERSE(@text))

set @end=@end-@start-@lastpart

declare @finaltext VARCHAR(MAX)
SELECT @finaltext=SUBSTRING(@text,@start+1,@end)

SELECT @start = CHARINDEX('|',@finaltext)

declare @oldstate varchar(MAX)
select @oldstate=SUBSTRING(@finaltext,0,@start)

select @lastpart=charindex('/',REVERSE(@finaltext))

declare @newstate varchar(MAX)
select @newstate= SUBSTRING(@finaltext,len(@finaltext)-@lastpart+2,@lastpart)

set @returntext=@oldstate + ' - ' + @newstate

End

return  @returntext




end
GO

GRANT EXECUTE ON [dbo].[ufnGetState] TO [QIC_report_role];
GO